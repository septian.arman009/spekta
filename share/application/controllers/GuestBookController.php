<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GuestBookController extends Share_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function form()
    {
        $this->load->view('guestbook/form');
    }

    public function create()
    {
        $post = fileGetContent();
        $visit_time = normalToIndoDateTime2($post->visit_time);
        $data = [
            'location' => 'KF-JKT',
            'name' => $post->name,
            'nik' => $post->nik,
            'phone' => $post->phone,
            'organization' => $post->organization,
            'company' => $post->company ?: '-',
            'company_purpose' => $post->company_purpose,
            'employee' => $post->employee,
            'unit' => $post->unit,
            'visit_needs' => $post->visit_needs,
            'visit_time' => $visit_time
        ];
        
        $insert = $this->General->create('guest_books_form', $data);

        response([
            'status' => 'success',
            'url' => $insert  ? base_url('guestbook/success/' . $insert) : base_url('guestbook/failed') 
        ]);
    }

    public function success($id)
    {
        $book = $this->General->getDataById('guest_books_form', $id);

        if(!$book) {
            $this->load->view('guestbook/failed', ['message' => 'Data tidak ditemukan']);
        } else {
            $this->load->view('guestbook/success', [
                'location' => $book->location,
                'name' => $book->name,
                'phone' => $book->phone,
                'nik' => $book->nik,
                'organization' => $book->organization,
                'company' => $book->company,
                'company_purpose' => $book->company_purpose,
                'employee' => $book->employee,
                'unit' => $book->unit,
                'visit_needs' => $book->visit_needs,
                'visit_time' => $book->visit_time
            ]);
        }
    }

    public function failed()
    {

        $this->load->view('guestbook/failed', ['message' => 'Gagal input buku tamu']);
    }
}