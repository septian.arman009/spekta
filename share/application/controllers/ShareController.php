<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ShareController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        echo "Helper for redirecting SPEKTA";
    }

    public function overtime($token)
    {
        $url = simpleEncrypt(str_replace('_', '=', $token), 'd');
        redirect($url);
    }

    public function editor()
    {
        $this->load->view("editor");
    }
}