<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,900&display=swap" rel="stylesheet">
    <script type="text/javascript" src="<?= asset('js/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?= asset('js/custom.js') ?>"></script>
</head>
<style>
    body {
        text-align: center;
        background: #EBF0F5;
    }

    h1 {
        color: #dd8484;
        font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
        font-weight: 900;
        font-size: 40px;
        margin-bottom: 10px;
    }

    p {
        color: #404F5E;
        font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
        font-size: 20px;
        margin: 0;
    }

    .card {
        background: white;
        padding: 60px;
        border-radius: 4px;
        box-shadow: 0 2px 3px #C8D0D8;
        display: inline-block;
        margin: 0 auto;
    }

    .text-input {
        width: 100%;
        box-sizing: border-box;
        border: 2px solid #ccc;
        border-radius: 4px;
        font-size: 16px;
        background-color: white;
        background-position: 10px 9px;
        background-repeat: no-repeat;
        background-size: 25px 25px;
        padding: 12px 20px 12px 40px;
        text-align: right;
    }

    .button {
        background: #e04f5f;
        border-radius: 999px;
        box-shadow: #e04f5f 0 10px 20px -10px;
        box-sizing: border-box;
        color: #FFFFFF;
        cursor: pointer;
        font-family: sans-serif;
        font-size: 16px;
        font-weight: 700;
        line-height: 24px;
        opacity: 1;
        outline: 0 solid transparent;
        padding: 8px 18px;
        user-select: none;
        -webkit-user-select: none;
        touch-action: manipulation;
        width: fit-content;
        word-break: break-word;
        border: 0;
        text-decoration: none;
    }

    .form-control {
        margin-top: 20px;
    }

    @media only screen and (max-device-width: 480px) {
        body {
            /* transform: scale(2.5, 2.5); */
            zoom: 225%;
        }
    }

    label {
        display: flex;
        text-align: left;
    }

    .hide {
        display: none;
    }
</style>

<body>
    <div class="card">
        <div
            style="display:flex;align-items:center;justify-content:center;border-radius:200px; height:200px; width:200px; background: #F8FAF5; margin:0 auto;">
            <img src='<?= asset('img/book.png') ?>' height="150" width="150" />
        </div>
        <h1 style='font-family: sans-serif'>Buku Tamu</h1>
        
        <div>
            <div class="form-control">
                <label>Nama</label>
                <input type="input" id="name" class="text-input" />
            </div>

            <div class="form-control">
                <label>Nomor KTP</label>
                <input type="input" id="nik" class="text-input" />
            </div>

            <div class="form-control">
                <label>Nomor Handphone</label>
                <input type="number" id="phone" class="text-input" />
            </div>
            
            <div class="form-control">
                <label>Organisasi</label>
                <select name="organization" class="text-input" onchange="toggleOrganize($(this).val())">
                    <option value='Perorangan' selected>Perorangan</option>
                    <option value='Perusahaan'>Perusahaan</option>
                </select>
            </div>

            <div class="form-control" id="org">
                <label>Nama Perusahaan</label>
                <input type="input" id="company" class="text-input" />
            </div>

            <div class="form-control" id="org">
                <label>Bertemu Dengan</label>
                <input type="input" id="employee" class="text-input" />
            </div>

            <div class="form-control">
                <label>Perusahaan Tujuan</label>
                <select id="company_purpose" class="text-input">
                    <option selected value='PT. Kimia Farma Tbk Plant Jakarta' selected>PT. Kimia Farma Tbk Plant Jakarta</option>
                    <option value='PT. National Disctribution Center'>PT. National Disctribution Center</option>
                </select>
            </div>

            <div class="form-control" id="org">
                <label>Bagian</label>
                <input type="input" id="unit" class="text-input" />
            </div>

            <div class="form-control" id="org">
                <label>Keperluan</label>
                <textarea id="visit_needs" class="text-input" rows="5"></textarea>
            </div>

            <div class="form-control" id="org">
                <label>Waktu Kedatangan</label>
                <input type="input" id="visit_time" class="text-input" value="<?= toIndoDateTime2(date('Y-m-d H:i:s')) ?>" disabled/>
            </div>

            <div class="form-control">
                <a type="button" class="button" id="button">Submit</a>
            </div>
        </div>
    </div>
</body>

<script>
    function toggleOrganize(value) {
        if(value != 'Perusahaan') {
            $("#org").addClass("hide");
            $("#organization").val("");
        } else {
            $("#org").removeClass("hide");
            $("#organization").val("");
        }
    }

    toggleOrganize('Perorangan');
    
    $("#button").on("click", function() {
        var name = $("#name").val();
        var nik = $("#nik").val();
        var phone = $("#phone").val();
        var organization = $("select[name='organization']").val();
        var company = $("#company").val();
        var company_purpose = $("#company_purpose").val();
        var employee = $("#employee").val();
        var unit = $("#unit").val();
        var visit_needs = $("#visit_needs").val();
        var visit_time = $("#visit_time").val();

        if(
            name == ''  || nik == '' || organization == '' || employee == '' || unit == '' || visit_needs == '' || visit_time == '' || phone == ''
        ) {
            alert("Form tidak lengkap, silahkan lengkapi form sebelum submit");
            return;
        }

        if(organization == 'Perusahaan' && company == '') {
            alert("Form tidak lengkap, silahkan lengkapi form sebelum submit");
            return;
        }

        reqJson('<?= base_url('guestbook/create') ?>', "POST", {
            name,
            nik,
            phone,
            organization,
            company,
            company_purpose,
            employee,
            unit,
            visit_needs,
            visit_time,
        }, (err, res) => {
            if(res.status === "success") {
                window.location = res.url;
            } else {
                alert(res.message);
            }
        });
    });
</script>

</html>