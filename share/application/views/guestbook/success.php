<html>
  <head>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,900&display=swap" rel="stylesheet">
  </head>
    <style>
      body {
        text-align: center;
        padding: 40px 0;
        background: #EBF0F5;
      }
        h1 {
          color: #88B04B;
          font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
          font-weight: 900;
          font-size: 40px;
          margin-bottom: 10px;
        }
        p {
          color: #404F5E;
          font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
          font-size:20px;
          margin: 0;
        }
      i {
        color: #9ABC66;
        font-size: 100px;
        line-height: 200px;
        margin-left:-15px;
      }
      .card {
        background: white;
        padding: 60px;
        border-radius: 4px;
        box-shadow: 0 2px 3px #C8D0D8;
        display: inline-block;
        margin: 0 auto;
      }

      td {
        padding: 10px;
      }
    </style>
    <body>
      <div class="card">
      <div style="border-radius:200px; height:200px; width:200px; background: #F8FAF5; margin:0 auto;">
        <i class="checkmark">✓</i>
      </div>
        <h1>Sukses</h1> 
        <table>
          <tr>
            <td>Nama</td>
            <td>:</td>
            <td><?= $name ?></td>
          </tr>

          <tr>
            <td>Nomor KTP</td>
            <td>:</td>
            <td><?= $nik ?></td>
          </tr>

          <tr>
            <td>Nomor Handphone</td>
            <td>:</td>
            <td><?= $phone ?></td>
          </tr>

          <tr>
            <td>Organisasi</td>
            <td>:</td>
            <td><?= $organization ?></td>
          </tr>

          <?php if($organization == 'Perusahaan') { ?>
            <tr>
              <td>Perusahaan</td>
              <td>:</td>
              <td><?= $company ?></td>
            </tr>
          <?php } ?>

          <tr>
            <td>Bertemu Dengan</td>
            <td>:</td>
            <td><?= $employee ?></td>
          </tr>

          <tr>
            <td>Perusahaan Tujuan</td>
            <td>:</td>
            <td><?= $company_purpose ?></td>
          </tr>

          <tr>
            <td>Bagian</td>
            <td>:</td>
            <td><?= $unit ?></td>
          </tr>

          <tr>
            <td>Keperluan</td>
            <td>:</td>
            <td><?= $visit_needs ?></td>
          </tr>

          <tr>
            <td>Waktu Kedatangan</td>
            <td>:</td>
            <td><?= toIndoDateTime2($visit_time) ?></td>
          </tr>
        </table>
      </div>
    </body>
</html>