<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'ShareController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['overtime/(:any)'] = 'ShareController/overtime/$1';
$route['guestbook/form'] = 'GuestBookController/form';
$route['guestbook/create'] = 'GuestBookController/create';
$route['guestbook/success/(:num)'] = 'GuestBookController/success/$1';
$route['guestbook/failed'] = 'GuestBookController/failed';

$route['editor'] = 'ShareController/editor';
