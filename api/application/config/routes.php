<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = '';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['v1/login'] = 'auth/AuthController/login';
$route['v1/refresh'] = 'auth/AuthController/refresh';

$route['v1/absen'] = 'admin/AbsenController/getAbsens';
$route['v1/overtime'] = 'admin/OvertimeController/getOvertimes';
