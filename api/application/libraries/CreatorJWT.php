<?php
defined('BASEPATH') or exit('No direct script access allowed');

//application/libraries/CreatorJWT.php
require APPPATH . '/libraries/JWT.php';

class CreatorJWT
{

    /*************This function generate token private key**************/

    private $key = "1234567890qwertyuiopmnbvcxzasdfghjkl";
    public function GenerateToken($data)
    {
        $jwt = JWT::encode($data, $this->key);
        return $jwt;
    }

    /*************This function DecodeToken token **************/

    public function DecodeToken($token)
    {
        $decoded = JWT::decode($token, $this->key, array('HS256'));
        $decodedData = (array) $decoded;
        return $decodedData;
    }

    public function checkToken($token)
    {
        if(array_key_exists('Authorization', $token)) {
            $jwtData = $this->DecodeToken(str_replace('Bearer ', '', $token['Authorization']));
            if(count($jwtData) == 0) response(['status' => 'failed', 'message' => 'Invalid Access Token'], 401);

            $payload = [
                'rule' => simpleEncrypt($jwtData['rule'], 'd'),
                'location' => simpleEncrypt($jwtData['location'], 'd'),
                'username' => simpleEncrypt($jwtData['username'], 'd'),
                'login_time' => simpleEncrypt($jwtData['login_time'], 'd'),
                'expired_time' => simpleEncrypt($jwtData['expired_time'], 'd'),
            ];

            if($this->isExpired($payload['expired_time'])) response(['status' => 'failed', 'message' => 'Expired Access Token'], 401);

            return true;
        } else {
            response([
                'status' => false,
                'error' => 'Invalid Access Token',
            ], 401);
        }
    }

    public function isExpired($expired)
    {
        $currentTime = date('Y-m-d H\H:i:s');
        $tokenExpire = date('Y-m-d H:i:s', strtotime($expired));
        
        if($currentTime >= $tokenExpire) {
            return true;
        } else {
            return false;
        }
    }

    public function me($token)
    {
        if(array_key_exists('Authorization', $token)) {
            $jwtData = $this->DecodeToken(str_replace('Bearer ', '', $token['Authorization']));
            if(count($jwtData) > 0) {
                $payload = [
                    'rule' => simpleEncrypt($jwtData['rule'], 'd'),
                    'location' => simpleEncrypt($jwtData['location'], 'd'),
                    'username' => simpleEncrypt($jwtData['username'], 'd'),
                    'login_time' => simpleEncrypt($jwtData['login_time'], 'd'),
                    'expired_time' => simpleEncrypt($jwtData['expired_time'], 'd'),
                ];

                return $payload;
            } else {
                response([
                    'status' => false,
                    'error' => 'Invalid Access Token',
                ], 401);
            }
        } else {
            response([
                'status' => false,
                'error' => 'Invalid Access Token',
            ], 401);
        }
    }

    public function encode($token)
    {
        return $this->DecodeToken(str_replace('Bearer ', '', $token));
    }
}
