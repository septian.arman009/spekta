<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Auth
{
    public $kf_main;
    public $kf_hr;
    public $kf_qhse;
    public $kf_general;
    public $kf_mtn;
    public $kf_prod;
    public $kf_chat;

    public function __construct()
    {
        $this->dbActive();
    }

    public function dbActive()
    {
        $this->kf_main = ENVIRONMENT !== 'production' ? MAIN_DB_DEV : MAIN_DB;
        $this->kf_hr = ENVIRONMENT !== 'production' ? HR_DB_DEV : HR_DB;
        $this->kf_qhse = ENVIRONMENT !== 'production' ? QHSE_DB_DEV : QHSE_DB;
        $this->kf_general = ENVIRONMENT !== 'production' ? GENERAL_DB_DEV : GENERAL_DB;
        $this->kf_mtn = ENVIRONMENT !== 'production' ? MTN_DB_DEV : MTN_DB;
        $this->kf_prod = ENVIRONMENT !== 'production' ? PROD_DB_DEV : PROD_DB;
        $this->kf_chat = ENVIRONMENT !== 'production' ? CHAT_DB_DEV : CHAT_DB;
    }
}
