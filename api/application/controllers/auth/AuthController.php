<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Hautelook\Phpass\PasswordHash;

require APPPATH . '/libraries/CreatorJWT.php';

class AuthController extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
    }

    public function login()
    {
        $obj = fileGetContent();
        $username = $obj->username;
        $password = $obj->password;
        $passwordHash = new PasswordHash(8, false);

        $user = $this->Main->getWhere('api_users', ['username' => $username])->row();
        if (!$user) response(['status' => 401, 'message' => 'Username tidak ditemukan!'], 401);

        $userPasswordHash = $user->password;
        $checkPassword = $passwordHash->CheckPassword(md5($password), $userPasswordHash);

        if(!$checkPassword) response(['status' => 401, 'message' => 'Password tidak cocok!'], 401);

        $refreshToken = $this->jwt->GenerateToken([
            'rule' => simpleEncrypt('refresh_token', 'e'),
            'location' => simpleEncrypt($user->location, 'e'),
            'username' => simpleEncrypt($user->username, 'e'),
            'login_time' => simpleEncrypt(date('Y-m-d H:i:s'), 'e'),
            'expired_time' => simpleEncrypt(date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' + 1 days')), 'e'),
        ]);

        $data = [
            'username' => $user->username,
            'access_token' => $this->jwt->GenerateToken([
                'rule' => simpleEncrypt('access_token', 'e'),
                'location' => simpleEncrypt($user->location, 'e'),
                'username' => simpleEncrypt($user->username, 'e'),
                'login_time' => simpleEncrypt(date('Y-m-d H:i:s'), 'e'),
                'expired_time' => simpleEncrypt(date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' + 1 hours')), 'e'),
            ]),
            'refresh_token' => $refreshToken
        ];

        $this->Main->updateById('api_users', ['refresh_token' => $refreshToken], $user->id);

        response([
            'status' => 'success',
            'message' => 'Login Berhasil',
            'data' => $data
        ]);
    }

    public function refresh()
    {
        $obj = fileGetContent();
        $refreshToken = $obj->refresh_token;

        $user = $this->Main->getWhere('api_users', ['refresh_token' => $refreshToken])->row();
        if(!$user) response(['status' => 'failed', 'message' => 'Invalid refresh token'], 400);

        $payload = $this->getPayload($refreshToken);
        
        if($payload['rule'] != 'refresh_token') response(['status' => 'failed', 'message' => 'Invalid refresh token']);
        if($this->jwt->isExpired($payload['expired_time'])) response(['status' => 401, 'message' => 'Refresh token expired!'], 400);

        $refreshToken = $this->jwt->GenerateToken([
            'rule' => simpleEncrypt('refresh_token', 'e'),
            'location' => simpleEncrypt($user->location, 'e'),
            'username' => simpleEncrypt($user->username, 'e'),
            'login_time' => simpleEncrypt(date('Y-m-d H:i:s'), 'e'),
            'expired_time' => simpleEncrypt(date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' + 1 days')), 'e'),
        ]);

        $data = [
            'username' => $user->username,
            'access_token' => $this->jwt->GenerateToken([
                'rule' => simpleEncrypt('access_token', 'e'),
                'location' => simpleEncrypt($user->location, 'e'),
                'username' => simpleEncrypt($user->username, 'e'),
                'login_time' => simpleEncrypt(date('Y-m-d H:i:s'), 'e'),
                'expired_time' => simpleEncrypt(date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' + 1 hours')), 'e'),
            ]),
            'refresh_token' => $refreshToken
        ];

        $this->Main->updateById('api_users', ['refresh_token' => $refreshToken], $user->id);

        response([
            'status' => 'success',
            'message' => 'Login Berhasil',
            'data' => $data
        ]);
    }

    public function getPayload($token)
    {
        $payload = $this->jwt->encode($token);
        return [
            'rule' => simpleEncrypt($payload['rule'], 'd'),
            'location' => simpleEncrypt($payload['location'], 'd'),
            'username' => simpleEncrypt($payload['username'], 'd'),
            'login_time' => simpleEncrypt($payload['login_time'], 'd'),
            'expired_time' => simpleEncrypt($payload['expired_time'], 'd'),
        ];
    }
}