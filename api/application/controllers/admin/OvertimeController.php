<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class OvertimeController extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers());

        $this->load->model("PaginationModel", 'PaginationModel');
        $this->PaginationModel->myConstruct('hr');
    }

    public function getOvertimes()
    {
        $payload = $this->jwt->me($this->input->request_headers());
        $params = getParam();
        if(!isset($params['month'])) response(['status' => 400, 'message' => 'Month is required'], 400);
        if(!isset($params['year'])) response(['status' => 400, 'message' => 'Year is required'], 400);

        if(!is_numeric($params['month'])) response(['status' => 400, 'message' => 'Month must be a number 1-12'], 400);
        if(!is_numeric($params['year'])) response(['status' => 400, 'message' => 'Year must be a number ex: 2023'], 400);
        
        $month = $params['month'];
        $year = $params['year'];

        $absenTable = absenTable($year, $month);

        $options = [
            'fields' => [
                'id', 'emp_id', 'employee_name', 'sap_id', 'npp',
                'department_id', 'department_name', 'sub_department_id', 'sub_department_name', 'division_id', 'division_name',
                'task_id', 'emp_task_id', 'overtime_date', 'status_day', 'shift', 'overtime_start_time', 'overtime_end_time', 'absen_in', 'absen_out',
                'effective_hour', 'break_hour', 'real_hour', 'overtime_hour', 'premi_overtime', 'total_payment', 'deduction', 'meal', 'total_meal', 'notes', 'status', 'payment_status',
                'apv_spv', 'apv_spv_nip', 'apv_spv_date', 'spv_name',
                'apv_asman', 'apv_asman_nip', 'apv_asman_date', 'asman_name',
                'apv_ppic', 'apv_ppic_nip', 'apv_ppic_date', 'ppic_name',
                'apv_mgr', 'apv_mgr_nip', 'apv_mgr_date', 'mgr_name',
                'apv_pm', 'apv_pm_nip', 'apv_pm_date', 'pm_name',
            ],
            'global_search_fields' => [
                'emp_id' => 'a.emp_id',
                'employee_name' => 'c.employee_name',
                'sap_id' => 'c.sap_id',
                'npp' => 'c.nip',
                'department_id' => 'a.department_id',
                'department_name' => 'd.name',
                'sub_department_id' => 'a.sub_department_id',
                'sub_department_name' => 'e.name',
                'division_id' => 'a.division_id',
                'division_name' => 'f.name',
                'task_id' => 'a.task_id',
                'emp_task_id' => 'a.emp_task_id',
                'overtime_date' => 'a.overtime_date',
                'status_day' => 'a.status_day',
                'shift' => 'b.shift',
                'overtime_start_time' => 'a.start_date',
                'overtime_end_time' => 'a.end_date',
                'absen_in' => 'b.date_in',
                'absen_out' => 'b.date_out',
                'effective_hour' => 'a.effective_hour',
                'break_hour' => 'a.break_hour',
                'real_hour' => 'a.real_hour',
                'overtime_hour' => 'a.overtime_hour',
                'premi_overtime' => 'a.premi_overtime',
                'deduction' => 'a.deduction',
                'meal' => 'a.meal',
                'total_meal' => 'a.total_meal',
                'notes' => 'a.notes',
                'status' => 'a.status',
                'payment_status' => 'a.payment_status',
                'apv_spv' => 'h.apv_spv',
                'apv_spv_nip' => 'h.apv_spv_nip',
                'apv_spv_date' => 'h.apv_spv_date',
                'spv_name' => 'g.employee_name',

                'apv_asman' => 'h.apv_asman',
                'apv_asman_nip' => 'h.apv_asman_nip',
                'apv_asman_date' => 'h.apv_asman_date',
                'asman_name' => 'i.employee_name',

                'apv_ppic' => 'h.apv_ppic',
                'apv_ppic_nip' => 'h.apv_ppic_nip',
                'apv_ppic_date' => 'h.apv_ppic_date',
                'ppic_name' => 'j.employee_name',

                'apv_mgr' => 'h.apv_mgr',
                'apv_mgr_nip' => 'h.apv_mgr_nip',
                'apv_mgr_date' => 'h.apv_mgr_date',
                'mgr_name' => 'k.employee_name',

                'apv_pm' => 'h.apv_pm',
                'apv_pm_nip' => 'h.apv_pm_nip',
                'apv_pm_date' => 'h.apv_pm_date',
                'pm_name' => 'l.employee_name',
            ],
            'global_filter_sub_fields' => [
                'id' => 'a.id',
                'emp_id' => 'a.emp_id',
                'employee_name' => 'c.employee_name',
                'sap_id' => 'c.sap_id',
                'npp' => 'c.nip',
                'department_id' => 'a.department_id',
                'department_name' => 'd.name',
                'sub_department_id' => 'a.sub_department_id',
                'sub_department_name' => 'e.name',
                'division_id' => 'a.division_id',
                'division_name' => 'f.name',
                'task_id' => 'a.task_id',
                'emp_task_id' => 'a.emp_task_id',
                'overtime_date' => 'a.overtime_date',
                'status_day' => 'a.status_day',
                'shift' => 'b.shift',
                'overtime_start_time' => 'a.start_date',
                'overtime_end_time' => 'a.end_date',
                'absen_in' => 'b.date_in',
                'absen_out' => 'b.date_out',
                'effective_hour' => 'a.effective_hour',
                'break_hour' => 'a.break_hour',
                'real_hour' => 'a.real_hour',
                'overtime_hour' => 'a.overtime_hour',
                'premi_overtime' => 'a.premi_overtime',
                'deduction' => 'a.deduction',
                'meal' => 'a.meal',
                'total_meal' => 'a.total_meal',
                'notes' => 'a.notes',
                'status' => 'a.status',
                'payment_status' => 'a.payment_status',
                'apv_spv' => 'h.apv_spv',
                'apv_spv_nip' => 'h.apv_spv_nip',
                'apv_spv_date' => 'h.apv_spv_date',
                'spv_name' => 'g.employee_name',

                'apv_asman' => 'h.apv_asman',
                'apv_asman_nip' => 'h.apv_asman_nip',
                'apv_asman_date' => 'h.apv_asman_date',
                'asman_name' => 'i.employee_name',

                'apv_ppic' => 'h.apv_ppic',
                'apv_ppic_nip' => 'h.apv_ppic_nip',
                'apv_ppic_date' => 'h.apv_ppic_date',
                'ppic_name' => 'j.employee_name',

                'apv_mgr' => 'h.apv_mgr',
                'apv_mgr_nip' => 'h.apv_mgr_nip',
                'apv_mgr_date' => 'h.apv_mgr_date',
                'mgr_name' => 'k.employee_name',

                'apv_pm' => 'h.apv_pm',
                'apv_pm_nip' => 'h.apv_pm_nip',
                'apv_pm_date' => 'h.apv_pm_date',
                'pm_name' => 'l.employee_name',
            ],
            'queries' => "
                select #select# from(
                    select
                        a.id,
                        a.emp_id,
                        c.employee_name,
                        c.sap_id,
                        c.nip as npp,
                        a.department_id,
                        d.name as department_name,
                        a.sub_department_id,
                        e.name as sub_department_name,
                        a.division_id,
                        f.name as division_name,
                        a.task_id,
                        a.emp_task_id,
                        a.overtime_date,
                        a.status_day,
                        b.shift,
                        a.start_date as overtime_start_time,
                        a.end_date as overtime_end_time,
                        b.date_in as absen_in,
                        b.date_out as absen_out,
                        a.effective_hour,
                        a.break_hour,
                        a.real_hour,
                        a.overtime_hour,
                        a.premi_overtime,
                        (a.premi_overtime * a.overtime_hour) as total_payment,
                        a.deduction,
                        a.meal,
                        a.total_meal,
                        a.notes,
                        a.status,
                        a.payment_status,
                        h.apv_spv,
                        h.apv_spv_nip,
                        h.apv_spv_date,
                        g.employee_name as spv_name,
                        h.apv_asman,
                        h.apv_asman_nip,
                        h.apv_asman_date,
                        i.employee_name as asman_name,
                        h.apv_ppic,
                        h.apv_ppic_nip,
                        h.apv_ppic_date,
                        j.employee_name as ppic_name,
                        h.apv_mgr,
                        h.apv_mgr_nip,
                        h.apv_mgr_date,
                        k.employee_name as mgr_name,
                        h.apv_head as apv_pm,
                        h.apv_head_nip as apv_pm_nip,
                        h.apv_head_date as apv_pm_date,
                        l.employee_name as pm_name
                    from employee_overtimes_detail a
                    inner join $absenTable b on a.overtime_date = b.abs_date and b.emp_id = a.emp_id
                    inner join employees c on a.emp_id = c.id
                    inner join departments d on a.department_id = d.id 
                    inner join sub_departments e on a.sub_department_id = e.id 
                    inner join divisions f on a.division_id = f.id
                    inner join employee_overtimes h on a.task_id = h.task_id
                    left join employees g on h.apv_spv_nip = g.nip
                    left join employees i on h.apv_asman_nip = i.nip
                    left join employees j on h.apv_ppic_nip = j.nip
                    left join employees k on h.apv_mgr_nip = k.nip
                    left join employees l on h.apv_head_nip = l.nip
                    where a.location = '$payload[location]' 
                    and a.status not in('CANCELED','REJECTED')
                    and a.payment_status = 'VERIFIED'
                    #where#
                ) as foo
                #order# #perpage# #offset#
            ",
            'startWhere' => 'AND'
        ];

        $data = $this->PaginationModel->paginate($options, $params);

        response([
            'status' => 'success',
            'data' => $data
        ]);
    }
}