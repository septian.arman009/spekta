<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class AbsenController extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers());

        $this->load->model("PaginationModel", 'PaginationModel');
        $this->PaginationModel->myConstruct('hr');
    }

    public function getAbsens()
    {
        $payload = $this->jwt->me($this->input->request_headers());

        $params = getParam();
        if(!isset($params['month'])) response(['status' => 400, 'message' => 'Month is required'], 400);
        if(!isset($params['year'])) response(['status' => 400, 'message' => 'Year is required'], 400);

        if(!is_numeric($params['month'])) response(['status' => 400, 'message' => 'Month must be a number 1-12'], 400);
        if(!is_numeric($params['year'])) response(['status' => 400, 'message' => 'Year must be a number ex: 2023'], 400);

        $month = $params['month'];
        $year = $params['year'];

        $absenTable = absenTable($year, $month);

        $options = [
            'fields' => [
                'id', 'location', 
                'emp_id', 'employee_name', 'sap_id', 'npp', 'nik', 'parent_nik', 'npwp',
                'department_name', 'sub_department_name', 'division_name',
                'abs_date', 'shift', 'sch_date_in', 'sch_date_out', 'date_in', 'date_out',
                'gate', 'distance', 'lat', 'lng', 'distance_out', 'lat_out', 'lng_out',
                'qr_code_in', 'qr_code_out'
            ],
            'global_search_fields' => [
                'location' => 'a.location',
                'emp_id' => 'a.emp_id',
                'employee_name' => 'b.employee_name',
                'sap_id' => 'b.sap_id',
                'npp' => 'b.nip',
                'nik' => 'b.nik',
                'parent_nik' => 'b.parent_nik',
                'npwp' => 'b.npwp',
                'department_name' => 'c.name',
                'sub_department_name' => 'd.name',
                'division_name' => 'e.name',
                'abs_date' => 'a.abs_date',
                'shift' => 'a.shift',
                'sch_date_in' => 'a.sch_date_in',
                'sch_date_out' => 'a.sch_date_out',
                'date_in' => 'a.date_in',
                'date_out' => 'a.date_out',
                'gate' => 'a.gate',
                'distance' => 'a.distance',
                'lat' => 'a.lat',
                'lng' => 'a.lng',
                'distance_out' => 'a.distance_out',
                'lat_out' => 'a.lat_out',
                'lng_out' => 'a.lng_out',
                'qr_code_in' => 'a.qr_code_in',
                'qr_code_out' => 'a.qr_code_out'
            ],
            'global_filter_sub_fields' => [
                'id' => 'a.id',
                'location' => 'a.location',
                'emp_id' => 'a.emp_id',
                'employee_name' => 'b.employee_name',
                'sap_id' => 'b.sap_id',
                'npp' => 'b.nip',
                'nik' => 'b.nik',
                'parent_nik' => 'b.parent_nik',
                'npwp' => 'b.npwp',
                'department_name' => 'c.name',
                'sub_department_name' => 'd.name',
                'division_name' => 'e.name',
                'abs_date' => 'a.abs_date',
                'shift' => 'a.shift',
                'sch_date_in' => 'a.sch_date_in',
                'sch_date_out' => 'a.sch_date_out',
                'date_in' => 'a.date_in',
                'date_out' => 'a.date_out',
                'date_in' => 'a.date_in',
                'date_out' => 'a.date_out',
                'gate' => 'a.gate',
                'distance' => 'a.distance',
                'lat' => 'a.lat',
                'lng' => 'a.lng',
                'distance_out' => 'a.distance_out',
                'lat_out' => 'a.lat_out',
                'lng_out' => 'a.lng_out',
                'qr_code_in' => 'a.qr_code_in',
                'qr_code_out' => 'a.qr_code_out'
            ],
            'queries' => "
                select #select# from(
                    SELECT 
                        a.id, a.location, a.emp_id, 
                        a.abs_date, a.shift, a.sch_date_in, a.sch_date_out, a.date_in, a.date_out, a.gate,
                        a.distance, a.lat, a.lng,
                        a.distance_out, a.lat_out, a.lng_out,
                        a.qr_code_in, a.qr_code_out,

                        b.employee_name,
                        b.sap_id,
                        b.nip as npp,
                        b.nik,
                        b.parent_nik,
                        b.npwp,
                        c.name AS department_name,
                        d.name AS sub_department_name,
                        e.name AS division_name

                    FROM $absenTable a
                    INNER JOIN employees b ON a.emp_id = b.id
                    LEFT JOIN departments c ON b.department_id = c.id 
                    LEFT JOIN sub_departments d ON b.sub_department_id = d.id 
                    LEFT JOIN divisions e ON b.division_id = e.id
                    WHERE a.location = '$payload[location]'
                    #where#
                ) as foo
                #order# #perpage# #offset#
            ",
            'startWhere' => 'AND'
        ];

        $data = $this->PaginationModel->paginate($options, $params);

        response([
            'status' => 'success',
            'data' => $data
        ]);
    }
}