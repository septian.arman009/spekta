<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PaginationModel extends CI_Model
{    

    private $table;
    private $global_search_fields;
    private $global_filter_sub_fields;

    public function myConstruct($db_name = true)
    {
        parent::__construct();
        $this->db = $this->load->database($db_name, TRUE);
    }

    public function paginate($params, $request)
    {
        $this->table = isset($params['table']) ? $params['table'] : null;
        $this->global_search_fields = isset($params['global_search_fields']) ? $params['global_search_fields'] : [];
        $this->global_filter_sub_fields = isset($params['global_filter_sub_fields']) ? $params['global_filter_sub_fields'] : [];

        $startWhere = isset($params['startWhere']) ? $params['startWhere'] : 'WHERE';

        $fields = $params['fields'];
        $queries = isset($params['queries']) ? $params['queries'] : null;

        $select = isset($request['select']) ? $request['select'] : '*';
        $search = isset($request['search']) ? $request['search'] : '';
        $page = (int) isset($request['page']) ? $request['page'] : 1;
        $perpage = (int) isset($request['perpage']) ? $request['perpage'] : 10;

        $orders = isset($request['orders']) ? $request['orders'] : null;
        if(!$orders) {
            if(isset($params['order'])) {
                $orders = $params['order'];
            }
        }

        $filter = isset($request['filter']) ? $request['filter'] : null;
        $filterList = [];
        if ($filter) {
            $filterArray = explode(",", $filter);
            foreach ($filterArray as $vFilter) {
                $e = explode(":", $vFilter);
                if ($queries != null) {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter AND field, search value is required"];
                    } else if (!array_key_exists($e[0], $this->global_filter_sub_fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter AND field"];
                    }
                    $filterList[$this->global_filter_sub_fields[$e[0]]] = $e[1];
                } else {
                    if (!in_array($e[0], $fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter AND field"];
                    }
                    $filterList[$e[0]] = $e[1];
                }
            }
        }

        $ge = isset($request['ge']) ? $request['ge'] : null;
        $gteList = [];
        if ($ge) {
            $geArray = explode(",", $ge);
            foreach ($geArray as $vGe) {
                $e = explode(":", $vGe);
                if ($queries != null) {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter GREATER THAN EQUAL field, search value is required"];
                    } else if (!array_key_exists($e[0], $this->global_filter_sub_fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter GREATER THAN EQUAL field"];
                    }
                    $gteList[$this->global_filter_sub_fields[$e[0]]] = $e[1];
                } else {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on GREATER THAN EQUAL field, search value is required"];
                    } else if (!in_array($e[0], $fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on GREATER THAN EQUAL field"];
                    }
                    $gteList[$e[0]] = $e[1];
                }
            }
        }

        $filterNot = isset($request['filterNot']) ? $request['filterNot'] : null;
        $filterNotList = [];
        if ($filterNot) {
            $filterNotArray = explode(",", $filterNot);
            foreach ($filterNotArray as $vFilter) {
                $e = explode(":", $vFilter);
                if ($queries != null) {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter NOT field, search value is required"];
                    } else if (!array_key_exists($e[0], $this->global_filter_sub_fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter NOT field"];
                    }
                    $filterNotList[$this->global_filter_sub_fields[$e[0]]] = $e[1];
                } else {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filterAndNegative field, search value is required"];
                    } else if (!in_array($e[0], $fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filterAndNegative field"];
                    }
                    $filterNotList[$e[0]] = $e[1];
                }
            }
        }

        $filterFix = isset($request['filterFix']) ? $request['filterFix'] : null;
        $filterFixList = [];
        if ($filterFix) {
            $filterFixArray = explode(",", $filterFix);
            foreach ($filterFixArray as $vFilter) {
                $e = explode(":", $vFilter);
                if ($queries != null) {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter FIX field, search value is required"];
                    } else if (!array_key_exists($e[0], $this->global_filter_sub_fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter FIX field"];
                    }
                    $filterFixList[$this->global_filter_sub_fields[$e[0]]] = $e[1];
                } else {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter FIX field, search value is required"];
                    } else if (!in_array($e[0], $fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter FIX field"];
                    }
                    $filterFixList[$e[0]] = $e[1];
                }
            }
        }

        $filterRange = isset($request['filterRange']) ? $request['filterRange'] : null;
        $filterRangeList = [];
        if ($filterRange) {
            $filterRangeArray = explode(",", $filterRange);
            foreach ($filterRangeArray as $vFilter) {
                $e = explode(":", $vFilter);
            
                if ($queries != null) {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter RANGE field, search value is required"];
                    } else if (!array_key_exists($e[0], $this->global_filter_sub_fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter RANGE field"];
                    } else if(count(explode(".", $e[1])) <= 1) {
                        return ['status' => 400, 'message' => "Invalid value of $e[0] on filter RANGE must be separate by (.)"];
                    }
                    $filterRangeList[$this->global_filter_sub_fields[$e[0]]] = $e[1];
                } else {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter RANGE field, search value is required"];
                    } else if (!in_array($e[0], $fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter RANGE field"];
                    } else if(count(explode(".", $e[1])) <= 1) {
                        return ['status' => 400, 'message' => "Invalid value of $e[0] on filter RANGE must be separate by (.)"];
                    }
                    $filterRangeList[$e[0]] = $e[1];
                }
            }
        }

        $filterIn = isset($request['filterIn']) ? $request['filterIn'] : null;
        $filterInList = [];
        if ($filterIn) {
            $filterInArray = explode(",", $filterIn);
            foreach ($filterInArray as $vFilter) {
                $e = explode(":", $vFilter);
            
                if ($queries != null) {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter IN field, search value is required"];
                    } else if (!array_key_exists($e[0], $this->global_filter_sub_fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter IN field"];
                    }
                    $filterInList[$this->global_filter_sub_fields[$e[0]]] = $e[1];
                } else {
                    if(!isset($e[1]) || $e[1] == "") {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter IN field, search value is required"];
                    } else if (!in_array($e[0], $fields)) {
                        return ['status' => 400, 'message' => "Invalid field $e[0] on filter IN field"];
                    }
                    $filterInList[$e[0]] = $e[1];
                }
            }
        }

        if ($select == '*') {
            $select = implode(",", $fields);
        } else {
            if ($select != '') {
                $selects = explode(",", $select);
                foreach ($selects as $v) {
                    if (!in_array(strtolower($v), $fields) && !empty($v)) {
                        return ['status' => 400, 'message' => "Invalid field $v on Select field"];
                    }
                }
                $select = implode(",", $selects);
            } else {
                $select = implode(",", $fields);
            }
        }
        
        $expOrders = [];

        if($orders) {
            $expOrders = explode(',', $orders);
            foreach ($expOrders as $ord) {
                $expOrd = explode(':', $ord);
                if (count($expOrd) > 1&&!in_array(strtolower($expOrd[0]), $fields)) {
                    return ['status' => 400, 'message' => "Invalid order $expOrd[0]"];
                }
            }
        }

        if ($page <= 0) {
            $page = 1;
        }

        if ($perpage <= 0) {
            $perpage = 10;
        }
        return $this->getPagination($select, $search, $page, $perpage, $filterList, $filterNotList, $filterFixList, $filterInList, $gteList, $queries, $expOrders, $startWhere);
    }

    public function getPagination($select, $search, $page, $perpage, $filter = [], $filterNot = [], $filterFix = [], $filterIn = [], $gte = [], $queries = null, $orders, $startWhere)
    {
        $searchQuery = "";
        $where = "";

        if ($search !== '') {
            $no = 0;
            $whereSearch = "";
            foreach ($this->global_search_fields as $key => $v) {
                if(!is_numeric($v) && !is_bool($v)) {
                    if ($no == 0) {
                        $whereSearch .= "lower($v) like '%".strtolower($search)."%'";
                        $no++;
                    } else {
                        $whereSearch .= " or lower($v) like '%".strtolower($search)."%'";
                    }
                }
            }

            if($startWhere == 'AND') {
                $searchQuery = "$whereSearch and";
            } else {
                $searchQuery = " where ($whereSearch)";
            }
        }
        
        if (isset($filter) && count($filter) > 0) {
            $no = 0;
            $filterWhere = "";
            foreach ($filter as $key => $v) {
                if($v != '') {
                    if ($no == 0) {
                        if (!$filterWhere) {
                            $filterWhere .= "lower($key) like '%".strtolower($v)."%'";
                        } else {
                            $filterWhere .= " and lower($key) like '%".strtolower($v)."%'";
                        }
                        $no++;
                    } else {
                        $filterWhere .= " and lower($key) like '%".strtolower($v)."%'";
                    }
                }
            }

            if ($searchQuery == "") {
                if($startWhere == 'AND') {
                    $where = "and $filterWhere";
                } else {
                    $where = "where $filterWhere";
                }
            } else {
                if($startWhere == 'AND') {
                    $where .= " and $filterWhere";
                } else {
                    $where .= "where $filterWhere";
                }
            }
        }

        if (isset($filterFix) && count($filterFix) > 0) {
            $no = 0;
            $filterFixWhere = "";
            foreach ($filterFix as $key => $v) {
                if($v != '') {
                    if ($no == 0) {
                        if (!$filterFixWhere) {
                            $filterFixWhere .= "lower($key) = lower($v)";
                        } else {
                            $filterFixWhere .= " and lower($key) = lower($v)";
                        }
                        $no++;
                    } else {
                        $filterFixWhere .= " and lower($key) = 'lower($v)";
                    }
                }
            }

            if ($searchQuery == "") {
                if($startWhere == 'AND') {
                    $where = "and $filterFixWhere";
                } else {
                    $where = "where $filterFixWhere";
                }
            } else {
                if($startWhere == 'AND') {
                    $where .= " and $filterFixWhere";
                } else {
                    $where .= "where $filterFixWhere";
                }
            }
        }

        if (isset($filterNot) && count($filterNot) > 0) {
            $no = 0;
            $filterNotWhere = "";
            foreach ($filterNot as $key => $v) {
                if($v != '') {
                    if ($no == 0) {
                        if (!$filterNotWhere) {
                            $filterNotWhere .= "lower($key) != '".strtolower($v)."'";
                        } else {
                            $filterNotWhere .= " and lower($key) != '".strtolower($v)."'";
                        }
                        $no++;
                    } else {
                        $filterNotWhere .= " and lower($key) != '".strtolower($v)."'";
                    }
                }
            }

            if ($searchQuery == "") {
                if($startWhere == 'AND') {
                    $where = "and $filterNotWhere";
                } else {
                    $where = "where $filterNotWhere";
                }
            } else {
                if($startWhere == 'AND') {
                    $where .= " and $filterNotWhere";
                } else {
                    $where .= "where $filterNotWhere";
                }
            }
        }

        if (isset($filterRange) && count($filterRange) > 0) {
            $no = 0;
            $filterRangeWhere = "";
            foreach ($filterRange as $key => $v) {
                if($v != '') {
                    $v = explode(".", $v);
                    if ($no == 0) {
                        if (!$filterRangeWhere) {
                            $filterRangeWhere .= "date($key) between '$v[0]' and '$v[1]'";
                        } else {
                            $filterRangeWhere .= " and date($key) between '$v[0]' and '$v[1]'";
                        }
                        $no++;
                    } else {
                        $filterRangeWhere .= " and date($key) between '$v[0]' and '$v[1]'";
                    }
                }
            }

            if ($searchQuery == "") {
                if($startWhere == 'AND') {
                    $where = "and $filterRangeWhere";
                } else {
                    $where = "where $filterRangeWhere";
                }
            } else {
                if($startWhere == 'AND') {
                    $where .= " and $filterRangeWhere";
                } else {
                    $where .= "where $filterRangeWhere";
                }
            }
        }

        if (isset($filterIn) && count($filterIn) > 0) {
            $no = 0;
            $filterInWhere = "";
            foreach ($filterIn as $key => $v) {
                if($v != '') {
                    $v = explode(".", $v);
                    $value = "";
                    foreach ($v as $vl) {
                        if($value == "") {
                            $value = "'$vl'";
                        } else {
                            $value = $value.",'$vl'";
                        }
                    }

                    if ($no == 0) {
                        if (!$filterInWhere) {
                            $filterInWhere .= "$key in($value)";
                        } else {
                            $filterInWhere .= " and $key in($value)";
                        }
                        $no++;
                    } else {
                        $filterInWhere .= " and $key in($value)";
                    }
                }
            }

            if ($searchQuery == "") {
                if($startWhere == 'AND') {
                    $where = "and $filterInWhere";
                } else {
                    $where = "where $filterInWhere";
                }
            } else {
                if($startWhere == 'AND') {
                    $where .= " and $filterInWhere";
                } else {
                    $where .= "where $filterInWhere";
                }
            }
        }

        if (isset($gte) && count($gte) > 0) {
            $no = 0;
            $gteWhere = "";
            foreach ($gte as $key => $v) {
                if($v != '') {
                    if ($no == 0) {
                        if (!$gteWhere) {
                            $gteWhere .= "$key >= '$v'";
                        } else {
                            $gteWhere .= " and $key >= '$v'";
                        }
                        $no++;
                    } else {
                        $gteWhere .= " and $key >= '$v'";
                    }
                }
            }

            if ($searchQuery == "") {
                if($startWhere == 'AND') {
                    if($where == "") {
                        $where = "and $gteWhere";
                    } else {
                        $where .= " and $gteWhere";
                    }
                } else {
                    if($where == "") {
                        $where = "where $gteWhere";
                    } else {
                        $where .= " and $gteWhere";
                    }
                }
            } else {
                if($startWhere == 'AND') {
                    if($where == "") {
                        $where = "and $gteWhere";
                    } else {
                        $where .= " and $gteWhere";
                    }
                } else {
                    if($where == "") {
                        $where = "where $gteWhere";
                    } else {
                        $where .= " and $gteWhere";
                    }
                }
            }
        }

        $offset = ($page - 1) * $perpage;

        $newOrder = "";
        if(count($orders) > 0) {
            foreach ($orders as $ord) {
                $expOrd = explode(':', $ord);
                if(count($expOrd) > 0 && ($expOrd[1] == 'asc' || $expOrd[1] == 'desc')) {
                    if($newOrder == '') {
                        $newOrder .= "order by $expOrd[0] $expOrd[1]";
                    } else {
                        $newOrder .= ",$expOrd[0] $expOrd[1]";
                    }
                }
            }
        } 

        if ($queries != null) {
            $query0 = str_replace(
                array("#select#", "#search#", "#where#", "#order#", "#perpage#", "#offset#"),
                array('count(1) as total', $searchQuery, $where, '', '', '', ''),
                $queries
            );
            $query1 = str_replace(
                array("#select#", "#search#", "#where#", "#order#", "#perpage#", "#offset#"),
                array($select, $searchQuery, $where, $newOrder, 'limit ' . $perpage, 'offset ' . $offset),
                $queries
            );
        } else {
            $query0 = "select count(1) as total from $this->table $where";
            $query1 = "select $select from $this->table $where $newOrder limit $perpage offset $offset";
        }

        $total = $this->db->query($query0)->row()->total;
        $total_page = ceil($total / $perpage * 1.0);
        $data = $this->db->query($query1)->result();
        $end_index = $page * $perpage;

        $prev = null;
        $next = null;

        if ($end_index < $total) {
            $next = $page + 1;
        }

        if ($offset > 0) {
            $prev = $page - 1;
        }

        $totalData = count($data);

        return [
            'current_page' => intval($page),
            'data' => $data,
            'from' => intval($offset + 1),
            'to' => intval($offset + $totalData),
            'per_page' => intval($perpage),
            'total' => intval($total),
            'total_page' => ($total_page),
            'prev_page' => $prev ? intval($prev) : $prev,
            'next_page' => $next ? intval($next) : $next,
        ];
    }
}