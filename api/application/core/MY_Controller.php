<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
}

class API_Controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('BasicModel', 'Main');
        $this->Main->myConstruct('main');
        $this->load->model('BasicModel', 'Hr');
        $this->Hr->myConstruct('hr');
        $this->load->model('BasicModel', 'Qhse');
        $this->Qhse->myConstruct('qhse');
        $this->load->model('BasicModel', 'General');
        $this->General->myConstruct('general');
        $this->load->model('BasicModel', 'Mtn');
        $this->Mtn->myConstruct('mtn');
        $this->load->model('BasicModel', 'Prod');
        $this->Prod->myConstruct('prod');
        $this->load->model('BasicModel', 'Chat');
        $this->Chat->myConstruct('chat');
    }
}