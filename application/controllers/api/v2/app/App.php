<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class App extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers('authorization'));

        $this->load->model('api/MainModel', 'MainModel');
        $this->Other->myConstruct('main');
    }

    public function pushMessage()
    {
        $users = $this->Main->rawQuery("select * from users where fcm_token is not null and fcm_token != ''")->result();
        $totalToken = count($users);

        $fcmToken = [];
        $restToken = [];
        $index = 0;
        $data = 0;
        foreach ($users as $user) {
            $data++;
            $totalToken--;
            if($totalToken >= 100) {
                $fcmToken[$index][] = $user->fcm_token;
            } else if($totalToken < 100) {
                $restToken[] = $user->fcm_token;
            }
           
            if($data % 100 == 0) {
                $index++;
            } 
        }
        
        if(count($restToken) > 0) $fcmToken[$index + 1] = $restToken;

        $currentDate = date('Y-m-d H:i:s');

        $alertName = $this->input->post('alert_name');
        $emailTo = "";
        $subject = $this->input->post('subject');
        $subjectName = $this->input->post('subject_name');
        $token = time() . '_' . $alertName;
        $createdAt = toIndoDateTime($currentDate);
        $positive = "";
        $negative = "";

        $titleMessage = $this->input->post('title_message');
        $bodyMessage = $this->input->post('body_message');
        $message = '<div>
                        <div style="padding: 5px 0px 0px 10px;text-align:center;">
                            <img style="width: 220px;height: auto;" src="https://goreklame.com/assets/logo-kf.png" alt="kf">
                            <hr>
                            <p>PT Kimia Farma Tbk. Plant Jakarta</p>
                        </div>
                    
                        <div style="background: white;margin-top: 20px;border-radius: 5px;border: 1px solid #422800;padding: 10px;box-shadow: 5px 10px #ccc;">
                            <p>'.$titleMessage.'</p>
                            <p>'.$bodyMessage.'</p>
                        </div>
                    </div>';

        $dataEMail = [
            'alert_name' => $alertName,
            'email_to' => $emailTo,
            'subject' => $subject,
            'subject_name' => $subjectName,
            'apv_status' => 1,
            'token' => $token,
            'created_at' => $currentDate,
            'positive' => $positive,
            'negative' => $negative,
            'message' => $message,
            'status' => 1,
            'send_date' => $currentDate,
            'fcm_status' => 1
        ];

        $insert = $this->Main->create('email', $dataEMail);

        if($insert) {
            foreach ($fcmToken as $fToken) {
                $this->firebaselib->sendMultiNotification($fToken, [
                        'title' => $this->input->post('notif_title'),
                        'body' => $this->input->post('notif_message'),
                        'payload' => [
                            'alertName' => $alertName,
                            'emailTo' => $emailTo,
                            'subject' => $subject,
                            'subjectName' => $subjectName,
                            'apvStatus' => "CLOSED",
                            'token' => $token,
                            'createdAt' => $createdAt,
                            'positive' => $positive,
                            'negative' => $negative
                    ]
                ]);
            }
        }

        response([
            "status" => 200,
            "message" => "Kirim notifikasi berhasil"
        ]);
    }

    public function updateMobileVersion() {
        $version = $this->input->post('version');
        $message = $this->input->post('message');
        $force_update = $this->input->post('force_update');

        if(
            !isset($version) || !isset($message) || !isset($force_update)
        ) {
            response([
                'status' => 400,
                'message' => "Update versi gagal data tidak lengkap"
            ]);
        }

        $this->Main->update('version', [
            'version' => $version,
            'message' => $message,
            'force_update' => $force_update
        ], ['id' => 1]);

        $this->MainModel->cleanImeiFcm();

        response([
            'status' => 200,
            'message' => 'Update versi berhasil'
        ]);
    }
    
    public function checkVersion()
    {
        $imeiNo = $this->input->post('imeiNo');
        $version = $this->input->post('version') != null ? $this->input->post('version') : OLD_VERSION;
        $device = $this->input->post('device') ?? "Android";

        $currentVersion = $this->Main->getDataById('version', 1);
        $currentVersionName = $device == 'Android' ? $currentVersion->version : $currentVersion->ios_version;
        $payload = $this->jwt->me($this->input->request_headers('authorization'));

        $user = $this->Main->getDataById('users', $payload['userId']);

        $arman = $this->Main->getDataById('users', 22);
        if($imeiNo != $arman->imei_no && $version == $currentVersionName) {
            if($user->imei_no == '') $this->Main->updateById('users', ['imei_no' => $imeiNo], $user->id);
            $deviceImei = $user->imei_no != '' ? $user->imei_no : $imeiNo;
        } else {
            $deviceImei = $imeiNo;
        }

        response([
            'status' => 200,
            'data' => [
                'imei_no' => $deviceImei,
                'version' => $currentVersionName,
                'force_update' => $currentVersion->force_update,
                'downloadUrl' => $currentVersion->download_url,
                'outer_link' => $currentVersion->outer_link,
                'outer_name' => $currentVersion->outer_name,
                'android_id' => $currentVersion->android_id,
                'ios_id' => $currentVersion->ios_id,
                'message' => $currentVersion->message,
                'DEVICE_ID' => $arman->imei_no,
                'fcm_token' => $user->fcm_token,
                'id_card' => $user->id_card
            ]
        ]);
    }

    public function checkHomeCoordinates()
    {
        $version = $this->Main->getDataById('version', 1);
        response([
            'status' => 200,
            'data' => [
                'latitude' => floatval($version->lat),
                'longitude' => floatval($version->lng)
            ]
        ]);
    }
}