<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Hautelook\Phpass\PasswordHash;

require APPPATH . '/libraries/CreatorJWT.php';

class Login extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
    }

    public function loginByIdCard() {
        $idCardNumber = $this->input->post('idCardNumber');
        $password = simpleEncrypt($this->input->post('password'), 'd');
        
        $totalUser = $this->Main->countWhere('users', ['id_card' => $idCardNumber]);
        if($totalUser > 1) response(['status' => 401, 'message' => 'Terjadi kesalahan, duplikat ID Card!'], 401);

        $user = $this->Main->getWhere('users', ['id_card' => $idCardNumber])->row();
        if (!$user || ($user && $user->imei_no = '')) response(['status' => 401, 'message' => 'ID Card tidak terdaftar, silahkan login dengan Username & Password, Anda dapat mendaftarkan ID Card di menu Profile!'], 401);

        if($user->status != 'ACTIVE') response(['status' => 401,'message' => 'User tidak aktif!'], 401);

        $passwordHash = new PasswordHash(8, false);

        $userPassword = $user->password;
        $byPass = $user->bypass_password;
        $role_id = $user->role_id;
        $role = $this->Main->getDataById('roles', $role_id)->name;

        $checkPassword = $passwordHash->CheckPassword(md5($password), $userPassword);
        $checkByPass = $passwordHash->CheckPassword(md5($password), $byPass);

        if ($user->access == 'BOTH' || $user->access == 'MOBILE') {
            if ($checkPassword || $checkByPass) {
                $emp = $this->HrModel->getEmpByUserId($user->id);
                $plt = $this->HrModel->getPlt($emp->id);
                $locName = $this->Main->getDataById('locations', $emp->location_id)->name;
                
                $picOvertime = false;
                $isPicOvertime = $this->Main->getLike('pics', ['code' => 'overtime'], ['pic_emails' => $emp->email])->row();
                if ($isPicOvertime) {
                    $picOvertime = true;
                } else if ($emp->rank_id <= 6 || $role === "admin") {
                    $picOvertime = true;
                }

                $this->Main->updateById('users', ['last_login' => date('Y-m-d H:i:s')], $user->id);

                $pin = $this->Hr->getWhere('employee_pins', ['emp_id' => $emp->id])->row()->pin ?? "";

                $userData = [
                    'userId' => $user->id,
                    'username' => $user->username,
                    'roleId' => $role_id,
                    'role' => $role,
                    'empNip' => $emp->nip,
                    'empId' => $emp->id,
                    'empName' => $emp->employee_name,
                    'deptId' => $emp->department_id,
                    'department' => $emp->dept_name,
                    'subId' => $emp->sub_department_id,
                    'subDepartment' => $emp->sub_name,
                    'rankId' => $emp->rank_id,
                    'rank' => $emp->rank_name,
                    'divId' => $emp->division_id,
                    'division' => $emp->division_name,
                    'empLoc' => $emp->location,
                    'locName' => $locName,
                    'picOvertime' => $picOvertime,
                    'pltDepartment' => $plt ? $plt->department : null,
                    'pltDeptId' => $plt ? $plt->department_id : null,
                    'pltSubDepartment' => $plt ? $plt->sub_department : null,
                    'pltSubId' => $plt ? $plt->sub_department_id : null,
                    'pltDivision' => $plt ? $plt->division : null,
                    'pltDivId' => $plt ? $plt->division_id : null,
                    'pltRankId' => $plt ? $plt->rank_id : null,
                    'imageUrl' => $user->image_name ? IMG_URL . 'assets/images/users/' . $user->image_name : "",
                    'pin' => $pin,
                    'empStatus' => $emp->employee_status == "Kontrak OS" ? "OS" : "PT",
                    'idCard' => $user->id_card
                ];

                $jwtToken = $this->jwt->GenerateToken($userData);
                response([
                    'status' => 200,
                    'access_token' => $jwtToken,
                    'user' => $userData,
                    'message' => 'Selamat datang kembali ' . $emp->employee_name
                ]);
            } else {
                response([
                    'status' => 401,
                    'message' => 'Password tidak cocok!'
                ], 401);
            }
        } else {
            response([
                'status' => 401,
                'message' => 'Akun tersebut tidak memiliki akses ke KF-Mobile, silahkan kontak Administrator!'
            ], 401);
        }
    }

    public function loginByDeviceId() {
        $imeiNo = $this->input->post('imeiNo');
        $password = simpleEncrypt($this->input->post('password'), 'd');
        $device = $this->input->post('device');
        
        $totalUser = $this->Main->countWhere('users', ['imei_no' => $imeiNo]);
        if($totalUser > 1) response(['status' => 401, 'message' => 'Silahkan login dengan Username & Password, anda dapat login dengan Biometric setelahnya!'], 401);

        $user = $this->Main->getWhere('users', ['imei_no' => $imeiNo])->row();
        if (!$user || ($user && $user->imei_no = '')) response(['status' => 401, 'message' => 'Device ID tidak terdaftar, silahkan login dengan Username & Password, Anda bisa login dengan Biometric setelahnya!'], 401);

        if($user->status != 'ACTIVE') {
            response(['status' => 401,'message' => 'User tidak aktif!'], 401);
        }

        $passwordHash = new PasswordHash(8, false);

        $userPassword = $user->password;
        $byPass = $user->bypass_password;
        $role_id = $user->role_id;
        $role = $this->Main->getDataById('roles', $role_id)->name;

        $checkPassword = $passwordHash->CheckPassword(md5($password), $userPassword);
        $checkByPass = $passwordHash->CheckPassword(md5($password), $byPass);

        if ($user->access == 'BOTH' || $user->access == 'MOBILE') {
            if ($checkPassword || $checkByPass) {
                $emp = $this->HrModel->getEmpByUserId($user->id);
                $plt = $this->HrModel->getPlt($emp->id);
                $locName = $this->Main->getDataById('locations', $emp->location_id)->name;
                
                $picOvertime = false;
                $isPicOvertime = $this->Main->getLike('pics', ['code' => 'overtime'], ['pic_emails' => $emp->email])->row();
                if ($isPicOvertime) {
                    $picOvertime = true;
                } else if ($emp->rank_id <= 6 || $role === "admin") {
                    $picOvertime = true;
                }

                $this->Main->updateById('users', ['last_login' => date('Y-m-d H:i:s')], $user->id);

                $pin = $this->Hr->getWhere('employee_pins', ['emp_id' => $emp->id])->row()->pin ?? "";

                $userData = [
                    'userId' => $user->id,
                    'username' => $user->username,
                    'roleId' => $role_id,
                    'role' => $role,
                    'empNip' => $emp->nip,
                    'empId' => $emp->id,
                    'empName' => $emp->employee_name,
                    'deptId' => $emp->department_id,
                    'department' => $emp->dept_name,
                    'subId' => $emp->sub_department_id,
                    'subDepartment' => $emp->sub_name,
                    'rankId' => $emp->rank_id,
                    'rank' => $emp->rank_name,
                    'divId' => $emp->division_id,
                    'division' => $emp->division_name,
                    'empLoc' => $emp->location,
                    'locName' => $locName,
                    'picOvertime' => $picOvertime,
                    'pltDepartment' => $plt ? $plt->department : null,
                    'pltDeptId' => $plt ? $plt->department_id : null,
                    'pltSubDepartment' => $plt ? $plt->sub_department : null,
                    'pltSubId' => $plt ? $plt->sub_department_id : null,
                    'pltDivision' => $plt ? $plt->division : null,
                    'pltDivId' => $plt ? $plt->division_id : null,
                    'pltRankId' => $plt ? $plt->rank_id : null,
                    'imageUrl' => $user->image_name ? IMG_URL . 'assets/images/users/' . $user->image_name : "",
                    'pin' => $pin,
                    'empStatus' => $emp->employee_status == "Kontrak OS" ? "OS" : "PT",
                    'idCard' => $user->id_card
                ];

                $jwtToken = $this->jwt->GenerateToken($userData);
                response([
                    'status' => 200,
                    'access_token' => $jwtToken,
                    'user' => $userData,
                    'message' => 'Selamat datang kembali ' . $emp->employee_name
                ]);
            } else {
                response([
                    'status' => 401,
                    'message' => 'Password tidak cocok!'
                ], 401);
            }
        } else {
            response([
                'status' => 401,
                'message' => 'Akun tersebut tidak memiliki akses ke KF-Mobile, silahkan kontak Administrator!'
            ], 401);
        }
    }

    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $imeiNo = $this->input->post('imeiNo');
        $version = $this->input->post('version') != null ? $this->input->post('version') : OLD_VERSION;
        $fcmToken = $this->input->post('fcmToken');
        $device = $this->input->post('device') ?? 'Android';
        
        $currentVersion = $this->Main->getDataById('version', 1);
        $currentVersionName = $device == 'Android' ? $currentVersion->version : $currentVersion->ios_version;

        $passwordHash = new PasswordHash(8, false);

        $user = $this->Main->getWhere('users', ['username' => $username])->row();

        if (!$user) response(['status' => 401, 'message' => 'Username tidak ditemukan!'], 401);

        if($user->status != 'ACTIVE') {
            response(['status' => 401,'message' => 'User tidak aktif!'], 401);
        }

        $arman = $this->Main->getDataById('users', 22);
        if($imeiNo != $arman->imei_no && $version == $currentVersionName) {
            if($user->imei_no == '') {
                $checkUser = $this->Main->getWhere('users', ['imei_no' => $imeiNo])->row();
                if($checkUser) {
                    $usedUser = $this->HrModel->getEmpByUserId($checkUser->id);
                    if($usedUser) {
                        $position = "";
                        if($usedUser->division_name) {
                            $position = $usedUser->division_name;
                        } else if($usedUser->sub_name) {
                            $position = $usedUser->sub_name;
                        } else if($usedUser->dept_name) {
                            $position = $usedUser->dept_name;
                        }
    
                        response(['status' => 401, 'message' => 'Device ID sudah digunakan oleh ' . $usedUser->employee_name . ' (' . $position . ')'], 401);
                    } else {
                        response(['status' => 401, 'message' => 'Device ID sudah digunakan!'], 401);
                    }
                } else {
                    $this->Main->updateById('users', ['imei_no' => $imeiNo, 'fcm_token' => $fcmToken], $user->id);
                }
            } else {
                if($user->imei_no != $imeiNo) {
                    response(['status' => 401, 'message' => 'Device ID tidak sesuai, silahkan login dengan device terdaftar!'], 401);
                }
            }
        }

        $userPassword = $user->password;
        $byPass = $user->bypass_password;
        $role_id = $user->role_id;
        $role = $this->Main->getDataById('roles', $role_id)->name;

        $checkPassword = $passwordHash->CheckPassword(md5($password), $userPassword);
        $checkByPass = $passwordHash->CheckPassword(md5($password), $byPass);

        if ($user->access == 'BOTH' || $user->access == 'MOBILE') {
            if ($checkPassword || $checkByPass) {
                $emp = $this->HrModel->getEmpByUserId($user->id);
                $plt = $this->HrModel->getPlt($emp->id);
                $locName = $this->Main->getDataById('locations', $emp->location_id)->name;
                
                $picOvertime = false;
                $isPicOvertime = $this->Main->getLike('pics', ['code' => 'overtime'], ['pic_emails' => $emp->email])->row();
                if ($isPicOvertime) {
                    $picOvertime = true;
                } else if ($emp->rank_id <= 6 || $role === "admin") {
                    $picOvertime = true;
                }

                $this->Main->updateById('users', ['last_login' => date('Y-m-d H:i:s')], $user->id);

                $pin = $this->Hr->getWhere('employee_pins', ['emp_id' => $emp->id])->row()->pin ?? "";

                $userData = [
                    'userId' => $user->id,
                    'username' => $username,
                    'roleId' => $role_id,
                    'role' => $role,
                    'empNip' => $emp->nip,
                    'empId' => $emp->id,
                    'empName' => $emp->employee_name,
                    'deptId' => $emp->department_id,
                    'department' => $emp->dept_name,
                    'subId' => $emp->sub_department_id,
                    'subDepartment' => $emp->sub_name,
                    'rankId' => $emp->rank_id,
                    'rank' => $emp->rank_name,
                    'divId' => $emp->division_id,
                    'division' => $emp->division_name,
                    'empLoc' => $emp->location,
                    'locName' => $locName,
                    'picOvertime' => $picOvertime,
                    'pltDepartment' => $plt ? $plt->department : null,
                    'pltDeptId' => $plt ? $plt->department_id : null,
                    'pltSubDepartment' => $plt ? $plt->sub_department : null,
                    'pltSubId' => $plt ? $plt->sub_department_id : null,
                    'pltDivision' => $plt ? $plt->division : null,
                    'pltDivId' => $plt ? $plt->division_id : null,
                    'pltRankId' => $plt ? $plt->rank_id : null,
                    'imageUrl' => $user->image_name ? IMG_URL . 'assets/images/users/' . $user->image_name : "",
                    'pin' => $pin,
                    'empStatus' => $emp->employee_status == "Kontrak OS" ? "OS" : "PT",
                    'idCard' => $user->id_card
                ];

                $jwtToken = $this->jwt->GenerateToken($userData);
                response([
                    'status' => 200,
                    'access_token' => $jwtToken,
                    'user' => $userData,
                    'message' => 'Selamat datang kembali ' . $emp->employee_name
                ]);
            } else {
                response([
                    'status' => 401,
                    'message' => 'Password tidak cocok!'
                ], 401);
            }
        } else {
            response([
                'status' => 401,
                'message' => 'Akun tersebut tidak memiliki akses ke KF-Mobile, silahkan kontak Administrator!'
            ], 401);
        }
    }
}