<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class Overtime extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers('authorization'));

        $this->load->model('api/OvertimeModel', 'OvertimeModel');
        $this->OvertimeModel->myConstruct('hr');
    }

    public function getOvertimes()
    {
        $params = getParam();
        $date = $params['date'];

        $user = $this->jwt->me($this->input->request_headers('authorization'));
        $overtimes = $this->OvertimeModel->getOvertimes($user['empId'], $date);
        
        $realAbsenIn = '-';
        $realAbsenOut = '-';
        $difHourDateIn = '-';
        $difHourDateOut = '-';
        $statusApv = "-";
        $statusOvt = "CLOSED";

        $premi = 0;
        $totalOvtValue = 0;
        $totalOvtHour = 0;
        $totalDeduction = 0;
        $totalVerified = 0;
        $totalOnProgress = 0;
        $totalOvtVerified = 0;
        $overtimeList = [];
        
        foreach ($overtimes as $overtime) {
            
            if($overtime->date_in) {
                if($premi == 0) $premi = $overtime->premi_overtime;
                $totalOvtValue += $overtime->overtime_value;
                $totalOvtHour += $overtime->overtime_hour;
                $totalDeduction += $overtime->deduction;

                if($overtime->payment_status == 'VERIFIED') {
                    $totalVerified++;
                } else {
                    $totalOnProgress++;
                }

                $realAbsenIn = isset($overtime->date_in) ? toIndoDateTime($overtime->date_in) : '-';
                $realAbsenOut = isset($overtime->date_out) ? toIndoDateTime($overtime->date_out) : '-';

                $dateIn = date('Y-m-d H:i:s', strtotime($overtime->date_in));
                $dateOut = date('Y-m-d H:i:s', strtotime($overtime->date_out));
                $schDateIn = date('Y-m-d H:i:s', strtotime($overtime->sch_date_in));
                $schDateOut = date('Y-m-d H:i:s', strtotime($overtime->sch_date_out));
                // $ovtIn = date('Y-m-d H:i:s', strtotime($overtime->start_date));
                // $ovtOut = date('Y-m-d H:i:s', strtotime($overtime->end_date));

                $difHourDateInCount = 0;
                if($dateIn > $schDateIn) {
                    $difHourDateInCount = countHour($schDateIn, $dateIn, 'h-');
                    if($difHourDateInCount > 0) $difHourDateIn = normalHourMobile($difHourDateInCount);
                }

                $difHourDateOutCount = 0;
                if($dateOut > $schDateOut) {
                    $difHourDateOutCount = countHour($dateOut, $schDateOut, 'h-');
                    if($difHourDateOutCount > 0) $difHourDateOut = normalHourMobile($difHourDateOutCount);
                }

                if($overtime->apv_spv == 'APPROVED' || $overtime->apv_spv == 'BY PASS') $statusApv = 'SPV';
                if($overtime->apv_asman == 'APPROVED' || $overtime->apv_asman == 'BY PASS') $statusApv = 'ASMAN';
                if($overtime->apv_ppic == 'APPROVED' || $overtime->apv_ppic == 'BY PASS') $statusApv = 'PPIC';
                if($overtime->apv_mgr == 'APPROVED' || $overtime->apv_mgr == 'BY PASS') $statusApv = 'MANAGER';
                if($overtime->apv_head == 'APPROVED' || $overtime->apv_head == 'BY PASS') $statusApv = 'HEAD';

                if($overtime->status == 'CLOSED') $statusOvt = 'CLOSED';
                if($overtime->payment_status == 'VERIFIED') $statusOvt = 'VERIFIED';

                if($statusOvt == 'VERIFIED') $totalOvtVerified += $overtime->overtime_value;

                $dateOutTime  = 'No Record';
                if($realAbsenOut != '-') {
                    $dateOutTime = date('H:i', strtotime($overtime->date_out));
                    if(date('Y-m-d', strtotime($overtime->start_date)) != date('Y-m-d', strtotime($overtime->date_out))) {
                        $dateOutTime = toIndoDateTime5(date('Y-m-d H:i', strtotime($overtime->date_out)));
                    }
                }
                
                $overtimeList[] = [
                    'id' => $overtime->id,
                    'location' => $overtime->location,
                    'taskId' => $overtime->task_id,
                    'empId' => $overtime->emp_id,
                    'empTaskId' => $overtime->emp_task_id,
                    'overtimeDate' => toIndoDateDay($overtime->overtime_date),

                    'startDay' => toIndoDate(date('Y-m-d', strtotime($overtime->start_date))),
                    'startTime' => date('H:i', strtotime($overtime->start_date)),
                    'startDate' => toIndoDateTime($overtime->start_date),

                    'endDay' => toIndoDate(date('Y-m-d', strtotime($overtime->end_date))),
                    'endTime' => date('H:i', strtotime($overtime->end_date)),
                    'endDate' => toIndoDateTime($overtime->end_date),

                    'dateInDay' => $overtime->date_in ? toIndoDate(date('Y-m-d', strtotime($overtime->date_in))) : "No Record",
                    'dateInTime' => date('H:i', strtotime($overtime->date_in)),
                    'dateIn' => $realAbsenIn,

                    'dateOutDay' => $overtime->date_out ? toIndoDate(date('Y-m-d', strtotime($overtime->date_out))) : "No Record",
                    'dateOutTime' => $dateOutTime,
                    'dateOut' => $realAbsenOut,
                    
                    'late' => $difHourDateIn,
                    'earlyOut' => $difHourDateOut,
                    'statusDay' => $overtime->status_day,
                    'effectiveHour' => $overtime->effective_hour,
                    'breakHour' => $overtime->break_hour,
                    'realHour' => $overtime->real_hour,
                    'overtimeHour' => $overtime->overtime_hour,
                    'premiOvertime' => number_format($overtime->premi_overtime, 2, ',', '.'),
                    'overtimeValue' => number_format($overtime->overtime_value, 2, ',', '.'),
                    'deduction' => number_format($overtime->deduction, 2, ',', '.'),
                    'meal' => number_format($overtime->meal, 2, ',', '.'),
                    'totalMeal' => $overtime->total_meal,
                    'note' => $overtime->notes,
                    'machine1' => $overtime->machine_1_name,
                    'machine2' => $overtime->machine_2_name,
                    'overtimeSubDepartment' => $overtime->overtime_sub_department,
                    'overtimeDivision' => $overtime->overtime_division,
                    'statusApv' => $statusApv,
                    'statusOvt' => $statusOvt
                ];
            }
        }

        response([
            'status' => 200,
            'premi' => number_format($premi, 2, ',', '.'),
            'totalOvtValue' => number_format($totalOvtValue + $totalDeduction, 2, ',', '.'),
            'totalOvtVerified' => number_format($totalOvtVerified, 2, ',', '.'),
            'totalOvtHour' => number_format($totalOvtHour, 2, ',', '.'),
            'totalDeduction' => number_format($totalDeduction, 2, ',', '.'),
            'totalVerified' => $totalVerified . "",
            'totalOnProgress' => $totalOnProgress . "",
            'list' => $overtimeList
        ]);
    }
}