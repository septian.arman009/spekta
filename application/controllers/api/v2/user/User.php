<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

use Hautelook\Phpass\PasswordHash;

class User extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers('authorization'));

        $this->load->model('api/MainModel', 'MainModel');
        $this->MainModel->myConstruct('main');

        $this->load->model("api/PaginationModel", 'PaginationModel');
        $this->PaginationModel->myConstruct('main');
    }

    public function notification() {
        $params = getParam();
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $emp = $this->Hr->getDataById('employees', $payload['empId']);

        $options = [
            'fields' => [
                'id', 'alert_name', 'email_to', 'subject', 'subject_name', 
                'created_at', 'positive', 'negative', 'redirect', 'apv_status',
                'token', 'file', 'file_original'
            ],
            'global_search_fields' => [
                'alert_name' => 'alert_name',
                'email_to' => 'email_to',
                'subject' => 'subject',
                'subject_name' => 'subject_name',
                'created_at' => 'created_at',
                'positive' => 'positive',
                'negative' => 'negative',
                'redirect' => 'redirect',
                'apv_status' => 'apv_status',
                'token' => 'token',
                'file' => 'file',
                'file_original' => 'file_original',
            ],
            'global_filter_sub_fields' => [
                'id' => 'id',
                'alert_name' => 'alert_name',
                'email_to' => 'email_to',
                'subject' => 'subject',
                'subject_name' => 'subject_name',
                'created_at' => 'created_at',
                'positive' => 'positive',
                'negative' => 'negative',
                'redirect' => 'redirect',
                'apv_status' => 'apv_status',
                'token' => 'token',
                'file' => 'file',
                'file_original' => 'file_original',
            ],
            'queries' => "
                select #select# from(
                    select 
                    distinct
                    id,
                    alert_name,
                    email_to,
                    subject,
                    subject_name,
                    created_at,
                    positive,
                    negative,
                    redirect,
                    case when apv_status > 0 then 'CLOSED' else 'OPEN' end as apv_status,
                    token,
                    file,
                    file_original
                    from email
                    where 
                        (
                            (emp_direct_apv = $emp->id and
                            rank_apv is null and
                            dept_apv is null and
                            sub_dept_apv is null and
                            div_apv is null and
                            email_to like '%$emp->email%') 
                            or
                            (emp_direct_apv is null and
                            rank_apv is null and
                            dept_apv = $emp->department_id and
                            sub_dept_apv = $emp->sub_department_id and
                            div_apv is null and
                            email_to like '%$emp->email%')
                            or
                            (emp_direct_apv is null and
                            rank_apv is null and
                            dept_apv = $emp->department_id and
                            sub_dept_apv = $emp->sub_department_id and
                            div_apv = $emp->division_id and
                            email_to like '%$emp->email%')
                            or
                            (emp_direct_apv is null and
                            rank_apv is null and
                            dept_apv is null and
                            sub_dept_apv is null and
                            div_apv is null and
                            (alert_name like '%NOTIFIKASI_UPDATE_VERSI_%' or alert_name = 'INFORMASI_UMUM'))
                        ) and
                        token is not null and 
                        token != '' and
                        date(created_at) <= '2024-02-23'
                    #where#
                ) as foo
                #order# #perpage# #offset#
            ",
            'startWhere' => 'AND'
        ];

        $data = $this->PaginationModel->paginate($options, $params);

        foreach ($data['data'] as $dt) {
            $dt->alert_name = str_replace("_", " ", $dt->alert_name);
        }

        response([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function setIdCardNumber() {
        $idCardNumber = $this->input->post('idCardNumber');
        if($idCardNumber == "") {
            response([
                'status' => 400,
                'message' => 'Nomor ID Card tidak ditemukan!'
            ]);
        }
        
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $user = $this->Main->getDataById('users', $payload['userId']);

        $update = $this->Main->update('users', ['id_card' => $idCardNumber], ['id' => $user->id]);

        if($update) {
            $emp = $this->Hr->getWhere('employees', ['user_id' => $user->id])->row();
            $this->idCardNotification($user, $emp);
            
            response([
                'status' => 200,
                'message' => 'Update nomor ID Card berhasil berhasil'
            ]);
        } else {
            response([
                'status' => 400,
                'message' => 'Update nomor ID Card berhasil berhasil'
            ]);
        }
    }

    public function idCardNotification($user, $emp)
    {
        $currentDate = date('Y-m-d H:i:s');

        $alertName = "ID_CARD_REGISTRATION";
        $emailTo = "";
        $subject = "Registrasi ID Card Berhasil";
        $subjectName = "Registrasi ID Card Berhasil";
        $token = time() . "_ID_CARD_REGISTRATION";
        $createdAt = toIndoDateTime($currentDate);
        $positive = "";
        $negative = "";
        $message = '<div>
                        <div style="padding: 5px 0px 0px 10px;text-align:center;">
                            <img style="width: 220px;height: auto;" src="https://goreklame.com/assets/logo-kf.png" alt="kf">
                            <hr>
                            <p>PT Kimia Farma Tbk. Plant Jakarta</p>
                        </div>
                    
                        <div style="background: white;margin-top: 20px;border-radius: 5px;border: 1px solid #422800;padding: 10px;box-shadow: 5px 10px #ccc;">
                            <p>Hay '.$emp->employee_name.',</p>
                            <p>Selamat ID Card kamu berhasil didaftarkan, sekarang kamu bisa login dengan menempelkan ID Card kamu ke HP mu ya!</a>
                            <p style="text-align:center"><b>Salam Spekta...</b></p>
                        </div>
                    </div>';

        $dataEMail = [
            'alert_name' => $alertName,
            'email_to' => $emp->email,
            'subject' => $subject,
            'subject_name' => $subjectName,
            'apv_status' => 1,
            'token' => $token,
            'created_at' => $currentDate,
            'positive' => $positive,
            'negative' => $negative,
            'message' => $message,
            'status' => 0,
            'send_date' => $currentDate,
            'emp_direct_apv' => $emp->id
        ];

        $insert = $this->Main->create('email', $dataEMail);

        if($insert) {
            $this->firebaselib->sendNotification($user->fcm_token, [
                    'title' => "Selamat ID Card Berhasil Di Daftarkan",
                    'body' => "Sekarang kamu bisa login spekta dengan ID Card kamu ya, cukup tempelkan ID Card kamu ke belakang HP mu!",
                    'payload' => [
                        'alertName' => $alertName,
                        'emailTo' => $emailTo,
                        'subject' => $subject,
                        'subjectName' => $subjectName,
                        'apvStatus' => "CLOSED",
                        'token' => $token,
                        'createdAt' => $createdAt,
                        'positive' => $positive,
                        'negative' => $negative
                ]
            ]);
        }
    }

    public function setFcmToken() {
        $token = $this->input->post('token');
        if($token == "") {
            response([
                'status' => 400,
                'message' => 'Reset FCM Token gagal, token tidak ditemukan!'
            ]);
        }
        
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $user = $this->Main->getDataById('users', $payload['userId']);
        $arman = $this->Main->getDataById('users', 22);

        if($token != $arman->fcm_token) {
            $update = $this->Main->update('users', ['fcm_token' => $token], ['id' => $user->id]);
            if($update) {
                response([
                    'status' => 200,
                    'message' => 'Reset FCM Token berhasil'
                ]);
            } else {
                response([
                    'status' => 400,
                    'message' => 'Reset FCM Token gagal'
                ]);
            }
        } else {
            response([
                'status' => 400,
                'message' => 'Reset FCM Token gagal'
            ]);
        }
    }

    public function totalNotification() {
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $emp = $this->Hr->getDataById('employees', $payload['empId']);

        $total = $this->MainModel->totalNotification($emp);

        response([
            'status' => 200,
            'data' => $total
        ]);
    }

    public function me()
    {
        $imeiNo = $this->input->post('imeiNo');
        $version = $this->input->post('version') != null ? $this->input->post('version') : OLD_VERSION;
        $device = $this->input->post('device') ?? "Android";

        $currentVersion = $this->Main->getDataById('version', 1);
        $currentVersionName = $device == 'Android' ? $currentVersion->version : $currentVersion->ios_version;
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $user = $this->Main->getDataById('users', $payload['userId']);

        $arman = $this->Main->getDataById('users', 22);
        if($imeiNo != $arman->imei_no && $version == $currentVersionName) {
            if($user->imei_no == '') $this->Main->updateById('users', ['imei_no' => $imeiNo], $user->id);
            $deviceImei = $user->imei_no != '' ? $user->imei_no : $imeiNo;
        } else {
            $deviceImei = $imeiNo;
        }

        response([
            'status' => 200,
            'data' => $payload,
            'version' => $currentVersionName,
            'force_update' => $currentVersion->force_update,
            'imei_no' => $deviceImei,
            'downloadUrl' => $currentVersion->download_url,
            'outer_link' => $currentVersion->outer_link,
            'outer_name' => $currentVersion->outer_name,
            'android_id' => $currentVersion->android_id,
            'ios_id' => $currentVersion->ios_id,
            'message' => $currentVersion->message,
            'DEVICE_ID' => $arman->imei_no,
            'fcm_token' => $user->fcm_token,
            'id_card' => $user->id_card
        ]);
    }

    public function changePassword()
    {
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $currentPassword = $this->input->post('currentPassword');
        $newPassword = $this->input->post('newPassword');

        $passwordHash = new PasswordHash(8, false);

        $user = $this->Main->getWhere('users', ['username' => $payload['username']])->row();
        if (!$user) response(['status' => 401, 'message' => 'Username tidak ditemukan!'], 401);

        $checkPassword = $passwordHash->CheckPassword(md5($currentPassword), $user->password);
        if(!$checkPassword) {
            response([
                'status' => 400,
                'message' => 'Password lama tidak cocok'
            ]);
        } else {
            $identik = $passwordHash->CheckPassword(md5($newPassword), $user->password);
            if($identik) {
                response([
                    'status' => 400,
                    'message' => 'Password baru tidak boleh sama dengan password lama'
                ]);
            }

            $data = [
                'id' => $user->id,
                'password' => $passwordHash->HashPassword(md5($newPassword))
            ];

            $this->Main->updateById('users', $data, $user->id);
            response([
                'status' => 200,
                'message' => 'Ganti password berhasil'
            ]);
        }
    }

    public function updateProfile()
    {
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $user = $this->Main->getWhere('users', ['username' => $payload['username']])->row();
        if (!$user) response(['status' => 401, 'message' => 'Username tidak ditemukan!'], 401);

        if(isset($_FILES['image']['name'])) {
            $image = $_FILES['image']['name'];
            $extension = pathinfo($image, PATHINFO_EXTENSION);
            $filename = str_replace('.', '_', $user->username) . '_' . $user->nip . '.' . $extension;
            $config['upload_path']          = './assets/images/users';
            $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
            $config['file_name']            = $filename;
            $config['overwrite']            = true;
            $config['max_size']             = 10000;
            
            $this->load->library('upload', $config);

            $upload = $this->upload->do_upload('image');
            if($upload) {
                $this->Main->updateById('users', ['image_name' => $filename], $user->id);
                response([
                    'status' => 200,
                    'message' => 'Upload profile berhasil',
                    'imageUrl' => IMG_URL . 'assets/images/users/' . $filename
                ]);
            } else {
                response([
                    'status' => 400,
                    'message' => 'Upload profile gagal'
                ], 400);
            }
        } else {
            response([
                'status' => 400,
                'message' => 'File tidak boleh kosong'
            ], 400);
        }

    }
}