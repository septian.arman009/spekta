<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class Leave extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers('authorization'));

        $this->load->model("api/AbsenModel", 'AbsenModel');
        $this->AbsenModel->myConstruct('hr');
    }

    public function getLeaveTypes() {
        $types = $this->Hr->getAll('leave_types')->result();
        $list = [];
        foreach ($types as $type) {
            $list[] = [
                'id' => $type->id,
                'value' => $type->name
            ];
        }

        response(['status' => 'success', 'message' => 'Fetch leave types success', 'list' => $list]);
    }

    public function deleteLeave() {
        $params = getParam();
        $leave = $this->Hr->getDataById('kifest_leaves_temp', $params['id']);

        if(!$leave) {
            response([
                'status' => 'error',
                'message' => 'Cuti tidak ditemukan'
            ]);
        } else {
            if($leave->filename != '') {
                if(file_exists("./assets/leaves/$leave->filename")) {
                    unlink("./assets/leaves/$leave->filename");
                }
            }

            $this->Hr->deleteById('kifest_leaves_temp', $params['id']);
            response(['status' => 'success', 'message' => 'Hapus cuti berhasil']);
        }
    }

    public function createLeave() {
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $user = $this->Hr->getDataById('employees', $payload['empId']);
        $user = $this->HrModel->getEmployeeById($user->id);

        $dateStart = date('Y-m-d', strtotime($this->input->post('date_start')));
        $dateFinish = date('Y-m-d', strtotime($this->input->post('date_finish')));
        $leaveName = $this->input->post('leave_name');
        $reason = $this->input->post('reason');

        $isExist = $this->HrModel->checkLeaveExist($dateStart, $dateFinish, $user->nip);
        if($isExist) {
            response([
                'status' => 400,
                'message' => 'Data cuti sudah ada'
            ]);
        } else {
            $code = "LV/" . $user->location . "/$user->id-" . date("d", strtotime($dateStart)) . "/" . toRomawi(date('m', strtotime($dateStart))) . "/" . date('Y', strtotime($dateStart));
            $data = [
                'employee_npp' => $user->nip,
                'emp_id' => $user->id,
                'employee_leave_type_name' => $leaveName,
                'code' => $code,
                'date_start' => date('Y-m-d', strtotime($dateStart)),
                'date_finish' => date('Y-m-d', strtotime($dateFinish)),
                'description' => $reason,
                'approver_nip_1' => "",
                'approver_name_1' => "",
                'approved_at_1' => "",
                'rejected_at_1' => "",
                'created_at' => date('Y-m-d H:i:s'),
                'source_id' => 1,
                'status' => 'PROCESS'
            ];

            if($user->rank_id >= 5) {
                $isHaveAsman = $this->Hr->getOne('employees', ['sub_department_id' => $user->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                $isHaveAsmanPLT = $this->Hr->getOne('employee_ranks', ['sub_department_id' => $user->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                
                if ($user->sub_department_id == 0 || (!$isHaveAsman && !$isHaveAsmanPLT)) {
                    $data['approver_nip_1'] = $user->nip;
                    $data['approver_name_1'] = $user->employee_name;
                    $data['approved_at_1'] = date('Y-m-d H:i:s');
                    $data['rejected_at_1'] = '0000-00-00 00:00:00';
                    $data['status'] = "APPROVED";
                    
                    $upload = $this->uploadFile($_FILES, $user);
                    
                    if($upload['status'] == "success") {
                        $data['filename'] = $upload['filename'];
                        $leaveId = $this->Hr->create('kifest_leaves_temp', $data);
                        $this->leaveByPassEmail($this->Hr->getDataById('kifest_leaves_temp', $leaveId));
                    } else {
                        response([
                            'status' => 400,
                            'message' => $upload['message']
                        ]);
                    }
                } else {
                    $asman = null;

                    if ($isHaveAsman) {
                        $asman = $isHaveAsman;
                    } else if ($isHaveAsmanPLT) {
                        $asman = $this->Hr->getDataById('employees', $isHaveAsmanPLT->emp_id);
                    }

                    if($asman) {
                        $data['approver_nip_1'] = $asman->nip;
                        $data['approver_name_1'] = $asman->employee_name;

                        $upload = $this->uploadFile($_FILES, $user);
                    
                        if($upload['status'] == "success") {
                            $data['filename'] = $upload['filename'];
                            $leaveId = $this->Hr->create('kifest_leaves_temp', $data);
                        } else {
                            response([
                                'status' => 400,
                                'message' => $upload['message']
                            ]);
                        }
                      
                        $emailToken = time()."_LEAVE_REQUEST_".$asman->id;
                        $tokenApprove = simpleEncrypt("$leaveId:APPROVED:$emailToken");
                        $tokenReject = simpleEncrypt("$leaveId:REJECTED:$emailToken");
                        $linkApprove = LIVE_URL . "index.php?c=PublicController&m=approveLeave&token=$tokenApprove";
                        $linkReject = LIVE_URL . "index.php?c=PublicController&m=approveLeave&token=$tokenReject";

                        $linkApprove = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=positive&token=" . simpleEncrypt($linkApprove);
                        $linkReject = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=negative&token=" . simpleEncrypt($linkReject);

                        $message = $this->load->view('html/absen/email/leave_request', [
                            'emp' => $user,
                            'leave' => $data,
                            'asman' => $asman,
                            'linkApprove' => $linkApprove,
                            'linkReject' => $linkReject,
                        ], true);

                        $emailData = [
                            'alert_name' => 'LEAVE_REQUEST',
                            'email_to' => $asman->email,
                            'subject' => "Approval Cuti $user->employee_name pada tanggal " . toIndoDateDay($dateStart) . " sampai " . toIndoDateDay($dateFinish),
                            'subject_name' => "Spekta Alert: Approval Cuti $user->employee_name pada tanggal " . toIndoDateDay($dateStart) . " sampai " . toIndoDateDay($dateFinish),
                            'message' => $message,
                            'emp_direct_apv' => $asman->id,
                            'token' => $emailToken,
                            'positive' => $linkApprove,
                            'negative' => $linkReject
                        ];

                        $this->Main->create('email', $emailData);
                    }
                }
            } else {
                $data['approver_nip_1'] = $user->nip;
                $data['approver_name_1'] = $user->employee_name;
                $data['approved_at_1'] = date('Y-m-d H:i:s');
                $data['rejected_at_1'] = '0000-00-00 00:00:00';
                $data['status'] = "APPROVED";

                $upload = $this->uploadFile($_FILES, $user);
                    
                if($upload['status'] == "success") {
                    $data['filename'] = $upload['filename'];
                    $leaveId = $this->Hr->create('kifest_leaves_temp', $data);
                    $this->leaveByPassEmail($this->Hr->getDataById('kifest_leaves_temp', $leaveId));
                } else {
                    response([
                        'status' => 400,
                        'message' => $upload['message']
                    ]);
                }
            }

            response([
                'status' => 200,
                'message' => 'Pengajuan cuti berhasil dikirim'
            ]);
        }
    }

    public function uploadFile($files, $user) {
        if(isset($files['image']['name'])) {
            $image = $files['image']['name'];
            $extension = pathinfo($image, PATHINFO_EXTENSION);
            $filename = time() . '_' . $user->nip . '.' . $extension;
            $config['upload_path']          = './assets/leaves';
            $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
            $config['file_name']            = $filename;
            $config['overwrite']            = true;
            $config['max_size']             = 1000;
            
            $this->load->library('upload', $config);

            $upload = $this->upload->do_upload('image');

            if($upload) {
                return [
                    'status' => 'success',
                    'filename' => $filename
                ];
            } else {
                return [
                    'status' => 'error',
                    'message' => "Gagal mengupload file"
                ];
            }
        } else {
            return [
                'status' => 'error',
                'message' => "File tidak boleh kosong"
            ];
        }
    }

    public function leaveByPassEmail($leave) {
        $emp = $this->HrModel->getEmployeeById($leave->emp_id);
        $message = $this->load->view('html/absen/email/leave_request_hr', [
            'emp' => $emp,
            'leave' => $leave,
        ], true);

        $sdmEmails = "";
        $sdms = $this->Hr->getWhere('employees', ['sub_department_id' => 11])->result();
        foreach ($sdms as $sdm) {
            if($sdm->rank_id >= 4) {
                if($sdm->email != "" && validateEmail($sdm->email)) {
                    $sdmEmails .= $sdmEmails == "" ? $sdm->email : ",$sdm->email"; 
                }
            }
        }

        $emailData = [
            'alert_name' => 'LEAVE_REQUEST_HR',
            'email_to' => $sdmEmails,
            'subject' => "Approval Cuti $emp->employee_name pada tanggal " . toIndoDateDay($leave->date_start) . " sampai " . toIndoDateDay($leave->date_finish),
            'subject_name' => "Spekta Alert: Approval Cuti $emp->employee_name pada tanggal " . toIndoDateDay($leave->date_start) . " sampai " . toIndoDateDay($leave->date_finish),
            'message' => $message,
        ];

        $this->Main->create('email', $emailData);
    }

    public function getLeaves()
    {
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $user = $this->Hr->getDataById('employees', $payload['empId']);

        $year = $this->input->post('year');
        $month = $this->input->post('month');

        $params = [
            'year' => $year,
            'month' => $month,
            'employee_npp' => $user->nip
        ];

        $leaves = $this->HrModel->getLeaves($params, $user)->result();
        $list = [];

        foreach ($leaves as $leave) {
            $list[] = [
                'id' => $leave->id,
                'employee_name' => $leave->employee_name,
                'department_name' => $leave->department_name,
                'sub_department_name' => $leave->sub_department_name,
                'division_name' => $leave->division_name,
                'date_start' => toIndoDateDay($leave->date_start),
                'date_finish' => toIndoDateDay($leave->date_finish),
                'employee_leave_type_name' => $leave->employee_leave_type_name,
                'code' => $leave->code,
                'description' => $leave->description,
                'approver_name_1' => $leave->approver_name_1 != '' ? $leave->approver_name_1 : "-",
                'approver_name_2' => $leave->approver_name_2 ?? "-",
                'status' => $leave->status,
                'employee_name' => $leave->filename,
                'source' => $leave->source_id == 0 ? "Kifest" : "Spekta",
                'image_url' => $leave->filename != "" ? IMG_URL . "assets/leaves/$leave->filename" : "",
                'created_at' => toIndoDateDay(date('Y-m-d', strtotime($leave->created_at))),
                'filename' => $leave->filename,
            ];
        }

        response(['status' => 'success', 'message' => 'Fetch leaves list success', 'list' => $list]);
    }
}