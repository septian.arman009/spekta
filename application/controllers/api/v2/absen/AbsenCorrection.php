<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class AbsenCorrection extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers('authorization'));

        $this->load->model("api/AbsenModel", 'AbsenModel');
        $this->AbsenModel->myConstruct('hr');

        $this->load->model("HrModel");
        $this->HrModel->myConstruct('hr');

        $this->load->model("api/PaginationModel", 'PaginationModel');
        $this->PaginationModel->myConstruct('hr');
    }

    public function absenCorrections() {
        $params = getParam();
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $emp = $this->Hr->getDataById('employees', $payload['empId']);

        $absenTable = 'absen_' . date('Ym', strtotime($params['date']));
        unset($params['date']);

        $options = [
            'fields' => [
                'id',
                'abs_date', 'date_in', 'date_out', 'sch_date_in', 'sch_date_out', 
                'correction_status', 'shift', 'direct_approval_status', 'hr_approval_status', 'reject_reason',
                'employee_name', 'direct_name', 'hr_name', 'created_at', 'correction_shift',
                'correction_work_start', 'correction_work_end', 'correction_start_time', 'correction_end_time', 'shift_id',
                'reason', 'filename'
            ],
            'global_search_fields' => [
                'abs_date' => 'a.abs_date',
                'date_in' => 'a.date_in',
                'date_out' => 'a.date_out',
                'sch_date_in' => 'a.sch_date_in',
                'sch_date_out' => 'a.sch_date_out',
                'correction_status' => 'a.correction_status',
                'shift' => 'a.shift',
                'direct_approval_status' => 'ac.direct_approval_status',
                'hr_approval_status' => 'ac.hr_approval_status',
                'reject_reason' => 'ac.reject_reason',
                'employee_name' => 'emp.employee_name',
                'direct_name' => 'direct.direct_name',
                'hr_name' => 'hr.hr_name',
                'created_at' => 'ac.created_at',
                'correction_shift' => 'wt.correction_shift',
                'correction_work_start' => 'wt.correction_work_start',
                'correction_work_end' => 'wt.correction_work_end',
                'correction_start_time' => 'ac.correction_start_time',
                'correction_end_time' => 'ac.correction_end_time',
                'shift_id' => 'ac.shift_id',
                'reason' => 'ac.reason',
                'filename' => 'ac.filename',
            ],
            'global_filter_sub_fields' => [
                'id' => 'a.id',
                'abs_date' => 'a.abs_date',
                'date_in' => 'a.date_in',
                'date_out' => 'a.date_out',
                'sch_date_in' => 'a.sch_date_in',
                'sch_date_out' => 'a.sch_date_out',
                'correction_status' => 'a.correction_status',
                'shift' => 'a.shift',
                'direct_approval_status' => 'ac.direct_approval_status',
                'hr_approval_status' => 'ac.hr_approval_status',
                'reject_reason' => 'ac.reject_reason',
                'employee_name' => 'emp.employee_name',
                'direct_name' => 'direct.direct_name',
                'hr_name' => 'hr.hr_name',
                'created_at' => 'ac.created_at',
                'correction_shift' => 'wt.correction_shift',
                'correction_work_start' => 'wt.correction_work_start',
                'correction_work_end' => 'wt.correction_work_end',
                'correction_start_time' => 'ac.correction_start_time',
                'correction_end_time' => 'ac.correction_end_time',
                'shift_id' => 'ac.shift_id',
                'reason' => 'ac.reason',
                'filename' => 'ac.filename',
            ],
            'queries' => "
                select #select# from(
                    select 
                    a.id,
                    a.location,
                    a.emp_id,
                    a.gate,
                    a.distance, a.lat, a.lng,
                    a.distance_out, a.lat_out, a.lng_out,
                    a.qr_code_in, a.qr_code_out,
                    a.abs_date, a.date_in, a.date_out, a.sch_date_in, a.sch_date_out,
                    a.correction_status,
                    a.shift,
                    ac.direct_approval_id, ac.direct_approval_time, ac.direct_approval_status,
                    ac.hr_approval_id, ac.hr_approval_time, ac.hr_approval_status,
                    ac.filename, ac.id as correction_id, ac.reason, ac.reject_reason,
                    ac.correction_start_time, ac.correction_end_time, ac.shift_id,
                    emp.employee_name as employee_name,
                    direct.employee_name as direct_name,
                    hr.employee_name as hr_name,
                    cd.employee_name as created_name,
                    ud.employee_name as updated_name,
                    dept.name as department_name,
                    sub.name as sub_department_name,
                    dive.name as division_name,
                    ac.created_by,
                    ac.updated_by,
                    ac.created_at,
                    ac.updated_at,
                    wt.name as correction_shift,
                    wt.work_start as correction_work_start,
                    wt.work_end as correction_work_end
                    from $absenTable a 
                    inner join absen_correction ac on ac.absen_id = a.id and ac.absen_table = '$absenTable'
                    inner join employees emp on emp.id = a.emp_id
                    inner join employees direct on direct.id = ac.direct_approval_id
                    left join employees hr on hr.id = ac.hr_approval_id
                    inner join employees cd on cd.id = a.created_by
                    inner join employees ud on ud.id = a.updated_by
                    inner join departments dept on dept.id = emp.department_id
                    inner join sub_departments sub on sub.id = emp.sub_department_id
                    inner join divisions dive on dive.id = emp.division_id
                    left join work_time wt on wt.id = ac.shift_id 
                    where 
                        a.location = '$emp->location' and 
                        a.emp_id = $emp->id
                    #where#
                ) as foo
                #order# #perpage# #offset#
            ",
            'startWhere' => 'AND'
        ];

        $data = $this->PaginationModel->paginate($options, $params);

        foreach ($data['data'] as $correction) {

            $dateIn = date('Y-m-d', strtotime($correction->sch_date_in));
            $dateOut = date('Y-m-d', strtotime($correction->sch_date_in));
            $correction->sch_date_in = date('H:i', strtotime($correction->sch_date_in));
            if($dateIn != $dateOut) {
                $correction->sch_date_out = toIndoDateTime5($correction->sch_date_out);
            } else {
                $correction->sch_date_out = date('H:i', strtotime($correction->sch_date_out));
            }

            $correctionDateIn = "-";
            if($correction->correction_start_time) {
                $correctionDateIn = date('Y-m-d', strtotime($correction->correction_start_time));
                $correction->correction_start_time = date('H:i', strtotime($correction->correction_start_time));
            } else {
                $correction->correction_start_time = "-";
            }


            if($correction->correction_end_time) {
                $correctionDateOut = date('Y-m-d', strtotime($correction->correction_end_time));
                if($correctionDateIn != "-" && $correctionDateIn != $correctionDateOut) {
                    $correction->correction_end_time = toIndoDateTime5($correction->correction_end_time);
                } else {
                    $correction->correction_end_time = date('H:i', strtotime($correction->correction_end_time));
                }
            } else {
                $correction->correction_end_time = "-";
            }

            $correction->created_at = toIndoDateTime5($correction->created_at);

            $status = "ON PROCESS";
            if($correction->correction_status == 2) {
                $status = 'APPROVED';
            } else if($correction->correction_status == 3) {
                $status = 'COMPLETED';
            } else if($correction->correction_status == 4) {
                $status = 'REJECTED';
            } else if($correction->correction_status == 5) {
                $status = 'CANCELED';
            }

            $correction->correction_status = $status;

            if($correction->shift_id > 0) {
                $workStart = date('Y-m-d H:i:s', strtotime($correction->abs_date.' '.$correction->correction_work_start.':00'));
                $workEnd = date('Y-m-d H:i:s', strtotime($correction->abs_date.' '.$correction->correction_work_end.':00'));

                if($workEnd < $workStart) {
                    $workEnd = date('Y-m-d H:i:s', strtotime($correction->abs_date.' '.$correction->correction_work_end.':00  + 1 days'));
                }

                $toDateIn = date('Y-m-d', strtotime($workStart));
                $toDateOut = date('Y-m-d', strtotime($workEnd));
    
                $correction->correction_work_start = toIndoDateTime5($workStart);
                $correction->correction_work_end = toIndoDateTime5($workEnd);

                $correction->correction_work_start = date('H:i', strtotime($workStart));
                if($toDateIn != $toDateOut) {
                    $correction->correction_work_end = toIndoDateTime5($workEnd);
                } else {
                    $correction->correction_work_end = date('H:i', strtotime($workEnd));
                }
            }

            $correction->abs_date = toIndoDateDay($correction->abs_date);
            $correction->image_url = $correction->filename != "" ? IMG_URL . "assets/hr_correction_upload/$correction->filename" : "";
        }

        response([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function getAbsenForCorrection() {
        $params = getParam();
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $emp = $this->Hr->getDataById('employees', $payload['empId']);

        $absenTable = 'absen_' . date('Ym', strtotime($params['date']));
        unset($params['date']);

        $options = [
            'fields' => [
                'id', 'abs_date', 'shift', 'date_in', 'date_out', 'sch_date_in', 'sch_date_out', 'correction_status'
            ],
            'global_search_fields' => [
                'abs_date' => 'a.abs_date',
                'shift' => 'a.shift',
                'date_in' => 'a.date_in',
                'date_out' => 'a.date_out',
                'sch_date_in' => 'a.sch_date_in',
                'sch_date_out' => 'a.sch_date_out',
                'correction_status' => 'a.correction_status'
            ],
            'global_filter_sub_fields' => [
                'id' => 'a.id',
                'abs_date' => 'a.abs_date',
                'shift' => 'a.shift',
                'date_in' => 'a.date_in',
                'date_out' => 'a.date_out',
                'sch_date_in' => 'a.sch_date_in',
                'sch_date_out' => 'a.sch_date_out',
                'correction_status' => 'a.correction_status'
            ],
            'queries' => "
                select #select# from(
                    SELECT a.id,a.location, a.emp_id, a.gate, a.distance, a.distance_out,a.abs_date, a.sch_date_in, a.sch_date_out, a.date_in, a.date_out,
                            a.qr_code_in, a.qr_code_out, a.shift, a.correction_status, a.on_leave, a.visit_id, a.shift_id,
                            b.employee_name,
                            c.name AS department_name,
                            c.cost_center AS dept_cost_center,
                            d.name AS sub_department_name,
                            d.cost_center AS sub_cost_center,
                            e.name AS division_name,
                            e.cost_center AS div_cost_center,
                            r.cost_center AS rank_cost_center,
                            r.name AS rank_name,
                            ovt.payment_status,
                            ovt.start_date as ovt_start_date,
                            ovt.end_date as ovt_end_date,
                            ovt.total_meal as ovt_meal,
                            kl.employee_leave_type_name as leave_name,
                            c2.take_date as take_date_1,
                            c3.take_date as take_date_2,
                            wt.meal
                        FROM $absenTable a
                        INNER JOIN employees b ON a.emp_id = b.id
                        LEFT JOIN departments c ON b.department_id = c.id 
                        LEFT JOIN sub_departments d ON b.sub_department_id = d.id 
                        LEFT JOIN divisions e ON b.division_id = e.id
                        INNER JOIN ranks r ON r.id = b.rank_id
                        LEFT JOIN employee_overtimes_detail ovt ON ovt.emp_id = a.emp_id AND ovt.overtime_date = a.abs_date AND ovt.status not in('CANCELED', 'REJECTED')
                        LEFT JOIN kifest_leaves kl on (kl.date_start = a.abs_date or kl.date_finish = a.abs_date) and kl.emp_id = a.emp_id  
                        LEFT JOIN kf_general.canteen c2 on date(c2.take_date) = a.abs_date and c2.emp_id = a.emp_id
                        LEFT JOIN kf_general.canteen_overtime c3 on date(c3.take_date) = a.abs_date and c3.emp_id = a.emp_id
                        LEFT JOIN kf_hr.work_time wt on a.shift_id = wt.id 
                        WHERE 
                            a.location = '$emp->location' and
                            a.emp_id = $emp->id and
                            a.correction_status in(0,1,4,5) and 
                            a.visit_id = 0 and
                            a.on_leave = 0
                        GROUP BY a.id, a.abs_date
                        ORDER BY a.abs_date DESC
                    #where#
                ) as foo
                #order# #perpage# #offset#
            ",
            'startWhere' => 'AND'
        ];

        $data = $this->PaginationModel->paginate($options, $params);

        $origins = [];
        foreach ($data['data'] as $absen) {
            $origins[] = [
                'id' => $absen->id,
                'abs_date' => $absen->abs_date,
                'date_in' => $absen->date_in ?? "",
                'date_out' => $absen->date_out ?? "",
                'sch_date_in' => $absen->sch_date_in,
                'sch_date_out' => $absen->sch_date_out,
            ];
        }

        $datas = [];
        foreach ($data['data'] as $absen) {
            $dateIn = date('Y-m-d', strtotime($absen->sch_date_in));
            $dateOut = date('Y-m-d', strtotime($absen->sch_date_in));
            $absen->sch_date_in = date('H:i', strtotime($absen->sch_date_in));
            if($dateIn != $dateOut) {
                $absen->sch_date_out = toIndoDateTime($absen->sch_date_out);
            } else {
                $absen->sch_date_out = date('H:i', strtotime($absen->sch_date_out));
            }

            $absen->abs_date = toIndoDate2($absen->abs_date);

            if($absen->date_out && $absen->date_out != '') {
                $in = date('Y-m-d', strtotime($absen->date_in));
                $out = date('Y-m-d', strtotime($absen->date_out));

                if($in != $out) {
                    $absen->date_out = '('.toIndoDateTime3($absen->date_out).')';
                } else {
                    $absen->date_out = date('H:i', strtotime($absen->date_out));
                }
            } else {
                $absen->date_out = "(-)";
            }

            if($absen->date_in && $absen->date_in != '') {
                $absen->date_in = date('H:i', strtotime($absen->date_in));
            } else {
                $absen->date_in = "(-)";
            }

            $datas[] = [
                'id' => $absen->id,
                'value' => "$absen->abs_date ($absen->shift [$absen->sch_date_in-$absen->sch_date_out]/[$absen->date_in-$absen->date_out])"
            ]; 
        }

        $data['data'] = $datas;
        $data['origins'] = $origins;

        response([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function getShiftList() {
        $params = getParam();
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $emp = $this->Hr->getDataById('employees', $payload['empId']);

        $options = [
            'fields' => [
                'id', 'name', 'department_id', 'sub_department_id', 'division_id',
                'work_start', 'work_end', 'sub_department', 
            ],
            'global_search_fields' => [
                'name' => 'a.name',
                'department_id' => 'a.department_id',
                'sub_department_id' => 'a.sub_department_id',
                'division_id' => 'a.division_id',
                'work_start' => 'a.work_start',
                'work_end' => 'a.work_end',
                'sub_department' => 'c.name',
            ],
            'global_filter_sub_fields' => [
                'id' => 'a.id',
                'name' => 'a.name',
                'department_id' => 'a.department_id',
                'sub_department_id' => 'a.sub_department_id',
                'division_id' => 'a.division_id',
                'work_start' => 'a.work_start',
                'work_end' => 'a.work_end',
                'sub_department' => 'c.name',
            ],
            'queries' => "
                select #select# from(
                    SELECT 
                        a.*, 
                        b.name AS department,
                        c.name AS sub_department,
                        d.name AS division,
                        e.employee_name AS emp1,
                        f.employee_name AS emp2
                    FROM work_time a
                    LEFT JOIN departments b ON a.department_id = b.id
                    LEFT JOIN sub_departments c ON a.sub_department_id = c.id
                    LEFT JOIN divisions d ON a.division_id = d.id
                    LEFT JOIN employees e ON a.created_by = e.id
                    LEFT JOIN employees f ON a.updated_by = f.id
                    WHERE 
                        ((a.department_id = 0 AND a.sub_department_id = 0 AND a.division_id = 0) OR
                        (a.department_id = $emp->department_id AND a.sub_department_id = $emp->sub_department_id))
                    AND a.location = '$emp->location'
                    #where#
                ) as foo
                #order# #perpage# #offset#
            ",
            'startWhere' => 'AND'
        ];

        $data = $this->PaginationModel->paginate($options, $params);

        $datas = [];

        foreach ($data['data'] as $shift) {
            $datas[] = [
                'id' => $shift->id,
                'value' => $shift->department_id == 0 
                    ? $shift->name . " - Reguler ($shift->work_start - $shift->work_end)" 
                    : $shift->name . " - $shift->sub_department ($shift->work_start - $shift->work_end)"
            ]; 
        }

        $data['data'] = $datas;

        response([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function createCorrection() {
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $user = $this->Hr->getDataById('employees', $payload['empId']);
        $user = $this->HrModel->getEmployeeById($user->id);

        $absenData = [];
        $emailData = [];
        $updateAbsen = [];
        
        $absenId = $this->input->post('absen_id');
        $shiftId = $this->input->post('shift_id');
        $startTime = $this->input->post('start_time');
        $endTime = $this->input->post('end_time');
        $reason = $this->input->post('reason');

        $startHour = date('H:i', strtotime($startTime));
        $endHour = date('H:i', strtotime($endTime));

        $absenTable = "absen_" . date('Ym', strtotime($startTime));

        $absen = $this->AbsenModel->getAbsensWithEmp($absenTable, $absenId);

        if($startHour == "00:00" && $endHour == "00:00") {
            response([
                'status' => 400,
                'message' => "Koreksi absen gagal, tidak ada data untuk dikoreksi"
            ]);
        }

        if($startHour != "00:00") {
            $correctionStartTime = date('Y-m-d H:i:s', strtotime($startTime));
        } else {
            $correctionStartTime = $absen->date_in ? $absen->date_in : '-';
        }

        if($endHour != "00:00") {
            $correctionEndTime = date('Y-m-d H:i:s', strtotime($endTime));
        } else {
            $correctionEndTime = $absen->date_out ? $absen->date_out : '-';
        }

        $data = [
            'shift_id' => $shiftId,
            'absen_table' => $absenTable,
            'absen_id' => $absenId,
            'reason' => $reason,
            'created_by' => $user->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_by' => $user->id,
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $upload = $this->uploadFile($_FILES, $user);
        
        if($upload['status'] == "success") {
            $data['filename'] = $upload['filename'];

            if($correctionStartTime != '-') { 
                $data['correction_start_time'] = $correctionStartTime;
                $absen->correction_start_time = $correctionStartTime;
            }
            if($correctionEndTime != '-') {
                $data['correction_end_time'] = $correctionEndTime;
                $absen->correction_end_time = $correctionEndTime;
            }

            $absen->reason = $reason;
            
            if($absen->rank_id >= 5) {
                $isHaveAsman = $this->Hr->getOne('employees', ['sub_department_id' => $absen->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                $isHaveAsmanPLT = $this->Hr->getOne('employee_ranks', ['sub_department_id' => $absen->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                
                if ($absen->sub_department_id == 0 || (!$isHaveAsman && !$isHaveAsmanPLT)) {
                    $updateAbsen[] = [
                        'id' => $absen->id,
                        'correction_status' => 2,
                        'updated_by' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s')
                    ];

                    $data['direct_approval_id'] = $absen->emp_id;
                    $data['direct_approval_time'] = date('Y-m-d H:i:s');
                    $data['direct_approval_status'] = "APPROVED";

                    $this->correctionByPassEmail($absen);
                } else {
                    $updateAbsen[] = [
                        'id' => $absen->id,
                        'correction_status' => 1,
                        'updated_by' => $user->id,
                        'updated_at' => date('Y-m-d H:i:s')
                    ];

                    $asman = null;

                    if ($isHaveAsman) {
                        $asman = $isHaveAsman;
                    } else if ($isHaveAsmanPLT) {
                        $asman = $this->Hr->getDataById('employees', $isHaveAsmanPLT->emp_id);
                    }

                    if($asman) {
                        $data['direct_approval_id'] = $asman->id;
                        $emailToken = time().'_ABSEN_CORRECTION_'.$asman->id;
                        $tokenApprove = simpleEncrypt("$absenTable:$absen->id:APPROVED:$emailToken");
                        $tokenReject = simpleEncrypt("$absenTable:$absen->id:REJECTED:$emailToken");
                        $linkApprove = LIVE_URL . "index.php?c=PublicController&m=approveAbsenCorrection&token=$tokenApprove";
                        $linkReject = LIVE_URL . "index.php?c=PublicController&m=approveAbsenCorrection&token=$tokenReject";

                        $linkApprove = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=positive&token=" . simpleEncrypt($linkApprove);
                        $linkReject = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=negative&token=" . simpleEncrypt($linkReject);
                        
                        $message = $this->load->view('html/absen/email/approve_correction', [
                            'absen' => $absen,
                            'asman' => $asman,
                            'linkApprove' => $linkApprove,
                            'linkReject' => $linkReject,
                        ], true);

                        $emailData[] = [
                            'alert_name' => 'ABSEN_CORRECTION',
                            'email_to' => $asman->email,
                            'subject' => "Approval Koreksi Absen $absen->employee_name pada tanggal " . toIndoDateDay($absen->abs_date),
                            'subject_name' => "Spekta Alert: Approval Koreksi Absen $absen->employee_name pada tanggal " . toIndoDateDay($absen->abs_date),
                            'message' => $message,
                            'emp_direct_apv' => $asman->id,
                            'token' => $emailToken,
                            'positive' => $linkApprove,
                            'negative' => $linkReject
                        ];
                    }
                }
            } else {
                $updateAbsen[] = [
                    'id' => $absen->id,
                    'correction_status' => 2,
                    'updated_by' => $user->id,
                    'updated_at' => date('Y-m-d H:i:s')
                ];

                $data['direct_approval_id'] = $absen->emp_id;
                $data['direct_approval_time'] = date('Y-m-d H:i:s');
                $data['direct_approval_status'] = "APPROVED";

                $this->correctionByPassEmail($absen);
            }

            $this->Hr->delete('absen_correction', ['absen_id' => $absen->id, 'absen_table' => $absenTable]);
            $absenData[] = $data;

            if(count($absenData) > 0) $this->Hr->createMultiple('absen_correction', $absenData);
            if(count($updateAbsen) > 0) $this->Hr->updateMultiple($absenTable, $updateAbsen, 'id');
            if(count($emailData) > 0) $this->Main->createMultiple('email', $emailData);

            response([
                'status' => 200,
                'message' => "Koreksi absen berhasil dikirim"
            ]);
        } else {
            response([
                'status' => 400,
                'message' => $upload['message']
            ]);
        }
    }

    public function correctionByPassEmail($absen) {
        $message = $this->load->view('html/absen/email/approve_correction_hr', [
            'absen' => $absen,
        ], true);

        $sdmEmails = "";
        $sdms = $this->Hr->getWhere('employees', ['sub_department_id' => 11])->result();
        foreach ($sdms as $sdm) {
            if($sdm->rank_id >= 4) {
                if($sdm->email != "" && validateEmail($sdm->email)) {
                    $sdmEmails .= $sdmEmails == "" ? $sdm->email : ",$sdm->email"; 
                }
            }
        }

        $emailData = [
            'alert_name' => 'ABSEN_CORRECTION_HR',
            'email_to' => $sdmEmails,
            'subject' => "Approval Koreksi Absen $absen->employee_name pada tanggal " . toIndoDateDay($absen->abs_date),
            'subject_name' => "Spekta Alert: Approval Koreksi Absen $absen->employee_name pada tanggal " . toIndoDateDay($absen->abs_date),
            'message' => $message,
        ];

        $this->Main->create('email', $emailData);
    }

    public function uploadFile($files, $user) {
        if(isset($files['image']['name'])) {
            $image = $files['image']['name'];
            $extension = pathinfo($image, PATHINFO_EXTENSION);
            $filename = time() . '_' . $user->nip . '.' . $extension;
            $config['upload_path']          = './assets/hr_correction_upload';
            $config['allowed_types']        = 'jpg|jpeg|png|JPG|JPEG|PNG';
            $config['file_name']            = $filename;
            $config['overwrite']            = true;
            $config['max_size']             = 1000;
            
            $this->load->library('upload', $config);

            $upload = $this->upload->do_upload('image');

            if($upload) {
                return [
                    'status' => 'success',
                    'filename' => $filename
                ];
            } else {
                return [
                    'status' => 'error',
                    'message' => "Gagal mengupload file"
                ];
            }
        } else {
            return [
                'status' => 'error',
                'message' => "File tidak boleh kosong"
            ];
        }
    }

    public function cancelCorrection() {
        $payload = $this->jwt->me($this->input->request_headers('authorization'));

        $params = getParam();
        $absenId = $params['absen_id'];
        $date = $params['date'];

        $absenTable = "absen_" . date('Ym', strtotime($date));

        $absen = $this->AbsenModel->getAbsenCorrectionWithEmp($absenTable, $absenId);

        if($absen->correction_status == 1 || $absen->correction_status == 2) {
            if($absen->filename != '') {
                if(file_exists("./assets/hr_correction_upload/$absen->filename")) {
                    unlink("./assets/hr_correction_upload/$absen->filename");
                }
            }
            
            $this->Hr->update($absenTable, [
                'correction_status' => 5, 
                'updated_by' => $payload['empId'], 
                'updated_at' => date('Y-m-d H:i:s')
            ], ['id' => $absen->id]);

            $this->Hr->update('absen_correction', [
                'reject_reason' => 'Cancel by user',
                'updated_by' => $payload['empId'], 
                'updated_at' => date('Y-m-d H:i:s')
            ], [
                'absen_table' => $absenTable, 
                'absen_id' => $absen->id
            ]);

            response([
                'status' => 200,
                'message' => 'Koreksi absen berhasil dibatalkan'
            ]);
        } else {
            response([
                'status' => 400,
                'message' => 'Gagal membatalkan koreksi absen'
            ]);
        }
    }
}