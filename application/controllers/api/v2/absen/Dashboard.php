<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class Dashboard extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers('authorization'));
    }

    public function getSummaryByMonth()
    {
        $param = getParam();
        $inputedDate = $param['date'];
        $month = date('m', strtotime($inputedDate));
        $year = date('Y', strtotime($inputedDate));
        $absenTable = 'absen_' . date('Ym', strtotime($inputedDate));

        $user = $this->jwt->me($this->input->request_headers('authorization'));
        
        $totalDayInMonth = cal_days_in_month(CAL_GREGORIAN, intval($month), intval($year));

        $checkTable = $this->Hr->checkTable('kf_hr', $absenTable);
        if(!$checkTable) $this->Hr->createAbsenTable($absenTable);
        
        $absens = $this->Hr->getLike($absenTable, ['emp_id' => $user['empId'], 'shift !=' => 'OFF'], ['abs_date' => date('Y-m', strtotime($inputedDate))])->result();
        $absenList = [];
        foreach ($absens as $abs) {
            $absenList[$abs->abs_date] = $abs;
        }

        $in = 0;
        $notIn = 0;
        $late = 0;
        $earlyOut = 0;
        $totalWorkDay = 0;
        $weekEndDay = 0;
        $nationalDay = 0;
        $totalLateTime = 0;
        $totalEarlyOutTime = 0;
        $notCount = 0;

        for ($i=1; $i <= $totalDayInMonth; $i++) { 
            $day = $i < 10 ? "0$i" : $i;
            $date = date('Y-m-d', strtotime("$year-$month-$day"));

            $weekend = checkWeekend($date);
            $national = checkNationalDay($date);

            if($weekend) $weekEndDay++;
            if($national) $nationalDay++;

            if(!$weekend && !$national) {
                $totalWorkDay++;
                if(array_key_exists($date, $absenList)) {
                    if($absenList[$date]->date_in || $absenList[$date]->on_leave == 1) {
                        $in++;
                        if($absenList[$date]->on_leave == 0) {
                            $dateIn = date('Y-m-d H:i:s', strtotime($absenList[$date]->date_in));
                            $dateOut = date('Y-m-d H:i:s', strtotime($absenList[$date]->date_out));
                            $schIn = date('Y-m-d H:i:s', strtotime($absenList[$date]->sch_date_in));
                            $schOut = date('Y-m-d H:i:s', strtotime($absenList[$date]->sch_date_out));
                            
                            if($absenList[$date]->date_in && $dateIn > $schIn) {
                                $late++;
                                $totalLateTime += countHour($schIn, $dateIn, 'h');
                            }
                            
                            if($absenList[$date]->date_out && $dateOut && $dateOut < $schOut) {
                                $earlyOut++;
                                $totalEarlyOutTime += countHour($dateOut, $schOut, 'h');
                            }
                        }
                    } else {
                        if($date <= date('Y-m-d') && $absenList[$date]->shift != 'OFF') $notIn++;
                    }
                } else {
                    $notCount++;
                }
            }
        }

        $data = [
            'late' => $late,
            'earlyOut' => $earlyOut,
            'in' => $in,
            'notIn' => $notIn,
            'totalWorkDay' => $totalWorkDay - $notCount,
            'inPercent' => $in > 0 ? round(($in/($totalWorkDay - $notCount)) * 100) : 0,
            'notInPercent' => $notIn > 0 ? round(($notIn/($totalWorkDay - $notCount)) * 100) : 0,
            'weekEnd' => $weekEndDay,
            'nationalDay' => $nationalDay,
            'totalLateTime' => normalHourMobile($totalLateTime),
            'totalEarlyOutTime' => normalHourMobile($totalEarlyOutTime)
        ];

        response([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function getSummaryByYear()
    {
        $param = getParam();
        $year = $param['year'];

        $user = $this->jwt->me($this->input->request_headers('authorization'));

        $in = 0;
        $notIn = 0;
        $late = 0;
        $earlyOut = 0;
        $totalWorkDay = 0;
        $weekEndDay = 0;
        $nationalDay = 0;
        $totalLateTime = 0;
        $totalEarlyOutTime = 0;
        $notCount = 0;

        for ($i=1; $i <= 12; $i++) { 
            $month = $i < 10 ? "0$i" : $i;
            $absenTable = 'absen_' . date('Ym', strtotime("$year-$month-01"));

            $checkTable = $this->Hr->checkTable('kf_hr', $absenTable);
            if(!$checkTable) $this->Hr->createAbsenTable($absenTable);

            $absens = $this->Hr->getLike($absenTable, ['emp_id' => $user['empId'], 'shift !=' => 'OFF'], ['abs_date' => date('Y-m', strtotime("$year-$month"))])->result();
            $absenList = [];
            foreach ($absens as $abs) {
                $absenList[$abs->abs_date] = $abs;
            }

            $totalDayInMonth = cal_days_in_month(CAL_GREGORIAN, intval($month), intval($year));

            for ($j=1; $j <= $totalDayInMonth; $j++) { 
                $day = $j < 10 ? "0$j" : $j;
                $date = date('Y-m-d', strtotime("$year-$month-$day"));
    
                $weekend = checkWeekend($date);
                $national = checkNationalDay($date);
    
                if($weekend) $weekEndDay++;
                if($national) $nationalDay++;
    
                if(!$weekend && !$national) {
                    $totalWorkDay++;
                    if(array_key_exists($date, $absenList)) {
                        if($absenList[$date]->date_in || $absenList[$date]->on_leave == 1) {
                            $in++;
                            
                            if($absenList[$date]->on_leave == 0) {
                                $dateIn = date('Y-m-d H:i:s', strtotime($absenList[$date]->date_in));
                                $dateOut = date('Y-m-d H:i:s', strtotime($absenList[$date]->date_out));
                                $schIn = date('Y-m-d H:i:s', strtotime($absenList[$date]->sch_date_in));
                                $schOut = date('Y-m-d H:i:s', strtotime($absenList[$date]->sch_date_out));
                
                                if($absenList[$date]->date_in && $dateIn > $schIn) {
                                    $late++;
                                    $totalLateTime += countHour($schIn, $dateIn, 'h');
                                }
                
                                if($absenList[$date]->date_out && $dateOut && $dateOut < $schOut) {
                                    $earlyOut++;
                                    $totalEarlyOutTime += countHour($dateOut, $schOut, 'h');
                                }
                            }
                        } else {
                            if($date <= date('Y-m-d') && $absenList[$date]->shift != 'OFF') $notIn++;
                        }
                    } else {
                        $notCount++;
                    }
                }
            }
        }

        $data = [
            'late' => $late,
            'earlyOut' => $earlyOut,
            'in' => $in,
            'notIn' => $notIn,
            'totalWorkDayPass' => $totalWorkDay - $notCount,
            'totalWorkDay' => $totalWorkDay ,
            'inPercent' => $in > 0 ? round(($in/($totalWorkDay - $notCount)) * 100) : 0,
            'notInPercent' => $notIn > 0 ? round(($notIn/($totalWorkDay - $notCount)) * 100) : 0,
            'weekEnd' => $weekEndDay,
            'nationalDay' => $nationalDay,
            'totalLateTime' => normalHourMobile($totalLateTime),
            'totalEarlyOutTime' => normalHourMobile($totalEarlyOutTime)
        ];

        response([
            'status' => 200,
            'data' => $data
        ]);
    }
}