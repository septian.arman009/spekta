<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class Absen extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers('authorization'));

        $this->load->model("api/AbsenModel", 'AbsenModel');
        $this->AbsenModel->myConstruct('hr');
    }

    public function getAbsen()
    {
        $params = getParam();
        if (!isset($params['date']) && isDate($params['date'])) response(['status' => 400, 'message' => 'Date not set'], 400);

        $user = $this->jwt->me($this->input->request_headers('authorization'));
        $absenTable = 'absen_' . date('Ym', strtotime($params['date']));

        $checkTable = $this->Hr->checkTable('kf_hr', $absenTable);
        if (!$checkTable) $this->Hr->createAbsenTable($absenTable);

        $absen = $this->AbsenModel->getAbsens($params, $absenTable, $user)->row();

        if ($absen) {
            $dateIn = date('Y-m-d H:i:s', strtotime($absen->date_in));
            $dateOut = date('Y-m-d H:i:s', strtotime($absen->date_out));
            $schIn = date('Y-m-d H:i:s', strtotime($absen->sch_date_in));
            $schOut = date('Y-m-d H:i:s', strtotime($absen->sch_date_out));

            $lateTime = '0m';
            if ($dateIn > $schIn) {
                $lateTime = normalHourMobile(countHour($schIn, $dateIn, 'h'));
            }

            $earlyOut = '0m';
            if ($dateOut && $dateOut < $schOut) {
                $earlyOut = normalHourMobile(countHour($dateOut, $schOut, 'h'));
            }

            response([
                'status' => 200,
                'data' => [
                    'employeeName' => $absen->employee_name,
                    'deptName' => $absen->department_name,
                    'subDeptName' => $absen->sub_department_name,
                    'divName' => $absen->division_name,
                    'absDate' => $absen->abs_date,
                    'dateIn' => $absen->date_in,
                    'dateOut' => $absen->date_out,
                    'schDateIn' => $absen->sch_date_in,
                    'schDateOut' => $absen->sch_date_out,
                    'lateTime' => $lateTime,
                    'earlyOut' => $earlyOut,
                    'distance' => $absen->distance . ' Meter',
                    'gate' => $absen->gate,
                    'shift' => $absen->shift,
                    'onLeave' => intval($absen->on_leave),
                    'visitId' => intval($absen->visit_id),
                    'qrIn' => $absen->qr_code_in,
                    'qrOut' => $absen->qr_code_out
                ]
            ]);
        } else {
            response([
                'status' => 400,
                'message' => 'Absen tidak ditemukan'
            ], 400);
        }
    }

    public function getAbsens()
    {
        $params = getParam();
        $filterPresent = $params['filterPresent'];
        $filterDicipline = $params['filterDicipline'];

        if (!isset($params['year']) || !is_numeric($params['year'])) response(['status' => 400, 'message' => 'Year not set'], 400);
        if (!isset($params['month']) || !is_numeric($params['month'])) response(['status' => 400, 'message' => 'Month  not set'], 400);

        $user = $this->jwt->me($this->input->request_headers('authorization'));

        $year = $params['year'];
        if ($params['month'] < 10) {
            $month = str_replace("0", "", $params['month']);
            $month = "0$month";
        } else {
            $month = $params['month'];
        }

        unset($params['year']);
        unset($params['month']);

        $absenTable = 'absen_' . date('Ym', strtotime("$year-$month-01"));

        $checkTable = $this->Hr->checkTable('kf_hr', $absenTable);
        if (!$checkTable) $this->Hr->createAbsenTable($absenTable);

        $absens = $this->AbsenModel->getAbsens($params, $absenTable, $user)->result();
        $absenList = [];
        foreach ($absens as $abs) {
            $absenList[$abs->abs_date] = $abs;
        }

        $totalDayInMonth = cal_days_in_month(CAL_GREGORIAN, intval($month), intval($year));
        $currentIndex = 0;
        $index = 0;

        $list = [];
        for ($j = 1; $j <= $totalDayInMonth; $j++) {
            $day = $j < 10 ? "0$j" : $j;
            $date = date('Y-m-d', strtotime("$year-$month-$day"));

            if ($currentIndex == 0 && $filterPresent == 'all' && $filterDicipline == 'all') {
                if (date('Y-m-d', strtotime($date)) == date('Y-m-d')) {
                    $currentIndex = $index;
                }
            }

            if (array_key_exists($date, $absenList)) {
                $index++;

                $dateIn = date('Y-m-d H:i:s', strtotime($absenList[$date]->date_in));
                $dateOut = date('Y-m-d H:i:s', strtotime($absenList[$date]->date_out));
                $schIn = date('Y-m-d H:i:s', strtotime($absenList[$date]->sch_date_in));
                $schOut = date('Y-m-d H:i:s', strtotime($absenList[$date]->sch_date_out));

                $lateTime = '0m';
                if ($absenList[$date]->date_in && $dateIn > $schIn) {
                    $lateTime = normalHourMobile(countHour($schIn, $dateIn, 'h'));
                }

                $earlyOut = '0m';
                if ($absenList[$date]->date_out && $dateOut < $schOut) {
                    $earlyOut = normalHourMobile(countHour($dateOut, $schOut, 'h'));
                }

                $dayOut = date('H:i', strtotime($absenList[$date]->date_out));
                $dayOutFloat = clockToFloat($dayOut);
                $dateOut = $absenList[$date]->date_out;
                if ($dayOutFloat < 10 && date('Y-m-d', strtotime($absenList[$date]->sch_date_out)) != date('Y-m-d', strtotime($absenList[$date]->date_out))) {
                    $dayOut = toIndoDateTime5(date('Y-m-d H:i', strtotime($dateOut)));
                }

                $schTimeIn = $absenList[$date]->sch_date_in ? date('H:i', strtotime($absenList[$date]->sch_date_in)) : $absenList[$date]->sch_date_in;
                $schTimeOut = $absenList[$date]->sch_date_out ? date('H:i', strtotime($absenList[$date]->sch_date_out)) : $absenList[$date]->sch_date_out;

                if ($schTimeIn == '00:00') $schTimeIn = 'OFF';
                if ($schTimeOut == '00:00') $schTimeOut = 'OFF';

                $show = true;
                if (checkNationalDay($date)) {
                    if ($absenList[$date]->date_in) {
                        $show = true;
                    } else {
                        $show = false;
                    }
                } else {
                    $show = true;
                }

                if ($show) {
                    $list[] = [
                        'absDate' => toIndoDate(date('Y-m-d', strtotime($date))),

                        'dayIn' => $absenList[$date]->date_in ? toIndoDateDay(date('Y-m-d', strtotime($absenList[$date]->date_in))) : "-",
                        'dayOut' => $absenList[$date]->date_out ? toIndoDateDay(date('Y-m-d', strtotime($absenList[$date]->date_out))) : "-",
                        'timeIn' => $absenList[$date]->date_in ? date('H:i', strtotime($absenList[$date]->date_in)) : "-",
                        'timeOut' => $absenList[$date]->date_out ? $dayOut : "-",
                        'dateIn' => $absenList[$date]->date_in ? toIndoDateTime($absenList[$date]->date_in) : "-",
                        'dateOut' => $absenList[$date]->date_out ? toIndoDateTime($dateOut) : "-",

                        'schDayIn' => $absenList[$date]->sch_date_in ? toIndoDateDay(date('Y-m-d', strtotime($absenList[$date]->sch_date_in))) : $absenList[$date]->sch_date_in,
                        'schDayOut' => $absenList[$date]->sch_date_out ? toIndoDateDay(date('Y-m-d', strtotime($absenList[$date]->sch_date_out))) : $absenList[$date]->sch_date_out,
                        'schTimeIn' => $schTimeIn,
                        'schTimeOut' => $schTimeOut,
                        'schDateIn' => $absenList[$date]->sch_date_in ? toIndoDateDay(date('Y-m-d', strtotime($absenList[$date]->sch_date_in))) : $absenList[$date]->sch_date_in,
                        'schDateOut' => $absenList[$date]->sch_date_out ? toIndoDateDay(date('Y-m-d', strtotime($absenList[$date]->sch_date_out))) : $absenList[$date]->sch_date_out,

                        'lateTime' => $lateTime,
                        'earlyOut' => $earlyOut,
                        'gate' => $absenList[$date]->gate,
                        'shift' => $absenList[$date]->shift,
                        'onLeave' => intval($absenList[$date]->on_leave),
                        'visitId' => intval($absenList[$date]->visit_id),
                        'qrIn' => $absenList[$date]->qr_code_in ?: "-",
                        'qrOut' => $absenList[$date]->qr_code_out ?: "-",

                        'distance' => $absenList[$date]->distance ? $absenList[$date]->distance . ' Meter' : "(-) Meter",
                        'lat' => floatval($absenList[$date]->lat),
                        'lng' => floatval($absenList[$date]->lng),
                        'distanceOut' => $absenList[$date]->distance_out ? $absenList[$date]->distance_out . ' Meter' : "(-) Meter",
                        'latOut' => floatval($absenList[$date]->lat_out),
                        'lngOut' => floatval($absenList[$date]->lng_out),
                    ];
                }
            } else {
                if (!checkWeekend($date) && !checkNationalDay($date)) {
                    $index++;
                    $list[] = [
                        'absDate' => toIndoDate(date('Y-m-d', strtotime($date))),

                        'dayIn' => '-',
                        'dayOut' => '-',
                        'timeIn' => '-',
                        'timeOut' => '-',
                        'dateIn' => '-',
                        'dateOut' => '-',

                        'schDayIn' => '-',
                        'schDayOut' => '-',
                        'schTimeIn' => '-',
                        'schTimeOut' => '-',
                        'schDateIn' => '-',
                        'schDateOut' => '-',

                        'lateTime' => '0m',
                        'earlyOut' => '0m',
                        'gate' => '-',
                        'shift' => '-',
                        'onLeave' => 0,
                        'visitId' => 0,
                        'qrIn' => '-',
                        'qrOut' => '-',

                        'distance' => '0 Meter',
                        'lat' => 0,
                        'lng' => 0,
                        'distanceOut' => '0 Meter',
                        'latOut' => 0,
                        'lngOut' => 0
                    ];
                }
            }
        }

        $newList = [];
        foreach ($list as $key => $lt) {
            $tempList = null;

            if ($filterPresent == 'all') {
                $tempList = $lt;
            } else if ($filterPresent == 'in') {
                if ($lt['dayIn'] != '-') $tempList = $lt;
            } else if ($filterPresent == 'notIn') {
                if ($lt['dayIn'] == '-') $tempList = $lt;
            }

            if ($filterDicipline == 'late') {
                $tempList = $lt['lateTime'] != '0m' ? $lt : null;
            } else if ($filterDicipline == 'earlyOut') {
                $tempList = $lt['earlyOut'] != '0m' ? $lt : null;
            }

            if ($tempList) $newList[] = $tempList;
        }

        response([
            'status' => 200,
            'currentIndex' => $currentIndex,
            'total' => count($newList),
            'data' => $newList,
        ]);
    }

    public function scanQRAbsen()
    {
        $body = getInputPost();
        $lat = floatval($body['lat']);
        $lng = floatval($body['lng']);
        $distance = floatval($body['distance']);
        $qrScanned = $body['qr'];
        $type = $body['type'];

        $currentDate = date('Y-m-d');
        $currentDateTime = date('Y-m-d H:i:s');

        $payload = $this->jwt->me($this->input->request_headers('authorization'));

        if ($distance > 500 && $payload['empId'] != 431) {
            response([
                'status' => 400,
                'message' => "Absen masuk gagal, jarak anda terlalu jauh"
            ], 400);
        }

        $checkQr = $this->Hr->getWhereOr('gates', ['token' => $qrScanned, 'before_token' => $qrScanned])->row();
        if ($checkQr) {
            $emp = $this->Hr->getOne('employees', ['id' => $payload['empId']]);

            if ($type == "IN") {
                $absenTable = 'absen_' . date('Ym', strtotime($currentDate));
                $checkSchedule = $this->Hr->getWhere($absenTable, ['emp_id' => $emp->id, 'abs_date' => $currentDate])->row();

                if (!$checkSchedule) {
                    response([
                        'status' => 400,
                        'message' => 'Anda tidak memiliki jadwal kehadiran pada tanggal ' . toIndoDateDay($currentDate)
                    ], 400);
                }

                if ($checkSchedule->shift == 'OFF') {
                    response([
                        'status' => 400,
                        'message' => 'Absen masuk gagal, ' . toIndoDateDay($currentDate) . ' merupakan hari libur untuk anda'
                    ], 400);
                }

                if ($checkSchedule->on_leave == 1) {
                    response([
                        'status' => 'failed',
                        'message' => 'Tidak dapat melakukan absen masuk, status anda sedang cuti'
                    ], 400);
                }

                if ($checkSchedule->qr_code_in != '') {
                    response([
                        'status' => 400,
                        'message' => 'Anda sudah absen masuk pada ' . toIndoDateTime($checkSchedule->date_in)
                    ], 400);
                }

                $data = [
                    'abs_date' => $currentDate,
                    'date_in' => $currentDateTime,
                    'lat' => $lat,
                    'lng' => $lng,
                    'distance' => $distance,
                    'qr_code_in' => $qrScanned,
                    'gate' => $checkQr->gate_name,
                    'created_by' => $emp->id,
                    'created_at' => $currentDateTime,
                ];

                $update = $this->Hr->updateById($absenTable, $data, $checkSchedule->id);
                if ($update) {
                    response([
                        'status' => 200,
                        'message' => 'Absen masuk berhasil',
                        'data' => [
                            'gate' => $checkQr->gate_name,
                            'date' => date('Y-m-d H:i:s', strtotime($currentDateTime))
                        ]
                    ]);
                } else {
                    response([
                        'status' => 400,
                        'message' => 'Absen masuk gagal',
                        'data' => [
                            'gate' => $checkQr->gate_name,
                            'date' => date('Y-m-d H:i:s', strtotime($currentDateTime))
                        ]
                    ]);
                }
            } else if ($type == "OUT") {
                $absenTable = 'absen_' . date('Ym', strtotime($currentDate));
                $checkSchedule = $this->Hr->getWhere($absenTable, ['emp_id' => $emp->id, 'abs_date' => $currentDate])->row();

                if ($checkSchedule && $checkSchedule->qr_code_in != '') {
                } else {
                    $yesterdayDate = date('Y-m-d', strtotime($currentDate . " - 1 days"));
                    $absenTable = 'absen_' . date('Ym', strtotime($yesterdayDate));
                    $yesterdaySchedule = $this->Hr->getWhere($absenTable, ['emp_id' => $emp->id, 'abs_date' => $yesterdayDate])->row();

                    if (
                        $yesterdaySchedule &&
                        (
                            $yesterdaySchedule->shift != 'Shift 1' ||
                            $yesterdaySchedule->shift != 'Lembur Shift 1'
                        )
                    ) {
                        $checkSchedule = $yesterdaySchedule;
                        $currentDate = $yesterdayDate;
                    }
                }

                if (!$checkSchedule) {
                    response([
                        'status' => 400,
                        'message' => 'Anda tidak memiliki jadwal kehadiran pada tanggal ' . toIndoDateDay($currentDate)
                    ], 400);
                }

                if ($checkSchedule->shift == 'OFF') {
                    response([
                        'status' => 400,
                        'message' => 'Absen keluar gagal, ' . toIndoDateDay($currentDate) . ' merupakan hari libur untuk anda'
                    ], 400);
                }

                if ($checkSchedule->on_leave == 1) {
                    response([
                        'status' => 'failed',
                        'message' => 'Tidak dapat melakukan absen keluar, status anda sedang cuti'
                    ], 400);
                }

                $data = [
                    'abs_date' => $currentDate,
                    'date_out' => $currentDateTime,
                    'lat_out' => $lat,
                    'lng_out' => $lng,
                    'distance_out' => $distance,
                    'qr_code_out' => $qrScanned,
                    'updated_by' => $emp->id,
                    'updated_at' => $currentDateTime,
                ];

                $update = $this->Hr->updateById($absenTable, $data, $checkSchedule->id);
                if ($update) {
                    $newAbsen = $this->Hr->getDataById($absenTable, $checkSchedule->id);

                    $absDate = date('Y-m-d', strtotime($newAbsen->date_out));
                    $overtime = $this->Hr->getWhere('employee_overtimes_detail', ['emp_id' => $emp->id, 'overtime_date' => $absDate])->row();

                    if ($overtime) {
                        $time = clockToFloat(date("H:i:s", strtotime($overtime->start_date)));
                        if ($time > 6) {
                            $overtime = $this->Hr->getWhere('employee_overtimes_detail', ['emp_id' => $emp->id, 'overtime_date' => $newAbsen->abs_date])->row();
                        }

                        if ($overtime) {
                            $ovtIn = date('Y-m-d H:i:s', strtotime($overtime->start_date));
                            $ovtOut = date('Y-m-d H:i:s', strtotime($overtime->end_date));

                            $startTime = $ovtIn;
                            $endTime = $ovtOut;

                            $startTimeHour = date('H:i', strtotime($startTime));
                            $endTimeHour = date('H:i', strtotime($endTime));

                            $recalculate = totalHour($overtime->emp_id, $startTime, $endTime, $startTimeHour, $endTimeHour);

                            $fixData = [
                                'id' => $overtime->id,
                                'updated_by' => empId(),
                                'updated_at' => date('Y-m-d H:i:s'),
                                'status_day' => $recalculate['status_day'],
                                'effective_hour' => $recalculate['effective_hour'],
                                'break_hour' => $recalculate['break_hour'],
                                'real_hour' => $recalculate['real_hour'],
                                'overtime_hour' => $recalculate['overtime_hour'],
                                'premi_overtime' => $recalculate['premi_overtime'],
                                'overtime_value' => $recalculate['overtime_value'],
                                'total_meal' => $recalculate['total_meal'],
                                'deduction' => 0
                            ];

                            $this->Hr->updateById('employee_overtimes_detail', $fixData, $overtime->id);
                        }
                    }

                    if ($overtime && $overtime->payment_status != 'VERIFIED') {
                        $absen = $this->Hr->getDataById($absenTable, $checkSchedule->id);

                        $ovtIn = date('Y-m-d H:i:s', strtotime($overtime->start_date));
                        $ovtOut = date('Y-m-d H:i:s', strtotime($overtime->end_date));
                        $dateIn = date('Y-m-d H:i:s', strtotime($absen->date_in));
                        $dateOut = date('Y-m-d H:i:s', strtotime($absen->date_out));

                        if ($dateIn > $ovtIn || $dateOut < $ovtOut) {
                            $startTime = $ovtIn;
                            $endTime = $ovtOut;

                            if ($dateIn > $ovtIn) $startTime = fixingTime($dateIn, 'in');
                            if ($dateOut < $ovtOut) $endTime = fixingTime($dateOut, 'out');

                            $startTimeHour = date('H:i', strtotime($startTime));
                            $endTimeHour = date('H:i', strtotime($endTime));

                            $recalculate = totalHour($overtime->emp_id, $startTime, $endTime, $startTimeHour, $endTimeHour);
                            $deduction = ($overtime->overtime_value + $overtime->deduction) - $recalculate['overtime_value'];
                            $data = [
                                'id' => $overtime->id,
                                'updated_by' => empId(),
                                'updated_at' => date('Y-m-d H:i:s'),
                                'status_day' => $recalculate['status_day'],
                                'effective_hour' => $recalculate['effective_hour'],
                                'break_hour' => $recalculate['break_hour'],
                                'real_hour' => $recalculate['real_hour'],
                                'overtime_hour' => $recalculate['overtime_hour'],
                                'premi_overtime' => $recalculate['premi_overtime'],
                                'overtime_value' => $recalculate['overtime_value'],
                                'total_meal' => $recalculate['total_meal'],
                                'deduction' => $deduction > 0 ? $deduction : 0
                            ];

                            $this->Hr->updateById('employee_overtimes_detail', $data, $overtime->id);
                        }
                    }

                    response([
                        'status' => 200,
                        'message' => 'Absen keluar berhasil',
                        'data' => [
                            'gate' => $checkQr->gate_name,
                            'date' => date('Y-m-d H:i:s', strtotime($currentDateTime))
                        ]
                    ]);
                }
            }
        } else {
            response([
                'status' => 400,
                'message' => "QR Expired, please try again"
            ], 400);
        }
    }
}
