<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class Outoffice extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers('authorization'));

        $this->load->model("api/AbsenModel", 'AbsenModel');
        $this->AbsenModel->myConstruct('hr');

        $this->load->model("HrModel");
        $this->HrModel->myConstruct('hr');
    }

    public function getOutoffices()
    {
        $params = getParam();
        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $user = $this->Hr->getDataById('employees', $payload['empId']);
        $outOffices = $this->AbsenModel->getOutoffices($params, $user, $payload['empId'])->result();

        $list = [];
        foreach ($outOffices as $office) {
            
            $start = date('Y-m-d H:i:s', strtotime($office->actual_in));
            $end = date('Y-m-d H:i:s', strtotime($office->end_time));

            $lateTime = 0;
            if($office->actual_in && $office->actual_in != "0000-00-00 00:00:00" && $start > $end) {
                $lateTime = countHour($start, $end, 'h') * 60;
            }

            $lateTime;

            $actualInHour = "(-)";
            if($office->actual_in && $office->actual_in != "0000-00-00 00:00:00") {
                if(date("Y-m-d", strtotime($office->actual_in)) == $office->out_date) {
                    $actualInHour = date("H:i", strtotime($office->actual_in));
                } else {
                    $actualInHour = toIndoDateTime2($office->actual_in);
                }
            }

            $actualOutHour = "(-)";
            if($office->actual_out && $office->actual_out != "0000-00-00 00:00:00") {
                if(date("Y-m-d", strtotime($office->actual_out)) == $office->out_date) {
                    $actualOutHour = date("H:i", strtotime($office->actual_out));
                } else {
                    $actualOutHour = toIndoDateTime2($office->actual_out);
                }
            }

            $list[] = [
                'id' => $office->id,
                'employeeName' => $office->employee_name,
                'outDate' => toIndoDateDay($office->out_date),
                'startTime' => toIndoDateTime($office->start_time),
                'endTime' => $office->not_back ? "(-)" : toIndoDateTime($office->end_time),
                'startHour' => date('H:i', strtotime($office->start_time)),
                'endHour' => $office->not_back ? "(-)" : date('H:i', strtotime($office->end_time)),
                'actualIn' => $office->actual_in && $office->actual_in != "0000-00-00 00:00:00" ? toIndoDateTime($office->actual_in) : "-",
                'actualInHour' => $actualInHour,
                'latIn' => $office->lat_in,
                'lngIn' => $office->lng_in,
                'distanceIn' => number_format($office->distance_in / 1000, 2, ",", ".") . " KM",
                'actualOut' => $office->actual_out && $office->actual_out != "0000-00-00 00:00:00" ? toIndoDateTime($office->actual_out) : "-",
                'actualOutHour' => $actualOutHour,
                'latOut' => $office->lat_out,
                'lngOut' => $office->lng_out,
                'distanceOut' => number_format($office->distance_out / 1000, 2, ",", ".") . " KM",
                'notBack' => $office->not_back == 1 ? true : false,
                'departmentName' => $office->department_name,
                'subDepartmentName' => $office->sub_department_name,
                'divisionName' => $office->division_name,
                'direactName' => $office->direct_name,
                'direactRankName' => $office->direct_rank_name,
                'direactDate' => $office->direct_date != '0000-00-00 00:00:00' ? toIndoDateTime($office->direct_date) : "-",
                'direactName2' => $office->direct_name_2,
                'direactRankName2' => $office->direct_rank_name_2,
                'direactDate2' => $office->direct_date_2 != '0000-00-00 00:00:00' ? toIndoDateTime($office->direct_date_2) : "-",
                'apvStatus' => $office->apv_status_2,
                'apvDate' => $office->apv_date_2 && $office->apv_date_2 != "0000-00-00 00:00:00" ? toIndoDateTime($office->apv_date_2) : "-",
                'lateTime' => $lateTime ."",
                'deduction' => $office->deduction > 0 ? number_format($office->deduction, 2, ',', '.') . "" : "0",
                'description' => $office->description,
                'type' => $office->type == 1 ? "Pribadi" : "Dinas"
            ];
        }

        response(['status' => 'success', 'message' => 'Fetch out offices list success', 'list' => $list]);
    }

    public function create()
    {
        $post = getInputPost();
        $currentDateTime = date('Y-m-d H:i:s');

        $payload = $this->jwt->me($this->input->request_headers('authorization'));
        $employee = $this->HrModel->getEmployeeById($payload['empId']);

        $startTime = date('Y-m-d H:i:s', strtotime("$post[outDate] $post[startTime]:00"));
        $endTime = date('Y-m-d H:i:s', strtotime("$post[outDate] $post[endTime]:00"));

        $expDate = explode("-", $post['outDate']);
        $absenTable = absenTable($expDate[0], $expDate[1]);

        $absen = $this->Hr->getWhere($absenTable, ['abs_date' => $post['outDate'], 'emp_id' => $payload['empId']])->row();
        if($absen) {
            if($absen->on_leave == 1) response(['status' => 'failed', 'message' => 'Tidak dapat membuat request, status anda sedang cuti'], 400);
        }

        if($post['notBack'] == 0 && $startTime >= $endTime) {
            response(['status' => 'failed', 'message' => 'Waktu keluar harus lebih besar dari waktu kembali'], 400);
        }

        $approver1 = "";
        $apvRank1 = 0;
        $apvNip1 = "";

        $approver2 = "";
        $apvRank2 = 0;
        $apvNip2 = "";
        
        if($employee->rank_id > 6) {
            $isHaveSpv = $this->Hr->getOne('employees', ['sub_department_id' => $employee->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['5', '6'], 'division_id' => [$employee->division_id]]);
            if($isHaveSpv) {
                $approver1 = $isHaveSpv;
                $apvRank1 = $isHaveSpv->rank_id;
                $apvNip1 = $isHaveSpv->nip;
            } else {
                $approver1 = $employee;
                $apvRank1 = $employee->rank_id;
                $apvNip1 = $employee->nip;
            }

            $isHaveAsman = $this->Hr->getOne('employees', ['sub_department_id' => $employee->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
            if($isHaveAsman) {
                $approver2 = $isHaveAsman;
                $apvRank2 = $isHaveAsman->rank_id;
                $apvNip2 = $isHaveAsman->nip;
            } else {
                $isHaveAsmanPLT = $this->Hr->getOne('employee_ranks', ['sub_department_id' => $employee->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                if($isHaveAsmanPLT) {
                    $approver2 = $this->Hr->getDataById('employees', $isHaveAsmanPLT->emp_id);
                    $apvRank2 = $approver2->rank_id;
                    $apvNip2 = $approver2->nip;
                } else {
                    $isHaveMgr = $this->Hr->getOne('employees', ['department_id' => $employee->department_id, 'rank_id' => 2, 'status' => 'ACTIVE']);
                    if($isHaveMgr) {
                        $approver2 = $isHaveMgr;
                        $apvRank2 = $isHaveMgr->rank_id;
                        $apvNip2 = $isHaveMgr->nip;
                    } else {
                        $isHaveMgrPLT = $this->Hr->getOne('employee_ranks', ['department_id' => $employee->department_id, 'rank_id' => 2, 'status' => 'ACTIVE']);
                        if($isHaveMgrPLT) {
                            $approver2 = $this->Hr->getDataById('employees', $isHaveMgrPLT->emp_id);
                            $apvRank2 = $approver2->rank_id;
                            $apvNip2 = $approver2->nip;
                        } else {
                            $isHaveHead = $this->Hr->getOne('employees', ['rank_id' => 1, 'status' => 'ACTIVE']);
                            if($isHaveHead) {
                                $approver2 = $isHaveHead;
                                $apvRank2 = $isHaveHead->rank_id;
                                $apvNip2 = $isHaveHead->nip;
                            } else {
                                $isHaveHeadPLT = $this->Hr->getOne('employee_ranks', ['rank_id' => 1, 'status' => 'ACTIVE']);
                                if($isHaveHeadPLT) {
                                    $approver2 = $this->Hr->getDataById('employees', $isHaveHeadPLT->emp_id);
                                    $apvRank2 = $approver2->rank_id;
                                    $apvNip2 = $approver2->nip;
                                } else {
                                    $approver2 = $employee;
                                    $apvRank2 = $employee->rank_id;
                                    $apvNip2 = $employee->nip;
                                }
                            }
                        }
                    }
                }
            }
        } else if($employee->rank_id >= 5 && $employee->rank_id <= 6) {
            $approver1 = $employee;
            $apvRank1 = $employee->rank_id;
            $apvNip1 = $employee->nip;

            $isHaveAsman = $this->Hr->getOne('employees', ['sub_department_id' => $employee->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
            if($isHaveAsman) {
                $approver2 = $isHaveAsman;
                $apvRank2 = $isHaveAsman->rank_id;
                $apvNip2 = $isHaveAsman->nip;
            } else {
                $isHaveAsmanPLT = $this->Hr->getOne('employee_ranks', ['sub_department_id' => $employee->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                if($isHaveAsmanPLT) {
                    $approver2 = $this->Hr->getDataById('employees', $isHaveAsmanPLT->emp_id);
                    $apvRank2 = $approver2->rank_id;
                    $apvNip2 = $approver2->nip;
                } else {
                    $isHaveMgr = $this->Hr->getOne('employees', ['department_id' => $employee->department_id, 'rank_id' => 2, 'status' => 'ACTIVE']);
                    if($isHaveMgr) {
                        $approver2 = $isHaveMgr;
                        $apvRank2 = $isHaveMgr->rank_id;
                        $apvNip2 = $isHaveMgr->nip;
                    } else {
                        $isHaveMgrPLT = $this->Hr->getOne('employee_ranks', ['department_id' => $employee->department_id, 'rank_id' => 2, 'status' => 'ACTIVE']);
                        if($isHaveMgrPLT) {
                            $approver2 = $this->Hr->getDataById('employees', $isHaveMgrPLT->emp_id);
                            $apvRank2 = $approver2->rank_id;
                            $apvNip2 = $approver2->nip;
                        } else {
                            $isHaveHead = $this->Hr->getOne('employees', ['rank_id' => 1, 'status' => 'ACTIVE']);
                            if($isHaveHead) {
                                $approver2 = $isHaveHead;
                                $apvRank2 = $isHaveHead->rank_id;
                                $apvNip2 = $isHaveHead->nip;
                            } else {
                                $isHaveHeadPLT = $this->Hr->getOne('employee_ranks', ['rank_id' => 1, 'status' => 'ACTIVE']);
                                if($isHaveHeadPLT) {
                                    $approver2 = $this->Hr->getDataById('employees', $isHaveHeadPLT->emp_id);
                                    $apvRank2 = $approver2->rank_id;
                                    $apvNip2 = $approver2->nip;
                                } else {
                                    $approver2 = $employee;
                                    $apvRank2 = $employee->rank_id;
                                    $apvNip2 = $employee->nip;
                                }
                            }
                        }
                    }
                }
            }
        } else if($employee->rank_id == 3 || $employee->rank_id == 4) {
            $approver1 = $employee;
            $apvRank1 = $employee->rank_id;
            $apvNip1 = $employee->nip;

            $isHaveMgr = $this->Hr->getOne('employees', ['department_id' => $employee->department_id, 'rank_id' => 2, 'status' => 'ACTIVE']);
            if($isHaveMgr) {
                $approver2 = $isHaveMgr;
                $apvRank2 = $isHaveMgr->rank_id;
                $apvNip2 = $isHaveMgr->nip;
            } else {
                $isHaveMgrPLT = $this->Hr->getOne('employee_ranks', ['department_id' => $employee->department_id, 'rank_id' => 2, 'status' => 'ACTIVE']);
                if($isHaveMgrPLT) {
                    $approver2 = $this->Hr->getDataById('employees', $isHaveMgrPLT->emp_id);
                    $apvRank2 = $approver2->rank_id;
                    $apvNip2 = $approver2->nip;
                } else {
                    $isHaveHead = $this->Hr->getOne('employees', ['rank_id' => 1, 'status' => 'ACTIVE']);
                    if($isHaveHead) {
                        $approver2 = $isHaveHead;
                        $apvRank2 = $isHaveHead->rank_id;
                        $apvNip2 = $isHaveHead->nip;
                    } else {
                        $isHaveHeadPLT = $this->Hr->getOne('employee_ranks', ['rank_id' => 1, 'status' => 'ACTIVE']);
                        if($isHaveHeadPLT) {
                            $approver2 = $this->Hr->getDataById('employees', $isHaveHeadPLT->emp_id);
                            $apvRank2 = $approver2->rank_id;
                            $apvNip2 = $approver2->nip;
                        } else {
                            $approver2 = $employee;
                            $apvRank2 = $employee->rank_id;
                            $apvNip2 = $employee->nip;
                        }
                    }
                }
            }
        } else if($employee->rank_id == 2) {
            $approver1 = $employee;
            $apvRank1 = $employee->rank_id;
            $apvNip1 = $employee->nip;

            $isHaveHead = $this->Hr->getOne('employees', ['rank_id' => 1, 'status' => 'ACTIVE']);
            if($isHaveHead) {
                $approver2 = $isHaveHead;
                $apvRank2 = $isHaveHead->rank_id;
                $apvNip2 = $isHaveHead->nip;
            } else {
                $isHaveHeadPLT = $this->Hr->getOne('employee_ranks', ['rank_id' => 1, 'status' => 'ACTIVE']);
                if($isHaveHeadPLT) {
                    $approver2 = $this->Hr->getDataById('employees', $isHaveHeadPLT->emp_id);
                    $apvRank2 = $approver2->rank_id;
                    $apvNip2 = $approver2->nip;
                } else {
                    $approver2 = $employee;
                    $apvRank2 = $employee->rank_id;
                    $apvNip2 = $employee->nip;
                }
            }
        } else if($employee->rank_id == 1) {
            $approver1 = $employee;
            $apvRank1 = $employee->rank_id;
            $apvNip1 = $employee->nip;

            $approver2 = $employee;
            $apvRank2 = $employee->rank_id;
            $apvNip2 = $employee->nip;
        }

        $dateExist = 0;
        $dt1 = "";
        $dt2 = "";

        $year = date('Y', strtotime($currentDateTime));
        $month = date('m', strtotime($currentDateTime));
        $schedules = $this->Hr->getWhere('out_office', ['emp_id' => $employee->id, 'YEAR(out_date)' => $year, 'MONTH(out_date)' => $month, 'status !=' => 'REJECTED', 'status !=' => 'CANCELED'])->result();
       
        foreach ($schedules as $sch) {
            if (checkDateExist($startTime, $sch->start_time, $sch->end_time)) {
                $dateExist++;
                $dt1 = $startTime;
            }

            if($post['notBack'] != 1) {
                if (checkDateExist($endTime, $sch->start_time, $sch->end_time)) {
                    $dateExist++;
                    $dt2 = $endTime;
                }
            }
        }
       
        if ($dateExist > 0) {

            $message = "";
            if ($dt1 != '' && $dt2 != '') {
                $message = "Izin Keluar Kantor $employee->employee_name Hari " . toIndoDateTime($dt1) . " dan " . toIndoDateTime($dt2) . " sudah dibuat!";
            } else if ($dt1 != '' && $dt2 == '') {
                $message = "Izin Keluar Kantor $employee->employee_name Hari " . toIndoDateTime($dt1) . " sudah dibuat!";
            } else if ($dt1 == '' && $dt2 != '') {
                $message = "Izin Keluar Kantor $employee->employee_name Hari " . toIndoDateTime($dt2) . " sudah dibuat!";
            }

            response(['status' => 'failed', 'message' => $message], 400);
        }

        $data = [
            'emp_id' => $payload['empId'],
            'location' => $employee->location,
            'department_id' => $employee->department_id,
            'sub_department_id' => $employee->sub_department_id,
            'division_id' => $employee->division_id,
            'purpose' => $post['purpose'],
            'description' => $post['description'],
            'type' => $post['type'],
            'out_date' => $post['outDate'],
            'start_time' => $startTime,
            'end_time' => $post['notBack'] == 1 ? "0000-00-00 00:00:00" : $endTime,
            'not_back' => $post['notBack'],
            'apv_rank_1' => $apvRank1,
            'apv_nip_1' => $apvNip1,
            'apv_status_1' => 'CREATED',
            'apv_rank_2' => $apvRank2,
            'apv_nip_2' => $apvNip2,
            'apv_status_2' => 'CREATED',
            'created_by' => $payload['empId'],
            'created_at' => $currentDateTime,
            'updated_by' => $payload['empId'],
            'updated_at' => $currentDateTime
        ];

        if($employee->rank_id == 1 || $apvNip1 == $employee->nip) {
            $data['apv_status_1'] = 'APPROVED';
            $data['apv_date_1'] = $currentDateTime;
            $data['status'] = 'PROCESS';
        } 

        if($apvNip2 == $employee->nip) {
            $data['apv_status_2'] = 'APPROVED';
            $data['apv_date_2'] = $currentDateTime;
            $data['status'] = 'APPROVED';
        }

        $insert = $this->Hr->create('out_office', $data);

        if($apvNip1 != $employee->nip) {
            $emailToken = time().'_OUT_OFFICE_#1_'.$approver1->id;
            $tokenApprove = simpleEncrypt("$insert:APPROVED:$emailToken");
            $tokenReject = simpleEncrypt("$insert:REJECTED:$emailToken");
            $linkApprove = LIVE_URL . "index.php?c=PublicController&m=approveOutOffice&token=$tokenApprove";
            $linkReject = LIVE_URL . "index.php?c=PublicController&m=approveOutOffice&token=$tokenReject";

            $linkApprove = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=positive&token=" . simpleEncrypt($linkApprove);
            $linkReject = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=negative&token=" . simpleEncrypt($linkReject);

            $message = $this->load->view('html/absen/email/approve_outoffice', [
                'employee' => $employee,
                'approver' => $approver1,
                'data' => $data,
                'linkApprove' => $linkApprove,
                'linkReject' => $linkReject,
            ], true);

            $emailData = [
                'alert_name' => 'OUT_OFFICE_#1',
                'email_to' => $approver1->email,
                'subject' => "Approval Izin Keluar Kantor $employee->employee_name pada tanggal " . toIndoDateDay($post['outDate']),
                'subject_name' => "Spekta Alert: Approval Izin Keluar Kantor $employee->employee_name pada tanggal " . toIndoDateDay($post['outDate']),
                'message' => $message,
                'token' => $emailToken,
                'emp_direct_apv' => $approver1->id,
                'positive' => $linkApprove,
                'negative' => $linkReject
            ];

            $this->Main->create('email', $emailData);
        } else {
            if($apvNip2 != $employee->nip) {
                $emailToken = time().'_OUT_OFFICE_2_'.$approver1->id;
                $tokenApprove = simpleEncrypt("$insert:APPROVED:$emailToken");
                $tokenReject = simpleEncrypt("$insert:REJECTED:$emailToken");
                $linkApprove = LIVE_URL . "index.php?c=PublicController&m=approveOutOffice&token=$tokenApprove";
                $linkReject = LIVE_URL . "index.php?c=PublicController&m=approveOutOffice&token=$tokenReject";
    
                $linkApprove = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=positive&token=" . simpleEncrypt($linkApprove);
                $linkReject = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=negative&token=" . simpleEncrypt($linkReject);
    
                $message = $this->load->view('html/absen/email/approve_outoffice', [
                    'employee' => $employee,
                    'approver' => $approver2,
                    'data' => $data,
                    'linkApprove' => $linkApprove,
                    'linkReject' => $linkReject,
                ], true);
    
                $emailData = [
                    'alert_name' => 'OUT_OFFICE_#2',
                    'email_to' => $approver2->email,
                    'subject' => "Approval Izin Keluar Kantor $employee->employee_name pada tanggal " . toIndoDateDay($post['outDate']),
                    'subject_name' => "Spekta Alert: Approval Izin Keluar Kantor $employee->employee_name pada tanggal " . toIndoDateDay($post['outDate']),
                    'message' => $message,
                    'token' => $emailToken,
                    'emp_direct_apv' => $approver2->id,
                    'positive' => $linkApprove,
                    'negative' => $linkReject
                ];
    
                $this->Main->create('email', $emailData);
            }
        }

        if($insert) {
            response(['status' => 'success', 'message' => 'Berhasil membuat izin keluar kantor']);
        } else {
            response(['status' => 'failed', 'message' => 'Gagal membuat izin keluar kantor'], 400);
        }
    }

    public function scanQR()
    {
        $currentDateTime = date('Y-m-d H:i:s');
        $deduction = 0;

        $post = getInputPost();
        $qrScanned = $post['qrScanned'];
        $outOfficeId = $post['outOfficeId'];
        $lat = $post['lat'];
        $lng = $post['lng'];
        $distance = $post['distance'];
        $action = $post['action'];

        $qrSystem = $this->Hr->getDataById('out_office_cfg', 1);
        if($qrScanned != $qrSystem->qr_token) response(['status' => 'failed', 'message' => 'QR Code tidak valid'], 400);

        if($distance > $qrSystem->distance) response(['status' => 'failed', 'message' => 'Jarak anda terlalu jauh ('. number_format($distance / 1000, 2, ",", ".") .' KM)'], 400);

        $outOffice = $this->Hr->getDataById('out_office', $outOfficeId);

        if(!$outOffice) response(['status' => 'failed', 'message' => 'Izin keluar kantor tidak ditemukan']);

        if($outOffice->status == 'CREATED') {
            $direct = $this->Hr->getWhere('employees', ['nip' => $outOffice->apv_nip])->row();
            response(['status' => 'failed', 'message' => 'Request keluar kantor anda belum di Approve oleh ' . $direct->employee_name], 400);
        }

        if($action == "OUT") {
            $message = 'Scan OUT keluar kantor berhasil';
            if(date('Y-m-d', strtotime($outOffice->start_time)) != date('Y-m-d')) {
                response([
                    'status' => 'failed', 
                    'message' => 'Tanggal izin keluar kantor tidak cocok, tanggal keluar adalah ' . toIndoDateDay(date('Y-m-d', strtotime($outOffice->out_date))) 
                ], 400);
            }

            if($outOffice->actual_out != '0000-00-00 00:00:00') {
                response(['status' => 'failed', 'message' => 'Anda sudah pernah Scan OUT sebelumnya, scan out tanggal ' . toIndoDateTime2($outOffice->actual_out)], 400);
            }

            $deduction = 0;

            if($outOffice->not_back == 1 && $outOffice->type == 1) {
                $deduction = 20000;
            }
            
            $data = [
                'actual_out' => $currentDateTime,
                'lat_out' => $lat,
                'lng_out' => $lng,
                'distance_out' => $distance,
                'deduction' => $deduction
            ];
        } else {
            $message = 'Scan IN keluar kantor berhasil';
            if($outOffice->actual_out == '0000-00-00 00:00:00') response(['status' => 'failed', 'message' => 'Anda belum Scan OUT sebelumnya'], 400);
            if($outOffice->actual_in != '0000-00-00 00:00:00') response(['status' => 'failed', 'message' => 'Anda sudah pernah Scan IN sebelumnya'], 400);
            if($outOffice->not_back == 1) response(['status' => 'failed', 'message' => 'Anda tidak perlu Scan IN untuk request Izin Keluar Tidak Kembali'], 400);

            $data = [
                'actual_in' => $currentDateTime,
                'lat_in' => $lat,
                'lng_in' => $lng,
                'distance_in' => $distance,
                'deduction' => $deduction
            ];

            $start = date('Y-m-d H:i:s', strtotime($outOffice->end_time));
            $end = date('Y-m-d H:i:s', strtotime($currentDateTime));

            if($outOffice->type == 1) {
                if($end > $start) {
                    $deduction = 0;

                    $diff = countHour($start, $end, 'h') * 60;
                    if($diff == 30) {
                        $deduction = 0; //5000
                    } else if($diff >= 31 && $diff <= 60) {
                        $deduction = 0; //10000
                    } else if($diff >= 61 && $diff <= 120) {
                        $deduction = 0; //15000
                    } else if($diff >= 121){
                        $deduction = 0; //20000
                    }
                    
                    $data['deduction'] = $deduction;
                }
            }
        }

        $this->Hr->updateById('out_office', $data, $outOffice->id);
        response(['status' => 'success', 'date' => $currentDateTime, 'message' => $message]);
    }
}