<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class General extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers('authorization'));

        $this->load->model('api/GeneralModel', 'GeneralModel');
        $this->GeneralModel->myConstruct('general');
    }

    function getMeetSchedules()
    {
        $params = getParam();

        $user = $this->jwt->me($this->input->request_headers('authorization'));
        $schedules = $this->GeneralModel->getMeetSchedules($params, $user)->result();

        foreach ($schedules as $schedule) {
            $schedule->meeting_type = ucwords($schedule->meeting_type);
            $schedule->startDay = toIndoDateDay(date('Y-m-d', strtotime($schedule->start_date)));
            $schedule->startHour = date('H:i', strtotime($schedule->start_date));
            $schedule->endDay = toIndoDateDay(date('Y-m-d', strtotime($schedule->end_date)));
            $schedule->endHour = date('H:i', strtotime($schedule->end_date));
        }

        response([
            'status' => 'success',
            'data' => $schedules
        ]);
    }

    function getVehicleSchedules()
    {
        $params = getParam();

        $user = $this->jwt->me($this->input->request_headers('authorization'));
        $schedules = $this->GeneralModel->getVehicleSchedules($params, $user)->result();

        foreach ($schedules as $schedule) {
            $schedule->startDay = toIndoDateDay(date('Y-m-d', strtotime($schedule->start_date)));
            $schedule->startHour = date('H:i', strtotime($schedule->start_date));
            $schedule->endDay = toIndoDateDay(date('Y-m-d', strtotime($schedule->end_date)));
            $schedule->endHour = date('H:i', strtotime($schedule->end_date));
            $schedule->trip_type = strtoupper($schedule->trip_type);
        }

        response([
            'status' => 'success',
            'data' => $schedules
        ]);
    }

    public function responseMeeting()
    {
        $meetingId = $this->input->post('meetingId');
        $action = $this->input->post('action');

        $user = $this->jwt->me($this->input->request_headers('authorization'));
        $emp = $this->Hr->getDataById('employees', $user['empId']);

        $currStatus = $this->General->getOne('meeting_participants', ['email' => $emp->email, 'meeting_id' => $meetingId]);
        $meeting = $this->General->getDataById('meeting_rooms_reservation', $meetingId);

        if($currStatus && $meeting) {
            $startDate = date('Y-m-d H:i:s', strtotime($meeting->start_date));
            $currentDate = date('Y-m-d H:i:s');

            if($startDate > $currentDate) {
                $this->General->update('meeting_participants', ['status' => $action], ['email' => $emp->email,  'meeting_id' => $meetingId]);
                if($currStatus->status == 'BELUM MEMUTUSKAN') {
                    if($action != 'HADIR') {
                        $data['participant_confirmed'] = $meeting->participant_confirmed + 1;
                        $data['participant_rejected'] = $meeting->participant_rejected;
                        $this->General->updateById('meeting_rooms_reservation', $data, $meetingId);
                    } else {
                        $data['participant_confirmed'] = $meeting->participant_confirmed;
                        $data['participant_rejected'] = $meeting->participant_rejected + 1;
                        $this->General->updateById('meeting_rooms_reservation', $data, $meetingId);
                    }
                } else if($currStatus->status == 'HADIR') {
                    if($action != 'HADIR') {
                        $data['participant_confirmed'] = $meeting->participant_confirmed - 1;
                        $data['participant_rejected'] = $meeting->participant_rejected + 1;
                        $this->General->updateById('meeting_rooms_reservation', $data, $meetingId);
                    }
                } else if($currStatus->status == 'TIDAK HADIR') {
                    if($action != 'TIDAK HADIR') {
                        $data['participant_confirmed'] = $meeting->participant_confirmed + 1;
                        $data['participant_rejected'] = $meeting->participant_rejected - 1;
                        $this->General->updateById('meeting_rooms_reservation', $data, $meetingId);
                    }
                }

                response([
                    'status' => 'success', 
                    'message' => 'Response meeting sukses',
                    'participant_confirmed' => $data['participant_confirmed'],
                    'participant_rejected' => $data['participant_rejected']
                ]);
            } else {
                response(['status' => 'failed', 'message' => 'Meeting sudah dimulai'], 400);
            }
        } else {
            response(['status' => 'failed', 'message' => 'Meeting tidak ditemukan'], 400);
        }
    }
}