<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/CreatorJWT.php';

class CanteenOvertime2 extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->jwt = new CreatorJWT();
        $this->jwt->checkToken($this->input->request_headers('authorization'));

        $this->load->model('api/CanteenModel', 'CanteenModel');
        $this->CanteenModel->myConstruct('general');
    }


    public function getQRCanteen()
    {
        //Ganti ke Y-m-d
        $date = date('Y-m-d');
        $absenTable = 'absen_' . date('Ym');
        $currentDate = date('Y-m-d H:i:s');
        
        $checkTable = $this->General->checkTable('kf_hr', $absenTable);
        if(!$checkTable) {
            $this->General->createAbsenTable($absenTable);
        }

        $user = $this->jwt->me($this->input->request_headers('authorization'));

        $checkAbsen = $this->Hr->getWhere($absenTable, ['abs_date' => $date, 'emp_id' => $user['empId']])->row();
        $checkShift = $this->Hr->getWhere('work_time', ['id' => $checkAbsen ? $checkAbsen->shift_id : 0])->row();
        $checkOvertime = $this->Hr->getWhere(
            'employee_overtimes_detail', ['overtime_date' => $date, 'emp_id' => $user['empId'], 'total_meal >=' => 1],
            '*', null, ['id' => 'desc'], [], ['status' => ['CANCELED', 'REJECTED']]
        )->row();

        $status = "";
        $menuName = "";
        $menuDescription = "";
        $menuFilename = "";
        $takeDate = "";
        $outDate = "-";
        $duration = "-";
        $type = "";
        $takeDateIso = "";

        $qrConfig = $this->General->getDataById('canteen_cfg', 1);
        $timeToEat = $qrConfig->time_to_eat;
        
        if(
            ($checkAbsen && $checkAbsen->qr_code_in != '' && $checkShift && $checkShift->meal == 1 && $checkOvertime && $checkOvertime->total_meal == 1) || 
            ($checkAbsen && $checkAbsen->qr_code_in != '' && $checkOvertime && $checkOvertime->total_meal == 2 && !checkWeekend($checkOvertime->overtime_date) && !checkNationalDay($checkOvertime)) ||
            ($checkAbsen && $checkAbsen->qr_code_in != '' && $checkOvertime && $checkOvertime->total_meal == 3)
        ) {

            $canteen = $this->General->getWhere('canteen_overtime_2', ['date(take_date)' => $checkAbsen->abs_date, 'emp_id' => $checkAbsen->emp_id])->row();
            if(!$canteen && $checkOvertime) {
                $canteen = $this->General->getWhere('canteen_overtime_2', [
                    'date(take_date)' => $checkOvertime->overtime_date, 
                    'emp_id' => $checkOvertime->emp_id,
                ])->row();
            }

            if($canteen) {
                $status = $canteen->status;
                $menuName = $canteen->menu_name;
                $menuFilename = $canteen->menu_filename ? IMG_URL . 'assets/images/daily_menus/' . $canteen->menu_filename : "";

                $takeDate = toIndoDateTime($canteen->take_date);
                $type = $canteen->type == 'take_away' ? "Take Away" : "Makan Di Kantin";
                $menuDescription = $canteen->menu_description;

                //Ganti ke $canteen->take_date
                $start = date('Y-m-d H:i:s', strtotime($canteen->take_date));

                if($canteen->out_date) {
                    $outDate = toIndoDateTime($canteen->out_date);
                    $duration = $canteen->duration . ' Menit';
                }

                if($canteen->status == 'OPEN') {
                    $checkMinute = countMinute(countHour($start, $currentDate, 'h'));
                    if($checkMinute >= $timeToEat) {
                        $status = 'CLOSED';
                        $endDate = date('Y-m-d H:i:s', strtotime($canteen->take_date . " +$timeToEat minutes"));
                        $outDate = toIndoDateTime($endDate);
                        $duration = $timeToEat . ' Menit';
                        $this->General->updateById('canteen_overtime_2', [
                            'status' => $status,
                            'out_date' => $endDate,
                            'duration' => $timeToEat
                        ], $canteen->id);
                    }
                }

                //Ganti ke $canteen->take_date
                $takeTime = new DateTime(date('Y-m-d H:i:s', strtotime($canteen->take_date)), new DateTimeZone('Asia/Jakarta'));
                $takeDateIso = $takeTime->format(DateTime::ATOM);
            }
            
            response([
                'status' => 200,
                'data' => [
                    'qrDate' => toIndoDate($date),
                    'qrCode' => $checkAbsen && $checkAbsen->qr_code_in != '' && $checkShift && $checkShift->meal == 1 
                                    ? $checkAbsen->qr_code_in 
                                    : $checkOvertime->emp_task_id,
                    'status' => $status,
                    'menuName' => $menuName,
                    'menuDescription' => $menuDescription,
                    'menuFilename' => $menuFilename,
                    'takeDate' => $takeDate,
                    'outDate' => $outDate,
                    'duration' => $duration,
                    'type' => $type,
                    'takeDateIso' => $takeDateIso,
                    'timeToEat' => $timeToEat."",
                    'source' => ($checkAbsen && $checkAbsen->qr_code_in != '' && $checkShift && $checkShift->meal == 1) ? 'absen' : 'overtime'
                ]
            ]);
        } else {
            response([
                'status' => 400,
                'data' => [
                    'qrDate' => toIndoDate($date),
                    'qrCode' => '',
                    'status' => $status,
                    'menuName' => $menuName,
                    'menuDescription' => $menuDescription,
                    'menuFilename' => $menuFilename,
                    'takeDate' => $takeDate,
                    'outDate' => $outDate,
                    'duration' => $duration,
                    'type' => $type,
                    'takeDateIso' => $takeDateIso,
                    'timeToEat' => $timeToEat."",
                    'source' => ''
                ],
                'message' => 'Anda belum absen masuk di tanggal ' . toIndoDate($date)
            ], 400);
        }
    }

    public function getCanteenHistory()
    {
        $user = $this->jwt->me($this->input->request_headers('authorization'));
        //Ganti ke Y-m-d
        $histories = $this->CanteenModel->getCanteenAbsenOvertime2History($user['empId'], date('Y-m-d'));
        
        $historyList = [];
        foreach ($histories as $history) {
            $start = date('Y-m-d H:i:s', strtotime($history->take_date));
            if($history->out_date) {
                $end = date('Y-m-d H:i:s', strtotime($history->out_date));
                $history->duration = normalHour(countHour($start, $end, 'h'));
            } else {
                $history->duration = "Sedang Makan";
            }
            
            $history->take_date = toIndoDateTime($history->take_date);
            $history->menu_filename = $history->menu_filename ? IMG_URL . 'assets/images/daily_menus/' . $history->menu_filename : "";
            $history->created_at = toIndoDateTime($history->created_at);
            $historyList[] = $history;
        }

        response([
            'status' => 200,
            'list' => $histories ?: [] 
        ]);
    }

    public function getDailyMenu()
    {
        $menus = $this->General->getWhere('menus', ['is_active' => true])->result();
        $config = $this->General->getDataById('canteen_cfg', 1);

        $list = [];
        foreach ($menus as $menu) {
            $list[] = [
                'id' => intval($menu->id),
                'location' => $menu->location,
                'name' => $menu->name,
                'description' => $menu->description,
                'available' => $menu->available - $menu->picked,
                'picked' => intval($menu->picked),
                'isActive' => $menu->is_active == 1 ? true : false,
                'image' => $menu->filename ? IMG_URL . 'assets/images/daily_menus/' . $menu->filename : ""
            ];
        }

        response([
            'status' => 200,
            'take_away' => $config->take_away,
            'list' => $list
        ]);
    }

    public function scanQrCanteen()
    {
        $body = getInputPost();

        $currentDate = date('Y-m-d H:i:s');
        $type = $body['type'];
        $menuId = $body['menuId'];
        $qrScanned = $body['qrScanned'];
        $source = $body['source'];

        $qrConfig = $this->General->getDataById('canteen_cfg', 1);

        if($qrScanned != $qrConfig->qr_token) response(['status' => 400, 'message' => 'QR invalid!'], 400);

        $menu = $this->General->getDataById('menus', $menuId);
        if(!$menu) response(['status' => 400, 'message' => 'Menu tidak ditemukan'], 400);
        if(!$menu->is_active)  response(['status' => 400, 'message' => 'Menu tidak aktif'], 400);

        $payload = $this->jwt->me($this->input->request_headers('authorization'));

        $employee = $this->Hr->getDataById('employees', $payload['empId']);
        if(!$employee) response(['status' => 400, 'message' => 'User tidak ditemukan'], 400);

        $checkCanteen = $this->General->getWhere('canteen_overtime_2', ['date(take_date)' => date('Y-m-d', strtotime($currentDate)), 'emp_id' => $payload['empId']])->row();
        if($checkCanteen) response(['status' => 400, 'message' => 'Anda sudah mengambil makan pada ' . toIndoDateTime($checkCanteen->take_date)], 400);

        $this->CanteenModel->pickedMenu($menu->id);

        $canteenData = [
            'location' => $payload['empLoc'],
            'emp_id' => $employee->id,
            'take_date' => $currentDate,
            'out_date' => $type == 'on_the_spot' ? null : $currentDate,
            'duration' => 0,
            'type' => $type,
            'status' => $type == 'on_the_spot' ? 'OPEN' : 'CLOSED',
            'menu_name' => $menu->name,
            'menu_description' => $menu->description,
            'menu_filename' => $menu->filename,
            'created_by' => $payload['empId'],
            'created_at' => $currentDate,
            'source' => $source
        ];

        $insert = $this->General->create('canteen_overtime_2', $canteenData);
        if($insert) {
            response(['status' => 200, 'message' => 'Berhasil mengambil makan siang']);
        } else {
            response(['status' => 400, 'message' => 'Gagal mengambil makan siang'], 400);
        }
    }
}