<?php

require APPPATH . '/libraries/SimpleXLSXGen.php';

defined('BASEPATH') or exit('No direct script access allowed');

class ExportController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('HrModel');
        $this->HrModel->myConstruct('hr');

        $this->auth->isAuth();
    }

    public function absenExport()
    {
        $param = getParam();

        $fromTitle = date('Ymd', strtotime($param['start']));
        $endTitle = date('Ymd', strtotime($param['end']));
        $from = date('Y-m-d', strtotime($param['start']));
        $end = date('Y-m-d', strtotime($param['end']));
        $status = $param['status'] ?? "Semua Status Absen";
        $empStatus = $param['empStatus'] ?? "Semua Status Karyawan";
        
        $absens = $this->HrModel->getAbsenList($param)->result();
        $toExcels = [];

        $toExcels[] = [
            '<style bgcolor="#FFFF00" color="#0000FF"><b>No</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>NPP</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Sap ID</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Nama Karyawan</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Sub Unit</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Bagian</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Sub Bagian</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Status</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Shift Kerja</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Tanggal</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Jam Masuk</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Jam Keluar</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Absen Masuk</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Absen Keluar</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Keterlambatan</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Pulang Cepat</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Jam Produktif</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Jam Tidak Produktif</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Lokasi Absen</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>Jarak Dengan Gate</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>QR Masuk</b></style>',
            '<style bgcolor="#FFFF00" color="#0000FF"><b>QR Keluar</b></style>',
        ];

        $no = 1;
        foreach ($absens as $absen) {
            $schIn = date('Y-m-d H:i:s', strtotime($absen->sch_date_in));
            $schOut = date('Y-m-d H:i:s', strtotime($absen->sch_date_out));
            $dateIn = $absen->date_in ? date('Y-m-d H:i:s', strtotime($absen->date_in)) : '-';
            $dateOut = $absen->date_out ? date('Y-m-d H:i:s', strtotime($absen->date_out)) : '-';

            $difHourDateIn = '-';
            $difHourDateOut = '-';

            if ($dateIn != '-') {
                $difHourDateInCount = countHour($schIn, $dateIn, 'h-');
                if ($difHourDateInCount > 0) {
                    $difHourDateIn = normalHour($difHourDateInCount);
                }
            }

            if ($dateOut != '-') {
                $difHourDateOutCount = countHour($dateOut, $schOut, 'h-');
                if ($difHourDateOutCount > 0) {
                    $difHourDateOut = normalHour($difHourDateOutCount);
                }
            }

            $prod = '-';
            $unProd = '-';
            if ($dateIn != '-' && $dateOut != '-' && $absen->on_leave == 0) {
                $prodCount = countHour($dateIn, $dateOut, 'h');
                $prod = normalHour($prodCount > 4 ? ($prodCount - 1) : $prodCount);

                $schCount = countHour($schIn, $schOut, 'h');
                if ($dateOut > $schOut) {
                    $schCount = countHour($schIn, $dateOut, 'h');
                }

                $unProdCount = (($schCount - 1) - ($prodCount > 4 ? ($prodCount - 1) : $prodCount));
                if ($unProdCount > 0) {
                    $unProd = normalHour($unProdCount);
                }
            }

            $distance = "-";
            if ($absen->distance) {
                $distance = "IN: $absen->distance Meter";
            }

            if ($absen->distance_out) {
                $distance .= " ~ OUT: $absen->distance_out Meter";
            }

            $toExcels[] = [
                $no,
                $absen->nip,
                $absen->sap_id,
                $absen->employee_name,
                $absen->department_name,
                $absen->sub_department_name,
                $absen->division_name,
                $absen->employee_status,
                $absen->leave_name != '' ? $absen->shift . ' (' . $absen->leave_name . ')' : $absen->shift,
                $absen->abs_date ? toIndoDate($absen->abs_date) : '-',
                $absen->sch_date_in ? toIndoDateTime($absen->sch_date_in) : '-',
                $absen->sch_date_out ? toIndoDateTime($absen->sch_date_out) : '-',
                $absen->date_in ? toIndoDateTime($absen->date_in) : '-',
                $absen->date_out ? toIndoDateTime($absen->date_out) : '-',
                $difHourDateIn,
                $difHourDateOut,
                $prod,
                $unProd,
                $absen->gate ?: '-',
                $distance,
                $absen->qr_code_in ?: '-',
                $absen->qr_code_out ?: '-',
            ];
            $no++;
        }

        $filename = "report_absen_" . $fromTitle . "_to_" . $endTitle . ".xlsx";
        return SimpleXLSXGen::fromArray($toExcels, "Absen Karyawan Dari ($from - $end) ($status) ($empStatus)")->downloadAs($filename);
    }
}
