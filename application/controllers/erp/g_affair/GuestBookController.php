<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GuestBookController extends Erp_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('GeneralModel');
        $this->GeneralModel->myConstruct('general');

        $this->auth->isAuth();
    }

    public function getGustBooks()
    {
        $books = $this->GeneralModel->getGustBooks(getParam())->result();
        $xml = "";
        $no = 1;
        foreach ($books as $book) {
            $xml .= "<row id='$book->id'>";
            $xml .= "<cell>" . cleanSC($no) . "</cell>";
            $xml .= "<cell>" . cleanSC($book->name) . "</cell>";
            $xml .= "<cell>" . cleanSC($book->nik) . "</cell>";
            $xml .= "<cell>" . cleanSC($book->phone) . "</cell>";
            $xml .= "<cell>" . cleanSC($book->organization) . "</cell>";
            $xml .= "<cell>" . cleanSC($book->company) . "</cell>";
            $xml .= "<cell>" . cleanSC($book->employee) . "</cell>";
            $xml .= "<cell>" . cleanSC($book->company_purpose) . "</cell>";
            $xml .= "<cell>" . cleanSC($book->unit) . "</cell>";
            $xml .= "<cell>" . cleanSC($book->visit_needs) . "</cell>";
            $xml .= "<cell>" . cleanSC(toIndoDateTime($book->visit_time)) . "</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }
}