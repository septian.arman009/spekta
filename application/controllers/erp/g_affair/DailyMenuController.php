<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DailyMenuController extends Erp_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('GeneralModel');
        $this->GeneralModel->myConstruct('general');

        $this->auth->isAuth();
    }

    public function getMenus()
    {
        $menus = $this->GeneralModel->getMenus(getParam())->result();
        $xml = "";
        $no = 1;
        foreach ($menus as $menu) {
            $xml .= "<row id='$menu->id'>";
            $xml .= "<cell>" . cleanSC($no) . "</cell>";
            $xml .= "<cell>" . cleanSC(0) . "</cell>";
            $xml .= "<cell>" . cleanSC($menu->name) . "</cell>";
            $xml .= "<cell>" . cleanSC($menu->description) . "</cell>";
            $xml .= "<cell>" . cleanSC($menu->available) . "</cell>";
            $xml .= "<cell>" . cleanSC($menu->picked) . "</cell>";
            $xml .= "<cell>" . cleanSC($menu->created_by) . "</cell>";
            $xml .= "<cell>" . cleanSC($menu->updated_by) . "</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    
    public function menuForm()
    {
        $params = getParam();
        if (isset($params['id'])) {
            $menu = $this->General->getDataById('menus', $params['id']);
            fetchFormData($menu);
        } else {
            $post = prettyText(getPost(), ['name', 'description']);
            if (!isset($post['id'])) {
                $this->createMenuForm($post);
            } else {
                $this->updateMenuForm($post);
            }
        }
    }

    public function checkBeforeAddFile()
    {
        $post = fileGetContent();
        $id = $post->id;
        $isExist = false;
        if (!$id) {
            $check = $this->General->getOne('menus', [
                'name' => $post->name,
            ]);
            if ($check) {
                $isExist = true;
            }
        } else {
            $menu = $this->General->getDataById('menus', $id);
            if ($menu) {
                if ($menu->name != $post->name) {
                    $check = $this->General->getOne('menus', [
                        'name' => $post->name,
                    ]);
                    if ($check) {
                        $isExist = true;
                    }
                }
            } else {
                response(['status' => 'deleted']);
            }
        }

        if (!$isExist) {
            response(['status' => 'success']);
        } else {
            response(['status' => 'exist', 'message' => 'Data menu digunakan!']);
        }
    }

    public function createMenuForm($post)
    {
        $menu = $this->General->getOne('menus', ['name' => $post['name']]);
        isExist(["Nama menu $post[name]" => $menu]);

        $post['location'] = empLoc();
        $post['is_active'] = false;
        $post['created_by'] = empId();
        $post['created_at'] = date('Y-m-d H:i:s');
        $post['updated_by'] = empId();
        $post['updated_at'] = date('Y-m-d H:i:s');
        $this->General->create('menus', $post);
        xmlResponse('inserted', $post['name']);
    }

    public function updateMenuForm($post)
    {
        $menu = $this->General->getOne('menus', ['name' => $post['name'], 'id !=' => $post['id']]);
        isExist(["Nama menu $post[name]" => $menu]);

        $post['updated_by'] = empId();
        $post['updated_at'] = date('Y-m-d H:i:s');
        $this->General->updateById('menus', $post, $post['id']);
        xmlResponse('updated', $post['name']);
    }

    public function activate()
    {
        $obj = fileGetContent();
        $menuIds = $obj->menuIds;
        $isActive = $obj->is_active;

        $data = [];
        foreach ($menuIds as $id) {

            if($isActive == 1) {
                $data[] = [
                    'id' => $id,
                    'is_active' => $isActive
                ];
            } else {
                $data[] = [
                    'id' => $id,
                    'is_active' => $isActive,
                    'available' => 0,
                    'picked' => 0
                ];
            }
        }

        $this->General->updateMultiple("menus", $data, 'id');
        
        $message = $isActive == 1 ? 'Berhasil mengaktifkan menu' : 'Berhasil menonaktifkan menu';
        echo json_encode(['status' => 'success', 'message' => $message]);
    }

    public function updateMenuQuantity()
    {
        $post = prettyText(getGridPost());
        $data = [];
        foreach ($post as $key => $value) {
            
            $data[] = [
                'id' => $key,
                'name' => $value['c2'],
                'description' => $value['c3'],
                'available' => $value['c4'],
                'picked' => $value['c5']
            ];
        }

        $update = $this->General->updateMultiple('menus', $data, 'id');
        if ($update) {
            xmlResponse('updated', 'Update jumlah porsi menu berhasil');
        } else {
            xmlResponse('error', 'Update jumlah porsi menu gagal!');
        }
    }

    public function menuDelete()
    {
        $post = fileGetContent();
        $mError = '';
        $mSuccess = '';
        $datas = $post->datas;
        foreach ($datas as $id => $data) {
            $menu = $this->General->getDataById('menus', $data->id);
            $this->General->delete('menus', ['id' => $data->id]);

            $isImageUsed = $this->General->getWhere('canteen', ['menu_filename' => $menu->filename])->row();
            if (!$isImageUsed && file_exists('./assets/images/daily_menus/' . $menu->filename)) {
                unlink('./assets/images/daily_menus/' . $menu->filename);
            }
            $mSuccess .= "- $data->field berhasil dihapus <br>";
        }

        response(['status' => 'success', 'mError' => $mError, 'mSuccess' => $mSuccess]);
    }
}