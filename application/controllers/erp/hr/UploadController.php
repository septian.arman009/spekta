<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UploadController extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('HrModel');
        $this->HrModel->myConstruct('hr');

        $this->load->library('SimpleXLSX');

        $this->auth->isAuth();
    }

    public function getFiles()
    {
        $files = $this->HrModel->getFiles(getParam())->result();
        $xml = "";
        $no = 1;
        foreach ($files as $file) {
            $xml .= "<row id='$file->id'>";
            $xml .= "<cell>". cleanSC($no) ."</cell>";
            $xml .= "<cell>". cleanSC($file->name) ."</cell>";
            $xml .= "<cell>". cleanSC($file->filename) ."</cell>";
            $xml .= "<cell>". cleanSC($file->created_by) ."</cell>";
            $xml .= "<cell>". cleanSC(toIndoDateTime($file->created_at)) ."</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function getFilesTemp()
    {
        $files = $this->HrModel->getFilesTemp(getParam())->result();
        $xml = "";
        $no = 1;
        foreach ($files as $file) {

            $invalid = $file->invalid == 'Update' ? [] : unserialize($file->invalid);
            
            $overtime = array_key_exists('overtime', $invalid) ? $invalid['overtime'] : $file->overtime;
            if($file->overtime == '1') {
                $overtime = 'Jam Tetap';
            } else if($file->overtime == '0') {
                $overtime = 'Jam Berjalan';
            }

            $color = "bgColor='#c94b62'";
            $colorOke = "bgColor='#75b175'";

            $xml .= "<row id='$file->id'>";
            $xml .= (count($invalid) == 0 ? "<cell $colorOke>" . cleanSC($no) : "<cell>" . cleanSC($no))  ."</cell>";
            $xml .= (array_key_exists('npp', $invalid) ? "<cell $color >" . cleanSC($invalid['npp']) : "<cell>" . cleanSC($file->nip)) . "</cell>";
            $xml .= (array_key_exists('sap_id', $invalid) ? "<cell $color >" . cleanSC($invalid['sap_id']) : "<cell>" . cleanSC($file->sap_id)) . "</cell>";
            $xml .= (array_key_exists('parent_nik', $invalid) ? "<cell $color >" . cleanSC($invalid['parent_nik']) : "<cell>" . cleanSC($file->parent_nik)) . "</cell>";
            $xml .= (array_key_exists('nik', $invalid) ? "<cell $color >" . cleanSC($invalid['nik']) : "<cell>" . cleanSC($file->nik)) . "</cell>";
            $xml .= (array_key_exists('npwp', $invalid) ? "<cell $color >" . cleanSC($invalid['npwp']) : "<cell>" . cleanSC($file->npwp)) . "</cell>";
            $xml .= (array_key_exists('phone', $invalid) ? "<cell $color >" . cleanSC($invalid['phone']) : "<cell>" . cleanSC($file->phone)) . "</cell>";
            $xml .= (array_key_exists('mobile', $invalid) ? "<cell $color >" . cleanSC($invalid['mobile']) : "<cell>" . cleanSC($file->mobile)) . "</cell>";
            $xml .= "<cell>". cleanSC($file->employee_name) ."</cell>";
            $xml .= (array_key_exists('direct_spv', $invalid) ? "<cell $color >" . cleanSC($invalid['direct_spv']) : "<cell>" . cleanSC($file->direct_spv_name)) . "</cell>";
            $xml .= "<cell>". cleanSC($file->birth_place) ."</cell>";
            $xml .= (array_key_exists('birth_date', $invalid) ? "<cell $color >" . cleanSC($invalid['birth_date']) : "<cell>" . cleanSC(toIndoDateDay($file->birth_date))) . "</cell>";
            $xml .= "<cell>". cleanSC($file->age) ."</cell>";
            $xml .= (array_key_exists('gender', $invalid) ? "<cell $color >" . cleanSC($invalid['gender']) : "<cell>" . cleanSC($file->gender)) . "</cell>";
            $xml .= (array_key_exists('religion', $invalid) ? "<cell $color >" . cleanSC($invalid['religion']) : "<cell>" . cleanSC($file->religion)) . "</cell>";
            $xml .= "<cell>". cleanSC($file->address) ."</cell>";
            $xml .= (array_key_exists('employee_status', $invalid) ? "<cell $color >" . cleanSC($invalid['employee_status']) : "<cell>" . cleanSC($file->employee_status)) . "</cell>";
            $xml .= (array_key_exists('os_name', $invalid) ? "<cell $color >" . cleanSC($invalid['os_name']) : "<cell>" . cleanSC($file->os_name)) . "</cell>";
            $xml .= (array_key_exists('email', $invalid) ? "<cell $color >" . cleanSC($invalid['email']) : "<cell>" . cleanSC($file->email)) . "</cell>";
            $xml .= (array_key_exists('department_id', $invalid) ? "<cell $color >" . cleanSC($invalid['department_id']) : "<cell>" . cleanSC($file->department_name)) . "</cell>";
            $xml .= (array_key_exists('sub_department_id', $invalid) ? "<cell $color >" . cleanSC($invalid['sub_department_id']) : "<cell>" . cleanSC($file->sub_department_name)) . "</cell>";
            $xml .= (array_key_exists('division_id', $invalid) ? "<cell $color >" . cleanSC($invalid['division_id']) : "<cell>" . cleanSC($file->division_name)) . "</cell>";
            $xml .= (array_key_exists('rank_id', $invalid) ? "<cell $color >" . cleanSC($invalid['rank_id']) : "<cell>" . cleanSC($file->rank_name)) . "</cell>";
            
            $xml .= (array_key_exists('overtime', $invalid) ? "<cell $color >" . cleanSC($overtime) : "<cell>" . cleanSC($overtime)) . "</cell>";

            $xml .= (array_key_exists('basic_sallary', $invalid) ? "<cell $color >" . cleanSC($invalid['basic_sallary']) : "<cell>" . cleanSC(toRp($file->basic_sallary))) . "</cell>";
            $xml .= (array_key_exists('total_sallary', $invalid) ? "<cell $color >" . cleanSC($invalid['total_sallary']) : "<cell>" . cleanSC(toRp($file->total_sallary))) . "</cell>";
            $xml .= (array_key_exists('premi_overtime', $invalid) ? "<cell $color >" . cleanSC($invalid['premi_overtime']) : "<cell>" . cleanSC(toRp($file->premi_overtime))) . "</cell>";

            $xml .= "<cell>". cleanSC($file->created_name) ."</cell>";
            $xml .= "<cell>". cleanSC(toIndoDateTime($file->created_at)) ."</cell>";
            $xml .= "<cell>". cleanSC(count($invalid)) ."</cell>";
            
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function fileForm()
    {
        $params = getParam();
        if (isset($params['id'])) {
            $file = $this->Hr->getDataById('files', $params['id']);
            fetchFormData($file);
        } else {
            $post = prettyText(getPost(), ['name', 'description']);
            $this->createFileForm($post);
        }
    }
    
    public function createFileForm($post)
    {
        $file = $this->Hr->getOne('files', ['name' => $post['name']]);
        isExist(["Nama file $post[name]" => $file]);

        if($post['filename'] == '') xmlResponse('error', 'Belum ada file yang dipilih');

        $post['location'] = empLoc();
        $post['created_by'] = empId();
        $post['created_at'] = date('Y-m-d H:i:s');
        $this->Hr->create('files', $post);
        xmlResponse('inserted', $post['name']);
    }

    public function fileDelete()
    {
        $post = fileGetContent();
        $mError = '';
        $mSuccess = '';
        $datas = $post->datas;
        foreach ($datas as $id => $data) {
            $file = $this->Hr->getDataById('files', $data->id);
            $this->Hr->delete('files', ['id' => $data->id]);

            $isImageUsed = $this->General->getWhere('canteen', ['menu_filename' => $file->filename])->row();
            if (!$isImageUsed && file_exists('./assets/hr_files_upload/' . $file->filename)) {
                unlink('./assets/hr_files_upload/' . $file->filename);
            }
            $mSuccess .= "- $data->field berhasil dihapus <br>";
        }

        response(['status' => 'success', 'mError' => $mError, 'mSuccess' => $mSuccess]);
    }

    public function deleteTemp()
    {
        $this->Hr->truncate('files_temp');
        response(['status' => 'success', 'message' => 'Data sementara behasil dihapus']);
    }

    public function checkBeforeAddFile()
    {
        $post = fileGetContent();
        $id = $post->id;
        $isExist = false;
        if (!$id) {
            $check = $this->Hr->getOne('files', [
                'name' => $post->name,
            ]);
            if ($check) {
                $isExist = true;
            }
        } else {
            $file = $this->Hr->getDataById('files', $id);
            if ($file) {
                if ($file->name != $post->name) {
                    $check = $this->Hr->getOne('files', [
                        'name' => $post->name,
                    ]);
                    if ($check) {
                        $isExist = true;
                    }
                }
            } else {
                response(['status' => 'deleted']);
            }
        }

        if (!$isExist) {
            response(['status' => 'success']);
        } else {
            response(['status' => 'exist', 'message' => 'Data file sudah digunakan!']);
        }
    }

    public function moveToTemp()
    {
        $obj = fileGetContent();
        $id = $obj->id;

        $fileTemp = $this->Hr->countTable('files_temp');
        if($fileTemp > 0) response(['status' => 'failed', 'message' => 'Data smentara tidak kosong, silahkan bersihkan terlebih dahulu'], 400);

        $file = $this->Hr->getDataById('files', $id);
        if(!$file) response(['status' => 'failed', 'message' => 'File tidak ditemukan'], 400);

        if ($xlsx = SimpleXLSX::parse('./assets/hr_files_upload/' . $file->filename)) {
            $header_values = $rows = [];
            foreach ($xlsx->rows() as $k => $r) {
                $cleanR = [];
                foreach ($r as $item) {
                    $cleanR[] = str_replace("'", "", trim($item));
                }

                if ($k === 0) {
                    $header_values = $cleanR;
                    continue;
                }
                $rows[] = array_combine($header_values, $cleanR);
            }

            $antiDupe = [
                'npp' => [],
                'sap_id' => [],
                'nik' => [],
                'parent_nik' => [],
                'npwp' => [],
                'phone' => [],
                'mobile' => [],
                'email' => [],
            ];

            $data = [];

            foreach ($rows as $key => $value) {
                $validator = [
                    'religion' => ['Islam', 'Kristen', 'Katolik', 'Budha', 'Hindu', 'Konghucu', 'Penganut Kepercayaan'],
                    'os' => [
                        'PT. BANGUN SINAR INDONESIA',
                        'PT. INDOPSIKO INDONESIA',
                        'PT. KREASIBOGA PRIMATAMA',
                        'PT. SINERGI INTEGRA SERVICES',
                        'PT. ISS INDONESIA',
                        'PT. SINAR PRAPANCA',
                        'PT. PKSS',
                        'KOPERASI',
                        '-'
                    ],
                    'status' => ['PKWT', 'Kontrak OS', 'Permanen'],
                    'gender' => ['L' => 'Laki-Laki', 'P' => 'Perempuan'],
                ];
                
                $isValid = [];
                $isUpdate = false;

                if($value['npp'] == '') {
                    $isValid['npp']= 'NPP tidak boleh kosong';
                }  else {
                    $isUpdate = true;
                }

                if($value['email'] != '-' && !validateEmail($value['email'])) {
                    $isValid['email'] = 'Email tidak valid';
                } else {
                    $checkEmail = $this->Hr->getWhere('employees', ['email' => $value['email'], 'status !=' => 'DELETE'])->row();
                    if($checkEmail && $checkEmail->nip != $value['npp']) $isValid['email'] = "Email sudah digunakan";
                }

                if($value['sap_id'] != '-') {
                    $checkSapId = $this->Hr->getWhere('employees', ['sap_id' => $value['sap_id'], 'status !=' => 'DELETE'])->row();
                    if($checkSapId && $checkSapId->nip != $value['npp']) $isValid['sap_id'] = "ID SAP sudah digunakan";
                }

                if($value['nik'] != '-') {
                    $checkNik = $this->Hr->getWhere('employees', ['nik' => $value['nik'], 'status !=' => 'DELETE'])->row();
                    if($checkNik && $checkNik->nip != $value['npp']) $isValid['nik'] = "NIK sudah digunakan";
                }

                if($value['parent_nik'] != '-') {
                    $checkParentNik = $this->Hr->getWhere('employees', ['parent_nik' => $value['parent_nik'], 'status !=' => 'DELETE'])->row();
                    if($checkParentNik && $checkParentNik->nip != $value['npp']) $isValid['parent_nik'] = "KK sudah digunakan";
                }

                if($value['npwp'] != '-') {
                    $checkNpwp = $this->Hr->getWhere('employees', ['npwp' => $value['npwp'], 'status !=' => 'DELETE'])->row();
                    if($checkNpwp && $checkNpwp->nip != $value['npp']) $isValid['npwp'] = "NPWP sudah digunakan";
                }

                if($value['phone'] != '-') {
                    $checkPhone = $this->Hr->getWhere('employees', ['phone' => $value['phone'], 'status !=' => 'DELETE'])->row();
                    if($checkPhone && $checkPhone->nip != $value['npp']) $isValid['phone'] = "No. Telpon sudah digunakan";
                }

                if($value['mobile'] != '-') {
                    $checkHp = $this->Hr->getWhere('employees', ['mobile' => $value['mobile'], 'status !=' => 'DELETE'])->row();
                    if($checkHp && $checkHp->nip != $value['npp']) $isValid['mobile'] = "No. Handphone sudah digunakan";
                }

                if(in_array($value['npp'], $antiDupe['npp']) && $value['npp'] != '-') $isValid['npp'] = "NPP: $value[npp] duplikat";
                if(in_array($value['sap_id'], $antiDupe['sap_id']) && $value['sap_id'] != '-') $isValid['sap_id'] = "ID SAP: $value[sap_id] duplikat";
                if(in_array($value['nik'], $antiDupe['nik']) && $value['nik'] != '-') $isValid['nik'] = "NIK: $value[nik] duplikat";
                if(in_array($value['parent_nik'], $antiDupe['parent_nik']) && $value['parent_nik'] != '-') $isValid['parent_nik'] = "Nomor KK: $value[parent_nik] duplikat";
                if(in_array($value['npwp'], $antiDupe['npwp']) && $value['npwp'] != '-') $isValid['npwp'] = "NPWP: $value[npwp] duplikat";
                if(in_array($value['phone'], $antiDupe['phone']) && $value['phone'] != '-') $isValid['phone'] = "No. Telpon: $value[phone] duplikat";
                if(in_array($value['mobile'], $antiDupe['mobile']) && $value['mobile'] != '-') $isValid['mobile'] = "No. Handphone: $value[mobile] duplikat";
                if(in_array($value['email'], $antiDupe['email']) && $value['email'] != '-') $isValid['email'] = "Email: $value[email] duplikat";

                array_push($antiDupe['npp'], $value['npp']);
                array_push($antiDupe['sap_id'], $value['sap_id']);
                array_push($antiDupe['nik'], $value['nik']);
                array_push($antiDupe['parent_nik'], $value['parent_nik']);
                array_push($antiDupe['npwp'], $value['npwp']);
                array_push($antiDupe['phone'], $value['phone']);
                array_push($antiDupe['mobile'], $value['mobile']);
                array_push($antiDupe['email'], $value['email']);

                $age = 0;

                if($value['birth_date'] != "-") {
                    $expBirthDate = explode(" ", trim($value['birth_date']));
                    if(isset($expBirthDate[0])) {
                        if(!validateUploadDate($expBirthDate[0])) { 
                            $isValid['birth_date'] = 'BirthDate tidak valid';
                        } else {
                            $age = intval(countAge(date('Y-m-d', strtotime($expBirthDate[0]))));
                        }
                    } else {
                        $isValid['birth_date'] = 'BirthDate tidak valid';
                    }
                }
               
                if(!array_key_exists($value['gender'], $validator['gender'])) $isValid['gender'] = 'Hanya input gender L atu P';
                if(!in_array($value['employee_status'], $validator['status'])) $isValid['employee_status'] = 'Status Karyawan tidak terdaftar';                
                if(!in_array(ucwords(strtolower($value['religion'])), $validator['religion'])) $isValid['religion'] = 'Agama tidak terdaftar';                
                if(!in_array(strtoupper($value['os_name']), $validator['os'])) $isValid['os_name'] = 'Nama OS tidak terdaftar';
               
                $checkDept = $this->Hr->getDataById('departments', $value['department_id']);
                if(!$checkDept) $isValid['department_id'] = 'Sub Unit tidak valid';

                $checkSub = $this->Hr->getWhere('sub_departments', [
                    'id' => is_numeric($value['sub_department_id']) ? $value['sub_department_id'] : -1, 
                    'department_id' => is_numeric($value['department_id']) ? $value['department_id'] : -1
                ])->row();
                if(!$checkSub) $isValid['sub_department_id'] = 'Bagian tidak valid';

                $checkDiv = $this->Hr->getWhere('divisions', [
                    'id' => is_numeric($value['division_id']) ? $value['division_id'] : -1, 
                    'sub_department_id' => is_numeric($value['sub_department_id']) ? $value['sub_department_id'] : -1, 
                    'department_id' => is_numeric($value['department_id']) ? $value['department_id'] : -1
                ])->row();
                if(!$checkDiv) $isValid['division_id'] = 'Sub Bagian tidak valid';

                $checkRank = $this->Hr->getDataById('ranks', $value['rank_id']);
                if(!$checkRank) $isValid['rank_id'] = 'Jabatan tidak valid';
                
                if($value['overtime'] != '0' && $value['overtime'] != '1') $isValid['overtime'] = 'Hanya input overtime 0 atau 1';

                if(!is_numeric(floatval($value['basic_sallary']))) $isValid['basic_sallary'] = 'Gaji Pokok haruslah angka';
                if(!is_numeric(floatval($value['total_sallary']))) $isValid['total_sallary'] = 'Total Gaji haruslah angka';
                if(!is_numeric(floatval($value['premi_overtime']))) $isValid['premi_overtime'] = 'Premi Overtime haruslah angka';

                $checkSpv = $this->Hr->getWhereOr('employees', ['sap_id' => $value['direct_spv'], 'nip' => $value['direct_spv']])->row();

                $premi = 0;
                if($value['overtime'] == 1) {
                    if(is_numeric($value['premi_overtime'])) {
                        $premi = $value['premi_overtime'];
                    }
                } else if($value['overtime'] == 0) {
                    if(is_numeric($value['basic_sallary'])) {
                        $premi = ($value['basic_sallary'] / 173);
                    }
                }

                if(count($isValid) > 0) {
                    $isValidValue = serialize($isValid);
                } else {
                    if($isUpdate) {
                        $isValidValue = "Update";
                    } else {
                        serialize($isValid);
                    }
                }

                $data[] = [
                    'location' => empLoc(),
                    'nip' => $value['npp'],
                    'sap_id' => $value['sap_id'],
                    'parent_nik' => $value['parent_nik'],
                    'nik' => $value['nik'],
                    'npwp' => $value['npwp'],
                    'phone' => $value['phone'],
                    'address' => $value['address'],
                    'mobile' => $value['mobile'],
                    'direct_spv' => $checkSpv ? $checkSpv->nip : "",
                    
                    'employee_name' => strtoupper($value['employee_name']),
                    'birth_place' => ucwords(strtolower($value['birth_place'])),
                    'birth_date' => array_key_exists('birth_date', $isValid) || $value['birth_date'] == '-' ? '0000-00-00' : $value['birth_date'],
                    'age' => $age,
                    'gender' => array_key_exists('gender', $isValid) ? $value['gender'] : $validator['gender'][$value['gender']],
                    'religion' => ucwords(strtolower($value['religion'])),
                    'employee_status' => $value['employee_status'],
                    'os_name' => strtoupper($value['os_name']),
                    'email' => $value['email'],
                    'department_id' => array_key_exists('department_id', $isValid) ? -1 : $value['department_id'],
                    'sub_department_id' => array_key_exists('sub_department_id', $isValid) ? -1 : $value['sub_department_id'],
                    'division_id' => array_key_exists('division_id', $isValid) ? -1 : $value['division_id'],
                    'rank_id' => array_key_exists('rank_id', $isValid) ? -1 : $value['rank_id'],
                    'overtime' => array_key_exists('overtime', $isValid) ? -1 : $value['overtime'],

                    'basic_sallary' => array_key_exists('basic_sallary', $isValid) ? -1 : $value['basic_sallary'],
                    'total_sallary' => array_key_exists('total_sallary', $isValid) ? -1 : $value['total_sallary'],
                    'premi_overtime' => array_key_exists('premi_overtime', $isValid) ? -1 : $premi,

                    'invalid' => $isValidValue,

                    'created_by' => empId(),
                    'created_at' => date('Y-m-d H:i:s')
                ];
            }

            $createTemp = $this->Hr->createMultiple('files_temp', $data);
            if($createTemp) {
                $this->Hr->deleteById('files', $file->id);

                if(file_exists("./assets/hr_files_upload/$file->filename")) {
                    unlink("./assets/hr_files_upload/$file->filename");
                }

                response(['status' => 'success', 'message' => 'Success memindahkan file']);
            } else {
                response(['status' => 'failed', 'message' => 'Gagal memindahkan file'], 400);
            }
        } else {
            response(['status' => 'failed', 'message' => 'Gagal memindahkan file'], 400);
        }
    }

    public function saveToDatabase()
    {
        $temps = $this->Hr->getWhereOr('files_temp', ['invalid' => 'a:0:{}', 'invalid' => 'Update'])->result();
        
        $data = [];
        foreach ($temps as $temp) {
            $location = $this->Main->getWhere('locations', ['code' => $temp->location])->row();
            
            $checkEmp = $this->Hr->getWhere('employees', ['nip' => $temp->nip, 'status !=' => 'DELETE'])->row();
            if(!$checkEmp) {
                $data = [
                    'nip' => $temp->nip,
                    'location' => $temp->location,
                    'sap_id' => $temp->sap_id,
                    'parent_nik' => $temp->parent_nik,
                    'nik' => $temp->nik,
                    'npwp' => $temp->npwp,
                    'employee_name' => $temp->employee_name,
                    'birth_place' => $temp->birth_place,
                    'birth_date' => date('Y-m-d', strtotime($temp->birth_date)),
                    'gender' => $temp->gender,
                    'religion' => $temp->religion,
                    'age' => $temp->age,
                    'employee_status' => $temp->employee_status,
                    'os_name' => $temp->os_name,
                    'address' => $temp->address,
                    'phone' => $temp->phone,
                    'mobile' => $temp->mobile,
                    'email' => $temp->email,
                    'department_id' => $temp->department_id,
                    'sub_department_id' => $temp->sub_department_id,
                    'division_id' => $temp->division_id,
                    'rank_id' => $temp->rank_id,
                    'location_id' => $location->id,
                    'user_id' => 0,
                    'status' => 'ACTIVE',
                    'overtime' => $temp->overtime,
                    'direct_spv' => $temp->direct_spv,
                    'created_by' => empId(),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_by' => empId(),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];

                $this->Hr->create('employees', $data);
            } else {
                $data = [
                    'parent_nik' => $temp->parent_nik,
                    'nik' => $temp->nik,
                    'npwp' => $temp->npwp,
                    'employee_name' => $temp->employee_name,
                    'birth_place' => $temp->birth_place,
                    'birth_date' => date('Y-m-d', strtotime($temp->birth_date)),
                    'gender' => $temp->gender,
                    'religion' => $temp->religion,
                    'age' => $temp->age,
                    'employee_status' => $temp->employee_status,
                    'os_name' => $temp->os_name,
                    'address' => $temp->address,
                    'phone' => $temp->phone,
                    'mobile' => $temp->mobile,
                    'email' => $temp->email,
                    'department_id' => $temp->department_id,
                    'sub_department_id' => $temp->sub_department_id,
                    'division_id' => $temp->division_id,
                    'rank_id' => $temp->rank_id,
                    'location_id' => $location->id,
                    'status' => 'ACTIVE',
                    'overtime' => $temp->overtime,
                    'direct_spv' => $temp->direct_spv,
                    'updated_by' => empId(),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
        
                $this->Hr->updateById('employees', $data, $checkEmp->id);
            }
        }

        $empSallaries = $this->HrModel->getEmpWithSallary();
        $dataSallary = [];
        foreach ($empSallaries as $sallary) {
            $checkEmp = $this->Hr->getWhere('employees', ['nip' => $sallary->nip])->row();
            if($checkEmp) {
                $checkSallary = $this->Hr->getWhere('employee_sallary', ['emp_id' => $checkEmp->id])->row();
                if(!$checkSallary) {
                    $dataSallary = [
                        'emp_id' => $checkEmp->id,
                        'sap_id' => $checkEmp->sap_id,
                        'basic_sallary' => $sallary->basic_sallary,
                        'total_sallary' => $sallary->total_sallary,
                        'premi_overtime' => $sallary->premi_overtime,
                        'created_by' => empId(),
                        'updated_by' => empId(),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $this->Hr->create('employee_sallary', $dataSallary);
                } else {
                    $dataSallary = [
                        'basic_sallary' => $sallary->basic_sallary,
                        'total_sallary' => $sallary->total_sallary,
                        'premi_overtime' => $sallary->premi_overtime,
                        'updated_by' => empId(),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $this->Hr->updateById('employee_sallary', $dataSallary, $checkSallary->id);
                }
            }
        }

        $this->Hr->delete('files_temp', ['invalid' => 'a:0:{}', 'invalid' => 'Update']);
        response(['status' => 'success', 'message' => 'Berhasil import data karyawan ke database']);
    }

}