<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AttendanceController extends Erp_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('HrModel');
        $this->HrModel->myConstruct('hr');
        $this->load->model('AbsenModel');
        $this->AbsenModel->myConstruct('hr');
        $this->load->model('OvertimeModel', 'Overtime');
        $this->Overtime->myConstruct('hr');

        $this->load->model('api/MainModel', 'MainModel');
        $this->MainModel->myConstruct('main');

        $this->auth->isAuth();
    }

    public function getLeaves()
    {
        $leaves = $this->HrModel->getLeaves(getParam())->result();
        $xml = "";
        $no = 1;

        foreach ($leaves as $leave) {
            $color = "";

            if($leave->status == 'PROCESS') {
                $color = "bgColor='#efd898'";
                $id = "lv-" . $leave->id;
            } else  if($leave->status == 'APPROVED') {
                $color = "bgColor='#7ecbf1'";
                $id = "lv-" . $leave->id;
            } else {
                $id = "kl-" . $leave->id;
            }

            $xml .= "<row id='$id'>";
            $xml .= "<cell $color>" . cleanSC($no) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->employee_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->department_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->sub_department_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->division_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC(toIndoDateDay($leave->date_start)) . "</cell>";
            $xml .= "<cell $color>" . cleanSC(toIndoDateDay($leave->date_finish)) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->employee_leave_type_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->code) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->description) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->approver_name_1 != '' ? $leave->approver_name_1 : "-") . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->approver_name_2 ?? "-") . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->status) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->filename) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($leave->source_id == 0 ? "Kifest" : "Spekta") . "</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function getVisit()
    {
        $visits = $this->HrModel->getVisit(getParam())->result();
        $xml = "";
        $no = 1;
        foreach ($visits as $visit) {
            $xml .= "<row id='$visit->id'>";
            $xml .= "<cell>" . cleanSC($no) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->employee_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->department_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->sub_department_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->division_name) . "</cell>";
            $xml .= "<cell>" . cleanSC(toIndoDateDay($visit->visit_date)) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->visit_place_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->visit_description) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->visit_address) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->time_check_in) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->time_check_out) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->latitude) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->longitude) . "</cell>";
            $xml .= "<cell>" . cleanSC($visit->distance_text) . "</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function getInfoBoard()
    {
        $datas = $this->HrModel->getInfoBoard(getParam())->result();
        $xml = "";
        $no = 1;
        foreach ($datas as $data) {
            $xml .= "<row id='$data->id'>";
            $xml .= "<cell>" . cleanSC($no) . "</cell>";
            $xml .= "<cell>" . cleanSC(0) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->name) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->description) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->is_active ? 'Aktif' : 'Tidak Aktif') . "</cell>";
            $xml .= "<cell>" . cleanSC($data->created_by) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->updated_by) . "</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function infoForm()
    {
        $params = getParam();
        if (isset($params['id'])) {
            $info = $this->Hr->getDataById('information_board', $params['id']);
            $info->description = str_replace('</p><br/>', '</p>', $info->description);
            fetchFormData($info);
        } else {
            $post = getPost();
            $post['description'] = str_replace('</p>', '</p><br/>',$post['description']);
            $post['description'] = str_replace('&nbsp', '',$post['description']);

            if (!isset($post['id'])) {
                $this->createInfoForm($post);
            } else {
                $this->updateInfoForm($post);
            }
        }
    }
    
    public function checkBeforeAddFile()
    {
        $post = fileGetContent();
        $id = $post->id;
        $isExist = false;
        if (!$id) {
            $check = $this->Hr->getOne('information_board', [
                'name' => $post->name,
            ]);
            if ($check) {
                $isExist = true;
            }
        } else {
            $info = $this->Hr->getDataById('information_board', $id);
            if ($info) {
                if ($info->name != $post->name) {
                    $check = $this->Hr->getOne('information_board', [
                        'name' => $post->name,
                    ]);
                    if ($check) {
                        $isExist = true;
                    }
                }
            } else {
                response(['status' => 'deleted']);
            }
        }

        if (!$isExist) {
            response(['status' => 'success']);
        } else {
            response(['status' => 'exist', 'message' => 'Data informasi digunakan!']);
        }
    }

    public function createInfoForm($post)
    {
        $info = $this->Hr->getOne('information_board', ['name' => $post['name']]);
        isExist(["Nama info $post[name]" => $info]);

        $post['location'] = empLoc();
        $post['is_active'] = true;
        $post['created_by'] = empId();
        $post['updated_by'] = empId();
        $post['updated_at'] = date('Y-m-d H:i:s');
        $this->Hr->create('information_board', $post);
        xmlResponse('inserted', $post['name']);
    }

    public function updateInfoForm($post)
    {
        $info = $this->Hr->getOne('information_board', ['name' => $post['name'], 'id !=' => $post['id']]);
        isExist(["Nama info $post[name]" => $info]);

        $post['updated_by'] = empId();
        $post['updated_at'] = date('Y-m-d H:i:s');
        $this->Hr->updateById('information_board', $post, $post['id']);
        xmlResponse('updated', $post['name']);
    }

    public function activate()
    {
        $obj = fileGetContent();
        $infoIds = $obj->infoIds;
        $isActive = $obj->is_active;

        $data = [];
        foreach ($infoIds as $id) {
            $data[] = [
                'id' => $id,
                'is_active' => $isActive
            ];
        }

        if(count($data) > 0) $this->Hr->updateMultiple("information_board", $data, 'id');
        
        $message = $isActive == 1 ? 'Berhasil mengaktifkan informasi' : 'Berhasil menonaktifkan informasi';
        echo json_encode(['status' => 'success', 'message' => $message]);
    }

    public function infoDelete()
    {
        $post = fileGetContent();
        $mError = '';
        $mSuccess = '';
        $datas = $post->datas;
        foreach ($datas as $id => $data) {
            $info = $this->Hr->getDataById('information_board', $data->id);
            $this->Hr->delete('information_board', ['id' => $data->id]);
            if (file_exists('./assets/images/info/' . $info->filename)) {
                unlink('./assets/images/info/' . $info->filename);
            }
            $mSuccess .= "- $data->field berhasil dihapus <br>";
        }

        response(['status' => 'success', 'mError' => $mError, 'mSuccess' => $mSuccess]);
    }

    public function leaveForm()
    {
        $post = prettyText(getPost());
        $this->createLeaves($post);
    }

    public function createLeaves($post){
        $em = $this->Hr->getWhere('employees', ['nip' => $post['nip']])->row();
        $emp = $this->HrModel->getEmployeeById($em->id);
        
        if($emp) {
            $isExist = $this->HrModel->checkLeaveExist($post['date_start'], $post['date_finish'], $emp->nip);
            if($isExist) {
                xmlResponse('error', 'Cuti sudah ada!');
            } else {
                $code = "LV/" . empLoc() . "/$emp->id-" . date("d", strtotime($post['date_start'])) . "/" . toRomawi(date('m', strtotime($post['date_start']))) . "/" . date('Y', strtotime($post['date_start']));
                $data = [
                    'employee_npp' => $emp->nip,
                    'emp_id' => $emp->id,
                    'employee_leave_type_name' => $post['leave_name'],
                    'code' => $code,
                    'date_start' => date('Y-m-d', strtotime($post['date_start'])),
                    'date_finish' => date('Y-m-d', strtotime($post['date_finish'])),
                    'description' => $post['reason'],
                    'approver_nip_1' => "",
                    'approver_name_1' => "",
                    'approved_at_1' => "",
                    'rejected_at_1' => "",
                    'created_at' => date('Y-m-d H:i:s'),
                    'source_id' => 1,
                    'filename' => $post['filename'],
                    'status' => 'PROCESS'
                ];

                if($emp->rank_id >= 5) {
                    $isHaveAsman = $this->Hr->getOne('employees', ['sub_department_id' => $emp->sub_department_id], '*', ['rank_id' => ['3', '4']]);
                    $isHaveAsmanPLT = $this->Hr->getOne('employee_ranks', ['sub_department_id' => $emp->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                    
                    if ($emp->sub_department_id == 0 || (!$isHaveAsman && !$isHaveAsmanPLT)) {
                        $data['approver_nip_1'] = $emp->nip;
                        $data['approver_name_1'] = $emp->employee_name;
                        $data['approved_at_1'] = date('Y-m-d H:i:s');
                        $data['rejected_at_1'] = '0000-00-00 00:00:00';
                        $data['status'] = "APPROVED";

                        $leaveId = $this->Hr->create('kifest_leaves_temp', $data);
                        $this->leaveByPassEmail($this->Hr->getDataById('kifest_leaves_temp', $leaveId));
                    } else {
                        $asman = null;

                        if ($isHaveAsman) {
                            $asman = $isHaveAsman;
                        } else if ($isHaveAsmanPLT) {
                            $asman = $this->Hr->getDataById('employees', $isHaveAsmanPLT->emp_id);
                        }

                        if($asman) {
                            $data['approver_nip_1'] = $asman->nip;
                            $data['approver_name_1'] = $asman->employee_name;

                            $leaveId = $this->Hr->create('kifest_leaves_temp', $data);
                            $emailToken = time()."_LEAVE_REQUEST_".$asman->id;
                            $tokenApprove = simpleEncrypt("$leaveId:APPROVED:$emailToken");
                            $tokenReject = simpleEncrypt("$leaveId:REJECTED:$emailToken");
                            $linkApprove = LIVE_URL . "index.php?c=PublicController&m=approveLeave&token=$tokenApprove";
                            $linkReject = LIVE_URL . "index.php?c=PublicController&m=approveLeave&token=$tokenReject";

                            $linkApprove = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=positive&token=" . simpleEncrypt($linkApprove);
                            $linkReject = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=negative&token=" . simpleEncrypt($linkReject);

                            $message = $this->load->view('html/absen/email/leave_request', [
                                'emp' => $emp,
                                'leave' => $data,
                                'asman' => $asman,
                                'linkApprove' => $linkApprove,
                                'linkReject' => $linkReject,
                            ], true);

                            $emailData = [
                                'alert_name' => 'LEAVE_REQUEST',
                                'email_to' => $asman->email,
                                'subject' => "Approval Cuti $emp->employee_name pada tanggal " . toIndoDateDay($data['date_start']) . " sampai " . toIndoDateDay($data['date_finish']),
                                'subject_name' => "Spekta Alert: Approval Cuti $emp->employee_name pada tanggal " . toIndoDateDay($data['date_start']) . " sampai " . toIndoDateDay($data['date_finish']),
                                'message' => $message,
                                'emp_direct_apv' => $asman->id,
                                'token' => $emailToken,
                                'positive' => $linkApprove,
                                'negative' => $linkReject
                            ];

                            $this->Main->create('email', $emailData);
                        }
                    }
                } else {
                    $data['approver_nip_1'] = $emp->nip;
                    $data['approver_name_1'] = $emp->employee_name;
                    $data['approved_at_1'] = date('Y-m-d H:i:s');
                    $data['rejected_at_1'] = '0000-00-00 00:00:00';
                    $data['status'] = "APPROVED";

                    $leaveId = $this->Hr->create('kifest_leaves_temp', $data);
                    $this->leaveByPassEmail($this->Hr->getDataById('kifest_leaves_temp', $leaveId));
                }

                xmlResponse('inserted', 'Pengajuan cuti berhasil dikirim');
            }
        } else {
            xmlResponse('error', 'Karyawan tidak ditemukan!');
        }
    }

    public function leaveByPassEmail($leave) {
        $emp = $this->HrModel->getEmployeeById($leave->emp_id);
        $message = $this->load->view('html/absen/email/leave_request_hr', [
            'emp' => $emp,
            'leave' => $leave,
        ], true);

        $sdmEmails = "";
        $sdms = $this->Hr->getWhere('employees', ['sub_department_id' => 11])->result();
        foreach ($sdms as $sdm) {
            if($sdm->rank_id >= 4) {
                if($sdm->email != "" && validateEmail($sdm->email)) {
                    $sdmEmails .= $sdmEmails == "" ? $sdm->email : ",$sdm->email"; 
                }
            }
        }

        $emailData = [
            'alert_name' => 'LEAVE_REQUEST_HR',
            'email_to' => $sdmEmails,
            'subject' => "Approval Cuti $emp->employee_name pada tanggal " . toIndoDateDay($leave->date_start) . " sampai " . toIndoDateDay($leave->date_finish),
            'subject_name' => "Spekta Alert: Approval Cuti $emp->employee_name pada tanggal " . toIndoDateDay($leave->date_start) . " sampai " . toIndoDateDay($leave->date_finish),
            'message' => $message,
        ];

        $this->Main->create('email', $emailData);
    }


    public function correctionForm()
    {
        $params = getParam();
        if (isset($params['id'])) {
            $file = $this->Hr->getDataById('absen_correction', $params['id']);
            fetchFormData($file);
        } else {
            $post = prettyText(getPost());
            $this->createCorrectionForm($post);
        }
    }

    public function createCorrectionForm($post)
    {
        $labelInToDate = normalizeToIndoDateDay($post['label_in']);
        $labelOutToDate = normalizeToIndoDateDay($post['label_out']);
        $absenTable = "absen_" . date('Ym', strtotime($labelInToDate));

        $absens = $this->AbsenModel->getAbsensWithEmp($absenTable, $post['absen_ids']);
        $absenData = [];
        $emailData = [];
        $updateAbsen = [];

        foreach ($absens as $absen) {
            if($post['filename'] == '') {
                xmlResponse('error', 'File pendukung harus dilampirkan');
            }

            if($post['start_hour'] != '00' ||$post['start_minute'] != '00') {
                $correctionStartTime = date('Y-m-d H:i:s', strtotime("$labelInToDate $post[start_hour]:$post[start_minute]:00"));
            } else {
                $correctionStartTime = $absen->date_in ? $absen->date_in : '-';
            }

            if($post['end_hour'] != '00' || $post['end_minute'] != '00') {
                $correctionEndTime = date('Y-m-d H:i:s', strtotime("$labelOutToDate $post[end_hour]:$post[end_minute]:00"));
            } else {
                $correctionEndTime = $absen->date_out ? $absen->date_out : '-';
            }

            $data = [
                'shift_id' => $post['shift_id'],
                'absen_table' => $absenTable,
                'absen_id' => $absen->id,
                'reason' => $post['reason'],
                'filename' => $post['filename'],
                'created_by' => empId(),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_by' => empId(),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            if($correctionStartTime != '-') { 
                $data['correction_start_time'] = $correctionStartTime;
                $absen->correction_start_time = $correctionStartTime;
            }
            if($correctionEndTime != '-') {
                $data['correction_end_time'] = $correctionEndTime;
                $absen->correction_end_time = $correctionEndTime;
            }

            $absen->reason = $post['reason'];
 
            if($absen->rank_id >= 5) {
                $isHaveAsman = $this->Hr->getOne('employees', ['sub_department_id' => $absen->sub_department_id], '*', ['rank_id' => ['3', '4']]);
                $isHaveAsmanPLT = $this->Hr->getOne('employee_ranks', ['sub_department_id' => $absen->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                
                if ($absen->sub_department_id == 0 || (!$isHaveAsman && !$isHaveAsmanPLT)) {
                    $updateAbsen[] = [
                        'id' => $absen->id,
                        'correction_status' => 2,
                        'updated_by' => empId(),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];

                    $data['direct_approval_id'] = $absen->emp_id;
                    $data['direct_approval_time'] = date('Y-m-d H:i:s');
                    $data['direct_approval_status'] = "APPROVED";

                    $this->correctionByPassEmail($absen);
                } else {
                    $updateAbsen[] = [
                        'id' => $absen->id,
                        'correction_status' => 1,
                        'updated_by' => empId(),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];

                    $asman = null;

                    if ($isHaveAsman) {
                        $asman = $isHaveAsman;
                    } else if ($isHaveAsmanPLT) {
                        $asman = $this->Hr->getDataById('employees', $isHaveAsmanPLT->emp_id);
                    }

                    if($asman) {
                        $data['direct_approval_id'] = $asman->id;
                        $emailToken = time().'_ABSEN_CORRECTION_'.$asman->id;
                        $tokenApprove = simpleEncrypt("$absenTable:$absen->id:APPROVED:$emailToken");
                        $tokenReject = simpleEncrypt("$absenTable:$absen->id:REJECTED:$emailToken");
                        $linkApprove = LIVE_URL . "index.php?c=PublicController&m=approveAbsenCorrection&token=$tokenApprove";
                        $linkReject = LIVE_URL . "index.php?c=PublicController&m=approveAbsenCorrection&token=$tokenReject";

                        $linkApprove = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=positive&token=" . simpleEncrypt($linkApprove);
                        $linkReject = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=negative&token=" . simpleEncrypt($linkReject);

                        $message = $this->load->view('html/absen/email/approve_correction', [
                            'absen' => $absen,
                            'asman' => $asman,
                            'linkApprove' => $linkApprove,
                            'linkReject' => $linkReject,
                        ], true);

                        $emailData[] = [
                            'alert_name' => 'ABSEN_CORRECTION',
                            'email_to' => $asman->email,
                            'subject' => "Approval Koreksi Absen $absen->employee_name pada tanggal " . toIndoDateDay($absen->abs_date),
                            'subject_name' => "Spekta Alert: Approval Koreksi Absen $absen->employee_name pada tanggal " . toIndoDateDay($absen->abs_date),
                            'message' => $message,
                            'emp_direct_apv' => $asman->id,
                            'token' => $emailToken,
                            'positive' => $linkApprove,
                            'negative' => $linkReject
                        ];
                    }
                }
            } else {
                $updateAbsen[] = [
                    'id' => $absen->id,
                    'correction_status' => 2,
                    'updated_by' => empId(),
                    'updated_at' => date('Y-m-d H:i:s')
                ];

                $data['direct_approval_id'] = $absen->emp_id;
                $data['direct_approval_time'] = date('Y-m-d H:i:s');
                $data['direct_approval_status'] = "APPROVED";

                $this->correctionByPassEmail($absen);
            }

            $this->Hr->delete('absen_correction', ['absen_id' => $absen->id, 'absen_table' => $absenTable]);
            $absenData[] = $data;
        }
        
        if(count($absenData) > 0) $this->Hr->createMultiple('absen_correction', $absenData);
        if(count($updateAbsen) > 0) $this->Hr->updateMultiple($absenTable, $updateAbsen, 'id');
        if(count($emailData) > 0) $this->Main->createMultiple('email', $emailData);

        xmlResponse('inserted', 'Koreksi absen berhasil dikirim');
    }

    public function correctionByPassEmail($absen) {
        $message = $this->load->view('html/absen/email/approve_correction_hr', [
            'absen' => $absen,
        ], true);

        $sdmEmails = "";
        $sdms = $this->Hr->getWhere('employees', ['sub_department_id' => 11])->result();
        foreach ($sdms as $sdm) {
            if($sdm->rank_id >= 4) {
                if($sdm->email != "" && validateEmail($sdm->email)) {
                    $sdmEmails .= $sdmEmails == "" ? $sdm->email : ",$sdm->email"; 
                }
            }
        }

        $emailData = [
            'alert_name' => 'ABSEN_CORRECTION_HR',
            'email_to' => $sdmEmails,
            'subject' => "Approval Koreksi Absen $absen->employee_name pada tanggal " . toIndoDateDay($absen->abs_date),
            'subject_name' => "Spekta Alert: Approval Koreksi Absen $absen->employee_name pada tanggal " . toIndoDateDay($absen->abs_date),
            'message' => $message,
        ];

        $this->Main->create('email', $emailData);
    }

    public function cancelCorrection()
    {
        $post = fileGetContent();
        $absenIds = $post->absenIds;
        $month = $post->month;
        $year = $post->year;
        $absenTable = absenTable($year, $month);

        $absens = $this->AbsenModel->getAbsenCorrectionWithEmp($absenTable, implode(",", $absenIds));
        $absenData = [];  

        foreach ($absens as $absen) {
            if($absen->correction_status == 1 || $absen->correction_status == 2) {
                if($absen->filename != '') {
                    if(file_exists("./assets/hr_correction_upload/$absen->filename")) {
                        unlink("./assets/hr_correction_upload/$absen->filename");
                    }
                }

                $absenData[] = [
                    'id' => $absen->id,
                    'correction_status' => 5
                ];

                $this->Hr->update('absen_correction', [
                    'reject_reason' => 'Cancel by user',
                    'updated_by' => empId(), 
                    'updated_at' => date('Y-m-d H:i:s')
                ], [
                    'absen_table' => $absenTable, 
                    'absen_id' => $absen->id
                ]);
            }
        }

        if(count($absenData) > 0) $this->Hr->updateMultiple($absenTable, $absenData, 'id');

        response([
            'status' => 'success',
            'message' => 'Koreksi absen berhasil dihapus'
        ]);
    }

    public function approveLeave()
    {
        $post = fileGetContent();
        $id = explode("-", $post->id)[1];

        $leave = $this->Hr->getDataById('kifest_leaves_temp', $id);
        if(!$leave) {
            response([
                'status' => 'error',
                'message' => 'Cuti tidak ditemukan'
            ], 400);
        } else {
            $data = [
                'leave_id' => 0,
                'employee_id' => 0,
                'employee_npp' => $leave->employee_npp,
                'emp_id' => $leave->emp_id,
                'employee_leave_type_id' => 0,
                'employee_leave_type_name' => $leave->employee_leave_type_name,
                'code' => $leave->code,
                'date_start' => $leave->date_start,
                'date_finish' => $leave->date_finish,
                'description' => $leave->description,
                'approver_nip_1' => $leave->approver_nip_1,
                'approver_name_1' => $leave->approver_name_1,
                'approved_at_1' => $leave->approved_at_1,
                'rejected_at_1' => $leave->rejected_at_1,
                'approver_nip_2' => empNip(),
                'approver_name_2' => empName(),
                'approved_at_2' => date('Y-m-d H:i:s'),
                'rejected_at_2' => '0000-00-00 00:00:00',
                'created_at' => date('Y-m-d H:i:s'),
                'source_id' => $leave->source_id,
                'filename' => $leave->filename,
            ];

            $this->Hr->create('kifest_leaves', $data);
            $this->Hr->delete('kifest_leaves_temp', ['id' => $id]);

            $date1 = new DateTime($leave->date_start);
            $date2 = new DateTime($leave->date_finish);
            $interval = $date1->diff($date2);
            $dayBeetwen = $interval->format('%a');
            
            $absenTable = absenTable2('absen_' . date('Ym', strtotime($leave->date_start)));
            $this->Hr->update($absenTable, ['on_leave' => 1], ['abs_date' => $leave->date_start, 'emp_id' => $leave->emp_id]);

            for ($i=1; $i <= $dayBeetwen; $i++) { 
                $newDate = date('Y-m-d', strtotime($leave->date_start . " + $i days"));
                $absenTable = absenTable2('absen_' . date('Ym', strtotime($newDate)));

                $this->Hr->update($absenTable, ['on_leave' => 1], ['abs_date' => $newDate, 'emp_id' => $leave->emp_id]);
            }

            response([
                'status' => 'success',
                'message' => 'Approve curi berhasil'
            ]);
        }
    }

    public function rejectLeave()
    {
        $post = fileGetContent();
        $id = explode("-", $post->id)[1];

        $leave = $this->Hr->getWhere('kifest_leaves_temp', ['id' => $id])->row();
        if(!$leave) {
            response([
                'status' => 'error',
                'message' => 'Cuti tidak ditemukan'
            ], 400);
        } else {
            $this->Hr->update('kifest_leaves_temp', ['status' => 'REJECTED'], ['id' => $id]);
            response([
                'status' => 'success',
                'message' => 'Reject curi berhasil'
            ]);
        }
    }

    public function approveCorrection()
    {
        $post = fileGetContent();
        $absenIds = $post->absenIds;
        $month = $post->month;
        $year = $post->year;
        $absenTable = absenTable($year, $month);

        $absens = $this->AbsenModel->getAbsenCorrectionWithEmp($absenTable, implode(",", $absenIds));
        $absenData = [];        
        foreach ($absens as $absen) {
            if($absen->correction_status == 2 && $absen->payment_status != 'VERIFIED') {
                $data = [
                    'id' => $absen->id,
                    'correction_status' => 3
                ];

                if(isset($absen->correction_start_time) && $absen->correction_start_time != '0000-00-00 00:00:00') { 
                    $data['qr_code_in'] = time() . "-g98";
                    $data['date_in'] = $absen->correction_start_time;
                }

                if(isset($absen->correction_end_time) && $absen->correction_end_time != '0000-00-00 00:00:00') {
                    $data['qr_code_out'] = time() . "-g99";
                    $data['date_out'] = $absen->correction_end_time;
                }

                if(isset($absen->shift_id) && $absen->shift_id > 0) {
                    $shift = $this->Hr->getWhere('work_time', ['id' => $absen->shift_id])->row();
                    $data['shift_id'] = $shift->id;
                    $data['shift'] = $shift->name;

                    $shiftDate = date('Y-m-d', strtotime($absen->correction_start_time));
                    $timeIn = clockToFloat($shift->work_start);
                    $timeOut = clockToFloat($shift->work_end);

                    $startDate = date('Y-m-d H:i:s', strtotime($shiftDate . ' ' . $shift->work_start . ':00'));
                    $endDate = date('Y-m-d H:i:s', strtotime($shiftDate . ' ' . $shift->work_end . ':00'));

                    if($timeIn > $timeOut) {
                        $endDate = date('Y-m-d H:i:s', strtotime($shiftDate . ' ' . $shift->work_end . ':00' . ' +1 day'));
                    }
                    $data['sch_date_in'] = $startDate;
                    $data['sch_date_out'] = $endDate;
                }

                $absenData[] = $data;

                $correctionData[] = [
                    'id' => $absen->correction_id,
                    'hr_approval_id' => empId(),
                    'hr_approval_time' => date('Y-m-d H:i:s'),
                    'hr_approval_status' => 'APPROVED',
                    'updated_by' => empId(),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
        }
    
        if(count($absenData) > 0) $this->Hr->updateMultiple($absenTable, $absenData, 'id');
        if(count($correctionData) > 0) $this->Hr->updateMultiple('absen_correction', $correctionData, 'id');

        foreach ($absens as $absen) {
            $newAbsen = $this->Hr->getDataById($absenTable, $absen->id);

            $absDate = date('Y-m-d', strtotime($newAbsen->date_out));
            $overtime = $this->Hr->getWhere('employee_overtimes_detail', ['emp_id' => $newAbsen->emp_id, 'overtime_date' => $absDate])->row();

            if($overtime) {
                $time = clockToFloat(date("H:i:s", strtotime($overtime->start_date)));
                if($time > 6) {
                    $overtime = $this->Hr->getWhere('employee_overtimes_detail', ['emp_id' => $newAbsen->id, 'overtime_date' => $newAbsen->abs_date])->row();
                }

                if($overtime) {
                    $ovtIn = date('Y-m-d H:i:s', strtotime($overtime->start_date));
                    $ovtOut = date('Y-m-d H:i:s', strtotime($overtime->end_date));
                    
                    $startTime = $ovtIn;
                    $endTime = $ovtOut;
                    
                    $startTimeHour = date('H:i', strtotime($startTime));
                    $endTimeHour = date('H:i', strtotime($endTime));   
                                    
                    $recalculate = totalHour($overtime->emp_id, $startTime, $endTime, $startTimeHour, $endTimeHour);
        
                    $fixData = [
                        'id' => $overtime->id,
                        'updated_by' => empId(),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'effective_hour' => $recalculate['effective_hour'],
                        'break_hour' => $recalculate['break_hour'],
                        'real_hour' => $recalculate['real_hour'],
                        'overtime_hour' => $recalculate['overtime_hour'],
                        'premi_overtime' => $recalculate['premi_overtime'],
                        'overtime_value' => $recalculate['overtime_value'],
                        'total_meal' => $recalculate['total_meal'],
                        'deduction' => 0
                    ];

                    $this->Hr->updateById('employee_overtimes_detail', $fixData, $overtime->id);
                }
            }
          
            if($overtime && $overtime->payment_status != 'VERIFIED') {
                $ovtIn = date('Y-m-d H:i:s', strtotime($overtime->start_date));
                $ovtOut = date('Y-m-d H:i:s', strtotime($overtime->end_date));
                $dateIn = date('Y-m-d H:i:s', strtotime($newAbsen->date_in));
                $dateOut = date('Y-m-d H:i:s', strtotime($newAbsen->date_out));

                if($dateIn > $ovtIn || $dateOut < $ovtOut) {
                    $startTime = $ovtIn;
                    $endTime = $ovtOut;
                    
                    if($dateIn > $ovtIn) $startTime = fixingTime($dateIn, 'in');
                    if($dateOut < $ovtOut) $endTime = fixingTime($dateOut, 'out');
                    
                    $startTimeHour = date('H:i', strtotime($startTime));
                    $endTimeHour = date('H:i', strtotime($endTime));   
                                
                    $recalculate = totalHour($overtime->emp_id, $startTime, $endTime, $startTimeHour, $endTimeHour);
                    $deduction = ($overtime->overtime_value + $overtime->deduction) - $recalculate['overtime_value'];
                    $data = [
                        'id' => $overtime->id,
                        'updated_by' => empId(),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'status_day' => $recalculate['status_day'],
                        'effective_hour' => $recalculate['effective_hour'],
                        'break_hour' => $recalculate['break_hour'],
                        'real_hour' => $recalculate['real_hour'],
                        'overtime_hour' => $recalculate['overtime_hour'],
                        'premi_overtime' => $recalculate['premi_overtime'],
                        'overtime_value' => $recalculate['overtime_value'],
                        'total_meal' => $recalculate['total_meal'],
                        'deduction' => $deduction > 0 ? $deduction : 0
                    ];

                    $this->Hr->updateById('employee_overtimes_detail', $data, $overtime->id);
                }
            }
        }

        response([
            'status' => 'success',
            'message' => 'Koreksi absen berhasil disetujui'
        ]);
    }

    
    public function rejectAbsenForm()
    {
        $post = prettyText(getPost());
        $absenIds = explode(',', $post['absen_ids']);
        $reject_reason = $post['reject_reason'];
        $month = $post['month'];
        $year = $post['year'];
        $absenTable = absenTable($year, $month);

        $absens = $this->AbsenModel->getAbsenCorrectionWithEmp($absenTable, implode(",", $absenIds));
        $absenData = [];
        $absenCorrection = [];
        foreach ($absens as $absen) {
            if($absen->correction_status == 1 || $absen->correction_status == 2) {
                if($absen->filename != '') {
                    if(file_exists("./assets/hr_correction_upload/$absen->filename")) {
                        unlink("./assets/hr_correction_upload/$absen->filename");
                    }
                }

                $absenData[] = [
                    'id' => $absen->id,
                    'correction_status' => 4
                ];
                $absenCorrection[] = [
                    'absen_id' => $absen->id,
                    'absen_table' => $absenTable,
                    'reject_reason' => $reject_reason,
                ];
            }
        }

        if(count($absenData) > 0) $this->Hr->updateMultiple($absenTable, $absenData, 'id');
        foreach ($absenCorrection as $correction) {
            $this->Hr->update('absen_correction', 
                [
                    'reject_reason' => $correction['reject_reason'],
                    'hr_approval_id' =>  empId(),
                    'hr_approval_time' => date('Y-m-d H:i:s'),
                    'hr_approval_status' => 'REJECTED',
                    'updated_by' => empId(),
                    'updated_at' => date('Y-m-d H:i:s')
                ], 
                [
                    'absen_table' => $correction['absen_table'], 
                    'absen_id' => $correction['absen_id']
                ]
            );
        }

        xmlResponse('updated', 'Koreksi absen berhasil direject');
    }

    public function getOutoffice()
    {
        $datas = $this->HrModel->getOutoffice(getParam())->result();
        $xml = "";
        $no = 1;
        foreach ($datas as $data) {
            $start = date('Y-m-d H:i:s', strtotime($data->actual_in));
            $end = date('Y-m-d H:i:s', strtotime($data->end_time));

            $lateTime = 0;
            if($data->actual_in && $data->actual_in != "0000-00-00 00:00:00" && $start > $end ) {
                $lateTime = countHour($start, $end, 'h') * 60;
            }

            $color = "";
            if($data->status == 'APPROVED') {
                $color = "bgColor='#75b175'";
            } else if($data->status == 'REJECTED') {
                $color = "bgColor='#c94b62'";
            } else if($data->status == 'CANCELED') {
                $color = "bgColor='#dda94a'";
            }

            $xml .= "<row id='$data->id'>";
            $xml .= "<cell $color>" . cleanSC($no) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->employee_name) . "</cell>";
            $xml .= "<cell>" . cleanSC(toIndoDateDay($data->out_date)) . "</cell>";
            $xml .= "<cell>" . cleanSC(toIndoDateTime($data->start_time)) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->not_back ? "-" : toIndoDateTime($data->end_time)) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->actual_out && $data->actual_in != "0000-00-00 00:00:00"  ? toIndoDateTime($data->actual_out) : "-") . "</cell>";
            $xml .= "<cell>" . cleanSC($data->distance_in . " Meter") . "</cell>";
            $xml .= "<cell>" . cleanSC($data->actual_in && $data->actual_in != "0000-00-00 00:00:00" ? toIndoDateTime($data->actual_in) : "-") . "</cell>";
            $xml .= "<cell>" . cleanSC($data->distance_out . " Meter") . "</cell>";
            $xml .= "<cell>" . cleanSC($data->department_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->sub_department_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->division_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->first_apv_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->apv_status_1) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->second_apv_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->apv_status_2) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->status) . "</cell>";
            $xml .= "<cell>" . cleanSC($data->apv_date_2 && $data->apv_date_2 != "0000-00-00 00:00:00" ? toIndoDateTime($data->apv_date_2) : "-") . "</cell>";
            $xml .= "<cell>" . cleanSC($lateTime . " Menit") . "</cell>";
            $xml .= "<cell>" . cleanSC($data->deduction > 0 ? number_format($data->deduction, 2, ',', '.') : 0) . "</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function viewAttachment()
    {
        $post = fileGetContent();
        $expId = explode("-", $post->id);
        $type = $expId[0];
        $id = $expId[1];

        $leave = $this->Hr->getOne($type == 'lv' ? 'kifest_leaves_temp' : 'kifest_leaves', ['id' => $id]);
        
        if ($leave && $leave->filename) {
            $imgUrl = base_url('assets/leaves/' . $leave->filename);
            $template = "<a style='width:100%;height:100%;overflow: auto;' href='$imgUrl' target='_blank'>
                            <img src='$imgUrl'  style='width:100%;' />
                        </a>";
        } else {
            $imgUrl = base_url('public/img/no-image.png');
            $template = "<div style='width:100%;height:100%;display:flex;flex-direction:column;justify-content:center;align-items:center'>
                                <img style='width:120px;height:100px;' src='$imgUrl' />
                        </div>";
        }
        response([
            'status' => 'success',
            'template' => $template,
        ]);
    }
}