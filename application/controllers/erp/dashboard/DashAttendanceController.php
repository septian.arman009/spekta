<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DashAttendanceController extends Erp_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('DashboardModel', 'Dashboard');
        $this->Dashboard->myConstruct('hr');

        $this->auth->isAuth();
    }

    public function getSummaryAbsen()
    {
        $post = fileGetContent();
        $year = $post->year;
        $month = $post->month;
        
        $dayInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $absenTable = absenTable($year, $month);
        $absens = $this->Dashboard->getSummaryAbsen($absenTable);
        
        $data = [];
        $series = [];

        $inArray = [];
        $shiftArray = [];

        foreach ($absens['totalIn'] as $absen) {
            $d = getDay($absen->abs_date);
            $inArray[$d]['in'] = $absen->total;
        }

        foreach ($absens['totalShift'] as $absen) {
            $d = getDay($absen->abs_date);
            if(array_key_exists($d, $inArray)) {
                $data[$d]['in'] = $inArray[$d]['in'];
            } else {
                $data[$d]['in'] = 0;
            }
            $data[$d]['schedule'] = $absen->total;
        }

        $categories = [];
        for ($i = 1; $i <= $dayInMonth; $i++) {
            if (array_key_exists($i, $data)) {
                $series[] = floatval($data[$i]['in'] > 0 ? number_format(floatval($data[$i]['in']/$data[$i]['schedule'] * 100), 2, ".", ",") : 0);
            } else {
                $series[] = 0;
            }
            $categories[] = $i;
        }

        $graph = [
            [
                'name' => mToMonth($month) . ' ' . $year,
                'data' => $series,
            ],
        ];

        response(['status' => 'success', 'categories' => $categories, 'series' => $graph]);
    }

    public function getSummaryDonutAbsen()
    {
        $post = fileGetContent();
        $year = $post->year;
        $month = $post->month;
        
        $absenTable = absenTable($year, $month);
        $absens = $this->Dashboard->getSummaryAbsenRow($absenTable);

        $in = 0;
        $notIn = 0;
        $total = 0;

        $in += $absens['totalIn'];
        $notIn += $absens['totalShift'] - $absens['totalIn'];
        $total += $absens['totalShift'];

        $data[] = [
            'name' => 'Masuk',
            'y' => $in > 0 ? floatval($in/$total * 100) : 0,
            'sliced' => true,
            'selected' => true,
        ];

        $data[] = [
            'name' => 'Tidak Masuk',
            'y' => $notIn > 0 ? floatval($notIn/$total * 100) : 0,
        ];

        response(['status' => 'success', 'series' => $data]);
    }

    public function getAbsenSummaryGrid()
    {
        $param = [
            'month' => date('m') < 10 ? date('m') : date('m'),
            'year' => date('Y'),
            'day' => date('d')
        ];

        $absens = $this->HrModel->getAbsenList($param)->result();
        
        $xml = "";

        $shift1 = 0;
        $shift2 = 0;
        $shift3 = 0;
        $shift1In = 0;
        $shift2In = 0;
        $shift3In = 0;

        $ovtShift2 = 0;
        $ovtShift1 = 0;
        $ovtShift3 = 0;
        $ovtShift1In = 0;
        $ovtShift2In = 0;
        $ovtShift3In = 0;

        foreach ($absens as $absen) {
            if($absen->shift == 'Shift 1') {
                if($absen->date_in != '' && $absen->date_in != '0000-00-00 00:00:00') $shift1In++;
                $shift1++;
            } else if($absen->shift == 'Shift 2'){
                if($absen->date_in != '' && $absen->date_in != '0000-00-00 00:00:00') $shift2In++;
                $shift2++;
            } else if($absen->shift == 'Shift 3') {
                if($absen->date_in != '' && $absen->date_in != '0000-00-00 00:00:00') $shift3In++;
                $shift3++;
            } else if($absen->shift == 'Lembur Shift 1') {
                if($absen->date_in != '' && $absen->date_in != '0000-00-00 00:00:00') $ovtShift1In++;
                $ovtShift1++;
            } else if($absen->shift == 'Lembur Shift 2') {
                if($absen->date_in != '' && $absen->date_in != '0000-00-00 00:00:00') $ovtShift2In++;
                $ovtShift2++;
            } else if($absen->shift == 'Lembur Shift 3') {
                if($absen->date_in != '' && $absen->date_in != '0000-00-00 00:00:00') $ovtShift3In++;
                $ovtShift3++;
            } 
        }

        $date = date('Y-m-d', strtotime("$param[year]-$param[month]-$param[day]"));

        if(!checkWeekend($date) && !checkNationalDay($date)) {
            $overtimes = $this->HrModel->getOvertimeList($param)->result();
            foreach ($overtimes as $absen) {
                if($absen->shift == 'Shift 1') {
                    if($absen->date_in != '' && $absen->date_in != '0000-00-00 00:00:00') $ovtShift1In++;
                    $ovtShift1++;
                } else if($absen->shift == 'Shift 2') {
                    if($absen->date_in != '' && $absen->date_in != '0000-00-00 00:00:00') $ovtShift2In++;
                    $ovtShift2++;
                } else if($absen->shift == 'Shift 3') {
                    if($absen->date_in != '' && $absen->date_in != '0000-00-00 00:00:00') $ovtShift3In++;
                    $ovtShift3++;
                }
            }
        }
        
        $xml .= "<row id='1'>";
        $xml .= "<cell>". cleanSC(1) ."</cell>";
        $xml .= "<cell>". cleanSC('Shift 1') ."</cell>";
        $xml .= "<cell>". cleanSC("$shift1In / $shift1") ."</cell>";
        $xml .= "</row>";

        $xml .= "<row id='2'>";
        $xml .= "<cell>". cleanSC(2) ."</cell>";
        $xml .= "<cell>". cleanSC('Shift 2') ."</cell>";
        $xml .= "<cell>". cleanSC("$shift2In / $shift2") ."</cell>";
        $xml .= "</row>";

        $xml .= "<row id='3'>";
        $xml .= "<cell>". cleanSC(3) ."</cell>";
        $xml .= "<cell>". cleanSC('Shift 3') ."</cell>";
        $xml .= "<cell>". cleanSC("$shift3In / $shift3") ."</cell>";
        $xml .= "</row>";

        $xml .= "<row id='4'>";
        $xml .= "<cell>". cleanSC(4) ."</cell>";
        $xml .= "<cell>". cleanSC('Lembur Shift 1') ."</cell>";
        $xml .= "<cell>". cleanSC("$ovtShift1In / $ovtShift1") ."</cell>";
        $xml .= "</row>";

        $xml .= "<row id='5'>";
        $xml .= "<cell>". cleanSC(5) ."</cell>";
        $xml .= "<cell>". cleanSC('Lembur Shift 2') ."</cell>";
        $xml .= "<cell>". cleanSC("$ovtShift2In / $ovtShift2") ."</cell>";
        $xml .= "</row>";

        $xml .= "<row id='6'>";
        $xml .= "<cell>". cleanSC(6) ."</cell>";
        $xml .= "<cell>". cleanSC('Lembur Shift 3') ."</cell>";
        $xml .= "<cell>". cleanSC("$ovtShift3In / $ovtShift3") ."</cell>";
        $xml .= "</row>";
     
        gridXmlHeader($xml);
    }
}