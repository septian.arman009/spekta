<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MAbsen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("AbsenModel");
        $this->AbsenModel->myConstruct('hr');

        $this->load->model('HrModel');
        $this->HrModel->myConstruct('hr');

        $this->auth->isAuth();
    }

    public function getEmployees()
    {
        $emps = $this->HrModel->getEmployee(getParam())->result();
        $xml = "";
        $no = 1;
        foreach ($emps as $emp) {
            $xml .= "<row id='$emp->id'>";
            $xml .= "<cell>" . cleanSC($no) . "</cell>";
            $xml .= "<cell>0</cell>";
            $xml .= "<cell>" . cleanSC($emp->nip) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->employee_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->shift_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->work_start) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->work_end) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->dept_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->sub_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->division_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->rank_name) . "</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function getManageShiftEmp()
    {
        $emps = $this->HrModel->getEmployee(getParam())->result();
        $xml = "";
        $no = 1;
        foreach ($emps as $emp) {
            $xml .= "<row id='$emp->id'>";
            $xml .= "<cell>" . cleanSC($no) . "</cell>";
            $xml .= "<cell>0</cell>";
            $xml .= "<cell>" . cleanSC($emp->nip) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->employee_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->dept_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->sub_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->division_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($emp->rank_name) . "</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function getAbsenOfMonth()
    {
        $param = getParam();
        $year = $param['year'];
        $month = $param['month'];
        $empId = $param['empId'];

        $days = cal_days_in_month(CAL_GREGORIAN, intval($param['month']), intval($param['year']));

        $absenTable = absenTable($year, $month);
        $empSchedule = [];

        if ($param['empId'] > 0) {
            $schedules = $this->Hr->getLike(
                $absenTable,
                ['emp_id' => $empId],
                ['abs_date' => $year . '-' . stringMonth($month)]
            )->result();

            foreach ($schedules as $sch) {
                $empSchedule[$sch->abs_date] = [
                    'abs_date' => $sch->abs_date,
                    'emp_id' => $sch->emp_id,
                    'shift' => $sch->shift,
                    'skip_weekend' => $sch->skip_weekend,
                    'date_in' => $sch->date_in,
                    'date_out' => $sch->date_out,
                    'sch_date_in' => date('H:i:s', strtotime($sch->sch_date_in)),
                    'sch_date_out' => date('H:i:s', strtotime($sch->sch_date_out))
                ];
            }
        }

        $xml = "";
        for ($i = 1; $i <= $days; $i++) {
            $dayIndex = $i < 10 ? "0$i" : $i;
            $monthIndex = $month < 10 ? "0$month" : $month;

            $date = date('Y-m-d', strtotime("$year-$month-$i"));
            $dateIndex = date('Y-m-d', strtotime("$year-$monthIndex-$dayIndex"));

            $color = null;
            if (checkWeekend($date)) {
                $color = "bgColor='#efd898'";
            } else if (checkNationalDay($date)) {
                $color = "bgColor='#7ecbf1'";
            }

            $dateIn = '00:00:00';
            $dateOut = '00:00:00';
            $schIn = '00:00:00';
            $schOut = '00:00:00';
            $shift = '-';
            $status = 0;
            $isWeekEnd = 0;

            $currentDate = date('Y-m-d');
            if ($date < $currentDate) {
                $color = "bgColor='#ddd'";
                $status = 1;
            }

            if (array_key_exists($dateIndex, $empSchedule)) {
                $dateIn = $empSchedule[$dateIndex]['sch_date_in'];
                $dateOut = $empSchedule[$dateIndex]['sch_date_out'];
                $shift = $empSchedule[$dateIndex]['shift'];
                if ($empSchedule[$dateIndex]['date_in'] != null) {
                    $status = 1;
                    $color = "bgColor='#75b175'";
                }

                $schIn = $empSchedule[$dateIndex]['date_in'] ? date('H:i:s', strtotime($empSchedule[$dateIndex]['date_in'])) : $schIn;
                $schOut = $empSchedule[$dateIndex]['date_out'] ? date('H:i:s', strtotime($empSchedule[$dateIndex]['date_out'])) : $schOut;
            }

            if (checkWeekend($date)) {
                if (isset($empSchedule[$dateIndex])) {
                    $isWeekEnd = $empSchedule[$dateIndex]['skip_weekend'] == 0 ? 0 : 1;
                } else {
                    $isWeekEnd = 1;
                }
            }

            $xml .= "<row id='$dateIndex'>";
            $xml .= "<cell $color>" . cleanSC($i) . "</cell>";
            $xml .= "<cell $color>" . cleanSC(toIndoDateDay($date)) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($shift) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($dateIn) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($dateOut) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($schIn) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($schOut) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($status) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($isWeekEnd) . "</cell>";
            $xml .= "</row>";
        }

        gridXmlHeader($xml);
    }

    public function getShiftList()
    {
        $shifts = $this->AbsenModel->getMappedShiftGrid(getParam())->result();
        $shiftList = [];
        foreach ($shifts as $shift) {
            $shiftList['options'][] = [
                'value' => $shift->id,
                'text' => $shift->department_id == 0
                    ? $shift->name . " - Reguler ($shift->work_start - $shift->work_end)"
                    : $shift->name . " - $shift->sub_department ($shift->work_start - $shift->work_end)"
            ];
        }

        echo json_encode($shiftList);
    }

    public function shiftForm()
    {
        $params = getParam();
        if (isset($params['id'])) {
            $emp = $this->Hr->getDataById('employee_families', $params['id']);
            fetchFormData($emp);
        } else {
            $post = getPost();
            if (isset($post['id'])) {
                $this->updateWorkShift($post);
            } else {
                $this->createWorkShift($post);
            }
        }
    }

    public function updateWorkShift($post)
    {
        $date = date('Y-m-d', strtotime($post['id']));
        $empId = $post['empId'];

        $schedule = $this->Hr->getDataById('work_time', $post['shift_id']);

        $timeIn = clockToFloat($schedule->work_start);
        $timeOut = clockToFloat($schedule->work_end);

        $startDate = date('Y-m-d H:i:s', strtotime($date . ' ' . $schedule->work_start . ':00'));
        $endDate = date('Y-m-d H:i:s', strtotime($date . ' ' . $schedule->work_end . ':00'));

        if ($timeIn > $timeOut) {
            $endDate = date('Y-m-d H:i:s', strtotime($date . ' ' . $schedule->work_end . ':00' . ' +1 day'));
        }

        $data = [
            'location' => empLoc(),
            'emp_id' => $empId,
            'abs_date' => $date,
            'sch_date_in' => $startDate,
            'sch_date_out' => $endDate,
            'shift' => $schedule->name,
            'shift_id' => $schedule->id,
            'created_by' => empId(),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_by' => empId(),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $expDate = explode("-", $date);
        $absenTable = absenTable($expDate[0], $expDate[1]);
        $exist = $this->Hr->getOne($absenTable, ['emp_id' => $empId, 'abs_date' => $date]);

        $message = "Update shift kerja berhasil";
        if ($exist) {
            $this->Hr->updateById($absenTable, $data, $exist->id);
        } else {
            $this->Hr->create($absenTable, $data);
            $message = "Insert shift kerja berhasil";
        }

        xmlResponse('updated', $message);
    }

    public function createWorkShift($post)
    {
        $isWithWeekend = $post['is_with_weekend'] == '1';

        $begin = new DateTime($post['start_date']);
        $end = new DateTime($post['end_date']);
        $end->modify('+1 day');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $totalUpdate = 0;
        $totalInsert = 0;
        $empIds = explode(',', $post['employee']);

        $schedule = $this->Hr->getDataById('work_time', $post['shift_id']);
        $currentDate = new DateTime(date('Y-m-d'));

        foreach ($period as $dt) {

            $date = date('Y-m-d', strtotime($dt->format("l Y-m-d H:i:s\n")));
            $expDate = explode('-', $date);
            $absenTable = absenTable($expDate[0], $expDate[1]);
            $dtDate = new DateTime($date);

            $do = false;

            if ($isWithWeekend && !checkNationalDay($date) && $dtDate >= $currentDate) {
                $do = true;
            } else if (!checkWeekend($date) && !checkNationalDay($date) && $dtDate >= $currentDate) {
                $do = true;
            }

            if ($do) {
                $timeIn = clockToFloat($schedule->work_start);
                $timeOut = clockToFloat($schedule->work_end);

                $startDate = date('Y-m-d H:i:s', strtotime($date . ' ' . $schedule->work_start . ':00'));
                $endDate = date('Y-m-d H:i:s', strtotime($date . ' ' . $schedule->work_end . ':00'));

                if ($timeIn > $timeOut) {
                    $endDate = date('Y-m-d H:i:s', strtotime($date . ' ' . $schedule->work_end . ':00' . ' +1 day'));
                }

                foreach ($empIds as $empId) {
                    $emp = $this->Hr->getDataById('employees', $empId);

                    if ($emp->shift_status == 1) {
                        $data = [
                            'location' => empLoc(),
                            'emp_id' => $empId,
                            'abs_date' => $date,
                            'sch_date_in' => $startDate,
                            'sch_date_out' => $endDate,
                            'shift' => $schedule->name,
                            'shift_id' => $schedule->id,
                            'skip_weekend' => !$isWithWeekend ? 1 : 0,
                            'created_by' => empId(),
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_by' => empId(),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];

                        $exist = $this->Hr->getOne($absenTable, ['emp_id' => $empId, 'abs_date' => $date]);
                        if ($exist) {
                            $totalUpdate++;
                            $this->Hr->updateById($absenTable, $data, $exist->id);
                        } else {
                            $this->Hr->create($absenTable, $data);
                            $totalInsert++;
                        }
                    }
                }
            }
        }
        xmlResponse('inserted', "$totalUpdate Update, $totalInsert Insert");
    }

    public function updateNonShift()
    {
        $post = getPost();

        $empId = $post['empId'];
        $schedule = $this->Hr->getDataById('work_time', $post['shift_id']);

        $data = ['shift_id' => $schedule->id];

        $this->Hr->updateById('employees', $data, $empId);
        xmlResponse('updated', "Update shift kerja berhasil");
    }

    public function moveShift()
    {
        $obj = fileGetContent();
        $data = [];
        foreach ($obj->empIds as $empId) {
            $data[] = [
                'id' => $empId,
                'shift_status' => $obj->shift_status,
                'shift_id' => $obj->shift_status == 1 ? 0 : 1
            ];
        }

        $this->Hr->updateMultiple("employees", $data, 'id');

        if ($obj->shift_status == 0) {
            foreach ($obj->empIds as $empId) {
                $absenTable = 'absen_' . date('Ym');
                $checkTable = $this->Hr->checkTable('kf_hr', $absenTable);
                if ($checkTable) {
                    $this->Hr->deleteWhereIn($absenTable, [], ['abs_date >' => date('Y-m-d'), 'emp_id' => $empId]);
                }
            }
        }

        echo json_encode(['status' => 'success', 'message' => 'Memindahkan karyawan ke Non Shift berhasil']);
    }

    public function getAbsenList()
    {
        $param = getParam();
        $absens = $this->HrModel->getAbsenList($param)->result();

        $xml = "";
        $no = 1;
        foreach ($absens as $absen) {
            $color = "";

            $schIn = date('Y-m-d H:i:s', strtotime($absen->sch_date_in));
            $schOut = date('Y-m-d H:i:s', strtotime($absen->sch_date_out));
            $dateIn = $absen->date_in ? date('Y-m-d H:i:s', strtotime($absen->date_in)) : '-';
            $dateOut = $absen->date_out ? date('Y-m-d H:i:s', strtotime($absen->date_out)) : '-';

            $difHourDateIn = '-';
            $difHourDateOut = '-';

            if (checkWeekend($absen->abs_date)) {
                $color = "bgColor='#efd898'";
            } else if (checkNationalDay($absen->abs_date)) {
                $color = "bgColor='#7ecbf1'";
            }

            if ($dateIn != '-') {
                $difHourDateInCount = countHour($schIn, $dateIn, 'h-');
                if ($difHourDateInCount > 0) {
                    $color = "bgColor='#f5a06e'";
                    $difHourDateIn = normalHour($difHourDateInCount);
                }
            } else {
                $color = "bgColor='#f5a06e'";
            }

            if ($dateOut != '-') {
                $difHourDateOutCount = countHour($dateOut, $schOut, 'h-');
                if ($difHourDateOutCount > 0) {
                    $color = "bgColor='#f5a06e'";
                    $difHourDateOut = normalHour($difHourDateOutCount);
                }
            } else {
                $color = "bgColor='#f5a06e'";
            }

            if ($absen->on_leave == 1) {
                $color = "bgColor='#bce0de'";
            }

            if ($absen->visit_id > 1) {
                $color = "bgColor='#bfb3a8'";
            }

            $prod = '-';
            $unProd = '-';
            if ($dateIn != '-' && $dateOut != '-' && $absen->on_leave == 0) {
                $prodCount = countHour($dateIn, $dateOut, 'h');
                $prod = normalHour($prodCount > 4 ? ($prodCount - 1) : $prodCount);

                $schCount = countHour($schIn, $schOut, 'h');
                if ($dateOut > $schOut) {
                    $schCount = countHour($schIn, $dateOut, 'h');
                }

                $unProdCount = (($schCount - 1) - ($prodCount > 4 ? ($prodCount - 1) : $prodCount));
                if ($unProdCount > 0) {
                    $unProd = normalHour($unProdCount);
                }
            }

            $distance = "-";
            if ($absen->distance) {
                $distance = "IN: $absen->distance Meter";
            }

            if ($absen->distance_out) {
                $distance .= " ~ OUT: $absen->distance_out Meter";
            }

            $xml .= "<row id='$absen->id'>";
            $xml .= "<cell $color>" . cleanSC($no) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->nip) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->sap_id) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->employee_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->department_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->sub_department_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->division_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->employee_status) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->leave_name != '' ? $absen->shift . ' (' . $absen->leave_name . ')' : $absen->shift) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->abs_date ? toIndoDate($absen->abs_date) : '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->sch_date_in ? toIndoDateTime($absen->sch_date_in) : '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->sch_date_out ? toIndoDateTime($absen->sch_date_out) : '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->date_in ? toIndoDateTime($absen->date_in) : '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->date_out ? toIndoDateTime($absen->date_out) : '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($difHourDateIn) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($difHourDateOut) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($prod) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($unProd) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->gate ?: '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($distance) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->qr_code_in ?: '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->qr_code_out ?: '-') . "</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function getAbsenEmp()
    {
        $param = getParam();
        $absens = $this->HrModel->getAbsenList($param)->result();

        $empAbsens = [];

        $totalProd = 0;
        $totalUnProd = 0;
        $totalLate = 0;
        $totalEarly = 0;

        foreach ($absens as $absen) {
            $schIn = date('Y-m-d H:i:s', strtotime($absen->sch_date_in));
            $schOut = date('Y-m-d H:i:s', strtotime($absen->sch_date_out));
            $dateIn = $absen->date_in ? date('Y-m-d H:i:s', strtotime($absen->date_in)) : '-';
            $dateOut = $absen->date_out ? date('Y-m-d H:i:s', strtotime($absen->date_out)) : '-';

            $difHourDateIn = 0;
            $difHourDateOut = 0;
            $absIn = 0;
            $absNotIn = 0;

            if ($dateIn != '-') {
                $absIn = 1;
                $difHourDateInCount = countHour($schIn, $dateIn, 'h-');
                if ($difHourDateInCount > 0) $difHourDateIn = $difHourDateInCount;
            } else {
                $absNotIn = 1;
            }

            if ($dateOut != '-') {
                $difHourDateOutCount = countHour($dateOut, $schOut, 'h-');
                if ($difHourDateOutCount > 0) $difHourDateOut = $difHourDateOutCount;
            }

            $prod = 0;
            $unProd = 0;

            if ($dateIn != '-' && $dateOut != '-' && $absen->on_leave == 0) {
                $prodCount = countHour($dateIn, $dateOut, 'h');
                $prod = $prodCount > 4 ? ($prodCount - 1) : $prodCount;

                $schCount = countHour($schIn, $schOut, 'h');
                if ($dateOut > $schOut) {
                    $schCount = countHour($schIn, $dateOut, 'h');
                }

                $unProdCount = (($schCount - 1) - ($prodCount > 4 ? ($prodCount - 1) : $prodCount));
                if ($unProdCount > 0) {
                    $unProd = $unProdCount;
                }
            }

            if (!array_key_exists($absen->emp_id, $empAbsens)) {
                $empAbsens[$absen->emp_id] = [
                    'emp_id' => $absen->emp_id,
                    'npp' => $absen->nip,
                    'sap_id' => $absen->sap_id,
                    'employee_name' => $absen->employee_name,
                    'department_name' => $absen->department_name,
                    'sub_department_name' => $absen->sub_department_name,
                    'division_name' => $absen->division_name,
                    'on_leave' => $absen->on_leave == 1 ? 1 : 0,
                    'on_visit' => $absen->visit_id == 1 ? 1 : 0,
                    'abs_in' => $absIn,
                    'abs_notin' => $absNotIn,

                    'prod_hour' => $prod,
                    'prod_hour_text' => normalHour($prod),

                    'un_prod_hour' => $unProd,
                    'un_prod_hour_text' => normalHour($unProd),

                    'div_hour_date_in' => $difHourDateIn,
                    'div_hour_date_in_text' => normalHour($difHourDateIn),

                    'div_hour_date_out' => $difHourDateOut,
                    'div_hour_date_out_text' => normalHour($difHourDateOut),

                    'leave_sick' => $absen->leave_name == 'Cuti Sakit' ? 1 : 0
                ];

                $totalProd += $prod;
                $totalUnProd += $unProd;
                $totalLate += $difHourDateIn;
                $totalEarly += $difHourDateOut;
            } else {
                $empAbsens[$absen->emp_id]['on_leave'] += $absen->on_leave > 0 ? 1 : 0 - ($absen->leave_name == 'Cuti Sakit' ? 1 : 0);
                $empAbsens[$absen->emp_id]['on_visit'] += $absen->visit_id > 1 ? 1 : 0;
                $empAbsens[$absen->emp_id]['leave_sick'] += $absen->leave_name == 'Cuti Sakit' ? 1 : 0;
                $empAbsens[$absen->emp_id]['abs_in'] += $absIn;
                $empAbsens[$absen->emp_id]['abs_notin'] += $absNotIn;

                $empAbsens[$absen->emp_id]['prod_hour'] += $prod;
                $empAbsens[$absen->emp_id]['prod_hour_text'] = normalHour($empAbsens[$absen->emp_id]['prod_hour']);

                $empAbsens[$absen->emp_id]['un_prod_hour'] += $unProd;
                $empAbsens[$absen->emp_id]['un_prod_hour_text'] = normalHour($empAbsens[$absen->emp_id]['un_prod_hour']);

                $empAbsens[$absen->emp_id]['div_hour_date_in'] += $difHourDateIn;
                $empAbsens[$absen->emp_id]['div_hour_date_in_text'] = normalHour($empAbsens[$absen->emp_id]['div_hour_date_in']);

                $empAbsens[$absen->emp_id]['div_hour_date_out'] += $difHourDateOut;
                $empAbsens[$absen->emp_id]['div_hour_date_out_text'] = normalHour($empAbsens[$absen->emp_id]['div_hour_date_out']);

                $totalProd += $prod;
                $totalUnProd += $unProd;
                $totalLate += $difHourDateIn;
                $totalEarly += $difHourDateOut;
            }
        }

        $xml = "";
        $no = 1;
        foreach ($empAbsens as $absen) {
            $xml .= "<row id='$absen[emp_id]'>";
            $xml .= "<cell>" . cleanSC($no) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['npp']) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['sap_id']) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['employee_name']) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['department_name']) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['sub_department_name']) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['division_name']) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['abs_in']) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['abs_notin']) . "</cell>";
            $xml .= "<cell>" . cleanSC(number_format($absen['prod_hour'], 2, ',', '.')) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['prod_hour_text']) . "</cell>";
            $xml .= "<cell>" . cleanSC(number_format($absen['un_prod_hour'], 2, ',', '.')) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['un_prod_hour_text']) . "</cell>";
            $xml .= "<cell>" . cleanSC(number_format($absen['div_hour_date_in'], 2, ',', '.')) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['div_hour_date_in_text']) . "</cell>";
            $xml .= "<cell>" . cleanSC(number_format($absen['div_hour_date_out'], 2, ',', '.')) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['div_hour_date_out_text']) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['on_leave']) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['on_visit']) . "</cell>";
            $xml .= "<cell>" . cleanSC(normalHour($totalProd)) . "</cell>";
            $xml .= "<cell>" . cleanSC(normalHour($totalUnProd)) . "</cell>";
            $xml .= "<cell>" . cleanSC(normalHour($totalLate)) . "</cell>";
            $xml .= "<cell>" . cleanSC(normalHour($totalEarly)) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen['leave_sick']) . "</cell>";
            $xml .= "</row>";
            $no++;
        }

        gridXmlHeader($xml);
    }

    public function getAbsenPersonil()
    {
        $param = getParam();
        $absens = $this->HrModel->getAbsenList($param)->result();

        $absenPersonil = [];

        $totalProd = 0;
        $totalUnProd = 0;
        $totalLate = 0;
        $totalEarly = 0;

        foreach ($absens as $absen) {
            $absenPersonil[$absen->id] = [
                'id' => $absen->id,
                'npp' => $absen->nip,
                'sap_id' => $absen->sap_id,
                'employee_name' => $absen->employee_name,
                'department_name' => $absen->department_name,
                'sub_department_name' => $absen->sub_department_name,
                'division_name' => $absen->division_name,
                'shift' => $absen->leave_name != '' ? $absen->shift . ' (' . $absen->leave_name . ')' : $absen->shift,
                'abs_date' => $absen->abs_date,
                'sch_date_in' => $absen->sch_date_in,
                'sch_date_out' => $absen->sch_date_out,
                'date_in' => $absen->date_in,
                'date_out' => $absen->date_out,
                'gate' => $absen->gate,
                'qr_code_in' => $absen->qr_code_in,
                'qr_code_out' => $absen->qr_code_out,
                'on_leave' => $absen->on_leave > 0 ? 1 : 0,
                'visit_id' => $absen->visit_id > 0 ? 1 : 0,
                'leave_name' => $absen->leave_name
            ];

            $color = "";

            $schIn = date('Y-m-d H:i:s', strtotime($absen->sch_date_in));
            $schOut = date('Y-m-d H:i:s', strtotime($absen->sch_date_out));
            $dateIn = $absen->date_in ? date('Y-m-d H:i:s', strtotime($absen->date_in)) : '-';
            $dateOut = $absen->date_out ? date('Y-m-d H:i:s', strtotime($absen->date_out)) : '-';

            $difHourDateIn = '-';
            $difHourDateOut = '-';
            $countDifHourDateIn = 0;
            $countDifHourDateOut = 0;

            if (checkWeekend($absen->abs_date)) {
                $color = "bgColor='#efd898'";
            } else if (checkNationalDay($absen->abs_date)) {
                $color = "bgColor='#7ecbf1'";
            }

            if ($dateIn != '-') {
                $difHourDateInCount = countHour($schIn, $dateIn, 'h-');
                if ($difHourDateInCount > 0) {
                    $color = "bgColor='#f5a06e'";
                    $countDifHourDateIn = $difHourDateInCount;
                    $difHourDateIn = normalHour($difHourDateInCount);
                }
            } else {
                $color = "bgColor='#f5a06e'";
            }

            if ($dateOut != '-') {
                $difHourDateOutCount = countHour($dateOut, $schOut, 'h-');
                if ($difHourDateOutCount > 0) {
                    $color = "bgColor='#f5a06e'";
                    $countDifHourDateOut = $difHourDateOutCount;
                    $difHourDateOut = normalHour($difHourDateOutCount);
                }
            } else {
                $color = "bgColor='#f5a06e'";
            }

            if ($absen->on_leave == 1) {
                $color = "bgColor='#bce0de'";
            }

            if ($absen->visit_id > 1) {
                $color = "bgColor='#bfb3a8'";
            }

            $prod = '-';
            $unProd = '-';
            $countProd = 0;
            $countUnProd = 0;

            if ($dateIn != '-' && $dateOut != '-' && $absen->on_leave == 0) {
                $prodCount = countHour($dateIn, $dateOut, 'h');

                $countProd = $prodCount > 4 ? ($prodCount - 1) : $prodCount;
                $prod = normalHour($prodCount > 4 ? ($prodCount - 1) : $prodCount);

                $schCount = countHour($schIn, $schOut, 'h');
                if ($dateOut > $schOut) {
                    $schCount = countHour($schIn, $dateOut, 'h');
                }

                $unProdCount = (($schCount - 1) - ($prodCount > 4 ? ($prodCount - 1) : $prodCount));
                if ($unProdCount > 0) {
                    $countUnProd = $unProdCount;
                    $unProd = normalHour($unProdCount);
                }
            }

            $distance = "-";
            if ($absen->distance) {
                $distance = "IN: $absen->distance Meter";
            }

            if ($absen->distance_out) {
                $distance .= " ~ OUT: $absen->distance_out Meter";
            }

            $absenPersonil[$absen->id]['color'] = $color;
            $absenPersonil[$absen->id]['difHourDateIn'] = $difHourDateIn;
            $absenPersonil[$absen->id]['difHourDateOut'] = $difHourDateOut;
            $absenPersonil[$absen->id]['prod'] = $prod;
            $absenPersonil[$absen->id]['unProd'] = $unProd;
            $absenPersonil[$absen->id]['distance'] = $distance;
            $absenPersonil[$absen->id]['countDifHourDateIn'] = number_format($countDifHourDateIn, 2, ',', '.');
            $absenPersonil[$absen->id]['countDifHourDateOut'] = number_format($countDifHourDateOut, 2, ',', '.');
            $absenPersonil[$absen->id]['countProd'] = number_format($countProd, 2, ',', '.');
            $absenPersonil[$absen->id]['countUnProd'] = number_format($countUnProd, 2, ',', '.');

            $totalProd += $countProd;
            $totalUnProd += $countUnProd;
            $totalLate += $countDifHourDateIn;
            $totalEarly += $countDifHourDateOut;
        }

        $xml = "";
        $no = 1;
        foreach ($absenPersonil as $personil) {
            $xml .= "<row id='$personil[id]'>";
            $xml .= "<cell $personil[color]>" . cleanSC($no) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['npp']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['sap_id']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['employee_name']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['department_name']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['sub_department_name']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['division_name']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['shift']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['abs_date'] ? toIndoDate($personil['abs_date']) : '-') . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['sch_date_in'] ? toIndoDateTime($personil['sch_date_in']) : '-') . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['sch_date_out'] ? toIndoDateTime($personil['sch_date_out']) : '-') . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['date_in'] ? toIndoDateTime($personil['date_in']) : '-') . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['date_out'] ? toIndoDateTime($personil['date_out']) : '-') . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['difHourDateIn']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['difHourDateOut']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['prod']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['unProd']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['gate'] ?: '-') . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['distance']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['qr_code_in'] ?: '-') . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['qr_code_out'] ?: '-') . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['countDifHourDateIn'] ?: 0) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['countDifHourDateOut'] ?: 0) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['countProd'] ?: 0) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['countUnProd'] ?: 0) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC(normalHour($totalProd)) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC(normalHour($totalUnProd)) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC(normalHour($totalLate)) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC(normalHour($totalEarly)) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['on_leave']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['visit_id']) . "</cell>";
            $xml .= "<cell $personil[color]>" . cleanSC($personil['leave_name']) . "</cell>";
            $xml .= "</row>";
            $no++;
        }

        gridXmlHeader($xml);
    }

    public function getAbsenCorrections()
    {
        $params = getParam();
        $absens = $this->AbsenModel->getAbsenCorrections($params);
        $xml = "";
        $no = 1;

        foreach ($absens as $absen) {
            $status = 'ON PROCESS';
            $color = "";

            if ($absen->correction_status == 2) {
                $color = "bgColor='#efd898'";
                $status = 'APPROVED';
            } else if ($absen->correction_status == 3) {
                $status = 'COMPLETED';
                $color = "bgColor='#75b175'";
            } else if ($absen->correction_status == 4) {
                $status = 'REJECTED';
                $color = "bgColor='#c94b62'";
            } else if ($absen->correction_status == 5) {
                $status = 'CANCELED';
                $color = "bgColor='#dda94a'";
            }

            $xml .= "<row id='$absen->id'>";
            $xml .= "<cell $color>" . cleanSC($no) . "</cell>";
            $xml .= "<cell $color>0</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->employee_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->sub_department_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->division_name) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->shift) . "</cell>";
            $xml .= "<cell $color>" . cleanSC(toIndoDateDay($absen->abs_date)) . "</cell>";
            $xml .= "<cell $color>" . cleanSC(toIndoDateTime($absen->sch_date_in)) . "</cell>";
            $xml .= "<cell $color>" . cleanSC(toIndoDateTime($absen->sch_date_out)) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->date_in ? toIndoDateTime($absen->date_in) : '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->date_out ? toIndoDateTime($absen->date_out) : '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->correction_shift) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->correction_start_time && $absen->correction_start_time != '0000-00-00 00:00:00' ? toIndoDateTime($absen->correction_start_time) : '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->correction_end_time && $absen->correction_end_time != '0000-00-00 00:00:00' ? toIndoDateTime($absen->correction_end_time) : '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->reason ?: '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->reject_reason ?: '-') . "</cell>";
            $xml .= "<cell $color>" . cleanSC(toIndoDateTime($absen->created_at)) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($status) . "</cell>";
            $xml .= "<cell $color>" . cleanSC($absen->filename) . "</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function getAbsenForCorrections()
    {
        $params = getParam();
        $params['in_correction_status'] = "0,1,4,5";
        $params['equal_on_leave'] = 0;
        $params['equal_visit_id'] = 0;

        $shifts = $this->AbsenModel->getMappedShiftGrid(array())->result();
        $index = 0;
        $indexShift = [];
        foreach ($shifts as $shift) {
            $indexShift[$shift->id] = $index;
            $index++;
        }

        $empRank = empRank();
        $empSub = empSub();

        if ($empRank >= 5) {
            $params['customSearch'] = " AND b.sub_department_id = $empSub";
        }

        $absens = $this->HrModel->getAbsenList($params)->result();
        $xml = "";
        $no = 1;
        foreach ($absens as $absen) {
            $labelIn = toIndoDateDay(date('Y-m-d', strtotime($absen->sch_date_in)));
            $labelOut = toIndoDateDay(date('Y-m-d', strtotime($absen->sch_date_out)));

            $hourIn = $absen->date_in ? date("H", strtotime($absen->date_in)) : '00';
            $minuteIn = $absen->date_in ? date("i", strtotime($absen->date_in)) : '00';
            $hourOut = $absen->date_out ? date("H", strtotime($absen->date_out)) : '00';
            $minuteOut = $absen->date_out ? date("i", strtotime($absen->date_out)) : '00';

            $hourSchIn = date("H", strtotime($absen->sch_date_in));
            $minuteSchIn = date("i", strtotime($absen->sch_date_in));
            $hourSchOut = date("H", strtotime($absen->sch_date_out));
            $minuteSchOut = date("i", strtotime($absen->sch_date_out));

            $correctionStatus = "AVAILABLE";

            if ($absen->correction_status == 1) {
                $correctionStatus = "ON PROCESS";
            } else if ($absen->correction_status == 2) {
                $correctionStatus = "APPROVED";
            } else if ($absen->correction_status == 3) {
                $correctionStatus = "COMPLETED";
            } else if ($absen->correction_status == 4) {
                $correctionStatus = "REJECTED";
            } else if ($absen->correction_status == 5) {
                $correctionStatus = "CANCELED";
            }

            if ($absen->payment_status && $absen->payment_status == 'VERIFIED') {
                $correctionStatus = "OVERTIME VERIFIED";
            }

            $xml .= "<row id='$absen->id'>";
            $xml .= "<cell>" . cleanSC($no) . "</cell>";
            $xml .= "<cell>0</cell>"; //1
            $xml .= "<cell>" . cleanSC($absen->employee_name) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen->shift) . "</cell>";
            $xml .= "<cell>" . cleanSC(toIndoDateDay($absen->abs_date)) . "</cell>";
            $xml .= "<cell>" . cleanSC(toIndoDateTime($absen->sch_date_in)) . "</cell>";
            $xml .= "<cell>" . cleanSC(toIndoDateTime($absen->sch_date_out)) . "</cell>";
            $xml .= "<cell>" . cleanSC($absen->date_in ? toIndoDateTime($absen->date_in) : '-') . "</cell>";
            $xml .= "<cell>" . cleanSC($absen->date_out ? toIndoDateTime($absen->date_out) : '-') . "</cell>";

            $xml .= "<cell>" . cleanSC($correctionStatus) . "</cell>"; //9
            $xml .= "<cell>" . cleanSC($labelIn) . "</cell>"; //10
            $xml .= "<cell>" . cleanSC($hourIn) . "</cell>"; //11
            $xml .= "<cell>" . cleanSC($minuteIn) . "</cell>"; //12
            $xml .= "<cell>" . cleanSC($labelOut) . "</cell>"; //13
            $xml .= "<cell>" . cleanSC($hourOut) . "</cell>"; //14
            $xml .= "<cell>" . cleanSC($minuteOut) . "</cell>"; //15

            $xml .= "<cell>" . cleanSC($hourSchIn) . "</cell>"; //16
            $xml .= "<cell>" . cleanSC($minuteSchIn) . "</cell>"; //17
            $xml .= "<cell>" . cleanSC($hourSchOut) . "</cell>"; //18
            $xml .= "<cell>" . cleanSC($minuteSchOut) . "</cell>"; //19
            $xml .= "<cell>" . cleanSC(array_key_exists($absen->shift_id, $indexShift) ? $indexShift[$absen->shift_id] : -1) . "</cell>"; //20

            $xml .= "</row>";
            $no++;
        }

        gridXmlHeader($xml);
    }

    public function printAbsenPersonal()
    {
        $param = getParam();
        $param['status'] = 'ALL';
        $param['empStatus'] = 'ALL';

        if (!isset($param['start'])) {
            return $this->load->view('html/invalid_response', ['message' => "Tanggal mulai tidak di set"]);
        } else if (isset($param['start']) && !validateDate($param['start'])) {
            return $this->load->view('html/invalid_response', ['message' => "Format tanggal mulai tidak valid"]);
        }

        if (!isset($param['end'])) {
            return $this->load->view('html/invalid_response', ['message' => "Tanggal akhir tidak di set"]);
        } else if (isset($param['end']) && !validateDate($param['end'])) {
            return $this->load->view('html/invalid_response', ['message' => "Format tanggal akhir tidak valid"]);
        }

        if (!isset($param['deptId'])) {
            return $this->load->view('html/invalid_response', ['message' => "Minimal export filter adalah per department"]);
        }

        $startDate = date('d-m-Y', strtotime($param['start']));
        $endDate = date('d-m-Y', strtotime($param['end']));

        $deptId = isset($param['deptId']) && $param['deptId'] > 0 ? $param['deptId'] : "";
        $subDeptId = isset($param['subDeptId']) && $param['subDeptId'] > 0 ? $param['subDeptId'] : "";
        $divId = isset($param['divId']) && $param['divId'] > 0 ? $param['divId'] : "";

        $title = "Laporan Absen Harian";
        if ($deptId > 0) {
            $dept = $this->Hr->getWhere('departments', ['id' => $deptId])->row();
            if (!$dept) {
                return $this->load->view('html/invalid_response', ['message' => "ID Sub Unit tidak valid"]);
            }

            $title = "Laporan Absen Harian ($dept->name) (" . toShortDate($param['start']) . " - " . toShortDate($param['end']) . ")";
        }

        if ($subDeptId > 0) {
            $subDept = $this->Hr->getWhere('sub_departments', ['id' => $subDeptId])->row();
            if (!$subDept) {
                return $this->load->view('html/invalid_response', ['message' => "ID Bagian tidak valid"]);
            }
            $title = "Laporan Absen Harian ($subDept->name) (" . toShortDate($param['start']) . " - " . toShortDate($param['end']) . ")";
        }

        if ($divId > 0) {
            $div = $this->Hr->getWhere('divisions', ['id' => $divId])->row();
            if (!$div) {
                return $this->load->view('html/invalid_response', ['message' => "ID Sub Bagian tidak valid"]);
            }
            $title = "Laporan Absen Harian ($div->name) (" . toShortDate($param['start']) . " - " . toShortDate($param['end']) . ")";
        }

        $absens = $this->HrModel->getAbsenList($param)->result();

        $absenList = [];

        $no = 1;
        foreach ($absens as $absen) {
            $schIn = date('Y-m-d H:i:s', strtotime($absen->sch_date_in));
            $schOut = date('Y-m-d H:i:s', strtotime($absen->sch_date_out));
            $dateIn = $absen->date_in ? date('Y-m-d H:i:s', strtotime($absen->date_in)) : '-';
            $dateOut = $absen->date_out ? date('Y-m-d H:i:s', strtotime($absen->date_out)) : '-';

            $difHourDateIn = '-';
            $difHourDateOut = '-';

            if ($dateIn != '-') {
                $difHourDateInCount = countHour($schIn, $dateIn, 'h-');
                if ($difHourDateInCount > 0) {
                    $difHourDateIn = normalHour($difHourDateInCount);
                }
            }

            if ($dateOut != '-') {
                $difHourDateOutCount = countHour($dateOut, $schOut, 'h-');
                if ($difHourDateOutCount > 0) {
                    $difHourDateOut = normalHour($difHourDateOutCount);
                }
            }

            $prod = '-';
            $unProd = '-';
            $prodCount = 0;
            $unProdCount = 0;

            if ($dateIn != '-' && $dateOut != '-' && $absen->on_leave == 0) {
                $prodCount = countHour($dateIn, $dateOut, 'h');
                $prod = normalHourMobile($prodCount > 4 ? ($prodCount - 1) : $prodCount);

                $schCount = countHour($schIn, $schOut, 'h');
                if ($dateOut > $schOut) {
                    $schCount = countHour($schIn, $dateOut, 'h');
                }

                $unProdCount = (($schCount - 1) - ($prodCount > 4 ? ($prodCount - 1) : $prodCount));
                if ($unProdCount > 0) {
                    $unProd = normalHourMobile($unProdCount);
                }
            }

            $distance = "-";
            if ($absen->distance) {
                $distance = "IN: $absen->distance Meter";
            }

            if ($absen->distance_out) {
                $distance .= " ~ OUT: $absen->distance_out Meter";
            }

            $absenList[$absen->nip][] = [
                'npp' => $absen->nip,
                'sap_id' => $absen->sap_id,
                'employee_name' => $absen->employee_name,
                'department_name' => $absen->department_name,
                'sub_department_name' => $absen->sub_department_name,
                'division_name' => $absen->division_name,
                'employee_status' => $absen->employee_status,
                'shift' => $absen->leave_name != '' ? $absen->shift . ' (' . $absen->leave_name . ')' : $absen->shift,
                'abs_date' => $absen->abs_date ? toShortDate($absen->abs_date) : '-',
                'sch_date_in' => $absen->sch_date_in ? dateToClock($absen->sch_date_in) : '-',
                'sch_date_out' => $absen->sch_date_out ? dateToClock($absen->sch_date_out) : '-',
                'date_in' => $absen->date_in ? dateToClock($absen->date_in) : '-',
                'date_out' => $absen->date_out ? dateToClock($absen->date_out) : '-',
                'difHourDateIn' => $difHourDateIn,
                'difHourDateOut' => $difHourDateOut,
                'prod' => $prod,
                'unProd' => $unProd,
                'gate' => $absen->gate ?: '-',
                'distance' => $distance,
                'qr_code_in' => $absen->qr_code_in,
                'qr_code_out' => $absen->qr_code_out,
                'ovt_start_date' => $absen->ovt_start_date ? dateToClock($absen->ovt_start_date) : '-',
                'ovt_end_date' => $absen->ovt_start_date ? dateToClock($absen->ovt_end_date) : '-',
            ];

            $no++;
        }

        $this->load->view('html/absen/print/absen_personal', [
            'data' => $absenList,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'title' => $title
        ]);
    }
}
