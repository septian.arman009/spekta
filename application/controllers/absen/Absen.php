<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class Absen extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("AbsenModel");
        $this->AbsenModel->myConstruct('hr');

        $this->machineId4 = "c6f11756-3b79-4104-ae0e-2239e01bf0a7";
        $this->machineId2 = "7c1a6420-7a37-42b8-9d02-93f77637bfbd";
        $this->machineId3 = "26d00c50-e0d8-4dd9-952c-133c40f7d6aa";
        $this->machineId1 = "c3257773-707c-491a-8dd7-e882561a017b";
        $this->myadmin = "2202ee2a-2991-46bf-a0dc-552095068c12";
    }

    function gateInvalid() {
        $this->load->view('html/invalid_response', ['message' => 'Hardware tidak valid!']);
    }

    function gateCheck() {
        $post = fileGetContent();

        $allow = false;

        if($post->gate == "g1" && ($post->machineId == $this->machineId1 || $post->machineId == $this->myadmin)) {
            $allow = true;
        } else if($post->gate == "g2" && ($post->machineId == $this->machineId2 || $post->machineId == $this->myadmin)) {
            $allow = true;
        } else if($post->gate == "g3" && ($post->machineId == $this->machineId3 || $post->machineId == $this->myadmin)) {
            $allow = true;
        } else if($post->gate == "g4" && ($post->machineId == $this->machineId4|| $post->machineId == $this->myadmin)) {
            $allow = true;
        } else if($post->gate == "g5" && ($post->machineId == $this->machineId4|| $post->machineId == $this->myadmin)) {
            $allow = true;
        }

        if($allow) {
            response(['status' => 'success', 'message' => 'Hardware valid']);
        } else {
            response(['status' => 'error', 'link' => base_url() . '/index.php?d=absen&c=Absen&m=gateInvalid']);
        }
    }

    public function qrGate()
    {
        $params = getParam();
        if(isset($params['gate'])) {
            $gate = $this->Hr->getOne('gates', ['gate' => $params['gate']]);
            if($gate) {
                $this->load->view('html/absen/gate', ['gate' => $gate]);
            } else {
                $this->load->view('html/invalid_response', ['message' => 'Gate tidak ditemukan!']);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => 'Gate tidak valid!']);
        }
    }

    public function genQrCode()
    {
        $post = fileGetContent();
        $token = time().'-'.$post->gate;
        $gate = $this->Hr->getOne('gates', ['gate' => $post->gate]);

        $allow = false;

        if($post->gate == "g1" && ($post->machineId == $this->machineId1 || $post->machineId == $this->myadmin)) {
            $allow = true;
        } else if($post->gate == "g2" && ($post->machineId == $this->machineId2 || $post->machineId == $this->myadmin)) {
            $allow = true;
        } else if($post->gate == "g3" && ($post->machineId == $this->machineId3 || $post->machineId == $this->myadmin)) {
            $allow = true;
        } else if($post->gate == "g4" && ($post->machineId == $this->machineId4 || $post->machineId == $this->myadmin)) {
            $allow = true;
        } else if($post->gate == "g5" && ($post->machineId == $this->machineId4 || $post->machineId == $this->myadmin)) {
            $allow = true;
        }

        if(!$allow) {
            response(['status' => 'error', 'message' => 'Hardware tidak valid']);
        }
        
        if($gate) {
            $update = $this->Hr->update('gates', ['token' => $token, 'before_token' =>  $gate->token], ['gate' => $post->gate]);
            if($update) {
                
                // $writer = new PngWriter();
                // $qrCode = QrCode::create($token)
                //     ->setEncoding(new Encoding('UTF-8'))
                //     ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
                //     ->setSize(300)
                //     ->setMargin(10)
                //     ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
                //     ->setForegroundColor(new Color(0, 0, 0))
                //     ->setBackgroundColor(new Color(255, 255, 255));
                // $label = Label::create($gate->gate_name)->setTextColor(new Color(255, 0, 0));
                // $result = $writer->write($qrCode, null, $label);
                // $result->saveToFile("./assets/qr_absen/$post->gate.png");
                // $dataUri = $result->getDataUri();
                // $newQR = "<img src='$dataUri' alt='$gate->token'>";

                response(['status' => 'success', 'newQR' => $gate->token]);
            } else {
                response(['status' => 'error', 'message' => 'Terjadi kesalahan, silahkan refresh']);
            }
        } else {
            response(['status' => 'error', 'message' => 'Terjadi kesalahan, silahkan refresh']);
        }
    }

    public function getMenuList()
    {
        $menus = $this->General->getWhere('menus', ['is_active' => true])->result();
        $this->load->view('html/absen/menus', ['menus' => $menus]);
    }

    public function getMenuListGa()
    {
        $menus = $this->General->getWhere('menus', ['is_active' => true])->result();
        $this->load->view('html/absen/canteen/menus', ['menus' => $menus]);
    }

    public function getInfoList()
    {
        $infos = $this->Hr->getWhere('information_board', ['is_active' => true])->result();
        $this->load->view('html/absen/information_boards', ['informations' => $infos]);
    }

    public function getShiftGrid()
    {
        $shiftRegs = $this->AbsenModel->getShiftGridReguler(getParam())->result();
        $shifts = $this->AbsenModel->getShiftGrid(getParam())->result();
        $xml = "";
        $no = 1;
        foreach ($shiftRegs as $shift) {
            $workTime = $shift->work_start.' - '.$shift->work_end;
            $xml .= "<row id='$shift->id'>";
            $xml .= "<cell>". cleanSC($no) ."</cell>";
            $xml .= "<cell>". cleanSC('ALL') ."</cell>";
            $xml .= "<cell>". cleanSC('ALL') ."</cell>";
            $xml .= "<cell>". cleanSC('ALL') ."</cell>";
            $xml .= "<cell>". cleanSC($shift->name) ."</cell>";
            $xml .= "<cell>". cleanSC($workTime) ."</cell>";
            $xml .= "<cell>". cleanSC($shift->emp1) ."</cell>";
            $xml .= "<cell>". cleanSC($shift->emp2) ."</cell>";
            $xml .= "<cell>". cleanSC(toIndoDateTime($shift->created_at)) ."</cell>";
            $xml .= "</row>";
            $no++;
        }
        
        foreach ($shifts as $shift) {
            $workTime = $shift->work_start.' - '.$shift->work_end;
            $xml .= "<row id='$shift->id'>";
            $xml .= "<cell>". cleanSC($no) ."</cell>";
            $xml .= "<cell>". cleanSC($shift->department) ."</cell>";
            $xml .= "<cell>". cleanSC($shift->sub_department) ."</cell>";
            $xml .= "<cell>". cleanSC($shift->division) ."</cell>";
            $xml .= "<cell>". cleanSC($shift->name) ."</cell>";
            $xml .= "<cell>". cleanSC($workTime) ."</cell>";
            $xml .= "<cell>". cleanSC($shift->emp1) ."</cell>";
            $xml .= "<cell>". cleanSC($shift->emp2) ."</cell>";
            $xml .= "<cell>". cleanSC(toIndoDateTime($shift->created_at)) ."</cell>";
            $xml .= "</row>";
            $no++;
        }
        gridXmlHeader($xml);
    }

    public function shiftForm()
    {
        $params = getParam();
        if (isset($params['id'])) {
            $cat = $this->Hr->getDataById('work_time', $params['id']);
            fetchFormData($cat);
        } else {
            $post = prettyText(getPost(), ['name']);
            if (!isset($post['id'])) {
                $this->createWorkTime($post);
            } else {
                $this->updateWorkTime($post);
            }
        }
    }

    public function createWorkTime($post)
    {
        if($post['is_reguler'] != '1') {
            $time = $this->Hr->getWhere('work_time', [
                'name' => $post['name'], 
                'work_start' => $post['work_start'],
                'work_end' => $post['work_end'],
                'division_id' => $post['division_id'],
                'location' => empLoc()
            ])->row();
        } else {
            $time = $this->Hr->getWhere('work_time', [
                'name' => $post['name'], 
                'work_start' => $post['work_start'],
                'work_end' => $post['work_end'],
                'location' => empLoc()
            ])->row();
        }
       
        isExist(["Kategori $post[name]" => $time]);
        unset($post['is_reguler']);
        $post['location'] = empLoc();
        $post['created_by'] = empId();
        $post['updated_by'] = empId();
        $post['updated_at'] = date('Y-m-d H:i:s');
        $this->Hr->create('work_time', $post);
        xmlResponse('inserted', $post['name']);
    }

    public function updateWorkTime($post)
    {
     
        $time = $this->Hr->getOne('work_time', [
            'department_id' => $post['department_id'], 
            'sub_department_id' => $post['sub_department_id'], 
            'division_id' => $post['division_id'], 
            'name' => $post['name'], 
            'work_start' => $post['work_start'],
            'work_end' => $post['work_end'],
            'id !=' => $post['id'],
            'location' => empLoc()
        ]);
       
        isExist(["Kategori $post[name]" => $time]);
        
        $post['location'] = empLoc();
        $post['created_by'] = empId();
        $post['updated_by'] = empId();
        $post['updated_at'] = date('Y-m-d H:i:s');
        $this->Hr->updateById('work_time', $post, $post['id']);
        xmlResponse('updated', $post['name']);
    }

    public function shiftDelete()
    {
        $post = fileGetContent();
        $mError = '';
        $mSuccess = '';
        $datas = $post->datas;
        foreach ($datas as $data) {

            $used = $this->Hr->getWhere("employees", ['shift_id' => $data->id])->row();
            if(!$used) {
                $mSuccess .= "- $data->field berhasil dihapus <br>";
                $this->Hr->delete('work_time', ['id' => $data->id]);
            } else {
                $mError .= "- $data->field gagal dihapus karena sudah digunakan di manajemen non shift <br>";
            }
        }

        response(['status' => 'success', 'mError' => $mError, 'mSuccess' => $mSuccess]);
    }

}
