<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CronController extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('OvertimeModel', 'Overtime');
        $this->Overtime->myConstruct('hr');

        $this->load->model('api/CanteenModel', 'CanteenModel');
        $this->CanteenModel->myConstruct('general');

        $this->load->model('HrModel');
        $this->HrModel->myConstruct('hr');

        $this->load->model('api/MainModel', 'MainModel');
        $this->MainModel->myConstruct('main');
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=clearOutOffice
    public function clearOutOffice()
    {
        $currentDate = date('2023-10-03');
        $outs = $this->Hr->getWhere('out_office', [
            'type' => 1, 
            'not_back' => 0, 
            'distance_in' => 0, 
            'out_date' => $currentDate
        ])->result();

        foreach ($outs as $out) {
            $data = [
                'deduction' => 20000,
                'lat_in' => '-6.2000169',
                'lng_in' => '106.9099266',
                'distance' => 100,
                'update_by' => 1,
                'update_at' => date('Y-m-d H:i:s')
            ];

            $this->Hr->update('out_office', $data, ['id' => $out->id]);
        }
    }
    

    //@URL: http://localhost/spekta/index.php?c=CronController&m=sendEmail
    public function sendEmail()
    {
        $status = $this->Main->getDataById('email_send', 1)->status;
        if ($status == 'enable') {
            $emails = $this->MainModel->getEmails();
            
            $fcm_limit = $this->Main->getDataById('version', 1)->fcm_limit;

            foreach ($emails as $email) {
                $send = $this->sendmail->sendEmail($email->subject, $email->message, $email->email_to, $email->email_cc, $email->subject_name);
                if ($send) {
                    $data = [
                        'status' => 1,
                        'send_date' => date('Y-m-d H:i:s'),
                    ];
                    $this->Main->updateById('email', $data, $email->id);

                    if(
                        (
                            $email->alert_name == 'ABSEN_CORRECTION' ||
                            $email->alert_name == 'LEAVE_REQUEST' ||
                            $email->alert_name == 'OUT_OFFICE_#1' ||
                            $email->alert_name == 'OUT_OFFICE_#2' ||
                            $email->alert_name == 'OVERTIME_REVISION_REQUEST' ||
                            $email->alert_name == 'OVERTIME_APPROVEMENT' ||
                            $email->alert_name == 'TRIP_REQUEST_CONFIRMATION'
                        ) && 
                        $email->fcm_token != '' && 
                        $fcm_limit < 500
                    ) {
                        $this->MainModel->countFcm();

                        $title = 'Push Notification';
                        $body = 'Push Notification Spekta Mobile';

                        if($email->alert_name == "ABSEN_CORRECTION") {
                            $title = 'Permintaan Koreksi Absen';
                        } else if($email->alert_name == "LEAVE_REQUEST") {
                            $title = 'Permintaan Cuti(Leave)';
                        } else if($email->alert_name == "OUT_OFFICE_#1" || $email->alert_name == "OUT_OFFICE_#2") {
                            $title = 'Permintaan Izin Keluar Kantor';
                        } else if($email->alert_name == "OVERTIME_REVISION_REQUEST") {
                            $title = 'Permintaan Rervisi Lembur';
                        } else if($email->alert_name == "OVERTIME_APPROVEMENT") {
                            $title = 'Approval Lembur';
                        } else if($email->alert_name == "TRIP_REQUEST_CONFIRMATION"){
                            $title = 'Konfirmasi Perjalanan Dinas';
                        }
                        
                        $body = $email->subject;
                        $this->firebaselib->sendNotification($email->fcm_token, [
                                'title' => $title,
                                'body' => $body,
                                'payload' => [
                                    'alertName' => $email->alert_name,
                                    'emailTo' => $email->email_to,
                                    'subject' => $email->subject,
                                    'subjectName' => $email->subject_name,
                                    'apvStatus' => $email->apv_status == 0 ? "OPEN" : "CLOSED",
                                    'token' => $email->token,
                                    'createdAt' => toIndoDateTime($email->created_at),
                                    'positive' => $email->positive,
                                    'negative' => $email->negative
                            ]
                        ]);
                    }
                }
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=pushFcm
    public function pushFcm()
    {
        $status = $this->Main->getDataById('email_send', 1)->status;
        if ($status == 'enable') {
            $emails = $this->MainModel->getFcmEmails();
            
            $fcm_limit = $this->Main->getDataById('version', 1)->fcm_limit;

            foreach ($emails as $email) {
                if($email->dept_apv > 0 && $email->sub_dept_apv > 0 && $email->div_apv > 0 && $email->rank_apv) {
                    $emps = $this->MainModel->getEmployeeWithFcm($email->dept_apv, $email->sub_dept_apv, $email->div_apv, $email->rank_apv);
                } else if($email->dept_apv > 0 && $email->sub_dept_apv > 0 && $email->div_apv > 0) {
                    $emps = $this->MainModel->getEmployeeWithFcm($email->dept_apv, $email->sub_dept_apv, $email->div_apv);
                } else if($email->dept_apv > 0 && $email->sub_dept_apv > 0) {
                    $emps = $this->MainModel->getEmployeeWithFcm($email->dept_apv, $email->sub_dept_apv);
                }

                $fcmToken = [];
                foreach ($emps as $emp) {
                    $fcmToken[] = $emp->fcm_token;
                }
                
                if(
                    (
                        $email->alert_name == 'MEETING_NOTIFICATION' ||
                        $email->alert_name == 'TRIP_REQUEST_NOTIFICATION' ||
                        $email->alert_name == 'LEAVE_REQUEST_HR'
                    ) && 
                    count($fcmToken) > 0 && 
                    $fcm_limit < 500
                ) {
                    $this->MainModel->countFcm();

                    $title = 'Push Notification';
                    $body = 'Push Notification Spekta Mobile';

                    if($email->alert_name == "MEETING_NOTIFICATION") {
                        $title = 'Reservasi Ruang Meeting';
                    } else if($email->alert_name == "TRIP_REQUEST_NOTIFICATION") {
                        $title = 'Reservasi Kendaraan Dinas';
                    } else if($email->alert_name == "LEAVE_REQUEST_HR") {
                        $title = 'Permintaan Koreksi Absen';
                    }
                    
                    $body = $email->subject;
                    $this->firebaselib->sendMultiNotification($fcmToken, [
                            'title' => $title,
                            'body' => $body,
                            'payload' => [
                                'alertName' => $email->alert_name,
                                'emailTo' => $email->email_to,
                                'subject' => $email->subject,
                                'subjectName' => $email->subject_name,
                                'apvStatus' => $email->apv_status == 0 ? "OPEN" : "CLOSED",
                                'token' => $email->token,
                                'createdAt' => toIndoDateTime($email->created_at),
                                'positive' => $email->positive,
                                'negative' => $email->negative
                        ]
                    ]);

                    $this->Main->updateById('email', ['fcm_status' => 1], $email->id);
                } else {
                    $this->Main->updateById('email', ['fcm_status' => 1], $email->id);
                }
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=clearEmail
    public function clearEmail()
    {
        $last7Day = backDayToDate(date('Y-m-d'), 7);
        $this->Main->delete('email', ['created_at <' => $last7Day, 'status' => 1]);
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=updateStatusReservasi
    public function updateStatusReservasi()
    {
        $vehicles = $this->General->getWhere('vehicles_reservation', ['status' => 'APPROVED'])->result();
        $vhcData = [];
        foreach ($vehicles as $vhc) {
            $now = new DateTime(date('Y-m-d'));
            $exp = new DateTime(addDayToDate(date('Y-m-d', strtotime($vhc->start_date)), 1));
            if ($exp < $now) {
                $vhcData[] = [
                    'id' => $vhc->id,
                    'status' => 'CLOSED',
                ];
            }
        }
        if (count($vhcData) > 0) {
            $this->General->updateMultiple('vehicles_reservation', $vhcData, 'id');
        }

        $mrooms = $this->General->getWhere('meeting_rooms_reservation', ['status' => 'APPROVED'])->result();
        $rmData = [];
        foreach ($mrooms as $mroom) {
            $now = new DateTime(date('Y-m-d'));
            $exp = new DateTime(addDayToDate(date('Y-m-d', strtotime($mroom->start_date)), 1));
            if ($exp < $now) {
                $rmData[] = [
                    'id' => $mroom->id,
                    'status' => 'CLOSED',
                ];
            }
        }
        if (count($rmData) > 0) {
            $this->General->updateMultiple('meeting_rooms_reservation', $rmData, 'id');
        }
    }

    // @URL: http://localhost/spekta/index.php?c=CronController&m=autoAppvAsman
    public function autoAppvAsman()
    {
        $date = date('Y-m-d H:i:s');
        $overtimes = $this->Overtime->getAppvAsman(backDayToDate($date, 1));
        foreach ($overtimes as $overtime) {
            $asman = $this->Hr->getOne('employees', ['sub_department_id' => $overtime->sub_department_id], '*', ['rank_id' => ['3', '4']]);
            if ($asman) {
                $empNip = $asman->nip;
            } else {
                $asman = $this->Hr->getOne('employee_ranks', ['sub_department_id' => $overtime->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                $emp = $this->Hr->getDataById('employees', $asman->emp_id);
                $empNip = $emp->nip;
            }

            $data = [
                'apv_asman' => 'APPROVED',
                'apv_asman_nip' => $empNip,
                'apv_asman_date' => date('Y-m-d H:i:s'),
            ];

            $currDate = date('Y-m-d H:i:s');
            $newCurrDate = new DateTime($currDate);
            $ovtStartDate = new DateTime($overtime->start_date);
            if ($overtime->apv_ppic_nip == '-') {
                if ($overtime->apv_mgr_nip == '-') {
                    $data['apv_ppic_date'] = $newCurrDate < $ovtStartDate ? $overtime->start_date : $currDate;
                    $data['apv_mgr_date'] = $newCurrDate < $ovtStartDate ? $overtime->start_date : $currDate;
                } else {
                    $data['apv_ppic_date'] = $newCurrDate < $ovtStartDate ? $overtime->start_date : $currDate;
                }
            }

            $update = $this->Hr->update('employee_overtimes', $data, ['task_id' => $overtime->task_id]);
            if ($update) {
                if ($overtime->sub_department_id == 1 || $overtime->sub_department_id == 2 || $overtime->sub_department_id == 3 || $overtime->sub_department_id == 4 || $overtime->sub_department_id == 13) {
                    $isHavePPIC = $this->isHavePPIC($overtime);
                    if (!$isHavePPIC) {
                        $isHaveMgr = $this->isHaveMgr($overtime);
                        if (!$isHaveMgr) {
                            $this->isHaveHead($overtime);
                        }
                    }
                } else {
                    $isHaveMgr = $this->isHaveMgr($overtime);
                    if (!$isHaveMgr) {
                        $this->isHaveHead($overtime);
                    }
                }
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=autoRejectAsman
    public function autoRejectAsman()
    {
        $date = date('Y-m-d');
        $overtimes = $this->Overtime->getRejectAsman($date);
        foreach ($overtimes as $overtime) {
            $asman = $this->Hr->getOne('employees', ['sub_department_id' => $overtime->sub_department_id], '*', ['rank_id' => ['3', '4']]);
            if ($asman) {
                $empId = $asman->id;
                $empNip = $asman->nip;
            } else {
                $asman = $this->Hr->getOne('employee_ranks', ['sub_department_id' => $overtime->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                $emp = $this->Hr->getDataById('employees', $asman->emp_id);
                $empNip = $emp->nip;
                $empId = $emp->id;
            }

            $data = [
                'status' => 'REJECTED',
                'apv_asman' => 'REJECTED',
                'apv_asman_nip' => $empNip,
                'apv_asman_date' => date('Y-m-d H:i:s'),
            ];

            $update = $this->Hr->update('employee_overtimes', $data, ['task_id' => $overtime->task_id]);
            if ($update) {
                $dataDetail = [
                    'status' => 'REJECTED',
                    'status_by' => $empNip,
                    'updated_by' => $empId,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $this->Hr->update('employee_overtimes_detail', $dataDetail, ['task_id' => $overtime->task_id, 'status !=' => 'CANCELED']);
                $this->ovtlib->sendEmailReject('ASMAN', 'asman', $overtime, $overtime->task_id);
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=autoAppvPPIC
    public function autoAppvPPIC()
    {
        $date = date('Y-m-d H:i:s');
        $overtimes = $this->Overtime->getAppvPPIC(backHourToDate($date, 12));
        foreach ($overtimes as $overtime) {
            $asman = $this->Hr->getOne('employees', ['sub_department_id' => 9], '*', ['rank_id' => ['3', '4']]);
            if ($asman) {
                $empNip = $asman->nip;
            } else {
                $asman = $this->Hr->getOne('employee_ranks', ['sub_department_id' => 9, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                $emp = $this->Hr->getDataById('employees', $asman->emp_id);
                $empNip = $emp->nip;
            }

            $data = [
                'apv_ppic' => 'APPROVED',
                'apv_ppic_nip' => $empNip,
                'apv_ppic_date' => date('Y-m-d H:i:s'),
            ];

            $currDate = date('Y-m-d H:i:s');
            $newCurrDate = new DateTime($currDate);
            $ovtStartDate = new DateTime($overtime->start_date);
            if ($overtime->apv_mgr_nip == '-') {
                $data['apv_mgr_date'] = $newCurrDate < $ovtStartDate ? $overtime->start_date : $currDate;
            }

            $update = $this->Hr->update('employee_overtimes', $data, ['task_id' => $overtime->task_id]);
            if ($update) {
                $isHaveMgr = $this->isHaveMgr($overtime);
                if (!$isHaveMgr) {
                    $this->isHaveHead($overtime);
                }
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=autoAppvManager
    public function autoAppvManager()
    {
        $date = date('Y-m-d H:i:s');
        $overtimes = $this->Overtime->getAppvManager(backHourToDate($date, 12));
        foreach ($overtimes as $overtime) {
            $mgr = $this->Hr->getOne('employees', ['department_id' => $overtime->department_id, 'rank_id' => 2]);
            if ($mgr) {
                $empNip = $mgr->nip;
            } else {
                $mgr = $this->Hr->getOne('employee_ranks', ['department_id' => $overtime->department_id, 'status' => 'ACTIVE', 'rank_id' => 2]);
                $emp = $this->Hr->getDataById('employees', $mgr->emp_id);
                $empNip = $emp->nip;
            }

            $data = [
                'apv_mgr' => 'APPROVED',
                'apv_mgr_nip' => $empNip,
                'apv_mgr_date' => date('Y-m-d H:i:s'),
            ];
            $update = $this->Hr->update('employee_overtimes', $data, ['task_id' => $overtime->task_id]);
            if ($update) {
                $this->isHaveHead($overtime);
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=autoAppvHead
    public function autoAppvHead()
    {
        $date = date('Y-m-d H:i:s');
        $overtimes = $this->Overtime->getAppvHead(backHourToDate($date, 12));
        foreach ($overtimes as $overtime) {
            $head = $this->Hr->getOne('employees', ['rank_id' => 1]);
            if ($head) {
                $empId = $head->id;
                $empNip = $head->nip;
            } else {
                $head = $this->Hr->getOne('employee_ranks', ['status' => 'ACTIVE', 'rank_id' => 1]);
                $emp = $this->Hr->getDataById('employees', $head->emp_id);
                $empId = $emp->id;
                $empNip = $emp->nip;
            }

            $data = [
                'apv_head' => 'APPROVED',
                'apv_head_nip' => $empNip,
                'apv_head_date' => date('Y-m-d H:i:s'),
                'status' => 'CLOSED',
            ];
            $update = $this->Hr->update('employee_overtimes', $data, ['task_id' => $overtime->task_id]);
            if ($update) {
                $this->Hr->update('employee_overtimes_detail', [
                    'status' => 'CLOSED',
                    'updated_by' => $empId,
                    'updated_at' => date('Y-m-d H:i:s')],
                    ['task_id' => $overtime->task_id],
                    null,
                    ['status' => ['CANCELED', 'REJECTED']]);
            }
        }
    }

    public function isHavePPIC($overtime)
    {
        $isHavePPIC = $this->Hr->getOne('employees', ['sub_department_id' => 9, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
        $isHavePPICPLT = $this->Hr->getOne('employee_ranks', ['sub_department_id' => 9, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
        if ($overtime->apv_ppic_nip == '' && ($isHavePPIC || $isHavePPICPLT)) {
            if ($isHavePPIC) {
                $this->ovtlib->sendEmailAppv($isHavePPIC->email, 'PPIC', 'ppic', $overtime, $overtime->task_id);
                return true;
            } else if ($isHavePPICPLT) {
                $email = $this->Hr->getDataById('employees', $isHavePPIC->emp_id)->email;
                $this->ovtlib->sendEmailAppv($email, 'PPIC', 'ppic', $overtime, $overtime->task_id);
                return true;
            }
        } else {
            return false;
        }
    }

    public function isHaveMgr($overtime)
    {
        $isHaveMgr = $this->Hr->getOne('employees', ['department_id' => $overtime->department_id, 'rank_id' => 2, 'status' => 'ACTIVE']);
        $isHaveMgrPLT = $this->Hr->getOne('employee_ranks', ['department_id' => $overtime->department_id, 'rank_id' => 2, 'status' => 'ACTIVE']);
        if ($overtime->apv_mgr_nip == '' && ($isHaveMgr || $isHaveMgrPLT)) {
            if ($isHaveMgr) {
                $this->ovtlib->sendEmailAppv($isHaveMgr->email, 'Manager', 'mgr', $overtime, $overtime->task_id);
                return true;
            } else if ($isHaveMgrPLT) {
                $email = $this->Hr->getDataById('employees', $isHaveMgrPLT->emp_id)->email;
                $this->ovtlib->sendEmailAppv($email, 'Manager', 'mgr', $overtime, $overtime->task_id);
                return true;
            }
        } else {
            return false;
        }
    }

    public function isHaveHead($overtime)
    {
        $isHaveHead = $this->Hr->getOne('employees', ['rank_id' => 1, 'status' => 'ACTIVE']);
        $isHaveHeadPLT = $this->Hr->getOne('employees', ['rank_id' => 1, 'status' => 'ACTIVE']);
        if ($overtime->apv_head_nip == '' && ($isHaveHead || $isHaveHeadPLT)) {
            if ($isHaveHead) {
                $this->ovtlib->sendEmailAppv($isHaveHead->email, 'Plant Manager', 'head', $overtime, $overtime->task_id);
                return true;
            } else if ($isHaveHeadPLT) {
                $email = $this->Hr->getDataById('employees', $isHaveHeadPLT->emp_id)->email;
                $this->ovtlib->sendEmailAppv($email, 'Plant Manager', 'head', $overtime, $overtime->task_id);
                return true;
            }
        } else {
            return false;
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=alertEmpExp
    public function alertEmpExp()
    {
        $current = date('Y-m');
        $date = date('Y-m', strtotime("+1 months", strtotime($current)));
        $emps = $this->HrModel->getEmpNearExpired($date, 'KF-JKT');

        if(count($emps) > 0) {
            $location = $this->Main->getOne('locations', ['code' => 'KF-JKT'])->name;

            // SDM Notification
            $asmanSDM = $this->Hr->getOne('employees', ['sub_department_id' => 11, 'email !=' =>'', 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
            if(!$asmanSDM) {
                $isHaveAsmanPLT = $this->Hr->getOne('employee_ranks', ['sub_department_id' => 11, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
                if($isHaveAsmanPLT) {
                    $asmanSDM = $this->Hr->getOne('employees', ['id' => $isHaveAsmanPLT->emp_id, 'email !=' =>'']);
                }
            }
    
            if($asmanSDM) {
                $this->Main->create('email', [
                    'alert_name' => 'NEAR_EXPIRED_EMP_SDM',
                    'email_to' => $asmanSDM->email,
                    'subject' => "Alert Data Karyawan Habis Kontrak " . toIndoMonth($date),
                    'subject_name' => "Alert Data Karyawan Habis Kontrak " . toIndoMonth($date),
                    'message' => $this->load->view('html/hr/emp_near_expired', [
                        'emps' => $emps, 
                        'location' => $location, 
                        'date' => $date,
                        'sdm' => $asmanSDM
                    ], true),
                ]);
            }
    
            $emailSDM = $this->Hr->getWhere('employees', ['division_id' => 38], 'employee_name, email')->result();
            foreach ($emailSDM as $sdm) {
                $this->Main->create('email', [
                    'alert_name' => 'NEAR_EXPIRED_EMP_SDM',
                    'email_to' => $sdm->email,
                    'subject' => "Alert Data Karyawan Habis Kontrak " . toIndoMonth($date),
                    'subject_name' => "Alert Data Karyawan Habis Kontrak " . toIndoMonth($date),
                    'message' => $this->load->view('html/hr/emp_near_expired', [
                        'emps' => $emps, 
                        'location' => $location, 
                        'date' => $date,
                        'sdm' => $sdm
                    ], true),
                ]);
            }
            // END SDM Notification
    
            $depts = [];
            $divs = [];
            foreach ($emps as $emp) {
                $depts[$emp->sub_department_id] = $emp->sub_department_id;
                $divs[$emp->division_id] = $emp->division_id;
            }
            $asmans = $this->Hr->getWhere('employees', ['email !=' =>'', 'status' => 'ACTIVE'], 'employee_name, sub_department_id, email', null,null, ['rank_id' => ['3', '4'], 'sub_department_id' => $depts])->result();
            $spvs = $this->Hr->getWhere('employees', ['email !=' =>'', 'status' => 'ACTIVE'], 'employee_name, division_id, email', null,null, ['rank_id' => ['5', '6'], 'division_id' => $divs])->result();
            
            foreach ($asmans as $asman) {
                $this->Main->create('email', [
                    'alert_name' => 'NEAR_EXPIRED_EMP_ASMAN',
                    'email_to' => $asman->email,
                    'subject' => "Alert Data Karyawan Habis Kontrak " . toIndoMonth($date),
                    'subject_name' => "Alert Data Karyawan Habis Kontrak " . toIndoMonth($date),
                    'message' => $this->load->view('html/hr/emp_near_expired_asman', [
                        'emps' => $emps, 
                        'location' => $location, 
                        'date' => $date,
                        'employee' => $asman
                    ], true),
                ]);
            }
    
            foreach ($spvs as $spv) {
                $this->Main->create('email', [
                    'alert_name' => 'NEAR_EXPIRED_EMP_SPV',
                    'email_to' => $spv->email,
                    'subject' => "Alert Data Karyawan Habis Kontrak " . toIndoMonth($date),
                    'subject_name' => "Alert Data Karyawan Habis Kontrak " . toIndoMonth($date),
                    'message' => $this->load->view('html/hr/emp_near_expired_spv', [
                        'emps' => $emps, 
                        'location' => $location, 
                        'date' => $date,
                        'employee' => $spv
                    ], true),
                ]);
            }
    
            foreach ($emps as $emp) {
                if($emp->email) {
                    $this->Main->create('email', [
                        'alert_name' => 'NEAR_EXPIRED_EMP_PERSON',
                        'email_to' => $emp->email,
                        'subject' => "Alert Data Karyawan Habis Kontrak " . toIndoMonth($date),
                        'subject_name' => "Alert Data Karyawan Habis Kontrak " . toIndoMonth($date),
                        'message' => $this->load->view('html/hr/emp_expired', [
                            'location' => $location, 
                            'date' => $date,
                            'employee' => $emp
                        ], true),
                    ]);
                }
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=autoGenTableAbsen
    public function autoGenTableAbsen()
    {
        $date = explode('-', date('Y-m-d'));
        $year = $date[0];
        $month = $date[1];

        $this->Main->update('version', ['fcm_limit' => 0], ['id' => 1]);

        if ($month < 10) {
            $month = str_replace("0", "", $month);
            $month = "0$month";
        }

        if ($month == 12) {
            $year += 1;
            $month = "01";
        }

        $tableName = 'absen_'.$year.''.$month;
        $this->db->query("CREATE TABLE IF NOT EXISTS kf_hr.$tableName LIKE kf_hr.absen_master");
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=autoUpdateEmpAge
    public function autoUpdateEmpAge()
    {
        $this->HrModel->updateEmpAge();
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=setAbsenNonShift
    public function setAbsenNonShift()
    {
        $emps = $this->Hr->getWhere('employees', ['shift_status' => 0, 'status' => 'ACTIVE'])->result();

        $absenTable = 'absen_' . date('Ym');
        $checkTable = $this->Hr->checkTable('kf_hr', $absenTable);

        if (!$checkTable) {
            $this->db->query("CREATE TABLE IF NOT EXISTS kf_hr.$absenTable LIKE kf_hr.absen_master");
        }

        foreach ($emps as $emp) {
            if($emp->shift_id == 0) {
                $schedule = $this->Hr->getDataById('work_time', 1);
            } else {
                $schedule = $this->Hr->getDataById('work_time', $emp->shift_id);
            }

            $date = date('Y-m-d');

            $startDate = date('Y-m-d H:i:s', strtotime($date . ' ' . $schedule->work_start . ':00'));
            $endDate = date('Y-m-d H:i:s', strtotime($date . ' ' . $schedule->work_end . ':00'));

            if (!checkWeekend($date) && !checkNationalDay($date)) {
                $startDate = date('Y-m-d H:i:s', strtotime($date . ' ' . $schedule->work_start . ':00'));
                $endDate = date('Y-m-d H:i:s', strtotime($date . ' ' . $schedule->work_end . ':00'));

                $dataShift = [
                    'location' => $emp->location,
                    'emp_id' => $emp->id,
                    'abs_date' => $date,
                    'sch_date_in' => $startDate,
                    'sch_date_out' => $endDate,
                    'shift' => $schedule->name,
                    'shift_id' => $schedule->id,
                    'created_by' => $emp->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_by' => $emp->id,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $this->Hr->create($absenTable, $dataShift);
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=clearLunchTime
    public function clearLunchTime()
    {
        $currentDate = date('Y-m-d H:i:s');
        $config = $this->General->getDataById('canteen_cfg', 1);
        $canteens = $this->CanteenModel->getNotCloseCanteen();

        foreach ($canteens as $canteen) {
            $start = date('Y-m-d H:i:s', strtotime($canteen->take_date));

            $countMinute = countMinute(countHour($start, $currentDate, 'h'));
            if($countMinute > $config->time_to_eat) {
                $data = [
                    'out_date' => date('Y-m-d H:i:s', strtotime($canteen->take_date . " +$config->time_to_eat minutes")),
                    'duration' => $config->time_to_eat,
                    'status' => 'CLOSED'
                ];

                $this->General->updateById('canteen', $data, $canteen->id);
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=clearOvertimeLunchTime
    public function clearOvertimeLunchTime()
    {
        $currentDate = date('Y-m-d H:i:s');
        $config = $this->General->getDataById('canteen_cfg', 1);
        $canteens = $this->CanteenModel->getNotCloseCanteenOvertime();

        foreach ($canteens as $canteen) {
            $start = date('Y-m-d H:i:s', strtotime($canteen->take_date));

            $countMinute = countMinute(countHour($start, $currentDate, 'h'));
            if ($countMinute > $config->time_to_eat) {
                $data = [
                    'out_date' => date('Y-m-d H:i:s', strtotime($canteen->take_date . " +$config->time_to_eat minutes")),
                    'duration' => $config->time_to_eat,
                    'status' => 'CLOSED'
                ];

                $this->General->updateById('canteen_overtime', $data, $canteen->id);
            }
        }
    }

    public function getLeaves($absenTable, $employees, $yesterday)
    {
        $this->load->library('Guzzle_PHP_HTTP');
        $client = new \GuzzleHttp\Client();

        $nips = [];
        $nipsPer10 = [];
        $emps = [];

        foreach ($employees as $emp) {
            if(!array_key_exists($emp->nip, $emps)) {
                $emps[$emp->nip] = $emp->id;
                $nips[] = $emp->nip;
            }
        }

        $total = count($nips);
        $i = 1;
        $tmpNip = "";

        foreach ($nips as $nip) {
            $total--;

            if($i <= 300) {
                $tmpNip .= $tmpNip == "" ? $nip : ",$nip";
            }

            if($i == 300 && $total >= 300) {
                $nipsPer10[] = $tmpNip;
                $i = 1;
                $tmpNip = "";
            } else if($total == 0){
                $nipsPer10[] = $tmpNip;
                $i = 1;
                $tmpNip = "";
            }

            $i++;
        }

        if(count($nipsPer10) > 0) {
            foreach ($nipsPer10 as $nip_list) {
                $response = $client->request(
                    'GET', 
                    KIFEST_ENDPOINT_LEAVE, 
                    [
                        'headers' => [
                            'Accept' => 'application/json',
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer ' . KIFEST_TOKEN,
                        ],
                        'query' => [
                            'employee_npp' => $nip_list,
                            'date_start' => $yesterday,
                            'date_finish' => $yesterday
                        ]
                    ]
                );

                if($response->getStatusCode() == 200) {
                    $body = json_decode($response->getBody()->getContents());
                    foreach ($body->data as $data) {
                        $apvNip1 = "";
                        $apvName1 = "";
                        $apvAt1 = "";
                        $rejectAt1 = "";
        
                        $apvNip2 = "";
                        $apvName2 = "";
                        $apvAt2 = "";
                        $rejectAt2= "";
        
                        $no = 1;
                        foreach ($data->employee_leave_approvements as $apv) {
                            if($no <= 2) {
                                if($no == 1) {
                                    $apvNip1 = $apv->employee->code_npp;
                                    $apvName1 = $apv->employee->name;
                                    $apvAt1 = $apv->approved_at; 
                                    $rejectAt1 = $apv->rejected_at; 
                                } else if($no == 2) {
                                    $apvNip2 = $apv->employee->code_npp;
                                    $apvName2 = $apv->employee->name;
                                    $apvAt2 = $apv->approved_at; 
                                    $rejectAt2 = $apv->rejected_at; 
                                }
                            }
                            $no++;
                        }
        
                        if($rejectAt1 == '' && $rejectAt2 == '') {
                            if(array_key_exists($data->employee->code_npp, $emps)) {
                                $leaveData = [
                                    'leave_id' => $data->id,
                                    'employee_id' => $data->employee_id,
                                    'employee_npp' => $data->employee->code_npp,
                                    'emp_id' => $emps[$data->employee->code_npp],
                                    'employee_leave_type_id' => $data->employee_leave_type_id,
                                    'employee_leave_type_name' => $data->employee_leave_type->leave_type->name,
                                    'code' => $data->code,
                                    'date_start' => $data->date_start,
                                    'date_finish' => $data->date_finish,
                                    'description' => $data->description,
                                    'approver_nip_1' => $apvNip1,
                                    'approver_name_1' => $apvName1,
                                    'approved_at_1' => $apvAt1,
                                    'rejected_at_1' => $rejectAt1,
                                    'approver_nip_2' => $apvNip2,
                                    'approver_name_2' => $apvName2,
                                    'approved_at_2' => $apvAt2,
                                    'rejected_at_2' => $rejectAt2,
                                    'created_at' => date('Y-m-d H:i:s')
                                ];
            
                                $checkLeave = $this->Hr->getWhere('kifest_leaves', ['code' => $data->code, 'emp_id' => $emps[$data->employee->code_npp]])->row();
                                if(!$checkLeave) {
                                    $this->Hr->create('kifest_leaves', $leaveData);
                                    $this->Hr->update($absenTable, ['on_leave' => 1], ['abs_date' => $yesterday, 'emp_id' => $emps[$data->employee->code_npp]]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=updateLeavesManual
    public function updateLeavesManual()
    {
        $params = getParam();
        $date = date('Y-m-d', strtotime($params['date']));
        $expDate = explode('-', $date);
        $absenTable = absenTable($expDate[0], $expDate[1]);
        $yesterday = date('Y-m-d', strtotime($date . ' -1 days'));
        $absens = $this->HrModel->getNotPresent($absenTable, $yesterday);
        $this->getLeaves($absenTable, $absens, $yesterday);
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=updateLeaves
    public function updateLeaves()
    {
        $date = date('Y-m-d');
        $expDate = explode('-', $date);
        $absenTable = absenTable($expDate[0], $expDate[1]);
        $yesterday = date('Y-m-d', strtotime($date . ' -1 days'));
        $absens = $this->HrModel->getNotPresent($absenTable, $yesterday);
        $this->getLeaves($absenTable, $absens, $yesterday);
    }

    public function getVisit($absenTable, $employees, $yesterday)
    {
        $this->load->library('Guzzle_PHP_HTTP');
        $client = new \GuzzleHttp\Client();

        $nips = [];
        $totalNip = [];
        $emps = [];

        foreach ($employees as $emp) {
            if (!array_key_exists($emp->nip, $emps)) {
                $emps[$emp->nip] = $emp->id;
                $nips[] = $emp->nip;
            }
        }

        $total = count($nips);
        $i = 1;
        $tmpNip = "";

        foreach ($nips as $nip) {
            $total--;

            if ($i <= 300) {
                $tmpNip .= $tmpNip == "" ? $nip : ",$nip";
            }

            if ($i == 300 && $total >= 300) {
                $totalNip[] = $tmpNip;
                $i = 1;
                $tmpNip = "";
            } else if ($total == 0) {
                $totalNip[] = $tmpNip;
                $i = 1;
                $tmpNip = "";
            }

            $i++;
        }

        if (count($totalNip) > 0) {
            foreach ($totalNip as $nip_list) {
                $response = $client->request(
                    'GET',
                    KIFEST_ENDPOINT_VISIT,
                    [
                        'headers' => [
                            'Accept' => 'application/json',
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer ' . KIFEST_TOKEN,
                        ],
                        'query' => [
                            'employee_npp' => $nip_list,
                            'date_start' => $yesterday,
                            'date_finish' => $yesterday
                        ]
                    ]
                );

                
                if ($response->getStatusCode() == 200) {
                    $body = json_decode($response->getBody()->getContents());
                    
                    foreach ($body->data as $data) {
                        $visitDate = date('Y-m-d', strtotime($data->visit_date));

                        $visitData = [
                            'employee_sap' => $data->code_sap,
                            'employee_npp' => $data->code_npp,
                            'emp_id' => $emps[$data->code_npp],
                            'visit_date' => date('Y-m-d', strtotime($data->visit_date)),
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ];

                        if(isset($data->visit_places[0])) {
                            $place = $data->visit_places[0];
                            $visitData['visit_place_name'] = $place->visit_place_name;
                            $visitData['visit_description'] = $place->visit_description;
                            $visitData['visit_address'] = $place->visit_address;
                            $visitData['time_check_in'] = $place->time_check_in;
                            $visitData['time_check_out'] = $place->time_check_out;
                            $visitData['photo'] = $place->photo;
                            $visitData['latitude'] = $place->latitude;
                            $visitData['longitude'] = $place->longitude;
                            $visitData['distance'] = $place->distance;
                            $visitData['distance_text'] = $place->distance_text;
                        }
                        
                        $checkVisit = $this->Hr->getWhere('kifest_visit', ['visit_date' => $visitDate, 'emp_id' => $emps[$data->code_npp]])->row();

                        if(!$checkVisit) {
                            $visit_id = $this->Hr->create('kifest_visit', $visitData);
                            $absen = $this->Hr->getWhere($absenTable, ['abs_date' => $yesterday, 'emp_id' => $emps[$data->code_npp]])->row();
                            $dataAbsen = [
                                'visit_id' => $visit_id,
                                'date_in' => $absen->sch_date_in,
                                'date_out' => $absen->sch_date_out,
                                'gate' => 'Visit',
                                'distance' => $place->distance,
                                'lat' => $place->latitude,
                                'lng' => $place->longitude,
                                'distance_out' => $place->distance,
                                'lat_out' => $place->latitude,
                                'lng_out' => $place->longitude,
                            ];
                            $this->Hr->update($absenTable, $dataAbsen, ['abs_date' => $yesterday, 'emp_id' => $emps[$data->code_npp]]);
                        }
                    }
                }
            }
        }
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=updateVisitOfficeManual
    function updateVisitOfficeManual()
    {
        $params = getParam();

        $date = date('Y-m-d', strtotime($params['date']));
        $expDate = explode('-', $date);
        $absenTable = absenTable($expDate[0], $expDate[1]);
        $yesterday = date('Y-m-d', strtotime($date . ' -1 days'));
        $absens = $this->HrModel->getNotPresent($absenTable, $yesterday);
        $this->getVisit($absenTable, $absens, $yesterday);
    }

    //@URL: http://localhost/spekta/index.php?c=CronController&m=updateVisitOffice
    function updateVisitOffice()
    {
        $date = date('Y-m-d');
        $expDate = explode('-', $date);
        $absenTable = absenTable($expDate[0], $expDate[1]);
        $yesterday = date('Y-m-d', strtotime($date . ' -1 days'));
        $absens = $this->HrModel->getNotPresent($absenTable, $yesterday);
        $this->getVisit($absenTable, $absens, $yesterday);
    }
}
