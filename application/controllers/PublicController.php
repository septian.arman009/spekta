<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PublicController extends Erp_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('OvertimeModel', 'Overtime');
        $this->Overtime->myConstruct('hr');
        $this->load->model('HrModel');
        $this->HrModel->myConstruct('hr');
        $this->load->model('OtherModel', 'Other');
        $this->Other->myConstruct('main');
        $this->load->model('AbsenModel');
        $this->AbsenModel->myConstruct('hr');
    }

    public function uploadAttachment($user, $files)
    {
        if(isset($files['file']['name'])) {
            $file = $files['file']['name'];
            $extension = pathinfo($file, PATHINFO_EXTENSION);
            $filename = str_replace('.', '_', $user->username) . '_' . $user->nip . '.' . $extension;
            $config['upload_path']          = './assets/email_attachment';
            $config['allowed_types']        = 'pdf';
            $config['file_name']            = $filename;
            $config['overwrite']            = true;
            $config['max_size']             = 10000;
            
            $this->load->library('upload', $config);

            $upload = $this->upload->do_upload('file');
            if($upload) {
                $eFileName = simpleEncrypt($filename, 'e');
                $fileUrl = IMG_URL . "index.php?c=Pc&m=email_pdfreader&mode=read&token=" . $eFileName;

                return [
                    'status' => 200,
                    'file' => $fileUrl,
                    'file_original' => IMG_URL . 'assets/email_attachment/' . $filename
                ];
            } else {
                return [
                    'status' => 400,
                    'message' => 'Upload profile gagal'
                ];
            }
        } else {
            return [
                'status' => 400,
                'message' => 'File tidak boleh kosong'
            ];
        }
    }

    public function testNotification() {
        $username = $this->input->post('username');
        $title = $this->input->post('title');
        $body = $this->input->post('body');
        $alertName = $this->input->post('alert_name');
        $subject = $this->input->post('subject');
        $titleMessage = $this->input->post('title_message');
        $bodyMessage = $this->input->post('body_message');

        $user = $this->Main->getWhere('users', ['username' => $username])->row();
        $emp = $this->Hr->getWhere('employees', ['user_id' => $user->id])->row();

        $currentDate = date('Y-m-d H:i:s');
        $token = time() . "_$alertName";

        $file = $this->uploadAttachment($user, $_FILES);

        $style = [
            'button_container' => 'padding:10px;text-align:center;margin-top:20px;',
            'button' => 'border: 2px solid #422800;
                        border-radius: 30px;
                        box-shadow: #422800 4px 4px 0 0;
                        color: #422800;
                        cursor: pointer;
                        display: inline-block;
                        font-weight: 600;
                        font-size: 12px;
                        padding: 0 12px;
                        line-height: 40px;
                        text-align: center;
                        text-decoration: none;
                        user-select: none;
                        -webkit-user-select: none;
                        touch-action: manipulation;
                        width:280px;'
        ];

        if($file['status'] == 200) {
            $message = '<div>
                            <div style="padding: 5px 0px 0px 10px;text-align:center;">
                                <img style="width: 220px;height: auto;" src="https://goreklame.com/assets/logo-kf.png" alt="kf">
                                <hr>
                                <p>PT Kimia Farma Tbk. Plant Jakarta</p>
                            </div>
                        
                            <div style="background: white;margin-top: 20px;border-radius: 5px;border: 1px solid #422800;padding: 10px;box-shadow: 5px 10px #ccc;">
                                <p>'.$titleMessage.'</p>
                                <p>'.$bodyMessage.'</p>
                            </div>

                            <div style="'.$style['button_container'].'">
                                <a href="'.$file['file'].'" target="_blank" style="'.$style['button'].'background-color: #3399cc;">Baca File</a>
                            </div>
                        </div>';
        } else {
            $message = '<div>
                            <div style="padding: 5px 0px 0px 10px;text-align:center;">
                                <img style="width: 220px;height: auto;" src="https://goreklame.com/assets/logo-kf.png" alt="kf">
                                <hr>
                                <p>PT Kimia Farma Tbk. Plant Jakarta</p>
                            </div>
                        
                            <div style="background: white;margin-top: 20px;border-radius: 5px;border: 1px solid #422800;padding: 10px;box-shadow: 5px 10px #ccc;">
                                <p>'.$titleMessage.'</p>
                                <p>'.$bodyMessage.'</p>
                            </div>

                            <div style="'.$style['button_container'].'">
                                <a href="'.$file['file'].'" target="_blank" style="'.$style['button'].'background-color: #3399cc;">Baca File</a>
                            </div>
                        </div>';
        }

        $dataEMail = [
            'alert_name' => $alertName,
            'email_to' => $emp->email,
            'subject' => $subject,
            'subject_name' => "Spekta Alert: $subject",
            'apv_status' => 1,
            'token' => $token,
            'created_at' => $currentDate,
            'positive' => "",
            'negative' => "",
            'message' => $message,
            'status' => 0,
            'send_date' => $currentDate,
            'fcm_status' => 1,
            'emp_direct_apv' => $emp->id
        ];
        
        if($file['status'] == 200) $dataEMail['file'] = $file['file'];
        if($file['status'] == 200) $dataEMail['file_original'] = $file['file_original'];
        
        $insert = $this->Main->create('email', $dataEMail);
        
        if($user->fcm_token != '' && $insert) {
            $this->firebaselib->sendNotification($user->fcm_token, [
                'title' => $title,
                'body' => $body,
                'payload' => [
                    'alertName' => $alertName,
                    'emailTo' => "nurulanwar50133@gmail.com",
                    'subject' => $subject,
                    'subjectName' => "Spekta Alert: $subject",
                    'apvStatus' => "CLOSED",
                    'token' => $token,
                    'createdAt' => $currentDate,
                    'positive' => "",
                    'negative' => "",
                    'file' => $file['status'] == 200 ? $file['file'] : "",
                    'file_original' => $file['status'] == 200 ? $file['file_original'] : ""
                ]
            ]);
        }
    }

    public function pinVerification()
    {
        $params = getParam();
        if(isset($params['token'])) {
            $this->load->view('html/pin_verification', ['token' => $params['token']]);
        } else {
            $this->load->view('html/invalid_response', ['message' => "Token tidak valid"]);
        }
    }

    public function verifyPin()
    {
        $post = fileGetContent();
        $token = $post->token;
        $pin = $post->pin;
        $checkPin = $this->Hr->getOne('employee_pins', ['pin' => $pin]);
        if ($checkPin) {
            $emp = $this->Hr->getDataById('employees', $checkPin->emp_id);
            response(['status' => 'success', 'url' => simpleEncrypt($token, 'd') . "&nip=$emp->nip&emp_id=$emp->id"]);
        } else {
            response(['status' => 'error', 'message' => 'PIN Tidak Valid']);
        }
    }

    public function generateOvertime()
    {
        $params = getParam();
        $taskId = simpleEncrypt($params['token'], 'd');
        $overtime = $this->Hr->getOne('employee_overtimes', ['task_id' => $taskId]);
        $emp = $this->Hr->getOne('employees', ['id' => $params['emp_id']]);
        if ($overtime) {
            $subAllow = [];
            if(isMtnSupport($overtime)) {
                $subAllow['5'] = true;
            }
            if(isQaSupport($overtime)) {
                $subAllow['7'] = true;
            }
            if(isQcSupport($overtime)) {
                $subAllow['8'] = true;
            }
            if(isWhsSupport($overtime)) {
                $subAllow['13'] = true;
            }

            if($emp->sub_department_id == 5) {
                $name = 'Teknik & Pemeliharaan';
            } else if($emp->sub_department_id == 7){
                $name = 'Sistem Mutu';
            } else if($emp->sub_department_id == 8){
                $name = 'Pengawasan Mutu';
            } else if($emp->sub_department_id == 13){
                $name = 'Penyimpanan';
            } 

            if(!array_key_exists($emp->sub_department_id, $subAllow)) {
                $this->load->view('html/invalid_response', ['message' => "Tidak ada kebutuhan Support bagian <b>$name</b>!"]);
            }

            $checkRef = $this->Hr->getOne('employee_overtimes_ref', ['task_id' => $overtime->task_id, 'sub_department_id' => $emp->sub_department_id]);
            if(!$checkRef) {
                $data = [
                    'task_id' => $overtime->task_id,
                    'sub_department_id' => $emp->sub_department_id,
                    'created_by' => $emp->id,
                ];
                $this->Hr->create('employee_overtimes_ref', $data);
                $this->load->view('html/valid_response', ['message' => "Berhasil menyimpan <b>Referensi Lembur Produksi</b>"]);
            } else {
                $this->load->view('html/invalid_response', ['message' => "<b>Referensi Lembur Produksi</b> sudah disimpan!"]);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => 'Token tidak valid!']);
        }
    }

    public function approveOvertimeSpv()
    {
        $params = getParam();
        $expParam = isset($params['token']) ? explode(':', simpleEncrypt($params['token'], 'd')) : [];
        if (count($expParam) >= 4) {
            $taskId = $expParam[0];
            $appvType = $expParam[1];
            $status = $expParam[2];
            $divId = $expParam[3];
            $token = isset($expParam[4]) ? $expParam[4] : "";
            $nip = $params['nip'];
            $empId = $params['emp_id'];
            $emp = $this->Hr->getOne('employees', ['nip' => $nip]);
            if($emp->division_id == $divId) {
                if($status == 'APPROVED') {
                    $data = [
                        'apv_spv' => 'APPROVED',
                        'apv_spv_nip' => $emp->nip,
                        'apv_spv_date' => date('Y-m-d H:i:s'),
                        'status_by' => $emp->nip
                    ];
                } else {
                    $data = [
                        'apv_spv' => 'REJECTED',
                        'apv_spv_nip' => $emp->nip,
                        'apv_spv_date' => date('Y-m-d H:i:s'),
                        'status' => 'REJECTED',
                        'status_by' => $emp->nip
                    ];
                }
                if($status == 'REJECTED') {
                    $overtime = $this->Hr->getOne('employee_overtimes', ['task_id' => $taskId]);
                    if($overtime->apv_asman_nip == '-' || $overtime->apv_asman_nip == '') {
                        $this->Hr->update('employee_overtimes_detail', $data, ['task_id' => $taskId, 'division_id' => $divId]);
                        $this->load->view('html/valid_response', ['message' => "<p>Lembur <b>$taskId</b> berhasil di <b>$status</b></p>"]);
                        if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                    } else {
                        $this->load->view('html/invalid_response', ['message' => "Oops.. Lemburan $taskId sudah di approve ASMAN!"]);
                        if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                    }
                } else {
                    $isTakeAction = $this->Hr->getOne('employee_overtimes_detail', ['task_id' => $taskId, 'division_id' => $divId, 'apv_spv !=' => 'CREATED']);
                    if(!$isTakeAction) {
                        $this->Hr->update('employee_overtimes_detail', $data, ['task_id' => $taskId, 'division_id' => $divId]);
                        $this->load->view('html/valid_response', ['message' => "<p>Lembur <b>$taskId</b> berhasil di <b>$status</b></p>"]);
                        if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                    } else {
                        $this->load->view('html/invalid_response', ['message' => "Oopss.. Lemburan ini sudah di <b>$status</b> sebelumnya!"]);
                        if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                    }
                }
            } else {
                $this->load->view('html/invalid_response', ['message' => 'Jabatan anda tidak sesuai dengan <b>Sub Bagian Lembur</b>']);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => 'Token tidak valid!']);
        }
    }

    public function approveOvertime()
    {
        $params = getParam();
        $expParam = isset($params['token']) ? explode(':', simpleEncrypt($params['token'], 'd')) : [];

        if (count($expParam) >= 3) {
            $taskId = $expParam[0];
            $appvType = $expParam[1];
            $status = $expParam[2];
            $token = isset($expParam[3]) ? $expParam[3] : "";
            $nip = $params['nip'];
            $empId = $params['emp_id'];
            $emp = $this->Hr->getOne('employees', ['nip' => $nip]);
            $overtime = $this->Overtime->getOvertime(['equal_task_id' => $taskId])->row();
            if ($appvType == 'spv') {
                $isSpvPLT = $this->Hr->getOne('employee_ranks', ['emp_id' => $empId, 'division_id' => $overtime->division_id, 'status' => 'ACTIVE'], 'rank_id,division_id', ['rank_id' => ['5', '6']]);
                if ($emp->rank_id != 5 && $emp->rank_id != 6 && !$isSpvPLT) {
                    $this->load->view('html/invalid_response', ['message' => 'Jabatan anda bukan <b>Supervisor</b>']);
                } else if ($emp->division_id != $overtime->division_id && ($isSpvPLT && $isSpvPLT->division_id != $overtime->division_id)) {
                    $this->load->view('html/invalid_response', ['message' => 'Jabatan anda tidak sesuai dengan <b>Sub Bagian Lembur</b>']);
                } else if (($emp->rank_id == 5 || $emp->rank_id == 6) && $emp->division_id == $overtime->division_id || $isSpvPLT) {
                    $this->approveAction($overtime, $emp, $taskId, $appvType, $status, $nip, $empId, $token);
                } else {
                    $this->load->view('html/invalid_response', ['message' => 'Oops..! <b>Terjadi Kesalahan</b>']);
                }
            } else if ($appvType == 'asman') {
                $isAsmanPLT = $this->Hr->getOne('employee_ranks', ['emp_id' => $empId, 'sub_department_id' => $overtime->sub_department_id, 'status' => 'ACTIVE'], 'rank_id,sub_department_id', ['rank_id' => ['3', '4']]);
                if ($emp->rank_id != 3 && $emp->rank_id != 4 && !$isAsmanPLT) {
                    $this->load->view('html/invalid_response', ['message' => 'Jabatan anda bukan <b>ASMAN</b>']);
                } else if ($emp->sub_department_id != $overtime->sub_department_id && ($isAsmanPLT && $isAsmanPLT->sub_department_id != $overtime->sub_department_id)) {
                    $this->load->view('html/invalid_response', ['message' => 'Jabatan anda tidak sesuai dengan <b>Bagian Lembur</b>']);
                } else if (($emp->rank_id == 3 || $emp->rank_id == 4) && $emp->sub_department_id == $overtime->sub_department_id || $isAsmanPLT) {
                    $this->approveAction($overtime, $emp, $taskId, $appvType, $status, $nip, $empId, $token);
                } else {
                    $this->load->view('html/invalid_response', ['message' => 'Oops..! <b>Terjadi Kesalahan</b>']);
                }
            } else if ($appvType == 'ppic') {
                $isPPICPLT = $this->Hr->getOne('employee_ranks', ['emp_id' => $empId, 'sub_department_id' => 9, 'status' => 'ACTIVE'], 'rank_id,sub_department_id', ['rank_id' => ['3', '4']]);
                if ($emp->rank_id != 3 && $emp->rank_id != 4 && !$isPPICPLT) {
                    $this->load->view('html/invalid_response', ['message' => 'Jabatan anda bukan <b>ASMAN</b>']);
                } else if ($emp->sub_department_id != $overtime->sub_department_id && ($isPPICPLT && $isPPICPLT->sub_department_id != $overtime->sub_department_id)) {
                    $this->load->view('html/invalid_response', ['message' => 'Jabatan anda tidak sesuai dengan <b>Bagian Lembur</b>']);
                } else if (($emp->rank_id == 3 || $emp->rank_id == 4) && $emp->sub_department_id == 9 || $isPPICPLT) {
                    $this->approveAction($overtime, $emp, $taskId, $appvType, $status, $nip, $empId, $token);
                } else {
                    $this->load->view('html/invalid_response', ['message' => 'Oops..! <b>Terjadi Kesalahan</b>']);
                }
            } else if ($appvType == 'mgr') {
                $isMgrPLT = $this->Hr->getOne('employee_ranks', ['emp_id' => $empId, 'department_id' => $overtime->department_id, 'rank_id' => 2, 'status' => 'ACTIVE'], 'rank_id,department_id');
                if ($emp->rank_id != 2 && !$isMgrPLT) {
                    $this->load->view('html/invalid_response', ['message' => 'Jabatan anda bukan <b>Manager</b>']);
                } else if ($emp->department_id != $overtime->department_id && ($isMgrPLT && $isMgrPLT->department_id != $overtime->department_id)) {
                    $this->load->view('html/invalid_response', ['message' => 'Jabatan anda tidak sesuai dengan <b>Sub Unit Lembur</b>']);
                } else if ($emp->rank_id == 2 && $emp->department_id == $overtime->department_id || $isMgrPLT) {
                    $this->approveAction($overtime, $emp, $taskId, $appvType, $status, $nip, $empId, $token);
                } else {
                    $this->load->view('html/invalid_response', ['message' => 'Oops..! <b>Terjadi Kesalahan</b>']);
                }
            } else if ($appvType == 'head') {
                $isHeadPLT = $this->Hr->getOne('employee_ranks', ['emp_id' => $empId, 'rank_id' => 1, 'status' => 'ACTIVE'], 'rank_id');
                if ($emp->rank_id != 1 && !$isHeadPLT) {
                    $this->load->view('html/invalid_response', ['message' => 'Jabatan anda bukan <b>Plant Manager</b>']);
                } else if ($emp->rank_id == 1 || $isHeadPLT) {
                    $this->approveAction($overtime, $emp, $taskId, $appvType, $status, $nip, $empId, $token);
                } else {
                    $this->load->view('html/invalid_response', ['message' => 'Oops..! <b>Terjadi Kesalahan</b>']);
                }
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => 'Token tidak valid!']);
        }
    }

    public function approveAction($overtime, $emp, $taskId, $appvType, $status, $nip, $empId, $token)
    {
        if ($appvType == 'spv') {
            $columnApv = 'apv_spv';
            $columnApvNip = 'apv_spv_nip';
            $columnApvDate = 'apv_spv_date';
        } else if ($appvType == 'asman') {
            $columnApv = 'apv_asman';
            $columnApvNip = 'apv_asman_nip';
            $columnApvDate = 'apv_asman_date';
        } else if ($appvType == 'ppic') {
            $columnApv = 'apv_ppic';
            $columnApvNip = 'apv_ppic_nip';
            $columnApvDate = 'apv_ppic_date';
        } else if ($appvType == 'mgr') {
            $columnApv = 'apv_mgr';
            $columnApvNip = 'apv_mgr_nip';
            $columnApvDate = 'apv_mgr_date';
        } else if ($appvType == 'head') {
            $columnApv = 'apv_head';
            $columnApvNip = 'apv_head_nip';
            $columnApvDate = 'apv_head_date';
        }

        if ($overtime->$columnApvNip == '') {
            if ($status == 'APPROVED') {
                $data = [
                    $columnApv => $status,
                    $columnApvNip => $nip,
                    $columnApvDate => date('Y-m-d H:i:s'),
                ];

                if ($emp->rank_id == 1) {
                    $data['status'] = 'CLOSED';
                    $data['updated_by'] = $empId;
                    $data['updated_at'] = date('Y-m-d H:i:s');
                }
            } else {
                $data = [
                    $columnApv => $status,
                    $columnApvNip => $nip,
                    $columnApvDate => date('Y-m-d H:i:s'),
                    'rejection_note' => $emp->employee_name . " : Rejected from email",
                    'status' => 'REJECTED',
                    'updated_by' => $empId,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }

            $currDate = date('Y-m-d H:i:s');
            $newCurrDate = new DateTime($currDate);
            $ovtStartDate = new DateTime($overtime->start_date);
            if($columnApv == 'apv_asman') {
                if($overtime->apv_ppic_nip == '-') {
                    if($overtime->apv_mgr_nip == '-') {
                        $data['apv_ppic_date'] = $newCurrDate < $ovtStartDate ? $overtime->start_date : $currDate;
                        $data['apv_mgr_date'] = $newCurrDate < $ovtStartDate ? $overtime->start_date : $currDate;
                    } else {
                        $data['apv_ppic_date'] = $newCurrDate < $ovtStartDate ? $overtime->start_date : $currDate;
                    }
                }
            } else if($columnApv == 'apv_ppic') {
                if($overtime->apv_mgr_nip == '-') {
                    $data['apv_mgr_date'] = $newCurrDate < $ovtStartDate ? $overtime->start_date : $currDate;
                }
            }

            $this->Hr->update('employee_overtimes', $data, ['task_id' => $taskId]);
            
            if($emp->rank_id == 1) {
                $dataDetail = [
                    'status' => 'CLOSED',
                    'status_by' => $nip,
                    'updated_by' => $empId,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $this->Hr->update('employee_overtimes_detail', $dataDetail, ['task_id' => $taskId, 'status !=' => 'CANCELED']);
            }
            
            if($status == 'REJECTED') {
                $dataDetail = [
                    'status' => 'REJECTED',
                    'status_by' => $nip,
                    'updated_by' => $empId,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $this->Hr->update('employee_overtimes_detail', $dataDetail, ['task_id' => $taskId, 'status !=' => 'CANCELED']);

                $ref = $this->Hr->getOne('employee_overtimes_ref',  ['task_id' => $taskId]);
                if($ref && $ref->task_id_support) {
                    $this->Hr->update('employee_overtimes', $data, ['task_id' => $ref->task_id_support]);
                    $this->Hr->update('employee_overtimes_detail', $dataDetail, ['task_id' => $ref->task_id_support, 'status !=' => 'CANCELED']);
                }
            }

            if ($appvType == 'spv') {
                if ($status == 'APPROVED') {
                    $isHaveAsman = $this->isHaveAsman($overtime, $taskId, $token);
                    if(!$isHaveAsman) {
                        $isHaveMgr = $this->isHaveMgr($overtime, $taskId, $token);
                        if(!$isHaveMgr) {
                            $this->isHaveHead($overtime, $taskId, $token);
                        }
                    }
                } else {
                    $this->ovtlib->sendEmailReject('Supervisor', 'spv', $overtime, $taskId, $token);
                }
            } else if ($appvType == 'asman' || $appvType == 'ppic') {
                if ($status == 'APPROVED') {
                    if($appvType == 'asman') {
                        if($overtime->sub_department_id == 1 || $overtime->sub_department_id == 2 || $overtime->sub_department_id == 3 || $overtime->sub_department_id == 4 || $overtime->sub_department_id == 13) {
                            $isHavePPIC = $this->isHavePPIC($overtime, $taskId, $token);
                            if(!$isHavePPIC) {
                                $isHaveMgr = $this->isHaveMgr($overtime, $taskId, $token);
                                if(!$isHaveMgr) {
                                    $this->isHaveHead($overtime, $taskId, $token);
                                }
                            }
                        } else {
                            $isHaveMgr = $this->isHaveMgr($overtime, $taskId, $token);
                            if(!$isHaveMgr) {
                                $this->isHaveHead($overtime, $taskId, $token);
                            }
                        }
                    } else if($appvType == 'ppic'){
                        $isHaveMgr = $this->isHaveMgr($overtime, $taskId, $token);
                        if(!$isHaveMgr) {
                            $this->isHaveHead($overtime, $taskId, $token);
                        }
                    }
                } else {
                    if($appvType == 'asman') {
                        $this->ovtlib->sendEmailReject('ASMAN', 'asman', $overtime, $taskId, $token);
                    } else if($appvType == 'ppic') {
                        $this->ovtlib->sendEmailReject('PPIC', 'ppic', $overtime, $taskId, $token);
                    }
                }
            } else if ($appvType == 'mgr') {
                if ($status == 'APPROVED') {
                    $this->isHaveHead($overtime, $taskId, $token);
                } else {
                    $this->ovtlib->sendEmailReject('Manager', 'mgr', $overtime, $taskId, $token);
                }
            } else {
                if ($status == 'REJECTED') {
                    $this->ovtlib->sendEmailReject('Plant Manager', 'head', $overtime, $taskId, $token);
                }
            }
            $this->load->view('html/valid_response', ['message' => "<p>Lembur <b>$overtime->task_id</b> berhasil di <b>$status</b></p>"]);
        } else {
            $employee = $overtime->$columnApvNip != '-' ? $this->Hr->getOne('employees', ['nip' => $overtime->$columnApvNip])->employee_name : null;
            $approver = $employee ? $employee : 'sistem';
            if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            $this->load->view('html/invalid_response', ['message' => "<p>Gagal approve lembur</p><br/> Lembur dengan No. Referensi: <b>$overtime->task_id</b> sudah di $overtime->status oleh $approver </p>"]);
        }
    }

    public function isHaveAsman($overtime, $taskId, $token)
    {
        $isHaveAsman = $this->Hr->getOne('employees', ['sub_department_id' => $overtime->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
        $isHaveAsmanPLT = $this->Hr->getOne('employee_ranks', ['sub_department_id' => $overtime->sub_department_id, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
        if ($isHaveAsman) {
            $this->ovtlib->sendEmailAppv($isHaveAsman->email, 'ASMAN', 'asman', $overtime, $taskId);
            if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            return true;
        } else if($isHaveAsmanPLT) {
            $email = $this->Hr->getDataById('employees', $isHaveAsmanPLT->emp_id)->email;
            $this->ovtlib->sendEmailAppv($email, 'ASMAN', 'asman', $overtime, $taskId);
            if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            return true;
        } else {
            return false;
        }
    }

    public function isHavePPIC($overtime, $taskId, $token)
    {
        $isHavePPIC = $this->Hr->getOne('employees', ['sub_department_id' => 9, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
        $isHavePPICPLT = $this->Hr->getOne('employee_ranks', ['sub_department_id' => 9, 'status' => 'ACTIVE'], '*', ['rank_id' => ['3', '4']]);
        if ($isHavePPIC) {
            $this->ovtlib->sendEmailAppv($isHavePPIC->email, 'PPIC', 'ppic', $overtime, $taskId);
            if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            return true;
        } else if($isHavePPICPLT) {
            $email = $this->Hr->getDataById('employees', $isHavePPICPLT->emp_id)->email;
            $this->ovtlib->sendEmailAppv($email, 'PPIC', 'ppic', $overtime, $taskId);
            if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            return true;
        } else {
            return false;
        }
    }

    public function isHaveMgr($overtime, $taskId, $token)
    {
        $isHaveMgr = $this->Hr->getOne('employees', ['department_id' => $overtime->department_id, 'rank_id' => 2, 'status' => 'ACTIVE']);
        $isHaveMgrPLT = $this->Hr->getOne('employee_ranks', ['department_id' => $overtime->department_id, 'rank_id' => 2, 'status' => 'ACTIVE']);
        if ($isHaveMgr) {
            $this->ovtlib->sendEmailAppv($isHaveMgr->email, 'Manager', 'mgr', $overtime, $taskId);
            if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            return true;
        } else if($isHaveMgrPLT) {
            $email = $this->Hr->getDataById('employees', $isHaveMgrPLT->emp_id)->email;
            $this->ovtlib->sendEmailAppv($email, 'Manager', 'mgr', $overtime, $taskId);
            if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            return true;
        } else {
            return false;
        }
    }

    public function isHaveHead($overtime, $taskId, $token)
    {
        $isHaveHead = $this->Hr->getOne('employees', ['rank_id' => 1, 'status' => 'ACTIVE']);
        $isHaveHeadPLT = $this->Hr->getOne('employee_ranks', ['rank_id' => 1, 'status' => 'ACTIVE']);
        if ($isHaveHead) {
            $this->ovtlib->sendEmailAppv($isHaveHead->email, 'Plant Manager', 'head', $overtime, $taskId);
            if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            return true;
        } else if($isHaveHeadPLT) {
            $email = $this->Hr->getDataById('employees', $isHaveHeadPLT->emp_id)->email;
            $this->ovtlib->sendEmailAppv($email, 'Plant Manager', 'head', $overtime, $taskId);
            if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            return true;
        } else {
            return false;
        }
    }

    public function responseMeeting()
    {
        $params = getParam();
        $token = isset($params['token']) ? explode(':', simpleEncrypt($params['token'], 'd')) : [];
       
        if(count($token) == 3) {
            $meetId = $token[0];
            $email = $token[1];
            $status = $token[2];
            $currStatus = $this->General->getOne('meeting_participants', ['email' => $email, 'meeting_id' => $meetId]);
            if(isset($currStatus->status) && $currStatus->status == 'BELUM MEMUTUSKAN') {
                if($status == 'accept') {
                    $this->General->update('meeting_participants', ['status' => 'HADIR'], ['email' => $email,  'meeting_id' => $meetId]);
                    $this->General->addValueBy('meeting_rooms_reservation', ['participant_confirmed' => $currStatus->total_participant], ['id' => $meetId]);
                    $this->load->view('html/valid_response', ['message' => "Konfirmasi diterima, anda akan HADIR di meeting tersebut"]);
                } else {
                    $this->General->update('meeting_participants', ['status' => 'TIDAK HADIR'], ['email' => $email,  'meeting_id' => $meetId]);
                    $this->General->addValueBy('meeting_rooms_reservation', ['participant_rejected' => $currStatus->total_participant], ['id' => $meetId]);
                    $this->load->view('html/valid_response', ['message' => "Konfirmasi diterima, anda memutuskan TIDAK HADIR di meeting tersebut"]);
                }
            } else if(isset($currStatus->status)){
                $this->load->view('html/invalid_response', ['message' => "Anda telah mengambil keputusan $currStatus->status untuk undangan ini!"]);
            } else {
                $this->load->view('html/invalid_response', ['message' => 'Token tidak valid!']);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => 'Token tidak valid!']);
        }
    }

    public function updateVehicleRevStatus()
    {
        $params = getParam();
        $expToken = isset($params['token']) ? explode(':', simpleEncrypt($params['token'], 'd')) : [];
        
        if(count($expToken) >= 2) {
            $revId = $expToken[0];
            $status = $expToken[1] == 'approve' ? 'APPROVED' : 'REJECTED';
            $token = isset($expToken[2]) ? $expToken[2] : "";
            $nip = $params['nip'];
            $empId = $params['emp_id'];

            $data = [
                'status' => $status,
                'updated_by' => $empId,
                'updated_at' => date('Y-m-d H:i:s')
            ];
            $emp = $this->Hr->getDataById('employees', $empId);
            $rev = $this->General->getDataById('vehicles_reservation', $revId);
            if($rev->status == 'CREATED') {
                if($status == 'APPROVED') {
                    if($emp->rank_id == 3 || $emp->rank_id == 4 || $emp->rank_id == 5 || $emp->rank_id == 6) {
                        $this->General->updateById('vehicles_reservation', $data, $revId);
                        if($emp->rank_id == 5 || $emp->rank_id == 6) {
                            $this->vehiclelib->approvalNotif('ASMAN', $revId);
                        } else if($emp->rank_id == 3 || $emp->rank_id == 4){
                            $this->vehiclelib->approvalNotif('Supervisor', $revId);
                        }
                        $this->load->view('html/valid_response', ['message' => "Berhasil <b>$status</b> reservasi kendaraan dinas"]);
                        if($token != "") $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                    } else {
                        $this->load->view('html/invalid_response', ['message' => "Gagal <b>$status</b> reservasi kendaraan dinas. Jabatan anda tidak sesuai!"]);
                    }
                } else if($status == 'REJECTED') {
                    if($emp->rank_id == 3 || $emp->rank_id == 4 || $emp->rank_id == 5 || $emp->rank_id == 6) {
                        $this->General->updateById('vehicles_reservation', $data, $revId);
                        if($emp->rank_id == 5 || $emp->rank_id == 6) {
                            $this->vehiclelib->rejectionNotif('ASMAN', $revId, $emp->employee_name);
                        } else if($emp->rank_id == 3 || $emp->rank_id == 4) {
                            $this->vehiclelib->rejectionNotif('Supervisor', $revId, $emp->employee_name);
                        }
                        $this->load->view('html/valid_response', ['message' => "Berhasil <b>$status</b> reservasi kendaraan dinas"]);
                        if($token != "") $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                    } else {
                        $this->load->view('html/invalid_response', ['message' => "Gagal <b>$status</b> reservasi kendaraan dinas. Jabatan anda tidak sesuai!"]);
                    }
                }
            } else {
                $this->load->view('html/invalid_response', ['message' => "Sudah di <b>$rev->status</b> sebelumnya!"]);
                if($token != "") $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => 'Token tidak valid!']);
        }
    }

    public function updateMeetRevStatus()
    {
        $params = getParam();
        $expToken = isset($params['token']) ? explode(':', simpleEncrypt($params['token'], 'd')) : [];
        
        if(count($expToken) >= 2) {
            $meetId = $expToken[0];
            $status = $expToken[1] == 'approve' ? 'APPROVED' : 'REJECTED';
            $token = isset($expToken[2]) ? $expToken[2] : "";
            $nip = $params['nip'];
            $empId = $params['emp_id'];

            $data = [
                'status' => $status,
                'updated_by' => $empId,
                'updated_at' => date('Y-m-d H:i:s')
            ];
            $emp = $this->Hr->getDataById('employees', $empId);
            $rev = $this->General->getDataById('meeting_rooms_reservation', $meetId);
            if($rev->status == 'CREATED') {
                if($status == 'APPROVED') {
                    if($emp->rank_id == 3 || $emp->rank_id == 4 || $emp->rank_id == 5 || $emp->rank_id == 6) {
                        $this->General->updateById('meeting_rooms_reservation', $data, $meetId);
                        $this->General->update('meeting_rooms_reservation', $data, ['ref' => $meetId]);
                        $this->mroomlib->meetInvitation($emp, $meetId);
                        $this->load->view('html/valid_response', ['message' => "Berhasil <b>$status</b> reservasi ruang meeting"]);
                        if($token != "") $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                    } else {
                        $this->load->view('html/invalid_response', ['message' => "Gagal <b>$status</b> reservasi ruang meeting. Jabatan anda tidak sesuai!"]);
                    }
                } else if($status == 'REJECTED') {
                    if($emp->rank_id == 3 || $emp->rank_id == 4 || $emp->rank_id == 5 || $emp->rank_id == 6) {
                        $this->General->updateById('meeting_rooms_reservation', $data, $meetId);
                        $this->General->update('meeting_rooms_reservation', $data, ['ref' => $meetId]);
                        if($emp->rank_id == 5 || $emp->rank_id == 6) {
                            $this->mroomlib->rejectionNotif('ASMAN', $meetId, $emp->employee_name);
                        } else if($emp->rank_id == 3 || $emp->rank_id == 4) {
                            $this->mroomlib->rejectionNotif('Supervisor', $meetId, $emp->employee_name);
                        }
                        $this->load->view('html/valid_response', ['message' => "Berhasil <b>$status</b> reservasi ruang meeting"]);
                        if($token != "") $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                    } else {
                        $this->load->view('html/invalid_response', ['message' => "Gagal <b>$status</b> reservasi ruang meeting. Jabatan anda tidak sesuai!"]);
                    }
                }
            } else {
                $this->load->view('html/invalid_response', ['message' => "Sudah di <b>$rev->status</b> sebelumnya!"]);
                if($token != "") $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => 'Token tidak valid!']);
        }
    }

    public function driverConfirm()
    {
        $params = getParam();
        $token = isset($params['token']) ? explode(':', simpleEncrypt($params['token'], 'd')) : [];
        if(count($token) >= 3) {
            $id = $token[0];
            $email = $token[1];
            $status = $token[2] == 'approve' ? 'DISETUJUI' : 'MENOLAK';
            $emailToken = isset($token[3]) ? $token[3] : "";
            $trip = $this->Other->getTripDetail($id);
            if($trip->driver == $email) {
                if($trip->driver_confirmed == 'BELUM MEMUTUSKAN') {
                    $data = [
                        'driver_confirmed' => $status,
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                    $this->General->updateById('vehicles_reservation', $data, $id);
                    $message = $status == 'DISETUJUI' ? "Perjalanan berhasil $status" : "Anda telah berhasil $status perjalan tersebut, silahkan cek email untuk update form Perjalanan!";
                    if($status == 'DISETUJUI') {
                        $driver = $this->Hr->getOne('employees', ['email' => $trip->driver]);
                        $linkForm = LIVE_URL . "index.php?c=PublicController&m=driverTripForm&token=".simpleEncrypt("$id:$trip->driver");
                        $messageEmail = $this->load->view('html/vehicles/email/driver_trip_form_notification', [
                            'data' => $trip, 'linkForm' => $linkForm, 'driver' => $driver
                        ], true);
                        $data = [
                            'alert_name' => 'TRIP_FORM_FOR_DRIVER',
                            'email_to' => $trip->driver,
                            'subject' => "Form Trip Driver untuk perjalanan ke $trip->destination (No. Tiket: $trip->ticket)",
                            'subject_name' => "Spekta Alert: Form Trip Driver untuk perjalanan ke $trip->destination (No. Tiket: $trip->ticket)",
                            'message' => $messageEmail,
                            'emp_direct_apv' => $driver->id,
                            'apv_status' => 1,
                            'token' => time() . "_TRIP_FORM_FOR_DRIVER",
                            'positive' => '',
                            'negative' => '',
                            'redirect' => $linkForm
                        ];
                        $this->Main->create('email', $data);
                    }
                    $this->load->view('html/valid_response', ['message' => $message]);
                    if($emailToken != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $emailToken]);
                } else {
                    $this->load->view('html/invalid_response', ['message' => "Anda telah mengambil keputusan (<b>$status</b>) untuk perjalanan ($id) ini!"]);
                }
            } else {
                $this->load->view('html/invalid_response', ['message' => "Anda bukan driver yang di tunjuk untuk perjalanan ($id) ini!"]);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => "Token tidak valid"]);
        }
    }

    public function noActionNeed() {
        $this->load->view('html/invalid_response', ['message' => "Tidak ada tindakan yg diperkukan untuk notifikasi ini!"]);
    }

    public function driverTripForm()
    {
        $params = getParam();
        $expToken = isset($params['token']) ? explode(':', simpleEncrypt($params['token'], 'd')) : [];

        if(count($expToken) == 2) {
            $id = $expToken[0];
            $email = $expToken[1];

            $trip = $this->Other->getTripDetail($id);
            if($trip) {
                if($trip->driver == $email) {
                    $this->load->view('html/vehicles/form_driver', ['trip' => $trip]);
                } else {
                    $this->load->view('html/invalid_response', ['message' => 'Anda bukan driver untuk perjalanan ini!']);
                }
            } else {
                $this->load->view('html/invalid_response', ['message' => 'Data perjalanan tidak ditemukan!']);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => 'Token tidak valid!']);
        }
    }

    public function updateKilometer()
    {
       $post = fileGetContent();
       $id = $post->id;

       $data = [
           'start_km' => $post->start_km,
           'end_km' => $post->end_km,
           'distance' => $post->end_km - $post->start_km
       ];

       if($post->end_km < $post->start_km && $post->end_km > 0) {
           response(['status' => 'error', 'message' => 'KM akhir harus lebih besar dari KM awal!']);
       }
       $message = 'Update kilometer berhasil';
       if($post->start_km > 0 && $post->end_km == 0) {
        $message = 'Update kilometer awal berhasil';
       } else if($post->start_km == 0 && $post->end_km > 0){
        $message = 'Update kilometer akhir berhasil';
       }
       $this->General->updateById('vehicles_reservation', $data, $id);
       response(['status' => 'success', 'message' => $message]);
    }

    public function ganttChart()
    {
        $params = getParam();
        $expToken = explode(':', simpleEncrypt($params['token'], 'd'));
        $taskId = $expToken[0];
        $subId = $expToken[1];
        $divId = $expToken[2];
        $month = $expToken[3];
        $year = $expToken[4];
        
        if($taskId == '-') {
            if($divId == '-') {
                $tasks = $this->Main->getWhere('projects_task', [
                    'sub_department_id' => $subId, 
                    'MONTH(start_date)' => $month, 
                    'YEAR(start_date)' => $year
                ])->result();
            } else {
                $tasks = $this->Main->getWhere('projects_task', [
                    'sub_department_id' => $subId, 
                    'division_id' => $divId, 
                    'MONTH(start_date)' => $month, 
                    'YEAR(start_date)' => $year
                ])->result();
            }
        } else {
            $tasks = $this->Main->getWhere('projects_task', ['task_id' => $taskId])->result();
        }

        $data = [];
        $dataLink = [];

        foreach ($tasks as $task) {
        
            $dt = [
                'id' => $task->id, 
                'text' => $task->text, 
                'start_date' => revDate($task->start_date), 
                'duration' => $task->duration, 
                'progress' => $task->progress, 
                'open' => 1
            ];
            if($task->parent > 0) {
                $dt['parent'] = $task->parent;
            }
            $data[] = $dt;
        }

        if($divId == '-') {
            $links = $this->Main->getWhere('projects_link', ['sub_department_id' => $subId])->result();
        } else {
            $links = $this->Main->getWhere('projects_link', ['sub_department_id' => $subId, 'division_id' => $divId])->result();
        }

        foreach ($links as $link) {
            $dataLink[] = [
                'id' => $link->id, 
                'source' => $link->source, 
                'target' => $link->target,
                'type' => $link->type,
            ];
        }
        $this->load->view('html/public/gantt/gantt_chart', ['data' => ['data' => $data, 'links' => $dataLink]]);
    }

    public function approveAbsenCorrection()
    {
        $params = getParam();
        $nip = $params['nip'];
        $asman = $this->Hr->getWhere('employees', ['nip' => $nip])->row();
        
        $expToken = explode(':', simpleEncrypt($params['token'], 'd'));
        $absenTable = $expToken[0];
        $absenId = $expToken[1];
        $action = $expToken[2];
        $token = isset($expToken[3]) ? $expToken[3] : "";

        $absenData = [
            'correction_status' => $action == 'APPROVED' ? 2 : 4,
            'updated_by' => $asman->id,
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $correctionData = [
            'direct_approval_time' => date('Y-m-d H:i:s'),
            'direct_approval_status' => $action
        ];

        $correction = $this->Hr->getWhere('absen_correction', ['absen_table' => $absenTable, 'absen_id' => $absenId])->row();

        if($correction->direct_approval_status == 'CREATED') {
            if($asman->id == $correction->direct_approval_id) {
                $this->Hr->updateById($absenTable, $absenData, $absenId);
                $this->Hr->update('absen_correction', $correctionData, ['absen_table' => $absenTable, 'absen_id' => $absenId]);

                if($action == "APPROVED") {
                    $absen = $this->AbsenModel->getAbsensWithEmpSingle($absenTable, $correction->absen_id);
                    $message = $this->load->view('html/absen/email/approve_correction_hr', [
                        'absen' => $absen,
                        'asman' => $asman
                    ], true);
            
                    $sdmEmails = "";
                    $sdms = $this->Hr->getWhere('employees', ['sub_department_id' => 11, 'division_id' => 38])->result();
                    foreach ($sdms as $sdm) {
                        if($sdm->rank_id >= 4) {
                            if($sdm->email != "" && validateEmail($sdm->email)) {
                                $sdmEmails .= $sdmEmails == "" ? $sdm->email : ",$sdm->email"; 
                            }
                        }
                    }
            
                    $emailData = [
                        'alert_name' => 'ABSEN_CORRECTION_HR',
                        'email_to' => $sdmEmails,
                        'subject' => "Approval Koreksi Absen $absen->employee_name pada tanggal " . toIndoDateDay($absen->abs_date),
                        'subject_name' => "Spekta Alert: Approval Koreksi Absen $absen->employee_name pada tanggal " . toIndoDateDay($absen->abs_date),
                        'message' => $message,
                    ];
            
                    $this->Main->create('email', $emailData);
                }

                $this->load->view('html/valid_response', ['message' => "Berhasil <b>$action</b> permintaan koreksi absen"]);
                if($token != "") $this->Main->update('email' , ['apv_status' => 1], ['token' => $token]);
            } else {
                $this->load->view('html/invalid_response', ['message' => "Gagal <b>$action</b> permintaan koreksi absen. Otoritas tidak sesuai!"]);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => "Gagal <b>$action</b> permintaan koreksi absen. Tindakan sudah dilakukan!"]);
            if($token != "") $this->Main->update('email' , ['apv_status' => 1], ['token' => $token]);
        }
    }

    public function approveOutOffice()
    {
        $params = getParam();
        $nip = $params['nip'];
        
        $expToken = explode(':', simpleEncrypt($params['token'], 'd'));
        $outOfficeId = $expToken[0];
        $action = $expToken[1];
        $token = isset($expToken[2]) ? $expToken[2] : "";

        $approver = $this->Hr->getWhere('employees', ['nip' => $nip])->row();
        $outData = $this->Hr->getDataById('out_office', $outOfficeId);
        $emp = $this->HrModel->getEmployeeById($outData->emp_id);

        if($approver->rank_id >= 5 && $approver->rank_id <= 6) {
            $data = [
                'apv_status_1' => $action,
                'apv_date_1' => date('Y-m-d H:i:s'),
                'updated_by' => $approver->id,
                'updated_at' => date('Y-m-d H:i:s'),
                'status' => $action == 'APPROVED' ? 'PROCESS' : $action
            ];

            if($outData->status == 'CREATED') {
                if($approver->nip == $outData->apv_nip_1) {
                    $approver2 = $this->Hr->getWhere('employees', ['nip' => $outData->apv_nip_2])->row();

                    $this->Hr->updateById('out_office', $data, $outOfficeId);

                    $emailToken = time()."_OUT_OFFICE_2_".$approver->id;
                    $tokenApprove = simpleEncrypt("$outOfficeId:APPROVED:$emailToken");
                    $tokenReject = simpleEncrypt("$outOfficeId:REJECTED:$emailToken");
                    $linkApprove = LIVE_URL . "index.php?c=PublicController&m=approveOutOffice&token=$tokenApprove";
                    $linkReject = LIVE_URL . "index.php?c=PublicController&m=approveOutOffice&token=$tokenReject";
        
                    $linkApprove = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=positive&token=" . simpleEncrypt($linkApprove);
                    $linkReject = LIVE_URL . "index.php?c=PublicController&m=pinVerification&action=negative&token=" . simpleEncrypt($linkReject);
        
                    $message = $this->load->view('html/absen/email/approve_outoffice_2', [
                        'employee' => $emp,
                        'approver' => $approver2,
                        'data' => $outData,
                        'linkApprove' => $linkApprove,
                        'linkReject' => $linkReject,
                    ], true);
        
                    $emailData = [
                        'alert_name' => 'OUT_OFFICE_#2',
                        'email_to' => $approver2->email,
                        'subject' => "Approval Izin Keluar Kantor $emp->employee_name pada tanggal " . toIndoDateDay($outData->out_date),
                        'subject_name' => "Spekta Alert: Approval Izin Keluar Kantor $emp->employee_name pada tanggal " . toIndoDateDay($outData->out_date),
                        'message' => $message,
                        'token' => $emailToken,
                        'emp_direct_apv' => $approver2->id,
                        'positive' => $linkApprove,
                        'negative' => $linkReject
                    ];
        
                    $this->Main->create('email', $emailData);

                    $this->load->view('html/valid_response', ['message' => "Berhasil <b>$action</b> permintaan izin keluar kantor"]);
                    if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                } else {
                    $this->load->view('html/invalid_response', ['message' => "Gagal <b>$action</b> permintaan izin keluar kantor. Otoritas tidak sesuai!"]);
                }
            } else {
                $this->load->view('html/invalid_response', ['message' => "Gagal <b>$action</b> permintaan izin keluar kantor. Tindakan sudah dilakukan!"]);
                if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            }
        } else {
            $data = [
                'apv_status_2' => $action,
                'apv_date_2' => date('Y-m-d H:i:s'),
                'updated_by' => $approver->id,
                'updated_at' => date('Y-m-d H:i:s'),
                'status' => $action
            ];

            if($outData->status == 'PROCESS') {
                if($approver->nip == $outData->apv_nip_2) {
                    $this->Hr->updateById('out_office', $data, $outOfficeId);
                    $this->load->view('html/valid_response', ['message' => "Berhasil <b>$action</b> permintaan izin keluar kantor"]);
                    if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                } else {
                    $this->load->view('html/invalid_response', ['message' => "Gagal <b>$action</b> permintaan izin keluar kantor. Otoritas tidak sesuai!"]);
                }
            } else {
                if($approver->nip == $outData->apv_nip_2) {
                    if($outData->status == 'CREATED') {
                        $this->load->view('html/invalid_response', ['message' => "Gagal <b>$action</b> permintaan izin keluar kantor. Supervisor belum approve!"]);
                    } else {
                        $this->load->view('html/invalid_response', ['message' => "Gagal <b>$action</b> permintaan izin keluar kantor. Tindakan sudah dilakukan!"]);
                        if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                    }
                } else {
                    $this->load->view('html/invalid_response', ['message' => "Gagal <b>$action</b> permintaan izin keluar kantor. Otoritas tidak sesuai!"]);
                }
            }
        }
    }

    public function approveLeave()
    {
        $params = getParam();
        $nip = $params['nip'];

        $expToken = explode(':', simpleEncrypt($params['token'], 'd'));
        $leaveId = $expToken[0];
        $status = $expToken[1];
        $token = isset($expToken[2]) ? $expToken[2] : "";

        $leaveData = [
            'status' => $status,
            'approved_at_1' => $status == 'APPROVED' ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00',
            'rejected_at_1' => $status == 'REJECTED' ? date('Y-m-d H:i:s') : '0000-00-00 00:00:00'
        ];

        $leave = $this->Hr->getDataById('kifest_leaves_temp', $leaveId);

        if($leave) {
            if($leave->status == 'PROCESS') {
                if($nip == $leave->approver_nip_1) {
                    $this->Hr->updateById("kifest_leaves_temp", $leaveData, $leaveId);
    
                    $emp = $this->HrModel->getEmployeeById($leave->emp_id);
                    $message = $this->load->view('html/absen/email/leave_request_hr', [
                        'emp' => $emp,
                        'leave' => $leave,
                    ], true);
    
                    $sdmEmails = "";
                    $sdms = $this->Hr->getWhere('employees', ['sub_department_id' => 11, 'division_id' => 38])->result();
                    foreach ($sdms as $sdm) {
                        if($sdm->rank_id >= 4) {
                            if($sdm->email != "" && validateEmail($sdm->email)) {
                                $sdmEmails .= $sdmEmails == "" ? $sdm->email : ",$sdm->email"; 
                            }
                        }
                    }
    
                    $emailData = [
                        'alert_name' => 'LEAVE_REQUEST_HR',
                        'email_to' => $sdmEmails,
                        'subject' => "Approval Cuti $emp->employee_name pada tanggal " . toIndoDateDay($leave->date_start) . " sampai " . toIndoDateDay($leave->date_finish),
                        'subject_name' => "Spekta Alert: Approval Cuti $emp->employee_name pada tanggal " . toIndoDateDay($leave->date_start) . " sampai " . toIndoDateDay($leave->date_finish),
                        'message' => $message,
                        'apv_status' => 1,
                        'positive' => '',
                        'negative' => '',
                        'dept_apv' => 3,
                        'sub_dept_apv' => 11,
                        'div_apv' => 38,
                        'token' => time()."_LEAVE_REQUEST_HR"
                    ];
    
                    $this->Main->create('email', $emailData);
                    
                    $this->load->view('html/valid_response', ['message' => "Berhasil <b>$status</b> permintaan koreksi absen"]);
                    if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
                } else {
                    $this->load->view('html/invalid_response', ['message' => "Gagal <b>$status</b> permintaan koreksi absen. Otoritas tidak sesuai!"]);
                }
            } else {
                $this->load->view('html/invalid_response', ['message' => "Gagal <b>" . str_replace("D", "", $status) . "</b> permintaan koreksi absen. Tindakan sudah dilakukan!"]);
                if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            }
        } else {
            if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            $this->load->view('html/invalid_response', ['message' => "Gagal <b>" . str_replace("D", "", $status) . "</b> permintaan koreksi absen. Data tidak ditemukan!"]);
        }
    }

    public function approveOvtPrsRevision(){
        $params = getParam();
        $nip = $params['nip'];

        $expToken = explode(':', simpleEncrypt($params['token'], 'd'));
        $revisionId = $expToken[0];
        $asmanId = $expToken[1];
        $action = $expToken[2];
        $token = isset($expToken[3]) ? $expToken[3] : "";

        $asman = $this->Hr->getDataById('employees', $asmanId);
        if($asman->nip == $nip) {
            $revision = $this->Hr->getDataById('overtime_revision_requests_personil', $revisionId);
            if($revision->status == 'CREATED') {
                if($action == 'APPROVED') {
                    $this->Hr->updateByid('overtime_revision_requests_personil', ['status' => 'APPROVED'], $revisionId);
                    if($token != "") $this->Main->update('email' , ['apv_status' => 1], ['token' => $token]);
                } else {
                    $this->Overtime->backStatusBefore($revision->task_id);
                    $empOvts = $this->Hr->getWhere('employee_overtimes_detail', ['task_id' => $revision->task_id, 'status' => 'CLOSED', 'revision_status !=' => 'NONE'])->result();
                    $dataHistory =[];
                    foreach ($empOvts as $ovt) {
                        $dataHistory[] = [
                            'rev_task_id' => $revision->rev_task_id,
                            'task_id' => $revision->task_id,
                            'emp_task_id' => $ovt->emp_task_id,
                            'status' => $ovt->status,
                            'revision_status' => $ovt->revision_status,
                            'status_before' => $ovt->status_before,
                        ];
                    }

                    if(count($dataHistory) > 0) {
                        $this->Hr->update('employee_overtimes', ['on_revision' => 0], ['task_id' => $revision->task_id]);
                    }

                    $this->Hr->update('overtime_revision_requests_personil', ['status' => 'REJECTED', 'updated_by' => $asmanId, 'updated_at' => date('Y-m-d H:i:s')], ['id' => $revision->id]);
                    $this->Hr->createMultiple('overtime_revision_requests_personil_history', $dataHistory);
                    $requestor = $this->Hr->getDataById('employees', $revision->created_by);
                    $this->sendRevisionPersonilEmail($revision, 'OVERTIME_PERSONIL_REVISION_REJECTION', $requestor);
                    if($token != "") $this->Main->update('email' , ['apv_status' => 1], ['token' => $token]);
                }

                $this->load->view('html/valid_response', ['message' => "Berhasil <b>$action</b> permintaan koreksi lembur"]);
            } else {
                $this->load->view('html/invalid_response', ['message' => "Gagal <b>" . str_replace("D", "", $action) . "</b> approve koreksi lembur. Tindakan sudah dilakukan!"]);
                if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => "Gagal <b>$action</b> permintaan koreksi lembur. Otoritas tidak sesuai!"]);
        }
    }

    public function sendRevisionPersonilEmail($revision, $alertName, $requestor)
    {
        $email = $requestor->email;
        $sdms = $this->Hr->getWhere('employees', ['division_id' => 38])->result();
        foreach ($sdms as $sdm) {
            $email = $email . ',' . $sdm->email;
        }
        
        $fullRev = $this->Overtime->getRevPersonil($revision->rev_task_id);
        $overtimes = $this->Overtime->getRevPersonilOvertime($revision->rev_task_id)->result();
        $message = $this->load->view('html/overtime/email/revision_overtime_personil_sdm', [
            'requestor' => $requestor,
            'revision' => $fullRev,
            'overtimes' => $overtimes,
            'location' => $this->auth->locName,
            'status' => $alertName,
        ], true);

        $prefix = 'Request';
        if ($alertName == 'OVERTIME_PERSONIL_REVISION_REJECTION') {
            $prefix = 'Rejection';
        } else if ($alertName == 'OVERTIME_PERSONIL_REVISION_CLOSED') {
            $prefix = 'Closed';
        }
        $data = [
            'alert_name' => $alertName,
            'email_to' => $email,
            'subject' => "$prefix Revisi Personil Lembur $fullRev->department (Task ID: $revision->rev_task_id)",
            'subject_name' => "Spekta Alert: $prefix Revisi Personil Lembur $fullRev->department (Task ID: $revision->rev_task_id)",
            'message' => $message,
        ];
        $this->Main->create('email', $data);
    }

    public function approveOvtHourRevision() {
        $params = getParam();
        $nip = $params['nip'];

        $expToken = explode(':', simpleEncrypt($params['token'], 'd'));
        $taskId = $expToken[0];
        $asmanId = $expToken[1];
        $action = $expToken[2];
        $token = isset($expToken[3]) ? $expToken[3] : "";
        

        $asman = $this->Hr->getDataById('employees', $asmanId);
        if($asman->nip == $nip) {
            $revision = $this->Hr->getWhere('overtime_revision_requests', ['task_id' => $taskId])->row();
            if($revision->status == 'CREATED') {
                if($action == 'APPROVED') {
                    $this->Hr->updateByid('overtime_revision_requests', ['status' => 'APPROVED'], $revision->id);
                    if($token != "") $this->Main->update('email' , ['apv_status' => 1], ['token' => $token]);
                } else {
                    $this->Hr->update('overtime_revision_requests', ['status' => 'REJECTED', 'updated_by' => $asmanId, 'updated_at' => date('Y-m-d H:i:s')], ['task_id' => $revision->task_id]);
                    $this->Hr->update('overtime_revision_requests_detail', ['status' => 'REJECTED'], ['task_id' => $revision->task_id]);
    
                    $empTasks = $this->Hr->getWhere('overtime_revision_requests_detail', ['task_id' => $revision->task_id])->result();
                    $empTaskIds = '';
                    foreach ($empTasks as $rev) {
                        if ($empTaskIds == '') {
                            $empTaskIds = $rev->emp_task_id;
                        } else {
                            $empTaskIds = $empTaskIds . "," . $rev->emp_task_id;
                        }
                    }
                    $requestor = $this->Hr->getDataById('employees', $revision->created_by);
                    $this->sendRevisionEmail($revision->task_id, $empTaskIds, 'OVERTIME_REVISION_REJECTION', $requestor);
                    if($token != "") $this->Main->update('email' , ['apv_status' => 1], ['token' => $token]);
                }

                $this->load->view('html/valid_response', ['message' => "Berhasil <b>$action</b> permintaan koreksi jam lembur"]);
            } else {
                $this->load->view('html/invalid_response', ['message' => "Gagal <b>" . str_replace("D", "", $action) . "</b> approve koreksi jam lembur. Tindakan sudah dilakukan!"]);
                if($token != '') $this->Main->update('email', ['apv_status' => 1], ['token' => $token]);
            }
        } else {
            $this->load->view('html/invalid_response', ['message' => "Gagal <b>$action</b> permintaan koreksi jam lembur. Otoritas tidak sesuai!"]);
        }
    }

    public function sendRevisionEmail($taskId, $empTaskIds, $alertName, $requestor)
    {
        $email = $requestor->email;
        $sdms = $this->Hr->getWhere('employees', ['division_id' => 38])->result();
        foreach ($sdms as $sdm) {
            $email = $email . ',' . $sdm->email;
        }
        $overtimes = $this->Overtime->getOvertimeDetail(['in_emp_task_id' => $empTaskIds])->result();
        $revision = $this->Overtime->getRevOvtGrid(['in_task_id' => $taskId])->row();
       
        $message = $this->load->view('html/overtime/email/revision_overtime', [
            'requestor' => $requestor,
            'overtimes' => $overtimes,
            'revision' => $revision,
            'location' => $this->auth->locName,
            'status' => $alertName,
        ], true);

        $prefix = 'Request';
        if ($alertName == 'OVERTIME_REVISION_REJECTION') {
            $prefix = 'Rejection';
        } else if ($alertName == 'OVERTIME_REVISION_CLOSED') {
            $prefix = 'Closed';
        }
        
        $data = [
            'alert_name' => $alertName,
            'email_to' => $email,
            'subject' => "$prefix Revisi Lembur $revision->department (Task ID: $taskId)",
            'subject_name' => "Spekta Alert: $prefix Revisi Lembur $revision->department (Task ID: $taskId)",
            'message' => $message,
        ];

        $this->Main->create('email', $data);
    }

    public function emailMessage() {
        $token = getParam()['token'];
        $email = $this->Main->getWhere('email', ['token' => $token])->row();
        
        if($email && $email->alert_name == 'TRIP_FORM_FOR_DRIVER') {
            if($email->redirect != '') {
                redirect($email->redirect);
            } else {
                $this->load->view('html/invalid_response', ['message' => "Halaman tidak ditemukan"]);
            }
        } else {
            if($email) {
                $this->load->view('html/email_message', ['message' => $email->message]);
            } else {
                $this->load->view('html/invalid_response', ['message' => "Halaman tidak ditemukan"]);
            }
        }
    }
}
