<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


//@my constants
define('MAIN_DB', 'kf_main');
define('HR_DB', 'kf_hr');
define('QHSE_DB', 'kf_qhse');
define('GENERAL_DB', 'kf_general');
define('MTN_DB', 'kf_maintenance');
define('PROD_DB', 'kf_production');
define('CHAT_DB', 'kf_chat');

define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'J@nuari1993');
define('DB_HOST', 'localhost:3306');

define('MAIN_DB_DEV', 'kf_main');
define('HR_DB_DEV', 'kf_hr');
define('QHSE_DB_DEV', 'kf_qhse');
define('GENERAL_DB_DEV', 'kf_general');
define('MTN_DB_DEV', 'kf_maintenance');
define('PROD_DB_DEV', 'kf_production');
define('CHAT_DB_DEV', 'kf_chat');

define('DB_USERNAME_DEV', 'root');
define('DB_PASSWORD_DEV', 'J@nuari1993');
define('DB_HOST_DEV', 'localhost:3306');

define('PERMANENT_USER', ['superuser' => true]);

define('SESSION_KEY', '25d55ad283aa400af464c76d713c07ad#e-kfpj');

define('EMAIL_PROTOCOL', 'smtp');
define('SMTP_HOST', 'rupakenca.iixcp.rumahweb.com');
define('SMTP_PORT', 465);
define('SYSTEM_MAIL', 'admin@spekta.id');
define('SYSTEM_MAILPASS', 'januari1993');
define('SYSTEM_MAIL_ALIAS',  "admin@spekta.id");
define('EMAIL_SSL', true);

define('LOGO_KF', 'https://goreklame.com/assets/logo-kf.png');
define('PIN_ADMIN', 'ea4gnr009');
define('LIVE_URL', 'http://192.168.1.17:5500/index.php/');
define('SHARE_URL', 'http://192.168.1.17:5500/share/overtime/');
define('HOME_PATH', 'http://192.168.1.17:5500/spektachat/');
define('IMG_URL', 'http://192.168.1.17:5500/');

define('KIFEST_TOKEN', 'PKNpa2sztT5icgfychRh9HnZgja86LmMDWgMHAcYdA9Mel8xSCF3COSMdWMHdPzTpjBN0krxrrsV7AHr9p1X1YIGUsx5UbIwsLX9KMBUfHJWqMZDvSkUQBam5qBNspUx');
define('KIFEST_ENDPOINT_LEAVE', 'https://kifest.kimiafarma.co.id/v3/api/v1/employee-leaves');
define('KIFEST_ENDPOINT_VISIT', 'https://kifest.kimiafarma.co.id/v3/api/v1/employee-presence-visits');

define('DEVICE_ID', 'TP1A.220624.014-SAMSUNG-SM-M526BR-M52XQ-DAE5DCF77CC7BF89');
define('FCM_TOKEN', 'flEljo4YTbq3fYtT5p7Q6j:APA91bF-q-dh9iiBLxO3Nq17QNqn2NiF-zaqB0VWQwOgvm0v1iLL5CQArcFiGNCD8xMDplf-5eWfCZBcO7wr4uek0QaZo_ob7VcyhOZjvFlYeJowbcEfWCa9NveDtWKpxqKTCozmZ20n');
define('OLD_VERSION', '0.0.0');