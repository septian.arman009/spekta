<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GeneralModel extends CI_Model
{
    public function myConstruct($db_name = true)
    {
        parent::__construct();
        $this->db = $this->load->database($db_name, true);
       
        $this->kf_chat = $this->auth->kf_chat;
        $this->kf_general = $this->auth->kf_general;
        $this->kf_hr = $this->auth->kf_hr;
        $this->kf_main = $this->auth->kf_main;
        $this->kf_mtn = $this->auth->kf_mtn;
        $this->kf_qhse = $this->auth->kf_qhse;
        $this->empLoc = empLoc();
    }

    public function getGustBooks($params)
    {
        $month = $params['month'];
        $year = $params['year'];

        unset($params['month']);
        unset($params['year']);
        
        $where = advanceSearch($params);
        $sql = "select 
                a.*
                from guest_books_form a
                where a.location = '$this->empLoc'
                and year(a.visit_time) = '$year'
                and month(a.visit_time) = '$month'
                $where";
                    
        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (
                       a.name like '%$params[search]%' OR
                       a.nik like '%$params[search]%' OR
                       a.organization like '%$params[search]%' OR
                       a.company like '%$params[search]%' OR
                       a.employee like '%$params[search]%' OR
                       a.unit like '%$params[search]%' OR
                       a.visit_needs like '%$params[search]%' OR
                       a.visit_time like '%$params[search]%' OR
                       a.phone like '%$params[search]%'
                    )";
        } 
        $sql .= " ORDER BY a.name ASC";
        return $this->db->query($sql);
    }

    public function getMenus($params)
    {
        $where = advanceSearch($params);
        $sql = "select 
                    a.id,
                    a.name,
                    a.location,
                    a.description,
                    a.available,
                    a.picked,
                    a.filename,
                    b.employee_name as created_by,
                    c.employee_name as updated_by
                from menus a
                inner join kf_hr.employees b on a.created_by = b.id
                inner join kf_hr.employees c on a.updated_by = c.id
                WHERE a.location = '$this->empLoc'
                $where";
                    
        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (
                       a.name like '%$params[search]%' OR
                       a.description like '%$params[search]%' OR
                       a.available like '%$params[search]%' OR
                       a.picked like '%$params[search]%' OR
                       b.employee_name like '%$params[search]%' OR
                       c.employee_name like '%$params[search]%'
                    )";
        } 
        $sql .= " ORDER BY a.name ASC";
        return $this->db->query($sql);
    }
}