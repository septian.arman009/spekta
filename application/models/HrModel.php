<?php
defined('BASEPATH') or exit('No direct script access allowed');

class HrModel extends CI_Model
{
    public function myConstruct($db_name = true)
    {
        parent::__construct();
        $this->db = $this->load->database($db_name, true);

        $this->kf_chat = $this->auth->kf_chat;
        $this->kf_general = $this->auth->kf_general;
        $this->kf_hr = $this->auth->kf_hr;
        $this->kf_main = $this->auth->kf_main;
        $this->kf_mtn = $this->auth->kf_mtn;
        $this->kf_qhse = $this->auth->kf_qhse;

        $this->load->model('BasicModel', 'BM');
        $this->BM->myConstruct('hr');

        $this->empLoc = empLoc();
    }

    public function getAbsens($table, $from, $to)
    {
        $checkTable = $this->BM->checkTable('kf_hr', $table);
        $absenList = [];
        if ($checkTable) {
            $absens = $this->db->query("SELECT * FROM $table WHERE abs_date BETWEEN '$from' AND '$to'")->result();
            foreach ($absens as $absen) {
                $absenList[$absen->abs_date] = $absen;
            }
        }

        return $absenList;
    }

    public function checkLeaveExist($start, $end, $nip)
    {
        $kifest = $this->db->query("select * from kifest_leaves where (date_start between '$start' and '$end' or date_finish between '$start' and '$end') and employee_npp = '$nip'")->row();
        $kifest_temp = $this->db->query("select * from kifest_leaves_temp where (date_start between '$start' and '$end' or date_finish between '$start' and '$end') and employee_npp = '$nip' and status != 'REJECTED'")->row();

        if (!$kifest && !$kifest_temp) {
            return false;
        } else {
            return true;
        }
    }

    public function getEmployeeById($empId)
    {
        $sql = "SELECT a.*,b.name AS division_name,c.name AS department_name,d.name AS rank_name,e.name AS sub_department_name,
                    (SELECT employee_name FROM employees WHERE nip = a.direct_spv) AS direct_spv_name
                    FROM employees a, divisions b, departments c, ranks d, sub_departments e 
                    WHERE a.division_id = b.id
                    AND a.department_id = c.id
                    AND a.rank_id = d.id
                    AND a.sub_department_id = e.id
                    AND a.id = $empId";

        return $this->db->query($sql)->row();
    }

    public function getEmployee($get)
    {
        $status = "and a.status != 'DELETE'";

        if (isset($get['filter_status'])) {
            if ($get['filter_status'] == "ACTIVE") {
                $status = "and a.status = 'ACTIVE'";
            } else if ($get['filter_status'] == 'INACTIVE') {
                $status = "and a.status = 'INACTIVE'";
            } else if ($get['filter_status'] == 'DELETE') {
                $status = "and a.status = 'DELETE'";
            }

            unset($get['filter_status']);
        }

        $where = advanceSearch($get);
        $location = $this->auth->isLogin() ? "AND a.location = '$this->empLoc'" : null;

        $sql = "select
                    a.*,
                    b.name as division_name,
                    c.name as sub_name,
                    d.name as dept_name,
                    e.name as rank_name,
                    f.employee_name as direct_spv_name,
                    g.work_start,
                    g.work_end,
                    g.name as shift_name
                    from employees a
                    left join divisions b on a.division_id = b.id 
                    left join sub_departments c on a.sub_department_id = c.id
                    left join departments d on a.department_id = d.id
                    left join ranks e on a.rank_id = e.id
                    left join employees f on a.direct_spv = f.nip
                    left join work_time g on a.shift_id = g.id
                    where a.nip != '9999' $status
                    $where
                    $location";

        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (
                        a.employee_name LIKE '%$get[search]%' OR 
                        a.NIP LIKE '%$get[search]%' OR
                        a.birth_place LIKE '%$get[search]%' OR
                        a.birth_date LIKE '%$get[search]%' OR
                        a.employee_status LIKE '%$get[search]%' OR
                        b.name LIKE '%$get[search]%' OR
                        c.name LIKE '%$get[search]%' OR
                        d.name LIKE '%$get[search]%' OR
                        e.name LIKE '%$get[search]%'
                    )";
        }
        $sql .= " ORDER BY a.employee_name ASC";
        return $this->db->query($sql);
    }

    public function getEmpSallary($get)
    {
        $where = advanceSearch($get);
        $sql = "SELECT a.*,b.basic_sallary,b.total_sallary,b.premi_overtime,c.name AS department,d.name AS sub_department, e.name AS division,f.name AS rank_name, f.grade,
                       (SELECT employee_name FROM employees WHERE id = b.created_by) AS emp1,
                       (SELECT employee_name FROM employees WHERE id = b.updated_by) AS emp2
                    FROM employees a, employee_sallary b, departments c, sub_departments d, divisions e, ranks f
                    WHERE a.id = b.emp_id
                    AND a.department_id = c.id
                    AND a.sub_department_id = d.id
                    AND a.division_id = e.id
                    AND a.rank_id = f.id
                    AND a.nip != '9999'
                    AND a.status = 'ACTIVE'
                    $where
                    AND a.location = '$this->empLoc'";

        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (
                        a.employee_name LIKE '%$get[search]%' OR 
                        (SELECT employee_name FROM employees WHERE id = b.created_by) LIKE '%$get[search]%' OR
                        (SELECT employee_name FROM employees WHERE id = b.updated_by) LIKE '%$get[search]%' OR
                        b.basic_sallary LIKE '%$get[search]%' OR
                        b.premi_overtime LIKE '%$get[search]%' OR
                        c.name LIKE '%$get[search]%' OR
                        d.name LIKE '%$get[search]%' OR
                        e.name LIKE '%$get[search]%'
                    )";
        }
        $sql .= " ORDER BY a.employee_name ASC";
        return $this->db->query($sql);
    }

    public function getRanks($empId)
    {
        return $this->db->select('a.*,b.name AS dept_name,c.name AS sub_name,d.name AS division_name, e.name AS rank_name')
            ->from('employee_ranks a')
            ->join('departments b', 'a.department_id = b.id')
            ->join('sub_departments c', 'a.sub_department_id = c.id')
            ->join('divisions d', 'a.division_id = d.id')
            ->join('ranks e', 'a.rank_id = e.id')
            ->where('a.emp_id', $empId)
            ->order_by('a.sk_date', 'DESC')
            ->get()
            ->result();
    }

    public function getEmpByUserId($userId)
    {
        return $this->db->select("a.*,b.name AS dept_name,c.name AS sub_name,d.name AS rank_name,e.name AS division_name")
            ->from('employees a')
            ->join('departments b', 'a.department_id = b.id')
            ->join('sub_departments c', 'a.sub_department_id = c.id')
            ->join('ranks d', 'a.rank_id = d.id')
            ->join('divisions e', 'a.division_id = e.id')
            ->where('a.user_id', $userId)
            ->get()
            ->row();
    }

    public function getPlt($empId)
    {
        return $this->db->select("a.*,b.name AS department,c.name AS sub_department,d.name AS division")
            ->from('employee_ranks a')
            ->join('departments b', 'a.department_id = b.id')
            ->join('sub_departments c', 'a.sub_department_id = c.id')
            ->join('divisions d', 'a.division_id = d.id')
            ->where('a.emp_id', $empId)
            ->where('a.status', 'ACTIVE')
            ->get()
            ->row();
    }

    public function getTrainings($empId)
    {
        return $this->db->select('a.*,b.name AS training_name')
            ->from('employee_trainings a')
            ->join('trainings b', 'a.training_id = b.id')
            ->where('a.emp_id', $empId)
            ->order_by('a.certificate_date', 'DESC')
            ->get()
            ->result();
    }

    public function getRequirement()
    {
        return $this->db->select('a.*,b.name AS division_name')
            ->from('overtime_requirement a')
            ->join('divisions b', 'a.division_id = b.id')
            ->get()
            ->result();
    }

    public function subWithDept()
    {
        return $this->db->select("a.*,b.name AS dept_name")
            ->from('sub_departments a')
            ->join('departments b', 'a.department_id = b.id')
            ->get()
            ->result();
    }

    public function divWithSub()
    {
        return $this->db->select("a.*,b.name AS sub_name")
            ->from('divisions a')
            ->join('sub_departments b', 'a.sub_department_id = b.id')
            ->get()
            ->result();
    }

    public function getPins()
    {
        return $this->db->select("a.pin,a.location,,b.email,b.employee_name")
            ->from("employee_pins a")
            ->join("employees b", "a.emp_id = b.id")
            ->get()
            ->result();
    }

    public function getRequestList($overtime)
    {
        $reqs = [];
        if ($overtime->ahu > 0) {
            $reqs[] = $overtime->ahu;
        }

        if ($overtime->compressor > 0) {
            $reqs[] = $overtime->compressor;
        }

        if ($overtime->pw > 0) {
            $reqs[] = $overtime->pw;
        }

        if ($overtime->steam > 0) {
            $reqs[] = $overtime->steam;
        }

        if ($overtime->dust_collector > 0) {
            $reqs[] = $overtime->dust_collector;
        }

        if ($overtime->wfi > 0) {
            $reqs[] = $overtime->wfi;
        }

        if ($overtime->mechanic > 0) {
            $reqs[] = $overtime->mechanic;
        }

        if ($overtime->electric > 0) {
            $reqs[] = $overtime->electric;
        }

        if ($overtime->hnn > 0) {
            $reqs[] = $overtime->hnn;
        }

        if ($overtime->jemputan > 0) {
            $reqs[] = $overtime->jemputan;
        }

        if ($overtime->qc > 0) {
            $reqs[] = $overtime->qc;
        }

        if ($overtime->qa > 0) {
            $reqs[] = $overtime->qa;
        }

        if ($overtime->penandaan > 0) {
            $reqs[] = $overtime->penandaan;
        }

        if ($overtime->gbb > 0) {
            $reqs[] = $overtime->gbb;
        }

        if ($overtime->gbk > 0) {
            $reqs[] = $overtime->gbk;
        }

        $name = '';
        $array = [];

        if (count($reqs) > 0) {
            $requires = $this->Hr->getWhereIn('overtime_requirement', ['id' => $reqs])->result();
            foreach ($requires as $req) {
                if ($name == '') {
                    $name = $req->name;
                } else {
                    $name = $name . ',' . $req->name;
                }
                $array[] = $req->name;
            }
        }

        return [
            'string' => $name !== '' ? "($name)" : null,
            'array' => $array
        ];
    }

    public function getPinGrid($get)
    {
        $where = advanceSearch($get);
        $sql = "SELECT a.*,b.employee_name,b.nip,c.name AS department,d.name AS sub_department,e.name AS division,f.name AS rank_name,
                       (SELECT employee_name FROM employees WHERE id = a.created_by) AS emp1,
                       (SELECT employee_name FROM employees WHERE id = a.updated_by) AS emp2
                    FROM employee_pins a, employees b, departments c, sub_departments d, divisions e, ranks f
                    WHERE a.emp_id = b.id
                    AND b.department_id = c.id
                    AND b.sub_department_id = d.id
                    AND b.division_id = e.id
                    AND b.rank_id = f.id
                    $where
                    AND a.location = '$this->empLoc'";

        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (
                        b.employee_name LIKE '%$get[search]%' OR 
                        (SELECT employee_name FROM employees WHERE id = a.created_by) LIKE '%$get[search]%' OR
                        (SELECT employee_name FROM employees WHERE id = a.updated_by) LIKE '%$get[search]%' OR
                        c.name LIKE '%$get[search]%' OR
                        d.name LIKE '%$get[search]%' OR
                        e.name LIKE '%$get[search]%' OR
                        f.name LIKE '%$get[search]%'
                    )";
        }
        $sql .= " ORDER BY b.employee_name ASC";
        return $this->db->query($sql);
    }

    public function getPinDetail($get, $id)
    {
        $where = advanceSearch($get);
        $sql = "SELECT a.*,b.employee_name,b.nip,c.name AS department,d.name AS sub_department,e.name AS division,f.name AS rank_name
                    FROM employee_pins a, employees b, departments c, sub_departments d, divisions e, ranks f
                    WHERE a.emp_id = b.id
                    AND b.department_id = c.id
                    AND b.sub_department_id = d.id
                    AND b.division_id = e.id
                    AND b.rank_id = f.id
                    AND a.id = '$id'
                    ORDER BY b.employee_name ASC";
        return $this->db->query($sql)->row();
    }

    public function getEmpNearExpired($date, $location)
    {
        $date = explode('-', $date);
        $sql = "SELECT a.*,b.employee_name,b.nip,c.name AS department,d.name AS sub_department,e.name AS division,f.name AS rank_name
                    FROM employees a, employees b, departments c, sub_departments d, divisions e, ranks f
                    WHERE a.id = b.id
                    AND b.department_id = c.id
                    AND b.sub_department_id = d.id
                    AND b.division_id = e.id
                    AND b.rank_id = f.id
                    AND YEAR(a.sk_end_date) = '$date[0]'
                    AND MONTH(a.sk_end_date) = '$date[1]'
                    AND a.sk_end_date != '0000-00-00'
                    AND a.employee_status != 'Permanen'
                    AND a.status = 'ACTIVE'
                    AND a.location = '$location'
                    ORDER BY b.employee_name ASC";
        return $this->db->query($sql)->result();
    }

    public function updateEmpAge()
    {
        return $this->db->query("UPDATE employees 
                                    SET age = DATE_FORMAT(FROM_DAYS(DATEDIFF(now(),birth_date)), '%Y')+0 
                                    WHERE birth_date = NOW()");
    }

    public function getAbsenList($param)
    {
        $absenTableStart = "";
        $absenTableEnd = "";
        $costCenter = isset($param['cost_center']) ? $param['cost_center'] : "";
        $customSearch = isset($param['customSearch']) ? $param['customSearch'] : "";
        if (isset($param['customSearch'])) unset($param['customSearch']);

        $deptId = isset($param['deptId']) && $param['deptId'] > 0 ? $param['deptId'] : "";
        $subDeptId = isset($param['subDeptId']) && $param['subDeptId'] > 0 ? $param['subDeptId'] : "";
        $divId = isset($param['divId']) && $param['divId'] > 0 ? $param['divId'] : "";
        if(isset($param['deptId'])) unset($param['deptId']);
        if(isset($param['subDeptId'])) unset($param['subDeptId']);
        if(isset($param['divId'])) unset($param['divId']);

        if (isset($param['start']) && isset($param['end'])) {
            $start = $param['start'];
            $end = $param['end'];
            $status = $param['status'];
            $empStatus = isset($param['empStatus']) ? $param['empStatus'] : "";

            if (isset($param['start'])) unset($param['start']);
            if (isset($param['end'])) unset($param['end']);
            if (isset($param['status'])) unset($param['status']);
            if (isset($param['empStatus'])) unset($param['empStatus']);

            $yearStart = date('Y', strtotime($start));
            $monthStart = date('m', strtotime($start));
            $yearEnd = date('Y', strtotime($end));
            $monthEnd = date('m', strtotime($end));

            $absenTableStart = absenTable($yearStart, $monthStart);
            $absenTableEnd = absenTable($yearEnd, $monthEnd);

            $columnAbsDate = 'a.abs_date';
            $columnStatus = 'a.date_in';

            $where = advanceSearch($param);

            if ($where == "") {
                if ($absenTableStart != $absenTableEnd) {
                    $where .= "AND $columnAbsDate BETWEEN '$start' AND '$end'";
                } else {
                    $where .= "AND $columnAbsDate BETWEEN '$start' AND '$end'";
                }
            } else {
                $where .= "AND $columnAbsDate BETWEEN '$start' AND '$end'";
            }

            if (isset($status) && $status != 'ALL') {
                if ($status == 'MASUK') {
                    $where .= "AND ($columnStatus is not null)";
                } else if ($status == 'TIDAK MASUK') {
                    $where .= "AND ($columnStatus is null)";
                }
            }
        } else if (isset($param['year']) && isset($param['month']) && isset($param['day'])) {

            $year = $param['year'];
            $month = $param['month'];
            $day = $param['day'];

            if (isset($param['year'])) unset($param['year']);
            if (isset($param['month'])) unset($param['month']);
            if (isset($param['day'])) unset($param['day']);

            $absenTableStart = absenTable($year, $month);

            $where = advanceSearch($param);
            $where .= " AND a.abs_date = '$year-$month-$day'";
        } else if (isset($param['year']) && isset($param['month'])) {
            $year = $param['year'];
            $month = $param['month'];

            if (isset($param['year'])) unset($param['year']);
            if (isset($param['month'])) unset($param['month']);

            $absenTableStart = absenTable($year, $month);

            $where = advanceSearch($param);
        }

        if ($costCenter != "") {
            $where .= " AND (a.div_cost_center like '%$costCenter%' OR 
                            a.sub_cost_center like '%$costCenter%' OR
                            a.dept_cost_center like '%$costCenter%' OR
                            a.rank_cost_center like '%$costCenter%')";
        }

        $exportWhere = "";
        if($deptId != "") {
            $exportWhere .= " AND b.department_id = $deptId";
        }

        if ($subDeptId != "") {
            $exportWhere .= " AND b.sub_department_id = $subDeptId";
        }

        if ($divId != "") {
            $exportWhere .= " AND b.division_id = $divId";
        }

        $filterEmpStatus = "";
        if (isset($empStatus) && $empStatus != "" && $empStatus != "ALL") {
            $filterEmpStatus = " AND b.status = '$empStatus'";
        }

        if ($absenTableEnd != "" && $absenTableStart != $absenTableEnd) {
            $sql = "
                SELECT * FROM(
                    SELECT CONCAT('$absenTableStart-' , a.id) as id,a.location, a.emp_id, b.nip, b.sap_id, a.gate, a.distance, a.distance_out,a.abs_date, a.sch_date_in, a.sch_date_out, a.date_in, a.date_out,
                        a.qr_code_in, a.qr_code_out, a.shift, a.correction_status, a.on_leave, a.visit_id, a.shift_id,
                        b.employee_name,
                        c.name AS department_name,
                        c.cost_center AS dept_cost_center,
                        d.name AS sub_department_name,
                        d.cost_center AS sub_cost_center,
                        e.name AS division_name,
                        e.cost_center AS div_cost_center,
                        r.cost_center AS rank_cost_center,
                        r.name AS rank_name,
                        ovt.payment_status,
                        ovt.start_date as ovt_start_date,
                        ovt.end_date as ovt_end_date,
                        ovt.total_meal as ovt_meal,
                        kl.employee_leave_type_name as leave_name,
                        c2.take_date as take_date_1,
                        c3.take_date as take_date_2,
                        wt.meal,
                        b.employee_status
                    FROM $absenTableStart a
                    INNER JOIN employees b ON a.emp_id = b.id
                    LEFT JOIN departments c ON b.department_id = c.id 
                    LEFT JOIN sub_departments d ON b.sub_department_id = d.id 
                    LEFT JOIN divisions e ON b.division_id = e.id
                    INNER JOIN ranks r ON r.id = b.rank_id
                    LEFT JOIN employee_overtimes_detail ovt ON ovt.emp_id = a.emp_id AND ovt.overtime_date = a.abs_date AND ovt.status not in('CANCELED', 'REJECTED')
                    LEFT JOIN kifest_leaves kl on kl.date_start = a.abs_date or kl.date_finish = a.abs_date and kl.emp_id = a.emp_id  
                    LEFT JOIN kf_general.canteen c2 on date(c2.take_date) = a.abs_date and c2.emp_id = a.emp_id
                    LEFT JOIN kf_general.canteen_overtime c3 on date(c3.take_date) = a.abs_date and c3.emp_id = a.emp_id
                    LEFT JOIN kf_hr.work_time wt on a.shift_id = wt.id 
                    WHERE a.location = '$this->empLoc' $filterEmpStatus $customSearch $exportWhere
                    GROUP BY a.id, a.abs_date
                    UNION 
                    SELECT CONCAT('$absenTableEnd-', a.id) as id,a.location, a.emp_id, b.nip, b.sap_id, a.gate, a.distance, a.distance_out,a.abs_date, a.sch_date_in, a.sch_date_out, a.date_in, a.date_out,
                        a.qr_code_in, a.qr_code_out, a.shift, a.correction_status, a.on_leave, a.visit_id, a.shift_id,
                        b.employee_name,
                        c.name AS department_name,
                        c.cost_center AS dept_cost_center,
                        d.name AS sub_department_name,
                        d.cost_center AS sub_cost_center,
                        e.name AS division_name,
                        e.cost_center AS div_cost_center,
                        r.cost_center AS rank_cost_center,
                        r.name AS rank_name,
                        ovt.payment_status,
                        ovt.start_date as ovt_start_date,
                        ovt.end_date as ovt_end_date,
                        ovt.total_meal as ovt_meal,
                        kl.employee_leave_type_name as leave_name,
                        c2.take_date as take_date_1,
                        c3.take_date as take_date_2,
                        wt.meal,
                        b.employee_status
                    FROM $absenTableEnd a
                    INNER JOIN employees b ON a.emp_id = b.id
                    LEFT JOIN departments c ON b.department_id = c.id 
                    LEFT JOIN sub_departments d ON b.sub_department_id = d.id 
                    LEFT JOIN divisions e ON b.division_id = e.id
                    INNER JOIN ranks r ON r.id = b.rank_id
                    LEFT JOIN employee_overtimes_detail ovt ON ovt.emp_id = a.emp_id AND ovt.overtime_date = a.abs_date AND ovt.status not in('CANCELED', 'REJECTED')
                    LEFT JOIN kifest_leaves kl on (kl.date_start = a.abs_date or kl.date_finish = a.abs_date) and kl.emp_id = a.emp_id  
                    LEFT JOIN kf_general.canteen c2 on date(c2.take_date) = a.abs_date and c2.emp_id = a.emp_id
                    LEFT JOIN kf_general.canteen_overtime c3 on date(c3.take_date) = a.abs_date and c3.emp_id = a.emp_id
                    LEFT JOIN kf_hr.work_time wt on a.shift_id = wt.id 
                    WHERE a.location = '$this->empLoc' $filterEmpStatus $customSearch $exportWhere
                    GROUP BY a.id, a.abs_date
                ) AS a WHERE 1=1 $where 
                ORDER BY employee_name,abs_date ASC";
        } else {
            $sql = "SELECT * FROM (
                        SELECT a.id,a.location, a.emp_id, b.nip, b.sap_id, a.gate, a.distance, a.distance_out,a.abs_date, a.sch_date_in, a.sch_date_out, a.date_in, a.date_out,
                            a.qr_code_in, a.qr_code_out, a.shift, a.correction_status, a.on_leave, a.visit_id, a.shift_id,
                            b.employee_name,
                            c.name AS department_name,
                            c.cost_center AS dept_cost_center,
                            d.name AS sub_department_name,
                            d.cost_center AS sub_cost_center,
                            e.name AS division_name,
                            e.cost_center AS div_cost_center,
                            r.cost_center AS rank_cost_center,
                            r.name AS rank_name,
                            ovt.payment_status,
                            ovt.start_date as ovt_start_date,
                            ovt.end_date as ovt_end_date,
                            ovt.total_meal as ovt_meal,
                            kl.employee_leave_type_name as leave_name,
                            c2.take_date as take_date_1,
                            c3.take_date as take_date_2,
                            wt.meal,
                            b.employee_status
                        FROM $absenTableStart a
                        INNER JOIN employees b ON a.emp_id = b.id
                        LEFT JOIN departments c ON b.department_id = c.id 
                        LEFT JOIN sub_departments d ON b.sub_department_id = d.id 
                        LEFT JOIN divisions e ON b.division_id = e.id
                        INNER JOIN ranks r ON r.id = b.rank_id
                        LEFT JOIN employee_overtimes_detail ovt ON ovt.emp_id = a.emp_id AND ovt.overtime_date = a.abs_date AND ovt.status not in('CANCELED', 'REJECTED')
                        LEFT JOIN kifest_leaves kl on (kl.date_start = a.abs_date or kl.date_finish = a.abs_date) and kl.emp_id = a.emp_id  
                        LEFT JOIN kf_general.canteen c2 on date(c2.take_date) = a.abs_date and c2.emp_id = a.emp_id
                        LEFT JOIN kf_general.canteen_overtime c3 on date(c3.take_date) = a.abs_date and c3.emp_id = a.emp_id
                        LEFT JOIN kf_hr.work_time wt on a.shift_id = wt.id 
                        WHERE a.location = '$this->empLoc' $filterEmpStatus $customSearch $exportWhere
                        GROUP BY a.id, a.abs_date
                        ORDER BY b.employee_name,a.abs_date ASC
                    ) AS a WHERE 1=1 $where";
        }

        return $this->db->query($sql);
    }


    public function getOvertimeList($param)
    {
        $absenTable = absenTable($param['year'], $param['month']);

        $year = $param['year'];
        $month = $param['month'];
        $day = $param['day'];

        unset($param['month']);
        unset($param['year']);
        unset($param['day']);

        $where = advanceSearch($param);
        $sql = "select 
                b.id, b.location, b.emp_id, b.gate, b.distance, b.distance_out, b.abs_date, b.date_in, b.date_out,
                b.qr_code_in, b.qr_code_out, b.shift, b.correction_status,
                c.employee_name,
                d.name AS department_name,
                d.cost_center AS dept_cost_center,
                e.name AS sub_department_name,
                e.cost_center AS sub_cost_center,
                f.name AS division_name,
                f.cost_center AS div_cost_center,
                g.cost_center AS rank_cost_center,
                a.payment_status,
                a.start_date as sch_date_in,
	            a.end_date as sch_date_out,
                a.total_meal as meal,
                c2.take_date as take_date_1,
                c3.take_date as take_date_2
                from employee_overtimes_detail a
                inner join $absenTable b on a.overtime_date = b.abs_date and a.emp_id = b.emp_id
                inner join employees c on b.emp_id = c.id 
                inner join departments d on c.department_id = d.id 
                inner join sub_departments e on c.sub_department_id = e.id 
                inner join divisions f on c.division_id = f.id 
                inner join ranks g on c.rank_id = g.id
                left join kf_general.canteen_overtime c2 on date(c2.take_date) = a.overtime_date and c2.emp_id = a.emp_id
                left join kf_general.canteen_overtime_2 c3 on date(c3.take_date) = a.overtime_date and c3.emp_id = a.emp_id
                where a.location = '$this->empLoc'
                and a.overtime_date = '$year-$month-$day'
                and a.status not in('CANCELED', 'REJECTED')
                $where
                order by a.overtime_date desc";

        return $this->db->query($sql);
    }

    public function countCanteenSheet()
    {
        $date = date('Y-m-d');
        $totalCanteen = $this->db->query("select count(1) as total from $this->kf_general.canteen where out_date is null and date(take_date) = '$date'")->row()->total;
        $totalCanteenOvt = $this->db->query("select count(1) as total from $this->kf_general.canteen_overtime where out_date is null and date(take_date) = '$date'")->row()->total;
        $totalCanteenOvt2 = $this->db->query("select count(1) as total from $this->kf_general.canteen_overtime_2 where out_date is null and date(take_date) = '$date'")->row()->total;
        return $totalCanteen + $totalCanteenOvt + $totalCanteenOvt2;
    }

    public function getInfoBoard($params)
    {
        $where = advanceSearch($params);
        $sql = "select 
                    a.id,
                    a.location,
                    a.name,
                    a.description,
                    a.is_active,
                    a.filename,
                    b.employee_name as created_by,
                    c.employee_name as updated_by
                from information_board a
                inner join kf_hr.employees b on a.created_by = b.id
                inner join kf_hr.employees c on a.updated_by = c.id
                WHERE a.location = '$this->empLoc'
                $where";

        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (
                       a.name like '%$params[search]%' OR
                       a.description like '%$params[search]%' OR
                       b.employee_name like '%$params[search]%' OR
                       c.employee_name like '%$params[search]%'
                    )";
        }
        $sql .= " ORDER BY a.name ASC";
        return $this->db->query($sql);
    }

    public function getFiles($params)
    {
        $where = advanceSearch($params);
        $sql = "select 
                    a.id,
                    a.location,
                    a.name,
                    a.description,
                    a.filename,
                    b.employee_name as created_by,
                    a.created_at
                from files a
                inner join kf_hr.employees b on a.created_by = b.id
                WHERE a.location = '$this->empLoc'
                $where";

        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (
                       a.name like '%$params[search]%' OR
                       a.description like '%$params[search]%' OR
                       b.employee_name like '%$params[search]%'
                    )";
        }
        $sql .= " ORDER BY a.name ASC";
        return $this->db->query($sql);
    }

    public function getFilesTemp($params)
    {
        $where = advanceSearch($params);
        $sql = "select
                    a.*,
                    b.name as department_name,
                    c.name as sub_department_name,
                    d.name as division_name,
                    e.name as rank_name,
                    f.employee_name as created_name,
                    g.employee_name as direct_spv_name
                    from files_temp a
                    left join departments b on a.department_id = b.id
                    left join sub_departments c on a.sub_department_id = c.id
                    left join divisions d on a.division_id = d.id 
                    left join ranks e on a.rank_id = e.id 
                    left join employees f on a.created_by = f.id
                    left join employees g on a.direct_spv = g.id
                WHERE a.location = '$this->empLoc'
                $where";

        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (
                       a.employee_name like '%$params[search]%' OR
                       a.nip like '%$params[search]%' OR
                       a.sap_id like '%$params[search]%' OR
                       a.nik like '%$params[search]%' OR
                       a.employee_name like '%$params[search]%' OR
                       b.name like '%$params[search]%' OR
                       c.name like '%$params[search]%' OR
                       d.name like '%$params[search]%' OR
                       e.name like '%$params[search]%' OR
                       f.employee_name like '%$params[search]%' OR
                       g.employee_name like '%$params[search]%'
                    )";
        }
        $sql .= " ORDER BY a.employee_name ASC";
        return $this->db->query($sql);
    }

    public function getEmpWithSallary()
    {
        $sql = "select 
                    a.nip,
                    a.basic_sallary,
                    a.total_sallary,
                    a.premi_overtime
                    from kf_hr.files_temp a
                    left join kf_hr.employees b on b.nip = a.nip
                    where a.basic_sallary > 0
                    and a.total_sallary > 0
                    and a.premi_overtime > 0";

        return $this->db->query($sql)->result();
    }

    public function getOutoffice($params)
    {
        $filter = date('Y-m', strtotime("$params[year]-$params[month]-01"));
        unset($params['year']);
        unset($params['month']);

        $where = advanceSearch($params);
        $sql = "select 
                a.*,
                b.name as department_name,
                c.name as sub_department_name,
                d.name as division_name,
                e.employee_name,
                f.employee_name as first_apv_name,
                g.employee_name as second_apv_name
                from out_office a
                inner join departments b on b.id = a.department_id 
                inner join sub_departments c on c.id = a.sub_department_id 
                inner join divisions d on d.id = a.division_id 
                inner join employees e on e.id = a.emp_id
                left join employees f on f.nip = a.apv_nip_1 
                left join employees g on g.nip = a.apv_nip_2 
                where a.location = '$this->empLoc' and a.out_date like '%$filter%'
                $where";

        $sql .= " ORDER BY a.out_date DESC";
        return $this->db->query($sql);
    }

    public function getNotPresent($absenTable, $date)
    {
        return $this->db->query("select 
                                    a.* from employees a
                                    inner join $absenTable b on a.id = b.emp_id 
                                    where b.abs_date = '$date' 
                                    and (b.date_in is null or (b.date_in is not null and b.date_out is null))")->result();
    }

    public function getNotPresentDummy()
    {
        return $this->db->query("select 
                                    a.* from employees a
                                    inner join absen_202304 b on a.id = b.emp_id ")->result();
    }

    public function getLeaves($param, $user = null)
    {
        $year = $param['year'];
        $month = $param['month'];
        $employee_npp = isset($param['employee_npp']) ? $param['employee_npp'] : "";

        unset($param['month']);
        unset($param['year']);
        unset($param['employee_npp']);

        $whereEmp = "";
        if ($employee_npp != "") {
            $whereEmp = " and b.employee_npp = '$employee_npp'";
        }

        $location = "";
        if ($this->empLoc) {
            $location = $this->empLoc;
        } else {
            if ($user) {
                $location = $user->location;
            }
        }

        $where = advanceSearch($param);
        $sql = "select * from (select 
                    b.id,
                    b.leave_id,
                    b.employee_id,
                    b.employee_npp,
                    b.emp_id,
                    b.employee_leave_type_id,
                    b.employee_leave_type_name,
                    b.code,
                    b.date_start,
                    b.date_finish,
                    b.description,
                    b.approver_nip_1,
                    b.approver_name_1,
                    b.approved_at_1,
                    b.rejected_at_1,
                    b.approver_nip_2,
                    b.approver_name_2,
                    b.approved_at_2,
                    b.rejected_at_2,
                    b.created_at,
                    b.source_id,
                    b.filename,
                    'DONE' as status,
                    a.employee_name,
                    c.name as department_name,
                    d.name as sub_department_name,
                    e.name as division_name
                    from employees a
                    inner join kifest_leaves b on b.emp_id = a.id
                    inner join departments c on c.id = a.department_id 
                    inner join sub_departments d on d.id = a.sub_department_id
                    inner join divisions e on e.id = a.division_id
                    where a.location = '$location'
                    and month(b.date_start) = '$month' and year(b.date_start) = '$year'
                    $whereEmp
                    $where
                union
                select 
                    b.id,
                    0 as leave_id,
                    0 as employee_id,
                    b.employee_npp,
                    b.emp_id,
                    0 as employee_leave_type_id,
                    b.employee_leave_type_name,
                    b.code,
                    b.date_start,
                    b.date_finish,
                    b.description,
                    b.approver_nip_1,
                    b.approver_name_1,
                    b.approved_at_1,
                    b.rejected_at_1,
                    '-' as approver_nip_2,
                    '-' as approver_name_2,
                    '-' as approved_at_2,
                    '-' as rejected_at_2,
                    b.created_at,
                    b.source_id,
                    b.filename,
                    b.status,
                    a.employee_name,
                    c.name as department_name,
                    d.name as sub_department_name,
                    e.name as division_name
                    from employees a
                    inner join kifest_leaves_temp b on b.emp_id = a.id
                    inner join departments c on c.id = a.department_id 
                    inner join sub_departments d on d.id = a.sub_department_id
                    inner join divisions e on e.id = a.division_id
                    where a.location = '$location'
                    and month(b.date_start) = '$month' and year(b.date_start) = '$year'
                    $whereEmp
                    $where
                ) as X order by date_start desc";
        return $this->db->query($sql);
    }

    public function getVisit($param)
    {
        $year = $param['year'];
        $month = $param['month'];

        unset($param['month']);
        unset($param['year']);

        $where = advanceSearch($param);
        $sql = "select 
                    b.*,
                    a.employee_name,
                    c.name as department_name,
                    d.name as sub_department_name,
                    e.name as division_name
                    from employees a
                    inner join kifest_visit b on b.emp_id = a.id
                    inner join departments c on c.id = a.department_id 
                    inner join sub_departments d on d.id = a.sub_department_id
                    inner join divisions e on e.id = a.division_id
                    where a.location = '$this->empLoc'
                    and month(b.visit_date) = '$month' and year(b.visit_date) = '$year'
                    $where";

        return $this->db->query($sql);
    }

    public function getEmpWithNoUserUpdate($date, $status)
    {
        $sql = "select 
                    a.*
                    from employees a
                    left join kf_main.users b on a.nip = b.nip
                    where date(a.created_at) >= '$date' and a.status = '$status' and a.user_id = 0";

        return $this->db->query($sql)->result();
    }
    public function getEmpWithNoUser()
    {
        $sql = "select 
                    a.*
                    from employees a
                    left join kf_main.users b on a.nip = b.nip
                    where b.id is null
                    and a.status = 'ACTIVE'";

        return $this->db->query($sql)->result();
    }
}
