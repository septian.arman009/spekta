<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GeneralModel extends CI_Model
{
    public function myConstruct($db_name = true)
    {
        parent::__construct();
        $this->db = $this->load->database($db_name, true);
       
        $this->kf_chat = $this->auth->kf_chat;
        $this->kf_general = $this->auth->kf_general;
        $this->kf_hr = $this->auth->kf_hr;
        $this->kf_main = $this->auth->kf_main;
        $this->kf_mtn = $this->auth->kf_mtn;
        $this->kf_qhse = $this->auth->kf_qhse;
    }

    public function getMeetSchedules($params, $user)
    {
        $month = $params['month'];
        $year = $params['year'];
        
        unset($params['month']);
        unset($params['year']);
        
        $where = advanceSearch($params);
        $sql = "select 
                    a.id,
                    b.id as participant_id,
                    c.employee_name,
                    d.name as room_name,
                    c.email,
                    a.name,
                    a.meeting_type,
                    a.description,
                    a.start_date,
                    a.end_date,
                    a.duration,
                    b.status,
                    a.total_participant,
                    a.participant_confirmed,
                    a.participant_rejected,
                    e.employee_name as requestor
                    from meeting_rooms_reservation a
                    inner join meeting_participants b on a.id = b.meeting_id 
                    inner join $this->kf_hr.employees c on b.email = c.email
                    inner join meeting_rooms d on a.room_id = d.id
                    inner join $this->kf_hr.employees e on e.id = a.created_by
                    where a.location = '$user[empLoc]'
                    and c.id = $user[empId]
                    and month(a.start_date) = $month
                    and year(a.start_date) = $year
                    and a.status in('APPROVED','CLOSED')
                    $where order by a.start_date desc";
                    
        return $this->db->query($sql);
    }

    public function getVehicleSchedules($params, $user)
    {
        $month = $params['month'];
        $year = $params['year'];

        unset($params['month']);
        unset($params['year']);

        $where = advanceSearch($params);
        $emp = $this->db->query("select * from kf_hr.employees where id = $user[empId]")->row();
        $sql = "select 
                    a.id,
                    a.location,
                    a.destination,
                    a.trip_type,
                    a.description,
                    b.name as vehicle_name,
                    b.brand as vehicle_brand,
                    b.type as vehicle_type,
                    b.police_no,
                    c.employee_name as driver_name,
                    c.phone as driver_phone,
                    a.driver_confirmed,
                    a.driver,
                    a.passenger,
                    a.start_date,
                    a.end_date,
                    a.duration,
                    a.total_passenger,
                    a.status,
                    a.distance,
                    a.reason
                from kf_general.vehicles_reservation a
                inner join kf_general.vehicles b on b.id = a.vehicle_id
                left join kf_hr.employees c on c.email = a.driver
                where a.location = '$user[empLoc]'
                and a.passenger like '%$emp->email%'
                and month(a.start_date) = $month
                and year(a.start_date) = $year
                and a.status in('APPROVED','CLOSED')
                $where order by a.start_date desc";
                    
        return $this->db->query($sql);
    }
}