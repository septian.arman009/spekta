<?php
defined('BASEPATH') or exit('No direct script access allowed');

class OvertimeModel extends CI_Model
{    
    public function myConstruct($db_name = true)
    {
        parent::__construct();
        $this->db = $this->load->database($db_name, true);
       
        $this->kf_chat = $this->auth->kf_chat;
        $this->kf_general = $this->auth->kf_general;
        $this->kf_hr = $this->auth->kf_hr;
        $this->kf_main = $this->auth->kf_main;
        $this->kf_mtn = $this->auth->kf_mtn;
        $this->kf_qhse = $this->auth->kf_qhse;
    }

    public function getOvertimes($empId, $date)
    {
        $absenTable = 'absen_' . date('Ym', strtotime($date));
        $yearMonth = date('Y-m', strtotime($date));
        
        $sql = "select 
                    ovt.id,
                    ovt.location,
                    ovt.task_id,
                    ovt.emp_id,
                    ovt.emp_task_id,
                    ovt.overtime_date,
                    ovt.start_date,
                    ovt.end_date,
                    abs.date_in,
                    abs.date_out,
                    abs.sch_date_in,
                    abs.sch_date_out,
                    ovt.status_day,
                    ovt.effective_hour,
                    ovt.break_hour,
                    ovt.real_hour,
                    ovt.overtime_hour,
                    ovt.premi_overtime,
                    ovt.overtime_value,
                    ovt.deduction,
                    ovt.meal,
                    ovt.total_meal,
                    ovt.status,
                    ovt.payment_status,
                    ovt.notes,
                    mach_1.name as machine_1_name,
                    mach_2.name as machine_2_name,
                    ovt_sub_dept.name as overtime_sub_department,
                    ovt_div.name as overtime_division,
                    emp_ovt.apv_spv,
                    emp_ovt.apv_asman,
                    emp_ovt.apv_ppic,
                    emp_ovt.apv_mgr,
                    emp_ovt.apv_head
                from employee_overtimes_detail ovt
                left join $this->kf_mtn.production_machines mach_1 on mach_1.id = ovt.machine_1
                left join $this->kf_mtn.production_machines mach_2 on mach_2.id = ovt.machine_2
                left join sub_departments ovt_sub_dept on ovt_sub_dept.id = ovt.ovt_sub_department
                left join divisions ovt_div on ovt_div.id = ovt.ovt_division
                inner join employee_overtimes emp_ovt on emp_ovt.task_id  = ovt.task_id
                left join $absenTable abs on abs.abs_date = ovt.overtime_date and abs.emp_id = ovt.emp_id
                where ovt.overtime_date like '%$yearMonth%' 
                and ovt.emp_id = $empId
                and ovt.status not in('CANCELED','REJECTED','CREATED') 
                order by ovt.overtime_date desc";

        return $this->db->query($sql)->result();
    }
}