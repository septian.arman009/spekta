<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AbsenModel extends CI_Model
{    
    public function myConstruct($db_name = true)
    {
        parent::__construct();
        $this->db = $this->load->database($db_name, true);
       
        $this->kf_chat = $this->auth->kf_chat;
        $this->kf_general = $this->auth->kf_general;
        $this->kf_hr = $this->auth->kf_hr;
        $this->kf_main = $this->auth->kf_main;
        $this->kf_mtn = $this->auth->kf_mtn;
        $this->kf_qhse = $this->auth->kf_qhse;
    }

    public function getAbsens($params, $table, $user)
    {
        $where = advanceSearch($params);
        $sql = "SELECT a.*, 
                       b.employee_name,
                       c.name as department_name,
                       d.name as sub_department_name,
                       e.name as division_name
                    FROM $table a
                    INNER JOIN employees b on a.emp_id = b.id
                    INNER JOIN departments c on b.department_id = c.id
                    INNER JOIN sub_departments d on b.sub_department_id = d.id
                    INNER JOIN divisions e on b.division_id = e.id
                    WHERE a.location = '$user[empLoc]'
                    AND a.emp_id = $user[empId]
                    $where";

        $sql .= " ORDER BY a.abs_date DESC";
        return $this->db->query($sql);
    }

    public function getOutoffices($params, $user, $empId)
    {
        $filter = date('Y-m', strtotime($params['date']));
        unset($params['date']);
        
        $where = advanceSearch($params);
        $sql = "select 
                a.*,
                b.name as department_name,
                c.name as sub_department_name,
                d.name as division_name,
                e.employee_name,
                f.employee_name as direct_name,
                g.name as direct_rank_name,
                h.employee_name as direct_name_2,
                i.name as direct_rank_name_2,
                a.apv_date_1 as direct_date,
                a.apv_date_2 as direct_date_2
                from out_office a
                inner join departments b on b.id = a.department_id 
                inner join sub_departments c on c.id = a.sub_department_id 
                inner join divisions d on d.id = a.division_id 
                inner join employees e on e.id = a.emp_id
                left join employees f on f.nip = a.apv_nip_1
                inner join ranks g on g.id = f.rank_id
                left join employees h on h.nip = a.apv_nip_2
                inner join ranks i on i.id = h.rank_id
                where a.location = '$user->location' 
                and e.id = $empId
                and a.out_date like '%$filter%'
                $where";

        $sql .= " ORDER BY a.out_date DESC";
        return $this->db->query($sql);
    }

    public function getAbsensWithEmp($absenTable, $absenId)
    {
        $sql = "select 
                    a.id,
                    a.location,
                    a.abs_date,
                    a.shift,
                    e.employee_name,
                    e.id as emp_id,
                    e.rank_id,
                    e.department_id,
                    e.sub_department_id,
                    e.division_id,
                    e.email,
                    dept.name as department_name,
                    sub.name as sub_department_name,
                    divi.name as division_name,
                    a.sch_date_in,
                    a.sch_date_out,
                    a.date_in,
                    a.date_out,
                    a.qr_code_in,
                    a.qr_code_out,
                    ac.reason
                    from $absenTable a 
                    inner join employees e on a.emp_id = e.id
                    inner join departments dept on dept.id = e.department_id
                    inner join sub_departments sub on sub.id = e.sub_department_id
                    inner join divisions divi on divi.id = e.division_id
                    left join absen_correction ac on ac.absen_table = '$absenTable' and ac.absen_id = a.id
                    where a.id = $absenId";
        return $this->db->query($sql)->row();
    }

    public function getAbsenCorrectionWithEmp($absenTable, $absenId)
    {
        $sql = "select 
                    a.id,
                    a.location,
                    a.abs_date,
                    a.shift,
                    a.correction_status,
                    ac.direct_approval_id, ac.direct_approval_time, ac.direct_approval_status,
                    ac.hr_approval_id, ac.hr_approval_time, ac.hr_approval_status,
                    ac.filename, ac.id as correction_id, ac.reason, ac.reject_reason,
                    ac.correction_start_time, ac.correction_end_time,
                    e.employee_name,
                    e.id as emp_id,
                    e.rank_id,
                    e.department_id,
                    e.sub_department_id,
                    e.division_id,
                    e.email,
                    dept.name as department_name,
                    sub.name as sub_department_name,
                    divi.name as division_name,
                    a.sch_date_in,
                    a.sch_date_out,
                    a.date_in,
                    a.date_out,
                    a.qr_code_in,
                    a.qr_code_out,
                    ac.shift_id,
                    ac.id as correction_id,
                    ovt.payment_status
                    from $absenTable a 
                    inner join absen_correction ac on ac.absen_id = a.id and ac.absen_table = '$absenTable'
                    inner join employees e on a.emp_id = e.id
                    inner join departments dept on dept.id = e.department_id
                    inner join sub_departments sub on sub.id = e.sub_department_id
                    inner join divisions divi on divi.id = e.division_id
                    left join employee_overtimes_detail ovt on ovt.emp_id = a.emp_id and ovt.overtime_date = a.abs_date
                    where a.id = $absenId";
        return $this->db->query($sql)->row();
    }
}