<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CanteenModel extends CI_Model
{    
    public function myConstruct($db_name = true)
    {
        parent::__construct();
        $this->db = $this->load->database($db_name, true);
       
        $this->kf_chat = $this->auth->kf_chat;
        $this->kf_general = $this->auth->kf_general;
        $this->kf_hr = $this->auth->kf_hr;
        $this->kf_main = $this->auth->kf_main;
        $this->kf_mtn = $this->auth->kf_mtn;
        $this->kf_qhse = $this->auth->kf_qhse;
    }

    public function getCanteenHistory($empId, $date)
    {
        $absenTable = 'absen_' . date('Ym', strtotime($date));
        
        $sql = "select
                    ct.id,
                    ct.location,
                    ct.emp_id,
                    ct.take_date,
                    ct.out_date,
                    ct.type,
                    ct.duration,
                    ab.qr_code_in,
                    ct.status,
                    ct.menu_name,
                    ct.menu_description,
                    ct.menu_filename,
                    ct.created_at
                from $this->kf_hr.$absenTable ab
                inner join canteen ct on ab.emp_id = ct.emp_id and ab.abs_date = date(ct.take_date)
                where ct.emp_id = $empId
                order by ct.take_date desc";

        return $this->db->query($sql)->result();
    }

    public function getCanteenAbsenOvertimeHistory($empId, $date)
    {
        $absenTable = 'absen_' . date('Ym', strtotime($date));

        $sql = "select * from (
                    select
                        ct.id,
                        ct.location,
                        ct.emp_id,
                        ct.take_date,
                        ct.out_date,
                        ct.type,
                        ct.duration,
                        if(ab.qr_code_in is not null, ab.qr_code_in, '-') as qr_code_in,
                        '-' as emp_task_id,
                        ct.status,
                        ct.menu_name,
                        ct.menu_description,
                        ct.menu_filename,
                        ct.created_at
                        from kf_general.canteen_overtime ct
                        inner join kf_hr.$absenTable ab on ab.emp_id = ct.emp_id and ab.abs_date = date(ct.take_date)
                        inner join kf_hr.work_time wt on wt.id = ab.shift_id and wt.meal = 1
                        where ct.emp_id = $empId and ct.source = 'absen'
                        group by ct.id
                    union
                    select 
                        ct.id,
                        ct.location,
                        ct.emp_id,
                        ct.take_date,
                        ct.out_date,
                        ct.type,
                        ct.duration,
                        '-' as qr_code_in,
                        if(ovt.emp_task_id is not null, ovt.emp_task_id, '-') as emp_task_id,
                        ct.status,
                        ct.menu_name,
                        ct.menu_description,
                        ct.menu_filename,
                        ct.created_at
                        from kf_general.canteen_overtime ct
                        inner join kf_hr.employee_overtimes_detail ovt on 
                            ovt.emp_id = ct.emp_id 
                            and ovt.overtime_date = date(ct.take_date)
                            and ovt.total_meal = 1
                        where ct.emp_id = $empId and ovt.overtime_date like '%".date('Y-m', strtotime($date))."%' 
                        and ct.source = 'overtime' 
                        and ovt.status not in('REJECTED','CANCELED')
                        group by ct.id
                ) as a order by take_date asc";

        return $this->db->query($sql)->result();
    }

    public function getCanteenAbsenOvertime2History($empId, $date)
    {
        $absenTable = 'absen_' . date('Ym', strtotime($date));

        $sql = "select * from (
                    select
                        ct.id,
                        ct.location,
                        ct.emp_id,
                        ct.take_date,
                        ct.out_date,
                        ct.type,
                        ct.duration,
                        if(ab.qr_code_in is not null, ab.qr_code_in, '-') as qr_code_in,
                        '-' as emp_task_id,
                        ct.status,
                        ct.menu_name,
                        ct.menu_description,
                        ct.menu_filename,
                        ct.created_at
                        from kf_general.canteen_overtime_2 ct
                        inner join kf_hr.$absenTable ab on ab.emp_id = ct.emp_id and ab.abs_date = date(ct.take_date)
                        inner join kf_hr.work_time wt on wt.id = ab.shift_id and wt.meal = 1
                        where ct.emp_id = $empId and ct.source = 'absen'
                        group by ct.id
                    union
                    select 
                        ct.id,
                        ct.location,
                        ct.emp_id,
                        ct.take_date,
                        ct.out_date,
                        ct.type,
                        ct.duration,
                        '-' as qr_code_in,
                        if(ovt.emp_task_id is not null, ovt.emp_task_id, '-') as emp_task_id,
                        ct.status,
                        ct.menu_name,
                        ct.menu_description,
                        ct.menu_filename,
                        ct.created_at
                        from kf_general.canteen_overtime_2 ct
                        inner join kf_hr.employee_overtimes_detail ovt on 
                            ovt.emp_id = ct.emp_id 
                            and ovt.overtime_date = date(ct.take_date)
                            and ovt.total_meal = 1
                        where ct.emp_id = $empId and ovt.overtime_date like '%".date('Y-m', strtotime($date))."%' 
                        and ct.source = 'overtime' 
                        and ovt.status not in('REJECTED','CANCELED')
                        group by ct.id
                ) as a order by take_date asc";

        return $this->db->query($sql)->result();
    }

    public function pickedMenu($id)
    {
        return $this->db->query("update menus set picked = picked + 1 where id = $id");
    }

    public function getNotCloseCanteen()
    {
        return $this->db->query("select * from canteen where out_date is null")->result();
    }

    public function getNotCloseCanteenOvertime()
    {
        return $this->db->query("select * from canteen_overtime where out_date is null")->result();
    }
}