<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MainModel extends CI_Model
{    
    public function myConstruct($db_name = true)
    {
        parent::__construct();
        $this->db = $this->load->database($db_name, true);
       
        $this->kf_chat = $this->auth->kf_chat;
        $this->kf_general = $this->auth->kf_general;
        $this->kf_hr = $this->auth->kf_hr;
        $this->kf_main = $this->auth->kf_main;
        $this->kf_mtn = $this->auth->kf_mtn;
        $this->kf_qhse = $this->auth->kf_qhse;
    }

    public function cleanImeiFcm() {
        return $this->db->query("update users set imei_no = '', fcm_token = '' where imei_no != ''");
    }

    public function getNotifications($emp) {
        $sql = "select * from (
                select 
                    id,
                    alert_name,
                    email_to,
                    subject,
                    subject_name,
                    created_at,
                    positive,
                    negative,
                    redirect,
                    case when apv_status > 0 then 'CLOSED' else 'OPEN' end as apv_status,
                    token
                    from email
                    where 
                        (
                            (emp_direct_apv = $emp->id and
                            rank_apv is null and
                            dept_apv is null and
                            sub_dept_apv is null and
                            div_apv is null) 
                            or
                            (emp_direct_apv is null and
                            rank_apv is null and
                            div_apv is null and
                            dept_apv = $emp->department_id and
                            sub_dept_apv = $emp->sub_department_id and
                            div_apv is null)
                            or
                            (emp_direct_apv is null and
                            rank_apv is null and
                            div_apv is null and
                            dept_apv = $emp->department_id and
                            sub_dept_apv = $emp->sub_department_id and
                            div_apv = $emp->division_id)
                        ) and
                        token is not null and 
                        token != '' and
                        email_to like '%$emp->email%'
                ) as notification order by id desc";
        return $this->db->query($sql)->result();
    }

    public function totalNotification($emp) {
        $sql = "select 
                case when count(1) < 100 then count(1) else 99 end as total
                from email
                where 
                    (
                        (emp_direct_apv = $emp->id and 
                        rank_apv is null and
                        dept_apv is null and
                        sub_dept_apv is null and
                        div_apv is null) 
                        or
                        (emp_direct_apv is null and
                        rank_apv is null and
                        div_apv is null and
                        dept_apv = $emp->department_id and
                        sub_dept_apv = $emp->sub_department_id and
                        div_apv is null)
                        or
                        (emp_direct_apv is null and
                        rank_apv is null and
                        div_apv is null and
                        dept_apv = $emp->department_id and
                        sub_dept_apv = $emp->sub_department_id and
                        div_apv = $emp->division_id)
                    ) and 
                    token is not null and 
                    token != '' and
                    email_to like '%$emp->email%' and
                    date(created_at) <= '2024-02-23' and
                    apv_status = 0";
        return $this->db->query($sql)->row()->total ?? 0;
    }

    public function getEmails() {
        $date = date('Y-m-d');
        
        $sql = "select 
                    e.*, u.fcm_token, emp.employee_name
                    from email e 
                    left join kf_hr.employees emp on emp.id = e.emp_direct_apv 
                    left join users u on u.id = emp.user_id 
                    where 
                        e.status = 0 and
                        DATE(e.created_at) = '$date'
                    limit 3";
        return $this->db->query($sql)->result();
    }

    
    public function getFcmEmails() {
        $date = date('Y-m-d');
        
        $sql = "select 
                    *
                    from email
                    where 
                        (
                            (emp_direct_apv is null and
                            rank_apv is null and
                            dept_apv is not null and dept_apv > 0 and
                            sub_dept_apv is not null and sub_dept_apv > 0 and
                            div_apv is null)
                            or
                            (emp_direct_apv is null and
                            rank_apv is null and
                            dept_apv is not null and dept_apv > 0 and
                            sub_dept_apv is not null and sub_dept_apv > 0 and 
                            div_apv is not null and div_apv > 0)
                            or
                            (emp_direct_apv is null and
                            rank_apv is not null and
                            dept_apv is not null and dept_apv > 0 and
                            sub_dept_apv is not null and sub_dept_apv > 0 and 
                            div_apv is not null and div_apv > 0)
                        ) and
                        fcm_status = 0 and
                        status = 1 and
                        DATE(created_at) = '$date'
                    limit 5";
        return $this->db->query($sql)->result();
    }

    public function getEmployeeWithFcm($dept, $subDept, $div = -1, $rank = "") {
        if($div > 0 && $rank != "") {
            $rankExp = explode("-", $rank);
            if($rankExp[1] <= 4) {
                $sql = "select 
                            e.id,
                            u.fcm_token
                            from $this->kf_hr.employees e 
                            inner join users u on e.user_id = u.id
                            where 
                                u.fcm_token != '' and
                                e.department_id = $dept and
                                e.sub_department_id = $subDept and
                                e.rank_id >= $rankExp[0] and 
                                e.rank_id <= $rankExp[1]";
                return $this->db->query($sql)->result();
            } else {
                if($rankExp[0] >= 3 && $rankExp[0] <= 4) {
                    $sql = "select 
                            e.id,
                            u.fcm_token
                            from $this->kf_hr.employees e 
                            inner join users u on e.user_id = u.id
                            where 
                                u.fcm_token != '' and
                                (
                                    (e.department_id = $dept and
                                     e.sub_department_id = $subDept and
                                     e.division_id = $div and
                                     e.rank_id >= $rankExp[0] and
                                     e.rank_id <= $rankExp[1])
                                    or
                                    (e.department_id = $dept and
                                     e.sub_department_id = $subDept and
                                     e.rank_id >= $rankExp[0] and
                                     e.rank_id <= 4)
                                )";

                    $emps = $this->db->query($sql)->result();
                } else {
                    $sql = "select 
                            e.id,
                            u.fcm_token
                            from $this->kf_hr.employees e 
                            inner join users u on e.user_id = u.id
                            where 
                                u.fcm_token != '' and
                                e.department_id = $dept and
                                e.sub_department_id = $subDept and
                                e.division_id = $div and
                                e.rank_id >= $rankExp[0] and 
                                e.rank_id <= $rankExp[1]";

                    $emps = $this->db->query($sql)->result();
                }

                return $emps;
            }
        } else if($div > 0) {
            $sql = "select 
                    e.id,
                    u.fcm_token
                    from $this->kf_hr.employees e 
                    inner join users u on e.user_id = u.id
                    where 
                        u.fcm_token != '' and
                        (
                            (e.department_id = $dept and
                            e.sub_department_id = $subDept and
                            e.division_id  = $div) 
                            or
                            (e.department_id = $dept and
                            e.sub_department_id = $subDept and
                            e.rank_id <= 4)
                        )";
            return $this->db->query($sql)->result();
        } else {
            $sql = "select 
                    e.id,
                    u.fcm_token
                    from $this->kf_hr.employees e 
                    inner join users u on e.user_id = u.id
                    where 
                        u.fcm_token != '' and
                        e.department_id = $dept and
                        e.sub_department_id = $subDept";
            return $this->db->query($sql)->result();
        }
    }

    public function countFcm() {
        $sql = "update version set fcm_limit = fcm_limit + 1 where id = 1";
        return $this->db->query($sql);
    }
}