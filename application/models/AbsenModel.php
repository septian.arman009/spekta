<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AbsenModel extends CI_Model
{    
    public function myConstruct($db_name = true)
    {
        parent::__construct();
        $this->db = $this->load->database($db_name, true);
        
        $this->kf_chat = $this->auth->kf_chat;
        $this->kf_general = $this->auth->kf_general;
        $this->kf_hr = $this->auth->kf_hr;
        $this->kf_main = $this->auth->kf_main;
        $this->kf_mtn = $this->auth->kf_mtn;
        $this->kf_qhse = $this->auth->kf_qhse;
        $this->empLoc = $this->auth->empLoc;
    }

    public function getAbsens($params, $empId, $postfix)
    {
        $isExist = $this->checkTableExist("absen_$postfix");
        if($isExist->num_rows() > 0) {
            $where = advanceSearch($params);
            $sql = "SELECT *,DATE(action_date) AS date FROM absen_$postfix a WHERE a.emp_id = $empId $where ORDER BY a.action_date DESC";
            return $this->db->query($sql);
        } else {
            return $isExist;
        }
    }

    public function checkTableExist($table)
    {
        return $this->db->query("SHOW TABLES LIKE '%$table%'");
    }

    public function getLastIn($empId, $postfix)
    {
       return $this->db->select('*, DATE(action_date) AS date')
                 ->from("absen_$postfix")
                 ->where('emp_id', $empId)
                 ->where('action', 'IN')
                 ->order_by('action_date', 'DESC')
                 ->limit(1)
                 ->get()
                 ->row();
    }

    public function getShiftGridReguler($get)
    {
        $where = advanceSearch($get);
        $sql = "SELECT a.*,
                    (SELECT employee_name FROM employees WHERE id = a.created_by) AS emp1,
                    (SELECT employee_name FROM employees WHERE id = a.updated_by) AS emp2
                    FROM work_time a
                    WHERE a.location = '$this->empLoc'
                    AND a.department_id = 0
                    AND a.sub_department_id = 0
                    AND a.division_id = 0
                    $where";

        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (a.name LIKE '%$get[search]%' OR
                          (SELECT employee_name FROM $this->kf_hr.employees WHERE id = a.created_by) LIKE '%$get[search]%' OR
                          (SELECT employee_name FROM $this->kf_hr.employees WHERE id = a.updated_by) LIKE '%$get[search]%'
                    )";
        }
        $sql .= " ORDER BY a.id ASC";
        return $this->db->query($sql);
    }

    public function getShiftGrid($get)
    {
        $where = advanceSearch($get);
        $sql = "SELECT a.*,b.name AS department,c.name AS sub_department,d.name AS division,
                    (SELECT employee_name FROM employees WHERE id = a.created_by) AS emp1,
                    (SELECT employee_name FROM employees WHERE id = a.updated_by) AS emp2
                    FROM work_time a, departments b, sub_departments c, divisions d
                    WHERE a.department_id = b.id
                    AND a.sub_department_id = c.id
                    AND a.division_id = d.id
                    AND a.location = '$this->empLoc'
                    AND a.department_id > 0
                    AND a.sub_department_id > 0
                    AND a.division_id > 0
                    $where";

        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (a.name LIKE '%$get[search]%' OR
                          (SELECT employee_name FROM $this->kf_hr.employees WHERE id = a.created_by) LIKE '%$get[search]%' OR
                          (SELECT employee_name FROM $this->kf_hr.employees WHERE id = a.updated_by) LIKE '%$get[search]%'
                    )";
        }
        $sql .= " ORDER BY a.id ASC";
        return $this->db->query($sql);
    }

    public function getMappedShiftGrid($get)
    {
        $empDept = empDept();
        $empSub = empSub();

        $where = advanceSearch($get);
        $sql = "SELECT a.*,b.name AS department,c.name AS sub_department,d.name AS division,
                    e.employee_name AS emp1,
                    f.employee_name AS emp2
                FROM work_time a
                LEFT JOIN departments b ON a.department_id = b.id
                LEFT JOIN sub_departments c ON a.sub_department_id = c.id
                LEFT JOIN divisions d ON a.division_id = d.id
                LEFT JOIN $this->kf_hr.employees e ON a.created_by = e.id
                LEFT JOIN $this->kf_hr.employees f ON a.updated_by = f.id
                WHERE 
                    ((a.department_id = 0 AND a.sub_department_id = 0 AND a.division_id = 0) OR
                    (a.department_id = $empDept AND a.sub_department_id = $empSub))
                AND a.location = '$this->empLoc'
                $where";

        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (a.name LIKE '%$get[search]%' OR
                          b.name LIKE '%$get[search]%' OR
                          c.name LIKE '%$get[search]%' OR
                          d.name LIKE '%$get[search]%' OR
                          e.employee_name LIKE '%$get[search]%' OR
                          f.employee_name LIKE '%$get[search]%'
                    )";
        }
        $sql .= " ORDER BY a.id ASC";
        return $this->db->query($sql);
    }

    public function getAbsenCorrections($params)
    {
        $absenTable = absenTable($params['year'], $params['month']);

        unset($params['month']);
        unset($params['year']);
        
        $where = advanceSearchWithAlias($params);
        $sql = "select 
                    a.id,
                    a.location,
                    a.emp_id,
                    a.gate,
                    a.distance, a.lat, a.lng,
                    a.distance_out, a.lat_out, a.lng_out,
                    a.qr_code_in, a.qr_code_out,
                    a.abs_date, a.date_in, a.date_out, a.sch_date_in, a.sch_date_out,
                    a.correction_status,
                    a.shift,
                    ac.direct_approval_id, ac.direct_approval_time, ac.direct_approval_status,
                    ac.hr_approval_id, ac.hr_approval_time, ac.hr_approval_status,
                    ac.filename, ac.id as correction_id, ac.reason, ac.reject_reason,
                    ac.correction_start_time, ac.correction_end_time,
                    emp.employee_name as employee_name,
                    direct.employee_name as direct_name,
                    hr.employee_name as hr_name,
                    cd.employee_name as created_name,
                    ud.employee_name as updated_name,
                    dept.name as department_name,
                    sub.name as sub_department_name,
                    dive.name as division_name,
                    ac.created_by,
                    ac.updated_by,
                    ac.created_at,
                    ac.updated_at,
                    wt.name as correction_shift
                    from $absenTable a 
                    inner join absen_correction ac on ac.absen_id = a.id and ac.absen_table = '$absenTable'
                    inner join employees emp on emp.id = a.emp_id
                    inner join employees direct on direct.id = ac.direct_approval_id
                    left join employees hr on hr.id = ac.hr_approval_id
                    inner join employees cd on cd.id = a.created_by
                    inner join employees ud on ud.id = a.updated_by
                    inner join departments dept on dept.id = emp.department_id
                    inner join sub_departments sub on sub.id = emp.sub_department_id
                    inner join divisions dive on dive.id = emp.division_id
                    left join work_time wt on wt.id = ac.shift_id
                    where a.location = '$this->empLoc'
                    $where";
      
        if (isset($get['search']) && $get['search'] !== "") {
            $sql .= "AND (
                        a.gate like '%$params[search]%' OR
                        a.distance like '%$params[search]%' OR
                        a.distance_out like '%$params[search]%' OR
                        a.shift like '%$params[search]%' OR
                        a.abs_date like '%$params[search]%' OR
                        emp.employee_name like '%$params[search]%' OR
                        hr.employee_name like '%$params[search]%' OR
                        cd.employee_name like '%$params[search]%' OR
                        ud.employee_name like '%$params[search]%' OR
                        ac.direct_approval_status like '%$params[search]%' OR
                        ac.hr_approval_status like '%$params[search]%'
                    )";
        } 
        $sql .= " ORDER BY ac.created_at DESC";
        return $this->db->query($sql)->result();
    }

    public function getAbsensWithEmp($absenTable, $absenIds)
    {
        $sql = "select 
                    a.id,
                    a.location,
                    a.abs_date,
                    a.shift,
                    e.employee_name,
                    e.id as emp_id,
                    e.rank_id,
                    e.department_id,
                    e.sub_department_id,
                    e.division_id,
                    e.email,
                    dept.name as department_name,
                    sub.name as sub_department_name,
                    divi.name as division_name,
                    a.sch_date_in,
                    a.sch_date_out,
                    a.date_in,
                    a.date_out,
                    a.qr_code_in,
                    a.qr_code_out,
                    ac.reason
                    from $absenTable a 
                    inner join employees e on a.emp_id = e.id
                    inner join departments dept on dept.id = e.department_id
                    inner join sub_departments sub on sub.id = e.sub_department_id
                    inner join divisions divi on divi.id = e.division_id
                    left join absen_correction ac on ac.absen_table = '$absenTable' and ac.absen_id = a.id
                    where a.id in($absenIds)
                    order by e.employee_name asc";
        return $this->db->query($sql)->result();
    }

    public function getAbsensWithEmpSingle($absenTable, $empId)
    {
        $sql = "select 
                    a.id,
                    a.location,
                    a.abs_date,
                    a.shift,
                    e.employee_name,
                    e.id as emp_id,
                    e.rank_id,
                    e.department_id,
                    e.sub_department_id,
                    e.division_id,
                    e.email,
                    dept.name as department_name,
                    sub.name as sub_department_name,
                    divi.name as division_name,
                    a.sch_date_in,
                    a.sch_date_out,
                    a.date_in,
                    a.date_out,
                    a.qr_code_in,
                    a.qr_code_out,
                    ac.correction_start_time,
                    ac.correction_end_time,
                    ac.reason
                    from $absenTable a 
                    inner join employees e on a.emp_id = e.id
                    inner join departments dept on dept.id = e.department_id
                    inner join sub_departments sub on sub.id = e.sub_department_id
                    inner join divisions divi on divi.id = e.division_id
                    left join absen_correction ac on ac.absen_id = a.id and absen_table = '$absenTable'
                    where a.id = $empId";
        return $this->db->query($sql)->row();
    }

    public function getAbsenCorrectionWithEmp($absenTable, $absenIds)
    {
        $sql = "select 
                    a.id,
                    a.location,
                    a.abs_date,
                    a.shift,
                    a.correction_status,
                    ac.direct_approval_id, ac.direct_approval_time, ac.direct_approval_status,
                    ac.hr_approval_id, ac.hr_approval_time, ac.hr_approval_status,
                    ac.filename, ac.id as correction_id, ac.reason, ac.reject_reason,
                    ac.correction_start_time, ac.correction_end_time,
                    e.employee_name,
                    e.id as emp_id,
                    e.rank_id,
                    e.department_id,
                    e.sub_department_id,
                    e.division_id,
                    e.email,
                    dept.name as department_name,
                    sub.name as sub_department_name,
                    divi.name as division_name,
                    a.sch_date_in,
                    a.sch_date_out,
                    a.date_in,
                    a.date_out,
                    a.qr_code_in,
                    a.qr_code_out,
                    ac.shift_id,
                    ac.id as correction_id,
                    ovt.payment_status
                    from $absenTable a 
                    inner join absen_correction ac on ac.absen_id = a.id and ac.absen_table = '$absenTable'
                    inner join employees e on a.emp_id = e.id
                    inner join departments dept on dept.id = e.department_id
                    inner join sub_departments sub on sub.id = e.sub_department_id
                    inner join divisions divi on divi.id = e.division_id
                    left join employee_overtimes_detail ovt on ovt.emp_id = a.emp_id and ovt.overtime_date = a.abs_date
                    where a.id in($absenIds)
                    order by e.employee_name asc";
        return $this->db->query($sql)->result();
    }
}