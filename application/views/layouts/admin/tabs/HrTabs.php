<?php
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function employeeTab() {
        if (!mainTab.tabs("employee")){
            mainTab.addTab("employee", tabsStyle("person_16.png", "Data Karyawan"), null, null, true, true);
            showEmployee();
        } else {
            mainTab.tabs("employee").setActive();
        }
    }

    function basicSallaryTab() {
        if (!mainTab.tabs("basic_sallary")){
            mainTab.addTab("basic_sallary", tabsStyle("money.png", "Data Gaji Karyawan"), null, null, true, true);
            showBasicSallary();
        } else {
            mainTab.tabs("basic_sallary").setActive();
        }
    }

    function masterNasionalFreeTab() {
        if (!mainTab.tabs("national_freeday")){
            mainTab.addTab("national_freeday", tabsStyle("calendar.png", "Data Libur Nasional"), null, null, true, true);
            showNationalFree();
        } else {
            mainTab.tabs("national_freeday").setActive();
        }
    }

    function masterBulanPuasaTab() {
        if (!mainTab.tabs("hr_data_bulan_puasa")){
            mainTab.addTab("hr_data_bulan_puasa", tabsStyle("calendar.png", "Data Bulan Puasa"), null, null, true, true);
            showBulanPuasa();
        } else {
            mainTab.tabs("hr_data_bulan_puasa").setActive();
        }
    }

    function superiorSallaryTab() {
        if (!mainTab.tabs("superior_sallary")){
            mainTab.addTab("superior_sallary", tabsStyle("money.png", "Data Gaji Atasan"), null, null, true, true);
            showSuperiorSallary();
        } else {
            mainTab.tabs("superior_sallary").setActive();
        }
    }

    function empPinTab() {
        if (!mainTab.tabs("hr_data_pin_karyawan")){
            mainTab.addTab("hr_data_pin_karyawan", tabsStyle("key.png", "Data PIN Karyawan", "background-size: 16px 16px"), null, null, true, true);
            showEmpPin();
        } else {
            mainTab.tabs("hr_data_pin_karyawan").setActive();
        }
    }
    
    function hrMonitorOvtTab() {
        if (!mainTab.tabs("hr_monitor_overtime")){
            mainTab.addTab("hr_monitor_overtime", tabsStyle("app18.png", "Monitoring Lembur", "background-size: 16px 16px"), null, null, true, true);
            showHrMonitorOvertime();
        } else {
            mainTab.tabs("hr_monitor_overtime").setActive();
        }
    }

    function hrProcessOvtTab() {
        if (!mainTab.tabs("hr_process_overtime")){
            mainTab.addTab("hr_process_overtime", tabsStyle("app18.png", "Pengajuan Lembur", "background-size: 16px 16px"), null, null, true, true);
            showHrProcessOvertime();
        } else {
            mainTab.tabs("hr_process_overtime").setActive();
        }
    }

    function hrReportOvtTab() {
        if (!mainTab.tabs("hr_report_overtime")){
            mainTab.addTab("hr_report_overtime", tabsStyle("app18.png", "Report Lembur", "background-size: 16px 16px"), null, null, true, true);
            showHrReportOvertime();
        } else {
            mainTab.tabs("hr_report_overtime").setActive();
        }
    }
    
    function hrVerifiedOvtTab() {
        if (!mainTab.tabs("hr_verified_overtime")){
            mainTab.addTab("hr_verified_overtime", tabsStyle("ok.png", "Verified Lembur", "background-size: 16px 16px"), null, null, true, true);
            showHrVerifiedOvertime();
        } else {
            mainTab.tabs("hr_verified_overtime").setActive();
        }
    }

    function hrRevisionOvtTab() {
        if (!mainTab.tabs("hr_revision_overtime")){
            mainTab.addTab("hr_revision_overtime", tabsStyle("clock.png", "Request Revisi Jam Lembur", "background-size: 16px 16px"), null, null, true, true);
            showHrRevisionOvertime();
        } else {
            mainTab.tabs("hr_revision_overtime").setActive();
        }
    }

    function hrRevisionOvtPersonilTab() {
        if (!mainTab.tabs("hr_revision_overtime_personil")){
            mainTab.addTab("hr_revision_overtime_personil", tabsStyle("clock.png", "Request Revisi Personil Lembur", "background-size: 16px 16px"), null, null, true, true);
            showHrRevisionOvertimePersonil();
        } else {
            mainTab.tabs("hr_revision_overtime_personil").setActive();
        }
    }

    function workShiftTab() {
        if (!mainTab.tabs("hr_work_shift")){
            mainTab.addTab("hr_work_shift", tabsStyle("clock.png", "Shift Kerja", "background-size: 16px 16px"), null, null, true, true);
            showWorkShift();
        } else {
            mainTab.tabs("hr_work_shift").setActive();
        }
    }

    function nonShiftManagementTab() {
        if (!mainTab.tabs("hr_manajemen_non_shift")){
            mainTab.addTab("hr_manajemen_non_shift", tabsStyle("clock.png", "Manajemen Non Shift", "background-size: 16px 16px"), null, null, true, true);
            showNonShiftManagement();
        } else {
            mainTab.tabs("hr_manajemen_non_shift").setActive();
        }
    }

    function absensiTab() {
        if (!mainTab.tabs("hr_attendance_list")){
            mainTab.addTab("hr_attendance_list", tabsStyle("clock.png", "Absensi", "background-size: 16px 16px"), null, null, true, true);
            absensi();
        } else {
            mainTab.tabs("hr_attendance_list").setActive();
        }
    }

    function absenCorrectionTab() {
        if (!mainTab.tabs("hr_attendance_correction")){
            mainTab.addTab("hr_attendance_correction", tabsStyle("clock.png", "Koreksi Absensi", "background-size: 16px 16px"), null, null, true, true);
            showAbsenCorrection();
        } else {
            mainTab.tabs("hr_attendance_correction").setActive();
        }
    }

    function approvalAbsenCorrectionTab() {
        if (!mainTab.tabs("hr_attendance_correction_approval")){
            mainTab.addTab("hr_attendance_correction_approval", tabsStyle("clock.png", "Approval Koreksi Absensi", "background-size: 16px 16px"), null, null, true, true);
            showApprovalAbsenCorrection();
        } else {
            mainTab.tabs("hr_attendance_correction_approval").setActive();
        }
    }

    function absensiInforBoardTab() {
        if (!mainTab.tabs("hr_attendance_info_board")){
            mainTab.addTab("hr_attendance_info_board", tabsStyle("info.png", "Informasi Screen Qr", "background-size: 16px 16px"), null, null, true, true);
            absensiInfoBoard();
        } else {
            mainTab.tabs("hr_attendance_info_board").setActive();
        }
    }
    
    function hrUploadEmployeeTab() {
        if (!mainTab.tabs("hr_upload_employee")){
            mainTab.addTab("hr_upload_employee", tabsStyle("info.png", "Upload Data Karyawan", "background-size: 16px 16px"), null, null, true, true);
            hrUploadEmployee();
        } else {
            mainTab.tabs("hr_upload_employee").setActive();
        }
    }

    function hrOurofficeTab() {
        if (!mainTab.tabs("hr_outoffice")){
            mainTab.addTab("hr_outoffice", tabsStyle("clock.png", "Izin Keluar Kantor", "background-size: 16px 16px"), null, null, true, true);
            hrOutoffice();
        } else {
            mainTab.tabs("hr_outoffice").setActive();
        }
    }

    function hrLeavesTab() {
        if (!mainTab.tabs("hr_leaves")){
            mainTab.addTab("hr_leaves", tabsStyle("clock.png", "Cuti (Kifest)", "background-size: 16px 16px"), null, null, true, true);
            hrLeaves();
        } else {
            mainTab.tabs("hr_leaves").setActive();
        }
    }

    function hrVisitTab() {
        if (!mainTab.tabs("hr_visit")){
            mainTab.addTab("hr_visit", tabsStyle("clock.png", "Absen Visit (Kifest)", "background-size: 16px 16px"), null, null, true, true);
            hrVisit();
        } else {
            mainTab.tabs("hr_visit").setActive();
        }
    }
    
JS;

echo $script;