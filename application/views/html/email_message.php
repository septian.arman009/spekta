<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notification</title>

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,900&display=swap" rel="stylesheet">
    <script type="text/javascript" src="<?= asset('js/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?= asset('js/custom.js') ?>"></script>
</head>
</head>
<body>
    <?= $message ?>
</body>

<script>
    const hide1 = document.getElementById('hide1');
    hide1.style.display = 'none';

    const hide2 = document.getElementById('hide2');
    hide2.style.display = 'none';
</script>
</html>