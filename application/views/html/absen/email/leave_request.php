<?php 
    $style = [
        'head' => 'padding: 5px 0px 0px 10px;text-align:center;',
        'img' => 'width: 220px;height: auto;',
        'body' => 'background: white;text-align:center;margin-top: 20px;border-radius: 5px;border: 1px solid #422800;padding: 10px;box-shadow: 5px 10px #ccc;',
        'p' => ' font-family: sans-serif;',
        'footer' => 'margin-top: 10px;',
        'table' => 'font-family:sans-serif;border-collapse: collapse;width:100%;',
        'th' => 'border: 1px solid #422800;padding: 8px;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #116171;color: #fff;',
        'td' => 'border: 1px solid #422800;padding: 8px;text-align:left',
        'button_container' => 'padding:10px;text-align:center;margin-top:20px;',
        'button' => 'border: 2px solid #422800;
                    border-radius: 30px;
                    box-shadow: #422800 4px 4px 0 0;
                    color: #422800;
                    cursor: pointer;
                    display: inline-block;
                    font-weight: 600;
                    font-size: 12px;
                    padding: 0 12px;
                    line-height: 40px;
                    text-align: center;
                    text-decoration: none;
                    user-select: none;
                    -webkit-user-select: none;
                    touch-action: manipulation;
                    width:280px;'
    ];
?>

<?php $locName = $this->Main->getOne('locations', ['code' => $emp->location])->name; ?>

<div>
    <div style="<?= $style['head'] ?>">
        <img style="<?= $style['img'] ?>" src="<?= LOGO_KF ?>" alt="kf">
        <hr style="border: 1px solid #422800">
        <p><b><?= $locName ?></b></p>
    </div>

    <div style="<?= $style['body'] ?>">
        <p>Dear <b><?= $asman->employee_name ?></b>,</p>
        <p>Berikut ini adalah data permintaan cuti dari <?= $emp->employee_name ?> pada tanggal:</p>
        <p><b><?= toIndoDateDay($leave['date_start']) ?> Sampai <?= toIndoDateDay($leave['date_finish']) ?> </b></p>

        <table style="<?= $style['table'] ?>">
            <tr>
                <th style="<?= $style['th'] ?>" colspan="2">Detail Cuti</th>
            </tr>
            <tr>
                <td style="<?= $style['td'] ?>">Nama Pemohon</td>
                <td style="<?= $style['td'] ?>"><b><?= $emp->employee_name ?></b></td>
            </tr>
            <tr>
                <td style="<?= $style['td'] ?>">Sub Unit</td>
                <td style="<?= $style['td'] ?>"><b><?= $emp->department_name ?></b></td>
            </tr>
            <tr>
                <td style="<?= $style['td'] ?>">Bagian</td>
                <td style="<?= $style['td'] ?>"><b><?= $emp->sub_department_name ?></b></td>
            </tr>
            <tr>
                <td style="<?= $style['td'] ?>">Sub Bagian</td>
                <td style="<?= $style['td'] ?>"><b><?= $emp->division_name ?></b></td>
            </tr>
            <tr>
                <td style="<?= $style['td'] ?>">Jenis Cuti</td>
                <td style="<?= $style['td'] ?>"><b><?= $leave['employee_leave_type_name'] ?></b></td>
            </tr>
            <tr>
                <td style="<?= $style['td'] ?>">Tanggal Mulai</td>
                <td style="<?= $style['td'] ?>"><b><?= toIndoDateDay($leave['date_start']) ?></b></td>
            </tr>
            <tr>
                <td style="<?= $style['td'] ?>">Tanggal Selesai</td>
                <td style="<?= $style['td'] ?>"><b><?= toIndoDateDay($leave['date_finish']) ?></b></td>
            </tr>
            <tr>
                <td style="<?= $style['td'] ?>">Keterangan</td>
                <td style="<?= $style['td'] ?>"><b><?= $leave['description'] ?></b></td>
            </tr>
        </table>

        <div style="<?= $style['button_container'] ?>" id='hide1'>
            <a href="<?= $linkApprove ?>" style="<?= $style['button'] ?> background-color: #3399cc;">Approve</a><br/><br/>
            <a href="<?= $linkReject ?>" style="<?= $style['button'] ?> background-color: #db8a10;">Reject</a>
        </div>
    </div>

    <div style="<?= $style['footer'] ?>" id='hide2'>
        <p>Notifikasi email ini dikirim secara otomatis oleh sistem dan tidak memerlukan balasan</p>
        <hr>
        <p>Kw. Industri Pulo Gadung, Blok N6-11, Jl. Rw. Gelam V No.1, RW.9, Jatinegara, Kec. Cakung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13920</p>
    </div>
</div>