<?php foreach ($informations as $information) { ?>
    <div class="slide fade">
        <div class="slide-img-container">
            <img class="slide-img" height="100%" width="100%" src="<?= imageAssets('info', $information->filename) ?>" />
        </div>
        <div class="slide-img-description">
            <div class="slide-img-title">
                <h3><?= $information->name ?></h3>
            </div>

            <div class="slide-img-body">
                <marquee direction="up" style="height:35vh;" scrollamount="2">
                    <?= str_replace('&nbsp', '', $information->description) ?>
                </marquee>
            </div>
        </div>
    </div>
<?php } ?>

<style>
    p {
        white-space: normal;
        padding: 0px;
        margin: 0px;
    }
</style>

<script>
    let slideIndex = 0;

    function showSlides() {
        let i;
        let slides = document.getElementsByClassName("slide");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {
            slideIndex = 1
        }
        slides[slideIndex - 1].style.display = "block";
        setTimeout(showSlides, 30000); // Change image every 2 seconds
    }

    showSlides();
</script>