<html>

<head>
    <title>S.P.E.K.T.A QR <?= $gate->gate_name ?></title>
    <link href='https://fonts.googleapis.com/css?family=Alkatra' rel='stylesheet'>
    <link rel="icon" href="<?= asset("img/spekta.png") ?>" type="image/x-icon" />
    <script src="<?= asset('js/jquery.min.js') ?>"></script>
    <script src="<?= asset('js/qrcode.min.js') ?>"></script>
    <script src="<?= asset('js/custom.js') ?>"></script>
</head>

<body>
    <div class="container">
        <div class="left">
            <div class="qr-container">
                <div id="qr_code"></div>
            </div>
            <div id="progressBar">
                <div class="bar"></div>
            </div>
        </div>
        <div class="right">
            <div class="information">
                <h1>S.P.E.K.T.A</h1>
                <div>
                    <p id="time">1:20PM</p>
                    <p id="day">SUN</p>
                    <p id="date">9 May 2021</p>
                </div>
            </div>

            <div class="main-info">
                <div class="main-info-1">
                    <div class="main-info-1-title">
                        <h1>Informasi Hari Ini</h1>
                    </div>

                    <div class="main-info-1-body">
                        <div id="info-list"></div>
                    </div>
                </div>

                <div class="main-info-2">
                    <div class="main-info-2-title">
                        <h1>Menu Hari Ini</h1>
                    </div>

                    <div class="main-info-2-body">
                        <marquee direction="up" style="height:80vh;padding-bottom:20px" scrollamount="5" behavior="alternate">
                            <div id="menu-list"></div>
                        </marquee>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<style>
    /* body {
        zoom: 75%;
    } */

    #info-list {
        width: 100%;
        height: 100%;
        /* padding: 10px; */
    }

    .fade {
        animation-name: fade;
        animation-duration: 1.5s;
    }

    @keyframes fade {
        from {
            opacity: .4
        }

        to {
            opacity: 1
        }
    }

    .slide-img-container {
        width: 100%;
        height: 45%;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
    }

    .slide-img-title{
       width: 100%;
       display: flex;
       justify-content: center;
       align-items: center;
    }

    .slide-img-body {
        text-align: left;
        padding: 20px;
    }

    .slide-img {
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
    }

    .slide-img-description {
        width: 100%;
        height: 55%;
    }

    .food-description-container {
        display: flex;
        flex-direction: row;
        justify-content: start;
        margin-left: 10px;
        align-items: center;
        flex: 2;
        height: 100%;
    }

    .food-description {
        display: flex;
        flex-direction: column;
        justify-content: start;
        margin-left: 10px;
        text-align: left;
        flex: 2
    }

    .food-counter {
        display: flex;
        flex-direction: column;
        justify-content: start;
        padding: 10px;
    }

    .food {
        width: 96.5%;
        height: auto;
        display: flex;
        flex-direction: row;
        justify-content: start;
        align-items: center;
        border: 1px solid #ccc;
        border-radius: 10px;
        margin: 10px;
    }

    .food-image-container {
        height: 180px;
        width: 180px;
        margin-left: 10px;
        border-right: 1px solid #ccc;
        border-radius: 90px;
    }

    .food-image {
        overflow: hidden;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
        border-radius: 90px;
    }

    .main-info {
        width: 95%;
        height: 95%;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
    }

    .main-info-1 {
        width: 49%;
        height: 99%;
        display: flex;
        flex-direction: column;
        justify-content: start;
        align-items: start;
    }

    .main-info-1-title {
        width: 100%;
        height: 50px;
        border: 1px solid #ccc;
        border-radius: 10px;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        background: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .main-info-1-body {
        margin-top: 10px;
        width: 100%;
        height: 80vh;
        border: 1px solid #ccc;
        border-radius: 10px;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        background: #fff;
        display: flex;
        flex-direction: column;
        justify-content: start;
        align-items: center;
    }

    .main-info-center {
        width: 2%;
        height: 100%;
    }

    .main-info-2 {
        width: 49%;
        height: 99%;
        display: flex;
        flex-direction: column;
        justify-content: start;
        align-items: start;
    }

    .main-info-2-title {
        width: 100%;
        height: 50px;
        border: 1px solid #ccc;
        border-radius: 10px;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        background: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .main-info-2-body {
        margin-top: 10px;
        width: 100%;
        height: 80vh;
        border: 1px solid #ccc;
        border-radius: 10px;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        background: #fff;
        display: flex;
        flex-direction: column;
        justify-content: start;
        /* overflow-x: hidden;
        overflow-y: scroll;
        overflow-y: auto; */
    }

    .container {
        width: 100%;
        height: 100%;
        background-color: #fff;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
    }

    .left {
        height: 100%;
        width: 30%;
        border: 1px solid #ccc;
        border-radius: 10px;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
    }

    .right {
        height: 100%;
        width: 69%;
        border: 1px solid #ccc;
        border-radius: 10px;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        display: flex;
        flex-direction: column;
        justify-content: start;
        align-items: center;
        background: #F8FAF5;
    }

    .qr-container {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        height: 95%;
        width: 100%;
        background: #F8FAF5;
        margin: 0 auto;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
    }

    #progressBar {
        width: 90%;
        margin: 10px auto;
        height: 22px;
        background-color: #ccc;
    }

    #progressBar div {
        height: 100%;
        text-align: right;
        padding: 0 10px;
        line-height: 22px;
        width: 0;
        background-color: #116171;
        box-sizing: border-box;
    }

    .information {
        width: 93.5%;
        height: 50px;
        background-color: #fff;
        box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        border: 1px solid #ccc;
        border-radius: 10px;
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin: 10px;
        padding: 10px 10px 20px 20px;
    }

    @import url('https://fonts.googleapis.com/css?family=Alkatra');

    p{
        display: inline;
        margin: 5px;
    }

    #time{
        font-size: 60px;
        color: #004AAD;
    }

    #day{
        font-size: 40px;
        color: #FF5757;
    }

    #date{
        font-size:20px;
        color: #FF5757;
    }

    div{
        font-family: 'Alkatra';
    }

    .main-info-2-body::-webkit-scrollbar {
        width: 12px;               /* width of the entire scrollbar */
    }

    .main-info-2-body::-webkit-scrollbar-track {
        background: orange;        /* color of the tracking area */
    }

    .main-info-2-body::-webkit-scrollbar-thumb {
        background-color: grey;    /* color of the scroll thumb */
        border-radius: 20px;       /* roundness of the scroll thumb */
        border: 3px solid orange;  /* creates padding around scroll thumb */
    }
</style>

<script>
    function getMachineId() {
        let machineId = localStorage.getItem('MachineId');
        if (!machineId) {
            machineId = crypto.randomUUID();
            localStorage.setItem('MachineId', machineId);
        }
        return machineId;
    }

    var machineId = getMachineId();

    var counter = 0;
    var validity = true;

    var timer = 10;
    var menuTimer = 15;
    var infoTimer = 30;

    setTimeout(() => {
        window.location.reload();
    }, 300000);

    reqJson("<?= base_url('index.php?d=absen&c=Absen&m=gateCheck') ?>", "POST", {
        gate: "<?= $gate->gate ?>",
        machineId: machineId
    }, (err, res) => {
        if (res.status === "success") {
            progress(timer, timer, $('#progressBar'));
        } else {
            $("#qr_code").html("<p style='font-size:32px'>Hardware tidak valid</p>");
        }
    });

    new QRCode(document.getElementById("qr_code"), {
        text: "<?= $gate->token ?>",
        width: 450,
        height: 450,
        colorDark : "#000000",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.H
    });

    function progress(timeleft, timetotal, $element) {
        if(validity) {
            if(counter == 10) {
                window.location.reload();
            }

            var progressBarWidth = timeleft * $element.width() / timetotal;
            $element.find('div').animate({
                width: progressBarWidth
            }, 500);

            if (timeleft > 0) {
                setTimeout(function () {
                    progress(timeleft - 1, timetotal, $element);
                }, 1000);
            } else {
                reqJson("<?= base_url('index.php?d=absen&c=Absen&m=genQrCode') ?>", "POST", {
                    gate: "<?= $gate->gate ?>",
                    machineId: machineId
                }, (err, res) => {
                    if (res.status === "success") {
                        $("#qr_code").html("");
                        new QRCode(document.getElementById("qr_code"), {
                            text: res.newQR,
                            width: 450,
                            height: 450,
                            colorDark : "#000000",
                            colorLight : "#ffffff",
                            correctLevel : QRCode.CorrectLevel.H
                        });
                        counter++;
                        progress(timer, timer, $('#progressBar'));
                    } else {
                        alert(res.message);

                        if(res.message != 'Hardware tidak valid') {
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        }
                    }
                });
            }
        }
    };

    function getMenuList(timeleft, timetotal) {
        if (timeleft > 0) {
            setTimeout(function () {
                getMenuList(timeleft - 1, timetotal);
            }, 1000);
        } else {
            reqJson("<?= base_url('index.php?d=absen&c=Absen&m=getMenuList') ?>", "GET", {}, (err, res) => {
                $("#menu-list").html(err.responseText);
                getMenuList(menuTimer, menuTimer);
            });
        }
    };

    function getInfoList(timeleft, timetotal) {
        if (timeleft > 0) {
            setTimeout(function () {
                getInfoList(timeleft - 1, timetotal);
            }, 1000);
        } else {
            reqJson("<?= base_url('index.php?d=absen&c=Absen&m=getInfoList') ?>", "GET", {}, (err, res) => {
                $("#info-list").html(err.responseText);
                getInfoList(infoTimer, infoTimer);
            });
        }
    };

    const getCurrentTimeDate = () => {
        let currentTimeDate = new Date();

        var weekday = new Array(7);
        weekday[0] = "MINGGU";
        weekday[1] = "SENIN";
        weekday[2] = "SELASA";
        weekday[3] = "RABU";
        weekday[4] = "KAMIS";
        weekday[5] = "JUM'AT";
        weekday[6] = "SABTU";

        var month = new Array();
        month[0] = "JANUARI";
        month[1] = "FEBRUARI";
        month[2] = "MARET";
        month[3] = "APRIL";
        month[4] = "MEI";
        month[5] = "JUNI";
        month[6] = "JULI";
        month[7] = "AGUSTUS";
        month[8] = "SEPTEMBER";
        month[9] = "OKTOBER";
        month[10] = "NOVEMBER";
        month[11] = "DESEMBER";

        var hours   =  currentTimeDate.getHours();

        var minutes =  currentTimeDate.getMinutes();
        minutes = minutes < 10 ? '0'+minutes : minutes;

         var AMPM = hours >= 12 ? 'PM' : 'AM';

        if(hours === 12){
            hours=12;
        }else{
            hours = hours%12;
        }

        var currentTime = `${hours}:${minutes}${AMPM}`;
        var currentDay = weekday[currentTimeDate.getDay()];

        var currentDate  = currentTimeDate.getDate();
        var currentMonth = month[currentTimeDate.getMonth()];
        var CurrentYear = currentTimeDate.getFullYear();

        var fullDate = `${currentDate} ${currentMonth} ${CurrentYear}`;

        document.getElementById("time").innerHTML = currentTime;
        document.getElementById("day").innerHTML = currentDay;
        document.getElementById("date").innerHTML = fullDate;

        setTimeout(getCurrentTimeDate, 500);

    }

    function init() {
        getMenuList(1, 1);
        getInfoList(1, 1);
        getCurrentTimeDate();
    }

    init();

</script>