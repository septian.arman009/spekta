<?php
require APPPATH . '/libraries/mc_table.php';

class PDF extends PDF_MC_Table
{
    public function Header()
    {
        $this->SetFont('Times', 'B', 16);
        $this->Cell(180, 1, 'Laporan Absen Harian', 0, 0, 'C');
        $this->Ln(10);
    }

    //Page Content
    public function Content($data, $startDate, $endDate)
    {
        $fontSize1 = 12;
        $fontSize2 = 10;
        $header = 21.8;

        if (count($data) == 0) {
            $this->SetFont('Times', '', 16);
            $this->Cell(180, 1, 'Tidak ada absen untuk di tampilkan', 0, 0, 'C');
            $this->Ln(10);
        }

        $totalData = count($data);
        $index = 1;

        foreach ($data as $absen) {
            $empNpp = "";
            $empName = "";
            $deptName = "";
            $subDeptName = "";
            $divName = "";

            if (count($absen) > 0) {
                $empNpp = $absen[0]['npp'];
                $empName = $absen[0]['employee_name'];
                $deptName = $absen[0]['department_name'];
                $subDeptName = $absen[0]['sub_department_name'];
                $divName = $absen[0]['division_name'];
            }

            $this->SetFont('Times', '', $fontSize1);
            $this->Cell(25, 5, 'No. ID', 0, 0, 'L');
            $this->Cell(130, 5, ': ' . $empNpp, 0, 0, 'L');
            $this->Cell(15, 5, 'Periode Waktu', 0, 0, 'L');
            $this->Ln();
            $this->Cell(25, 5, 'Nama', 0, 0, 'L');
            $this->Cell(115, 5, ': ' . ucwords(strtolower($empName)), 0, 0, 'L');
            $this->Cell(20, 5, "Dari $startDate s/d $endDate", 0, 0, 'L');
            $this->Ln();
            $this->Cell(25, 5, 'Sub Unit', 0, 0, 'L');
            $this->Cell(115, 5, ': ' . ucwords(strtolower($deptName)), 0, 0, 'L');
            $this->Ln();
            $this->Cell(25, 5, 'Bagian', 0, 0, 'L');
            $this->Cell(115, 5, ': ' . ucwords(strtolower($subDeptName)), 0, 0, 'L');
            $this->Ln();
            $this->Cell(25, 5, 'Sub Bagian', 0, 0, 'L');
            $this->Cell(115, 5, ': ' . ucwords(strtolower($divName)), 0, 0, 'L');

            $this->Ln(10);
            $this->SetFont('Times', 'B', $fontSize2);
            $this->Cell($header, 5, 'Tanggal', 1, 0, 'C');
            $this->Cell($header, 5, 'Jam Masuk', 1, 0, 'C');
            $this->Cell($header, 5, 'Jam Pulang', 1, 0, 'C');
            $this->Cell($header, 5, 'Scan Masuk', 1, 0, 'C');
            $this->Cell($header, 5, 'Scan Pulang', 1, 0, 'C');
            $this->Cell($header, 5, 'Terlambat', 1, 0, 'C');
            $this->Cell($header, 5, 'Plg Cpt', 1, 0, 'C');
            $this->Cell($header, 5, 'Lembur', 1, 0, 'C');
            $this->Cell($header, 5, 'Jam Prod', 1, 0, 'C');
            $this->Ln();

            $this->Ln(1);

            foreach ($absen as $npp => $value) {
                $this->SetFont('Times', '', $fontSize2);
                $this->Cell($header, 5, $value['abs_date'], 1, 0, 'C');
                $this->Cell($header, 5, $value['sch_date_in'], 1, 0, 'C');
                $this->Cell($header, 5, $value['sch_date_out'], 1, 0, 'C');
                $this->Cell($header, 5, $value['date_in'], 1, 0, 'C');
                $this->Cell($header, 5, $value['date_out'], 1, 0, 'C');
                $this->Cell($header, 5, $value['difHourDateIn'], 1, 0, 'C');
                $this->Cell($header, 5, $value['difHourDateOut'], 1, 0, 'C');
                $this->Cell(22, 5, $value['ovt_start_date'] != '-' ? $value['ovt_start_date'] . ' - ' . $value['ovt_end_date'] : '-', 1, 0, 'C');
                $this->Cell($header, 5, $value['prod'], 1, 0, 'L');
                $this->Ln();

                $data++;
            }

            $index++;
            
            if($index <= $totalData) {
                $this->AddPage();
            }
        }
    }

    //Page footer
    public function Footer()
    {
        //atur posisi 1.5 cm dari bawah
        $this->SetY(-15);
        //buat garis horizontal
        $this->Line(10, $this->GetY(), 290, $this->GetY());
        //Arial italic 9
        $this->SetFont('Times', 'I', 9);
        //nomor halaman
        $this->Cell(0, 10, 'Halaman ' . $this->PageNo() . ' dari {nb}', 0, 0, 'R');
    }
}

$pdf = new PDF('P');
$pdf->SetMargins(7, 15, 7, 7);
$pdf->SetTitle($title);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content($data, $startDate, $endDate);
$pdf->Output();
