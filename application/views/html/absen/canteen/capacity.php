<html>
    <head>
        <script src="<?= asset('js/jquery.min.js') ?>"></script>
        <script src="<?= asset('js/custom.js') ?>"></script>
    </head>
        
    <body>
        <div class="container">
            <div class="counter">0 / 0</div>
        </div>
    </body>
</html>

<style>
    .container {
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .counter {
        font-size: 150;
        font-weight: bold;
    }
</style>

<script>
    var menuTimer = 10;

    function getCapacity(timeleft, timetotal) {
        if (timeleft > 0) {
            setTimeout(function () {
                getCapacity(timeleft - 1, timetotal);
            }, 1000);
        } else {
            reqJson("<?= base_url('index.php?d=erp/g_affair&c=OtherController&m=countCapacity') ?>", "GET", {}, (err, res) => {
                if(res.status == 'success') {
                    $(".counter").html(`${res.total} / ${res.capacity}`);
                    getCapacity(menuTimer, menuTimer);
                }
            });
        }
    };

    function init() {
        getCapacity(1, 1);
    }
    init();
</script>
