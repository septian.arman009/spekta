<html>
    <head>
        <script src="<?= asset('js/jquery.min.js') ?>"></script>
        <script src="<?= asset('js/custom.js') ?>"></script>
    </head>
    
    <body>
        <div class="container">
            <div id="menu-list"></div>
        </div>
    </body>
</html>

<style>
    .food-description-container {
        display: flex;
        flex-direction: row;
        justify-content: start;
        margin-left: 10px;
        align-items: center;
        flex: 2
    }

    .food-description {
        display: flex;
        flex-direction: column;
        justify-content: start;
        margin-left: 10px;
        text-align: left;
        flex: 2;
    }

    .span-detail {
        border: 1px solid #ccc;
        background-color: #ddd;
        border-radius: 5px;
        margin-right: 3px;
        padding: 3px;
    }

    .food-counter {
        display: flex;
        flex-direction: column;
        justify-content: start;
        padding: 10px;
    }

    .food {
        width: 99%;
        height: 80px;
        display: flex;
        flex-direction: row;
        justify-content: start;
        align-items: center;
        border: 1px solid #ccc;
        border-radius: 10px;
        margin: 10px;
    }

    .food-image-container {
        height: 80px;
        width: 80px;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
        border-right: 1px solid #ccc;;
    }

    .food-image {
        overflow: hidden;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
    }

    .container {
        width: 100%;
        height: 100%;
        background-color: #fff;
    }

    div{
        font-family: 'Alkatra';
    }

    .main-info-2-body::-webkit-scrollbar {
        width: 12px;               /* width of the entire scrollbar */
    }

    .main-info-2-body::-webkit-scrollbar-track {
        background: orange;        /* color of the tracking area */
    }

    .main-info-2-body::-webkit-scrollbar-thumb {
        background-color: grey;    /* color of the scroll thumb */
        border-radius: 20px;       /* roundness of the scroll thumb */
        border: 3px solid orange;  /* creates padding around scroll thumb */
    }
</style>

<script>
    var menuTimer = 10;

    function getMenuList(timeleft, timetotal) {
        if (timeleft > 0) {
            setTimeout(function () {
                getMenuList(timeleft - 1, timetotal);
            }, 1000);
        } else {
            reqJson("<?= base_url('index.php?d=absen&c=Absen&m=getMenuListGa') ?>", "GET", {}, (err, res) => {
                $("#menu-list").html(err.responseText);
                getMenuList(menuTimer, menuTimer);
            });
        }
    };

    function init() {
        getMenuList(1, 1);
    }

    init();

</script>