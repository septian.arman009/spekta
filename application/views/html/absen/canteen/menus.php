<?php foreach ($menus as $menu) { ?>
    
    <div class="food">
        <div class="food-image-container">
            <img class="food-image" height="100%" width="100%" src="<?= imageAssets('daily_menus', $menu->filename) ?>" alt="food_image"/>
        </div>
        <div class="food-description-container">
            <div class="food-description">
                <span style='font-size: 24px; margin-bottom: 5px;'><?= $menu->name ?></span>
                <div class="food-description-detail">
                    <?php 
                        $descs = explode(",", $menu->description); 

                        foreach ($descs as $desc) {
                            echo "<span class='span-detail'>$desc</span>";
                        }
                    ?>
                </div>
            </div>

            <div class="food-counter">
                <span>Tersedia: </span>
                <span style='font-size: 32px'><?= $menu->available - $menu->picked ?></span>
            </div>
        </div>
    </div>

<?php } ?>
