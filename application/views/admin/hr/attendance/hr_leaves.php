<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function hrLeaves(){
        var legend = legendGrid();

        var totalFile;
        var fileError;

        var comboUrl = {
            nip: {
				url: User("getEmps"),
				reload: true
			},
        }

        var hrLeaveLayout = mainTab.cells("hr_leaves").attachLayout({
            pattern: "1C",
            cells: [
                {id: "a", header : false}
            ]
        });

        var leaveMenu = userLogged.role == "admin" || userLogged.subDepartment == 11
            ? [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "upload", text: "Input Cuti", img: "edit.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
                {id: "approve", text: "Approve", img: "check.png"},
                {id: "reject", text: "Reject", img: "messagebox_critical.png"},
                {id: "month", text: genSelectMonth("hr_leave_year", "hr_leave_month")},
                {id: "read", text: "Lihat File Pendukung", img: "read_16.png"},
            ]
            : [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "upload", text: "Input Cuti", img: "edit.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
                {id: "month", text: genSelectMonth("hr_leave_year", "hr_leave_month")},
                {id: "read", text: "Lihat File Pendukung", img: "read_16.png"},
            ];

        var hrLeaveMenu = hrLeaveLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: leaveMenu
        });

        hrLeaveMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "approve":
                    approve();
                    break;
                case "reject":
                    reject();
                    break;
                case "export":
                    leaveGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
                case "refresh":
                    rLeaveGrid();
                    break;
                case "upload":
                    inputLeave();
                    break;
                case "read":
                    if(leaveGrid.getSelectedRowId()) {
                        let id = leaveGrid.getSelectedRowId();
                        if(leaveGrid.cells(id, 14).getValue() == 'Spekta' && leaveGrid.cells(id, 13).getValue() != '') {
                            if(leaveGrid.cells(id, 13).getValue().includes(".pdf")) {
                                reqJson(Document("encryptFile"), "POST", { filename: leaveGrid.cells(id, 13).getValue() }, (err, res) => {
                                    window.open(BASE_URL + "index.php?c=Pc&m=leaves_pdfreader&mode=read&token=" + res.filename, "_blank");
                                });
                            } else {
                                reqJson(Attendance("viewAttachment"), "POST", {id: leaveGrid.getSelectedRowId()}, (err, res) => {
                                    if(res.status === "success") {
                                        var attachWin = createWindow("hr_leave_attachment", "Attachment Cuti: " + leaveGrid.cells(id, 8).getValue(), 1000, 800);
                                        myWins.window("hr_leave_attachment").skipMyCloseEvent = true;
                                        attachWin.attachHTMLString(res.template);
                                    }
                                });
                            }
                        } else {
                            eAlert("Tidak ada file pendukung yang bisa di tampilkan!");
                        }
                    } else {
                        eAlert("Belum ada file yang dipilih!");
                    }
                    break;
            }
        });

        hrLeaveMenu.setItemDisabled("approve");
        hrLeaveMenu.setItemDisabled("reject");

        function approve() {
            if(!leaveGrid.getSelectedRowId()) return eAlert("Belum ada data yang di pilih!");
            reqConfirm3(Attendance("approveLeave"), { id: leaveGrid.getSelectedRowId() }, (err, res) => {
                if(err) {
                    eAlert(err.message);
                } else {
                    if (res.status == 'success') {
                        rLeaveGrid();
                        hrLeaveMenu.setItemDisabled("approve");
                        hrLeaveMenu.setItemDisabled("reject");
                        sAlert(res.message);
                    } else {
                        eAlert(res.message);
                    }
                }
            });
        }

        function reject() {
            if(!leaveGrid.getSelectedRowId()) return eAlert("Belum ada data yang di pilih!");
            reqConfirm3(Attendance("rejectLeave"), { id: leaveGrid.getSelectedRowId() }, (err, res) => {
                if(err) {
                    eAlert(err.message);
                } else {
                    if (res.status == 'success') {
                        rLeaveGrid();
                        hrLeaveMenu.setItemDisabled("approve");
                        hrLeaveMenu.setItemDisabled("reject");
                        sAlert(res.message);
                    } else {
                        eAlert(res.message);
                    }
                }
            });
        }
        
        $("#hr_leave_year").on("change", function() {
            rLeaveGrid();
        });

        $("#hr_leave_month").on("change", function() {
            rLeaveGrid();
        });

        var hrLeaveStatus = hrLeaveLayout.cells("a").attachStatusBar();
        function leaveGridCount() {
            hrLeaveStatus.setText("Total baris: " + leaveGrid.getRowsNum() + " ("+ legend.hr_leaves + ")");
        }

        var leaveGrid =  hrLeaveLayout.cells("a").attachGrid();
        leaveGrid.setHeader("No,Nama,Sub Unit,Bagian,Sub Bagian,Tanggal Mulai Cuti, Tanggal Selesai Cuti,Jenis Cuti,No. Cuti,Keterangan,Approver #1,Approver #2,Status,Filename,Source");
        leaveGrid.attachHeader("#rspan,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter")
        leaveGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        leaveGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        leaveGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
        leaveGrid.setInitWidthsP("5,20,20,20,20,15,15,10,15,25,20,20,5,0,5");
        leaveGrid.enableSmartRendering(true);
        leaveGrid.enableMultiselect(false);
        leaveGrid.attachEvent("onXLE", function() {
            hrLeaveLayout.cells("a").progressOff();
        });
        leaveGrid.attachEvent("onRowSelect", function(rId, cIdn){
           if(leaveGrid.cells(rId, 12).getValue() != 'APPROVED') {
                hrLeaveMenu.setItemDisabled("approve");
                hrLeaveMenu.setItemDisabled("reject");
           } else {
                if(userLogged.role == "admin" || userLogged.subDepartment == 11) {
                    hrLeaveMenu.setItemEnabled("approve");
                    hrLeaveMenu.setItemEnabled("reject");
                } else {
                    hrLeaveMenu.setItemDisabled("approve");
                    hrLeaveMenu.setItemDisabled("reject");
                }
           }
        });
        leaveGrid.init();

        function rLeaveGrid() {
            let year = $("#hr_leave_year").val();
            let month = $("#hr_leave_month").val();

            let params = {
                month,
                year
            }

            if(userLogged.role !== "admin" && userLogged.rankId != 1 && userLogged.pltRankId != 1) {
                if(userLogged.rankId == 2 || userLogged.pltRankId == 2) {
                    params.in_department_id = userLogged.deptId+","+userLogged.pltDeptId;
                }else if(userLogged.rankId > 2 || userLogged.pltRankId > 2) {
                    if(userLogged.rankId == 3 || userLogged.pltRankId == 3 || userLogged.rankId == 4 || userLogged.pltRankId == 4) {
                        if(userLogged.subId == 9) {
                            params.in_sub_department_id = userLogged.subId+","+userLogged.pltSubId+",1,2,3,4,13";
                        } else {
                            params.in_sub_department_id = userLogged.subId+","+userLogged.pltSubId;
                        }
                    } else {
                        params.in_sub_department_id = userLogged.subId+","+userLogged.pltSubId;
                    }
                }
            }

            hrLeaveLayout.cells("a").progressOn();
            leaveGrid.clearAndLoad(Attendance("getLeaves", params), leaveGridCount);
        }

        rLeaveGrid();

        function inputLeave() {
            var inputLeaveWin = createWindow("input_leave_win", "Input Cuti", 550, 550);
            myWins.window("input_leave_win").skipMyCloseEvent = true;

            var winInputLeaveForm = inputLeaveWin.attachForm([
                {type: "fieldset", offsetTop: 30, offsetLeft: 30, label: "Pilih Jenis Cuti", list: [
                    {type: "combo", name: "nip", label: "Nama Karyawan", labelWidth: 130, inputWidth: 250, 
                        validate: "NotEmpty", 
                        required: true
                    },
                    {type: "combo", name: "leave_name", label: "Pilih Jenis Cuti", readonly: true, labelWidth: 130, inputWidth: 250, required: true,
                        validate: "NotEmpty", 
                        options:[
                            {value: "Cuti Sakit", text: "Cuti Sakit"},
                            {value: "Cuti Lainnya", text: "Cuti Lainnya"},
                        ]
                    },
                    {type: "calendar", name: "date_start", label: "Tanggal Cuti Dari", labelWidth: 130, inputWidth: 250, readonly: true, required: true},
                    {type: "calendar", name: "date_finish", label: "Tanggal Cuti Sampai", labelWidth: 130, inputWidth: 250, readonly: true, required: true},
                    {type: "input", name: "reason", label: "Alasan", labelWidth: 130, inputWidth: 250, rows: 5, required: true},
                    {type: "hidden", name: "filename", label: "Filename", readonly: true},
                    {type: "upload", name: "file_uploader", inputWidth: 420, required: true,
                        url: AppMaster("fileUpload2", {save: false, folder: "hr_leaves"}), 
                        swfPath: "./public/codebase/ext/uploader.swf", 
                        swfUrl: AppMaster("fileUpload2")
                    },
                ]},
                {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                    {type: "button", name: "save", className: "button_update", offsetLeft: 15, value: "Simpan"}
                ]},
            ]);

            var nipCombo = winInputLeaveForm.getCombo("nip");
				nipCombo.enableFilteringMode(true, 'nip');
        		nipCombo.attachEvent("onDynXLS", nipComboFilter);

				function nipComboFilter(text){
					nipCombo.clearAll();
					if(text.length > 3) {
						dhx.ajax.get(User('getEmps', {name: text}), function(xml){
							if(xml.xmlDoc.responseText) {
								nipCombo.load(xml.xmlDoc.responseText);
								nipCombo.openSelect();
							}
						});
					}
				};

            winInputLeaveForm.attachEvent("onBeforeFileAdd", async function (filename, size) {
                beforeFileAdd(winInputLeaveForm, {filename, size});
            });

            winInputLeaveForm.attachEvent("onBeforeFileUpload", function(mode, loader, formData){
                if(fileError) {
                    clearUploader(winInputLeaveForm, "file_uploader");
                    eAlert("File error silahkan upload file sesuai ketentuan!");
                    fileError = false;
                } else {
                    return true;
                }
            });

            winInputLeaveForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "save":
                        const uploader = winInputLeaveForm.getUploader("file_uploader");
                        if(uploader.getStatus() === -1) {
                            if(!fileError) {
                                uploader.upload();
                            } else {
                                uploader.clear();
                                eAlert("File error silahkan upload file sesuai ketentuan!");
                                fileError = false;
                            }
                        } else {
                            addLeaveSubmit();
                        }
                        break;
                }
            });

            winInputLeaveForm.attachEvent("onUploadFile", function(filename, servername){
                winInputLeaveForm.setItemValue("filename", servername);
                addLeaveSubmit();
            });

            function addLeaveSubmit() {
                if (!winInputLeaveForm.validate()) return eAlert("Input error!");

                setDisable(["save"], winInputLeaveForm, inputLeaveWin);

                let winInputLeaveFormDP = new dataProcessor(Attendance("leaveForm"));
                winInputLeaveFormDP.init(winInputLeaveForm);
                winInputLeaveForm.save();

                winInputLeaveFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                    let message = tag.getAttribute("message");
                    switch (action) {
                        case "inserted":
                            sAlert("Berhasil Menambahkan Record <br>" + message);
                            clearAllForm(winInputLeaveForm);
                            clearUploader(winInputLeaveForm, "file_uploader");
                            setEnable(["save"], winInputLeaveForm, inputLeaveWin);
                            rLeaveGrid();
                            closeWindow("input_leave_win");
                            break;
                        case "error":
                            eAlert("Gagal Menambahkan Record <br>" + message);
                            setEnable(["save"], winInputLeaveForm, inputLeaveWin);
                            break;
                    }
                });
            }
        }

        async function beforeFileAdd(form, file, id = null) {
            if(form.validate()) {
                var ext = file.filename.split(".").pop();
                if (ext == "pdf") {
                    if (file.size > 1000000) {
                        fileError = true;
                        eAlert("Tidak boleh melebihi 1 MB!");
                    } else {
                        if(totalFile > 0) {
                            eAlert("Maksimal 1 file");
                            fileError = true;
                        }
                    }		    
                } else {
                    eAlert("Hanya pdf saja yang bisa diupload!");
                    fileError = true;
                }
            } else {
                eAlert("Input error!");
            }	
        }
    }

JS;

header('Content-Type: application/javascript');
echo $script;