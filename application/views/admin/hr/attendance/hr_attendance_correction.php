<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

function showAbsenCorrection() {
        var legend = legendGrid();

        var comboUrl = {
            shift_id: {
                url: MAbsen("getShiftList"),
                reload: true
            }
        }

        var hours = [];
        var minutes = [];
        var selectedCorrection = [];
        var fileError;
        var totalFile;
        
        for(let h=0;h <= 23;h++){
            hours.push({
                value: h < 10 ? "0"+h : h,
                text: h < 10 ? "0"+h : h
            });
        }

        for(let m=0;m <= 60;m++){
            minutes.push({
                value: m < 10 ? "0"+m : m,
                text: m < 10 ? "0"+m : m
            });
        }

        var hrCorrectionLayout = mainTab.cells("hr_attendance_correction").attachLayout({
            pattern: "1C",
            cells: [
                {id: "a", header : false}
            ]
        });

        var hrCorrectionMenu = hrCorrectionLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "month", text: genSelectMonth("hr_attendance_correction_year", "hr_attendance_correction_month")},
                {id: "filter", text: "<div style='width:100%'>Status: <select style='height:22px;margin-top:3px' id='filter_correction_status'><option>ALL</option><option value='1'>ON PROCESS</option><option value='2'>APPROVED</option><option value='3'>COMPLETED</option><option value='4'>REJECTED</option><option value='5'>CANCELED</option></select></div>"},
                {id: "add", text: "Koreksi Baru", img: "add.png"},
                {id: "cancel", text: "Cancel Koreksi", img: "cancel.png"},
                {id: "read", text: "Lihat File Pendukung", img: "read_16.png"},
            ]
        });

        $("#filter_correction_status").on("change", function() {
            rCorrectionGrid();
        });

        hrCorrectionMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "refresh":
                    rCorrectionGrid();
                    break;
                case "add":
                    addCorrection();
                    break;
                case "cancel":
                    if(selectedCorrection.length == 0) return eAlert("Belum ada koreksi yang dipilih");
                    let month = $("#hr_attendance_correction_month").val();
                    let year = $("#hr_attendance_correction_year").val();
                    reqConfirm3(Attendance("cancelCorrection"), { absenIds: selectedCorrection, month, year }, (err, res) => {
                        if(err) {
                            eAlert(err.message);
                        } else {
                            if (res.status == 'success') {
                                selectedCorrection = [];
                                rCorrectionGrid();
                                sAlert(res.message);
                            } else {
                                eAlert(res.message);
                            }
                        }
                    });
                    break;
                case "read":
                    if(correctionGrid.getSelectedRowId()) {
                        let id = correctionGrid.getSelectedRowId();
                        reqJson(Document("encryptFile"), "POST", { filename: correctionGrid.cells(id, 18).getValue() }, (err, res) => {
                            window.open(BASE_URL + "index.php?c=Pc&m=correction_pdfreader&mode=read&token=" + res.filename, "_blank");
                        });
                    } else {
                        eAlert("Belum ada file yang dipilih!");
                    }
                    break;
            }
        });

        $("#hr_attendance_correction_year").on("change", function() {
            rCorrectionGrid();
        });

        $("#hr_attendance_correction_month").on("change", function() {
            rCorrectionGrid();
        });

        var hrCorrectionStatus = hrCorrectionLayout.cells("a").attachStatusBar();
        function correctionGridCount() {
            hrCorrectionStatus.setText("Total baris: " + correctionGrid.getRowsNum() + " ("+ legend.absen_correction + ")");

            for(let i=0;i < correctionGrid.getRowsNum();i++) {
                let id = correctionGrid.getRowId(i);
                if(correctionGrid.cells(id, 17).getValue() == 'ON PROCESS' || correctionGrid.cells(id, 17).getValue() == 'APPROVED') {
                    correctionGrid.cells(id, 1).setDisabled(false); 
                } else {
                    correctionGrid.cells(id, 1).setDisabled(true);
                }
            }
        }

        var correctionGrid = hrCorrectionLayout.cells("a").attachGrid();
        correctionGrid.setImagePath("./public/codebase/imgs/");
        correctionGrid.setHeader("No,<input type='checkbox' id='correction_checkbox' />,Nama Karyawan,Bagian,Sub Bagian,Shift Kerja,Tanggal,Jam Masuk,Jam Keluar,Absen Masuk,Absen Keluar,Koreksi Shift,Koreksi Masuk,Koreksi Keluar,Alasan,Alasan Reject,Tanggal Pengajuan,Status,Filename");
        correctionGrid.attachHeader("#rspan,#rspan,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter")
        correctionGrid.setColSorting("int,na,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        correctionGrid.setColTypes("rotxt,ch,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        correctionGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
        correctionGrid.setInitWidthsP("5,5,15,20,20,10,15,15,15,15,15,15,15,15,25,25,15,7,0");
        correctionGrid.enableSmartRendering(true);
        correctionGrid.enableMultiselect(false);
        correctionGrid.attachEvent("onXLE", function() {
            hrCorrectionLayout.cells("a").progressOff();
        });
        correctionGrid.attachEvent("onCheckbox", function(rId,cInd,state){
            if(state) {
                selectedCorrection.push(rId);
            } else {
                const index = selectedCorrection.indexOf(rId);
                if(index > -1) {
                    selectedCorrection.splice(index, 1);
                }
            }
        });
        correctionGrid.init();

        $("#correction_checkbox").on("change", function() {
            let checked = document.getElementById('correction_checkbox').checked;
            if(checked) {
                for(let i=0;i < correctionGrid.getRowsNum();i++) {
                    let id = correctionGrid.getRowId(i);
                    if(correctionGrid.cells(id, 17).getValue() == 'ON PROCESS' || correctionGrid.cells(id, 17).getValue() == 'APPROVED') {
                        selectedCorrection.push(id);
                        correctionGrid.cells(id, 1).setValue(1);
                    }
                }
            } else {
                selectedCorrection = [];
                for(let i=0;i < correctionGrid.getRowsNum();i++) {
                    let id = correctionGrid.getRowId(i);
                    if(correctionGrid.cells(id, 17).getValue() == 'ON PROCESS' || correctionGrid.cells(id, 17).getValue() == 'APPROVED') {
                        correctionGrid.cells(id, 1).setValue(0);
                    }
                }
            }
        });

        function rCorrectionGrid() {
            let status = $("#filter_correction_status").val();
            let month = $("#hr_attendance_correction_month").val();
            let year = $("#hr_attendance_correction_year").val();

            let params = {
                month, year
            }

            if(userLogged.role !== "admin" && userLogged.rankId != 1 && userLogged.pltRankId != 1 && userLogged.subId != 11) {
                if(userLogged.rankId > 6 || userLogged.pltRankId > 6) {
                    params.in_emp_sub_department_id = userLogged.subId+","+userLogged.pltSubId;
                } else {
                    params.equal_a_emp_id = userLogged.empId;
                }
            }

            if(status != "ALL") {
                params.equal_a_correction_status = status;
            }

            hrCorrectionLayout.cells("a").progressOn();
            correctionGrid.clearAndLoad(MAbsen("getAbsenCorrections", params), correctionGridCount);
        }

        function init() {
            rCorrectionGrid();
        }

        function addCorrection() {
            var selectedAbsen = [];
            var baseAbsen = {
                absenId: -1,
                labelIn: "",
                hourIn: "",
                minuteIn: "",
                labelOut: "",
                hourOut: "",
                minuteOut: "",
                hourSchIn: "",
                minuteSchIn: "",
                hourSchOut: "",
                minuteSchOut: ""
            };

            var absenWindow = createWindow("absen_window", "Koreksi Absen", 1200, 1050);
            myWins.window("absen_window").skipMyCloseEvent = true;

            var winLayout = absenWindow.attachLayout({
                pattern: "2E",
                cells: [
                    {id: "a", text: "Daftar Absen"},
                    {id: "b", text: "Form Revisi"}
                ]
            });

            let month = $("#hr_attendance_correction_month").val();
            let year = $("#hr_attendance_correction_year").val();

            var winGrid = winLayout.cells("a").attachGrid();
            winLayout.cells("a").progressOn();
            winGrid.setImagePath("./public/codebase/imgs/");
            winGrid.setHeader("No,<input type='checkbox' id='win_checkbox' />,Nama Karyawan,Shift Kerja,Tanggal,Jam Masuk,Jam Keluar,Absen Masuk,Absen Keluar,Correction Status,LabelIn,HourIn,MinuteIn,LabelOut,HourOut,MinuteHour,HourSchIn,MinuteSchIn,HourSchOut,MinuteSchHour,ShiftId");
            winGrid.attachHeader("#rspan,#rspan,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter")
            winGrid.setColSorting("int,na,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
            winGrid.setColTypes("rotxt,ch,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
            winGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
            winGrid.setInitWidthsP("5,5,20,15,25,25,25,25,25,25,0,0,0,0,0,0,0,0,0,0,0");
            winGrid.enableSmartRendering(true);
            winGrid.enableMultiselect(false);
            winGrid.attachEvent("onXLE", function() {
                winLayout.cells("a").progressOff();
            });
            winGrid.attachEvent("onCheckbox", function(rId,cInd,state){
                if(state) {
                    if(selectedAbsen.length == 0) {
                        selectedAbsen.push(rId);
                        baseAbsen = {
                            absenId: rId,
                            labelIn: winGrid.cells(rId, 10).getValue(),
                            hourIn: winGrid.cells(rId, 11).getValue(),
                            minuteIn: winGrid.cells(rId, 12).getValue(),
                            labelOut: winGrid.cells(rId, 13).getValue(),
                            hourOut: winGrid.cells(rId, 14).getValue(),
                            minuteOut: winGrid.cells(rId, 15).getValue(),
                            hourSchIn: winGrid.cells(rId, 16).getValue(),
                            minuteSchIn: winGrid.cells(rId, 17).getValue(),
                            hourSchOut: winGrid.cells(rId, 18).getValue(),
                            minuteSchOut: winGrid.cells(rId, 19).getValue(),
                            shiftId: winGrid.cells(rId, 20).getValue(),
                        };

                        shiftCombo.selectOption(baseAbsen.shiftId);

                        for(let i=0;i < winGrid.getRowsNum();i++) {
                            let id = winGrid.getRowId(i);

                            if(id != rId) {
                                if(
                                    winGrid.cells(id, 10).getValue() != baseAbsen.labelIn ||
                                    winGrid.cells(id, 13).getValue() != baseAbsen.labelOut ||
                                    winGrid.cells(id, 16).getValue() != baseAbsen.hourSchIn ||
                                    winGrid.cells(id, 17).getValue() != baseAbsen.minuteSchIn ||
                                    winGrid.cells(id, 18).getValue() != baseAbsen.hourSchOut ||
                                    winGrid.cells(id, 19).getValue() != baseAbsen.minuteSchOut
                                ) {
                                    winGrid.cells(id, 1).setDisabled(true); 
                                } else {
                                    winGrid.cells(id, 1).setDisabled(false); 
                                }
                            }
                        }

                        $("#label_in").html(baseAbsen.labelIn);
                        $("#label_out").html(baseAbsen.labelOut);

                        correctionForm.setItemValue('label_in', baseAbsen.labelIn);
                        correctionForm.setItemValue('label_out', baseAbsen.labelOut);

                        let hourInConbo = correctionForm.getCombo("start_hour");
                        let hourInIndex = hours.findIndex(e => e.value == baseAbsen.hourIn);
                        hourInConbo.selectOption(hourInIndex);

                        let minuteInCombo = correctionForm.getCombo("start_minute");
                        let minuteInIndex = minutes.findIndex(e => e.value == baseAbsen.minuteIn);
                        minuteInCombo.selectOption(minuteInIndex);

                        let hourOutCombo = correctionForm.getCombo("end_hour");
                        let hourOutIndex = hours.findIndex(e => e.value == baseAbsen.hourOut);
                        hourOutCombo.selectOption(hourOutIndex);

                        let minuteOuteCombo = correctionForm.getCombo("end_minute");
                        let minuteOutIndex = minutes.findIndex(e => e.value == baseAbsen.minuteOut);
                        minuteOuteCombo.selectOption(minuteOutIndex);
                    } else {
                        selectedAbsen.push(rId);

                        let hourInConbo = correctionForm.getCombo("start_hour");
                        let hourInIndex = hours.findIndex(e => e.value == baseAbsen.hourSchIn);
                        hourInConbo.selectOption(0);

                        let minuteInCombo = correctionForm.getCombo("start_minute");
                        let minuteInIndex = minutes.findIndex(e => e.value == baseAbsen.minuteSchIn);
                        minuteInCombo.selectOption(0);

                        let hourOutCombo = correctionForm.getCombo("end_hour");
                        let hourOutIndex = hours.findIndex(e => e.value == baseAbsen.hourSchOut);
                        hourOutCombo.selectOption(0);

                        let minuteOuteCombo = correctionForm.getCombo("end_minute");
                        let minuteOutIndex = minutes.findIndex(e => e.value == baseAbsen.minuteSchOut);
                        minuteOuteCombo.selectOption(0);
                    }
                } else {
                    var index = selectedAbsen.indexOf(rId);
                    selectedAbsen.splice(index, 1);
                    
                    if(selectedAbsen.length == 0) {
                       
                        baseAbsen = {};

                        $("#label_in").html("Pilih Absen");
                        $("#label_out").html("Pilih Absen");

                        correctionForm.setItemValue('label_in', "");
                        correctionForm.setItemValue('label_out', "");

                        for(let i=0;i < winGrid.getRowsNum();i++) {
                            let id = winGrid.getRowId(i);
                            winGrid.cells(id, 1).setDisabled(false);                                 
                        }

                        let hourInConbo = correctionForm.getCombo("start_hour");
                        hourInConbo.selectOption(0);

                        let minuteInCombo = correctionForm.getCombo("start_minute");
                        minuteInCombo.selectOption(0);

                        let hourOutCombo = correctionForm.getCombo("end_hour");
                        hourOutCombo.selectOption(0);

                        let minuteOuteCombo = correctionForm.getCombo("end_minute");
                        minuteOuteCombo.selectOption(0);
                    } if(selectedAbsen.length == 1) {
                        let hourInConbo = correctionForm.getCombo("start_hour");
                        let hourInIndex = hours.findIndex(e => e.value == baseAbsen.hourIn);
                        hourInConbo.selectOption(0);

                        let minuteInCombo = correctionForm.getCombo("start_minute");
                        let minuteInIndex = minutes.findIndex(e => e.value == baseAbsen.minuteIn);
                        minuteInCombo.selectOption(0);

                        let hourOutCombo = correctionForm.getCombo("end_hour");
                        let hourOutIndex = hours.findIndex(e => e.value == baseAbsen.hourOut);
                        hourOutCombo.selectOption(0);

                        let minuteOuteCombo = correctionForm.getCombo("end_minute");
                        let minuteOutIndex = minutes.findIndex(e => e.value == baseAbsen.minuteOut);
                        minuteOuteCombo.selectOption(0);
                    }
                }
            });
            winGrid.init();
            winGrid.clearAndLoad(MAbsen("getAbsenForCorrections", {month, year}), afterWinGrid);

            $("#win_checkbox").on("change", function() {
                if(selectedAbsen.length == 0) return eAlert("Belum ada absen dipilih!");

                let checked = document.getElementById('win_checkbox').checked;
                if(checked) {
                    let hourInConbo = correctionForm.getCombo("start_hour");
                    let hourInIndex = hours.findIndex(e => e.value == baseAbsen.hourSchIn);
                    hourInConbo.selectOption(hourInIndex);

                    let minuteInCombo = correctionForm.getCombo("start_minute");
                    let minuteInIndex = minutes.findIndex(e => e.value == baseAbsen.minuteSchIn);
                    minuteInCombo.selectOption(minuteInIndex);

                    let hourOutCombo = correctionForm.getCombo("end_hour");
                    let hourOutIndex = hours.findIndex(e => e.value == baseAbsen.hourSchOut);
                    hourOutCombo.selectOption(hourOutIndex);

                    let minuteOuteCombo = correctionForm.getCombo("end_minute");
                    let minuteOutIndex = minutes.findIndex(e => e.value == baseAbsen.minuteSchOut);
                    minuteOuteCombo.selectOption(minuteOutIndex);

                    for (let i = 0; i < winGrid.getRowsNum(); i++) {
                        let id = winGrid.getRowId(i);
                        if(
                            (
                                winGrid.cells(id, 9).getValue() == 'AVAILABLE' || 
                                winGrid.cells(id, 9).getValue() == 'REJECTED' || 
                                winGrid.cells(id, 9).getValue() == 'CANCELED'
                            ) &&
                            winGrid.cells(id, 10).getValue() == baseAbsen.labelIn &&
                            winGrid.cells(id, 13).getValue() == baseAbsen.labelOut &&
                            winGrid.cells(id, 16).getValue() == baseAbsen.hourSchIn &&
                            winGrid.cells(id, 17).getValue() == baseAbsen.minuteSchIn &&
                            winGrid.cells(id, 18).getValue() == baseAbsen.hourSchOut &&
                            winGrid.cells(id, 19).getValue() == baseAbsen.minuteSchOut
                        ) {
                            if(id != baseAbsen.absenId) {
                                selectedAbsen.push(id);
                                winGrid.cells(id, 1).setValue(1);
                            }
                        }
                    }
                } else {
                    selectedAbsen = [];
                    selectedAbsen.push(baseAbsen.absenId);

                    let hourInConbo = correctionForm.getCombo("start_hour");
                    let hourInIndex = hours.findIndex(e => e.value == baseAbsen.hourIn);
                    hourInConbo.selectOption(0);

                    let minuteInCombo = correctionForm.getCombo("start_minute");
                    let minuteInIndex = minutes.findIndex(e => e.value == baseAbsen.minuteIn);
                    minuteInCombo.selectOption(0);

                    let hourOutCombo = correctionForm.getCombo("end_hour");
                    let hourOutIndex = hours.findIndex(e => e.value == baseAbsen.hourOut);
                    hourOutCombo.selectOption(0);

                    let minuteOuteCombo = correctionForm.getCombo("end_minute");
                    let minuteOutIndex = minutes.findIndex(e => e.value == baseAbsen.minuteOut);
                    minuteOuteCombo.selectOption(0);

                    for (let i = 0; i < winGrid.getRowsNum(); i++) {
                        let id = winGrid.getRowId(i);
                        if(
                            (
                                winGrid.cells(id, 9).getValue() == 'AVAILABLE' || 
                                winGrid.cells(id, 9).getValue() == 'REJECTED' || 
                                winGrid.cells(id, 9).getValue() == 'CANCELED'
                            ) &&
                            winGrid.cells(id, 10).getValue() == baseAbsen.labelIn &&
                            winGrid.cells(id, 13).getValue() == baseAbsen.labelOut &&
                            winGrid.cells(id, 16).getValue() == baseAbsen.hourSchIn &&
                            winGrid.cells(id, 17).getValue() == baseAbsen.minuteSchIn &&
                            winGrid.cells(id, 18).getValue() == baseAbsen.hourSchOut &&
                            winGrid.cells(id, 19).getValue() == baseAbsen.minuteSchOut
                        ) {
                            if(id != baseAbsen.absenId) {
                                winGrid.cells(id, 1).setValue(0);
                            }
                        }
                    }
                }
            });

            var correctionForm = winLayout.cells("b").attachForm([
                {type: "fieldset", offsetLeft: 30, offsetTop: 30, label: "Jam Absen", list:[	
                    {type: "block", list: [
                        {type: "hidden", name: "label_in", label: "Label In", labelWidth: 130, inputWidth: 450, required: true},
                        {type: "hidden", name: "label_out", label: "Label Out", labelWidth: 130, inputWidth: 450, required: true},
                        {type: "hidden", name: "filename", label: "Filename", readonly: true},
                        {type: "combo", name: "shift_id", label: "Pilih Shift Kerja", readonly: true, labelWidth: 130, inputWidth: 450, required: true},
                        {type: "hidden", name: "absen_ids", label: "Absen DIpilih", readonly: true},
                        {type: "input", name: "reason", label: "Alasan", labelWidth: 130, inputWidth: 450, rows: 5, required: true},
                        {type: "upload", name: "file_uploader", inputWidth: 420,
                            url: AppMaster("fileUpload2", {save: false, folder: "hr_correction_upload"}), 
                            swfPath: "./public/codebase/ext/uploader.swf", 
                            swfUrl: AppMaster("fileUpload2")
                        },
                    ]},
                    {type: "block", list: [
                        {type: "combo", name: "start_hour", label: "<span id='label_in'>Pilih Absen</span>", labelWidth: 220, inputWidth: 80, required: true, readonly: true,
                            validate: "NotEmpty", 
                            options: hours
                        },
                        {type: "combo", name: "end_hour", label: "<span id='label_out'>Pilih Absen</span>", labelWidth: 220, inputWidth: 80, required: true, readonly: true,
                            validate: "NotEmpty", 
                            options: hours
                        },
                        {type: "newcolumn"},
                        {type: "combo", name: "start_minute", label: ":", labelWidth: 10, inputWidth: 80, required: true, readonly: true,
                            validate: "NotEmpty", 
                            options: minutes
                        },
                        {type: "combo", name: "end_minute", label: ":", labelWidth: 10, inputWidth: 80, required: true, readonly: true,
                            validate: "NotEmpty", 
                            options: minutes
                        },
                    ]},
                ]},
                {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                    {type: "button", name: "save", className: "button_update", offsetLeft: 15, value: "Simpan"}
                ]},
            ]);

            var shiftCombo = correctionForm.getCombo("shift_id");
            shiftCombo.load(comboUrl.shift_id.url);

            var times = createTime();
            var workTime = genWorkTime(times.times, 0, 48, true);
            var startHour = correctionForm.getCombo("start_hour");
            var endHour = correctionForm.getCombo("end_hour");
            var startMinute = correctionForm.getCombo("start_minute");
            var endMinute = correctionForm.getCombo("end_minute");

            correctionForm.attachEvent("onChange", function(name, value) {
                if(name === "start_hour" || name === "end_hour" || name === "start_minute" || name === "end_minute") {
                    dateChangeDetail(startHour, startMinute, endHour, endMinute);
                }
            });

            function dateChangeDetail(startHour, startMinute, endHour, endMinute) {
                if(selectedAbsen > 0) {
                    let newStartMinute = "";
                    if(startMinute.getSelectedValue() > 30) {
                        newStartMinute = "30";
                    } else {
                        newStartMinute = "00";
                    }
                    
                    let newEndMinute = "";
                    if(endMinute.getSelectedValue() > 30) {
                        newEndMinute = "30";
                    } else {
                        newEndMinute = "00";
                    }
                    
                    let startTime = startHour.getSelectedValue() + ":" + newStartMinute;
                    let endTime = endHour.getSelectedValue() + ":" + newEndMinute;
                    
                    
                    let startIndex = workTime.filterStart.indexOf(startTime);
                    let endIndex = workTime.filterStart.indexOf(endTime);

                    if(baseAbsen.labelIn != baseAbsen.labelOut) {
                        if(endIndex > startIndex) {
                            $("#label_out").html(baseAbsen.labelIn);
                            correctionForm.setItemValue('label_out', baseAbsen.labelIn);
                        } else {
                            $("#label_out").html(baseAbsen.labelOut);
                            correctionForm.setItemValue("label_out", aseAbsen.labelOut);
                        }
                    } else if(baseAbsen.labelIn == baseAbsen.labelOut) {
                        if(endIndex < startIndex) {
                            const newLabelOut = addDayAbsen(baseAbsen.labelOut);
                            $("#label_out").html(newLabelOut);
                            correctionForm.setItemValue("label_out", newLabelOut);
                        } else {
                            $("#label_out").html(baseAbsen.labelIn);
                            correctionForm.setItemValue("label_out", baseAbsen.labelIn);
                        }
                    }
                }
            }

            correctionForm.attachEvent("onBeforeFileAdd", async function (filename, size) {
                beforeFileAdd(correctionForm, {filename, size});
            });

            correctionForm.attachEvent("onBeforeFileUpload", function(mode, loader, formData){
                if(fileError) {
                    clearUploader(correctionForm, "file_uploader");
                    eAlert("File error silahkan upload file sesuai ketentuan!");
                    fileError = false;
                } else {
                    return true;
                }
            });

            correctionForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "save":
                        const uploader = correctionForm.getUploader("file_uploader");
                        if(uploader.getStatus() === -1) {
                            if(!fileError) {
                                uploader.upload();
                            } else {
                                uploader.clear();
                                eAlert("File error silahkan upload file sesuai ketentuan!");
                                fileError = false;
                            }
                        } else {
                            addCorrectionSubmit();
                        }
                        break;
                }
            });

            correctionForm.attachEvent("onUploadFile", function(filename, servername){
                correctionForm.setItemValue("filename", servername);
                addCorrectionSubmit();
            });

            function addCorrectionSubmit() {
                if(selectedAbsen.length == 0) return eAlert("Belum ada absen dipilih!");
                correctionForm.setItemValue('absen_ids', selectedAbsen.join(","));
                if (!correctionForm.validate()) return eAlert("Input error!");

                setDisable(["save"], correctionForm, winLayout.cells("b"));

                let correctionFormDP = new dataProcessor(Attendance("correctionForm"));
                correctionFormDP.init(correctionForm);
                correctionForm.save();

                correctionFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                    let message = tag.getAttribute("message");
                    switch (action) {
                        case "inserted":
                            sAlert("Berhasil Menambahkan Record <br>" + message);
                            clearAllForm(correctionForm);
                            clearUploader(correctionForm, "file_uploader");
                            setEnable(["save"], correctionForm, winLayout.cells("b"));
                            rCorrectionGrid();
                            closeWindow("absen_window");
                            break;
                        case "error":
                            eAlert("Gagal Menambahkan Record <br>" + message);
                            setEnable(["save"], correctionForm, winLayout.cells("b"));
                            break;
                    }
                });
            }

            function afterWinGrid() {
                for(let i=0;i < winGrid.getRowsNum();i++) {
                    let id = winGrid.getRowId(i);
                    if(
                        winGrid.cells(id, 9).getValue() != 'AVAILABLE' &&
                        winGrid.cells(id, 9).getValue() != 'REJECTED' &&
                        winGrid.cells(id, 9).getValue() != 'CANCELED'
                    ) {
                        winGrid.cells(id, 1).setDisabled(true);
                    } else {
                        winGrid.cells(id, 1).setDisabled(false); 
                    }
                }
            }
        }

        init();

        async function beforeFileAdd(form, file, id = null) {
            if(form.validate()) {
                var ext = file.filename.split(".").pop();
                if (ext == "pdf") {
                    if (file.size > 1000000) {
                        fileError = true;
                        eAlert("Tidak boleh melebihi 1 MB!");
                    } else {
                        if(totalFile > 0) {
                            eAlert("Maksimal 1 file");
                            fileError = true;
                        }
                    }		    
                } else {
                    eAlert("Hanya pdf saja yang bisa diupload!");
                    fileError = true;
                }
            } else {
                eAlert("Input error!");
            }	
        }
    }

JS;

header('Content-Type: application/javascript');
echo $script;