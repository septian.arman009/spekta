<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function hrOutoffice(){
        var legend = legendGrid();
        var hrOutLayout = mainTab.cells("hr_outoffice").attachLayout({
            pattern: "1C",
            cells: [
                {id: "a", header : false}
            ]
        });

        var hrOutMenu = hrOutLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
                {id: "month", text: genSelectMonth("hr_outoffice_year", "hr_outoffice_month")},
                {id: "filter", text: "<div style='width:100%'>Status: <select style='height:22px;margin-top:3px' id='filter_outoffice_status'><option>ALL</option><option>CREATED</option><option>APPROVED</option><option>REJECTED</option><option>CANCELED</option></select></div>"},
            ]
        });

        hrOutMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "export":
                    outGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
                case "refresh":
                    rOutGrid();
                    break;
            }
        });

        $("#hr_outoffice_year").on("change", function() {
            rOutGrid();
        });

        $("#hr_outoffice_month").on("change", function() {
            rOutGrid();
        });

        $("#filter_outoffice_status").on("change", function() {
            rOutGrid();
        });

        var hrOutStatus = hrOutLayout.cells("a").attachStatusBar();
        function outGridCount() {
            hrOutStatus.setText("Total baris: " + outGrid.getRowsNum() + " (" + legend.hr_outoffice + ")");
        }

        var outGrid =  hrOutLayout.cells("a").attachGrid();
        outGrid.setHeader("No,Nama,Tanggal Izin Keluar,Waktu Keluar,Waktu Masuk,Scan Keluar,Jakar Scan Keluar,Scan Masuk,Jarak Scan Masuk,Sub Unit,Bagian,Sub Bagian,Approver #1,Approver #1 Status,Approver #2,Approver #2 Status,Status,Tanggal Approval,Keterlambatan,Potongan");
        outGrid.attachHeader("#rspan,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter")
        outGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        outGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        outGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
        outGrid.setInitWidthsP("5,10,15,15,15,15,7,15,7,15,15,15,15,7,15,7,7,15,7,7");
        outGrid.enableSmartRendering(true);
        outGrid.enableMultiselect(false);
        outGrid.attachEvent("onXLE", function() {
            hrOutLayout.cells("a").progressOff();
        });
        outGrid.init();

        function rOutGrid() {
            const param = {
                year: $("#hr_outoffice_year").val(),
                month: $("#hr_outoffice_month").val()
            }
            const status = $("#filter_outoffice_status").val();
            if(status != 'ALL') param.equal_apv_status = status;

            hrOutLayout.cells("a").progressOn();
            outGrid.clearAndLoad(Attendance("getOutoffice", param), outGridCount);
        }

        rOutGrid();
    }

JS;

header('Content-Type: application/javascript');
echo $script;