<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function showApprovalAbsenCorrection() {
        var legend = legendGrid();

        var selectedCorrection = [];
        var countNonApproved = 0;

        var hrCorrectionLayout = mainTab.cells("hr_attendance_correction_approval").attachLayout({
            pattern: "1C",
            cells: [
                {id: "a", header : false}
            ]
        });

        var hrCorrectionMenu = hrCorrectionLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "month", text: genSelectMonth("hr_attendance_correction_approval_year", "hr_attendance_correction_approval_month")},
                {id: "filter", text: "<div style='width:100%'>Status: <select style='height:22px;margin-top:3px' id='filter_correction_approval_status'><option>ALL</option><option value='1'>ON PROCESS</option><option value='2'>APPROVED</option><option value='3'>COMPLETED</option><option value='4'>REJECTED</option><option value='5'>CANCELED</option></select></div>"},
                {id: "approve", text: "Approve", img: "check.png"},
                {id: "reject", text: "Reject", img: "cancel.png"},
                {id: "read", text: "Lihat File Pendukung", img: "read_16.png"},
            ]
        });

        $("#filter_correction_approval_status").on("change", function() {
            rCorrectionGrid();
        });

        hrCorrectionMenu.attachEvent("onClick", function(id) {
            let month = $("#hr_attendance_correction_approval_month").val();
            let year = $("#hr_attendance_correction_approval_year").val();
            switch (id) {
                case "refresh":
                    rCorrectionGrid();
                    break;
                case "approve":
                    if(selectedCorrection.length == 0) return eAlert("Belum ada koreksi yang dipilih");
                    reqConfirm3(Attendance("approveCorrection"), { absenIds: selectedCorrection, month, year }, (err, res) => {
                        if(err) {
                            eAlert(err.message);
                        } else {
                            if (res.status == 'success') {
                                countNonApproved = 0;
                                selectedCorrection = [];
                                rCorrectionGrid();
                                sAlert(res.message);
                            } else {
                                eAlert(res.message);
                            }
                        }
                    });
                    break;
                case "reject":
                    rejectHandler();
                    break;
                case "read":
                    if(correctionGrid.getSelectedRowId()) {
                        let id = correctionGrid.getSelectedRowId();
                        reqJson(Document("encryptFile"), "POST", { filename: correctionGrid.cells(id, 18).getValue() }, (err, res) => {
                            window.open(BASE_URL + "index.php?c=Pc&m=correction_pdfreader&mode=read&token=" + res.filename, "_blank");
                        });
                    } else {
                        eAlert("Belum ada file yang dipilih!");
                    }
                    break;
            }
        });

        function rejectHandler() {
            if(selectedCorrection.length == 0) return eAlert("Belum ada koreksi yang dipilih");

            var absenRejectWindow = createWindow("absen_reject_window", "Reject Koreksi Absen", 600, 350);
            myWins.window("absen_reject_window").skipMyCloseEvent = true;

            var rejectForm = absenRejectWindow.attachForm([
                {type: "fieldset", offsetLeft: 30, offsetTop: 30, label: "Jam Absen", list:[	
                    {type: "block", list: [
                        {type: "hidden", name: "absen_ids", label: "Absen DIpilih", readonly: true, required: true},
                        {type: "hidden", name: "month", label: "Bulan", labelWidth: 130, inputWidth: 250, required: true},
                        {type: "hidden", name: "year", label: "Tahun", labelWidth: 130, inputWidth: 250, required: true},
                        {type: "input", name: "reject_reason", label: "Alasan Reject", labelWidth: 130, inputWidth: 250, required: true, rows: 5},
                    ]},
                ]},
                {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                    {type: "button", name: "update", className: "button_update", offsetLeft: 15, value: "Simpan"},
                    {type: "newcolumn"},
                    {type: "button", name: "cancel", className: "button_no", offsetLeft: 30, value: "Cancel"}
                ]},
            ]);

            let month = $("#hr_attendance_correction_approval_month").val();
            let year = $("#hr_attendance_correction_approval_year").val();

            rejectForm.setItemValue('absen_ids', selectedCorrection.join(","));
            rejectForm.setItemValue('month', month);
            rejectForm.setItemValue('year', year);

            rejectForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "update":
                        if (!rejectForm.validate())  return eAlert("Input error!");

                        setDisable(["update", "cancel"], rejectForm, absenRejectWindow);
                        let rejectFormDP = new dataProcessor(Attendance("rejectAbsenForm"));
                        rejectFormDP.init(rejectForm);
                        rejectForm.save();
                        
                        rejectFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                            let message = tag.getAttribute("message");
                            switch (action) {
                                case "updated":
                                    countNonApproved = 0;
                                    selectedCorrection = [];
                                    sAlert("Berhasil Mengubah Record <br>" + message);
                                    closeWindow('absen_reject_window');
                                    rCorrectionGrid();
                                    break;
                            }
                        });
                        break;
                    case "cancel":
                        closeWindow('absen_reject_window');
                        break;
                }
            });
        }

        $("#hr_attendance_correction_approval_year").on("change", function() {
            rCorrectionGrid();
        });

        $("#hr_attendance_correction_approval_month").on("change", function() {
            rCorrectionGrid();
        });

        var hrCorrectionStatus = hrCorrectionLayout.cells("a").attachStatusBar();
        function correctionGridCount() {
            hrCorrectionStatus.setText("Total baris: " + correctionGrid.getRowsNum() + " ("+ legend.absen_correction + ")");

            for(let i=0;i < correctionGrid.getRowsNum();i++) {
                let id = correctionGrid.getRowId(i);
                if(correctionGrid.cells(id, 17).getValue() == 'ON PROCESS' || correctionGrid.cells(id, 17).getValue() == 'APPROVED') {
                    correctionGrid.cells(id, 1).setDisabled(false);
                } else {
                    correctionGrid.cells(id, 1).setDisabled(true);
                }
            }
        }

        var correctionGrid = hrCorrectionLayout.cells("a").attachGrid();
        correctionGrid.setImagePath("./public/codebase/imgs/");
        correctionGrid.setHeader("No,<input type='checkbox' id='correction_approval_checkbox' />,Nama Karyawan,Bagian,Sub Bagian,Shift Kerja,Tanggal,Jam Masuk,Jam Keluar,Absen Masuk,Absen Keluar,Koreksi Shift,Koreksi Masuk,Koreksi Keluar,Alasan,Alasan Reject,Tanggal Pengajuan,Status,Filename");
        correctionGrid.attachHeader("#rspan,#rspan,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter")
        correctionGrid.setColSorting("int,na,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        correctionGrid.setColTypes("rotxt,ch,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        correctionGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
        correctionGrid.setInitWidthsP("5,5,15,20,20,10,15,15,15,15,15,15,15,15,25,25,15,7,0");
        correctionGrid.enableSmartRendering(true);
        correctionGrid.enableMultiselect(false);
        correctionGrid.attachEvent("onXLE", function() {
            hrCorrectionLayout.cells("a").progressOff();
        });
        correctionGrid.attachEvent("onCheckbox", function(rId,cInd,state){
            if(state) {
                selectedCorrection.push(rId);
                if(correctionGrid.cells(rId, 17).getValue() != 'APPROVED') {
                    countNonApproved++;
                    hrCorrectionMenu.setItemDisabled('approve');
                }
            } else {
                const index = selectedCorrection.indexOf(rId);
                if(index > -1) {
                    selectedCorrection.splice(index, 1);
                }

                if(correctionGrid.cells(rId, 17).getValue() != 'APPROVED') {
                    countNonApproved--;
                    if(countNonApproved == 0) {
                        hrCorrectionMenu.setItemEnabled('approve');
                    }
                }
            }
        });
        correctionGrid.init();

        $("#correction_approval_checkbox").on("change", function() {
            let checked = document.getElementById('correction_approval_checkbox').checked;
            if(checked) {
                for(let i=0;i < correctionGrid.getRowsNum();i++) {
                    let id = correctionGrid.getRowId(i);
                    if(correctionGrid.cells(id, 17).getValue() == 'ON PROCESS' || correctionGrid.cells(id, 17).getValue() == 'APPROVED') {
                        selectedCorrection.push(id);
                        correctionGrid.cells(id, 1).setValue(1);
                        if(correctionGrid.cells(id, 17).getValue() != 'APPROVED') {
                            countNonApproved++;
                            hrCorrectionMenu.setItemDisabled('approve');
                        }
                    }
                }
            } else {
                selectedCorrection = [];
                for(let i=0;i < correctionGrid.getRowsNum();i++) {
                    let id = correctionGrid.getRowId(i);
                    if(correctionGrid.cells(id, 17).getValue() == 'ON PROCESS' || correctionGrid.cells(id, 17).getValue() == 'APPROVED') {
                        correctionGrid.cells(id, 1).setValue(0);
                        if(correctionGrid.cells(id, 17).getValue() != 'APPROVED') {
                            countNonApproved--;
                            if(countNonApproved == 0) {
                                hrCorrectionMenu.setItemEnabled('approve');
                            }
                        }
                    }
                }
            }
        });

        function rCorrectionGrid() {
            let status = $("#filter_correction_approval_status").val();
            let month = $("#hr_attendance_correction_approval_month").val();
            let year = $("#hr_attendance_correction_approval_year").val();

            let params = {
                month, year
            }

            if(status != "ALL") {
                params.equal_a_correction_status = status;
            }

            hrCorrectionLayout.cells("a").progressOn();
            correctionGrid.clearAndLoad(MAbsen("getAbsenCorrections", params), correctionGridCount);
        }

        function init() {
            rCorrectionGrid();
        }

        init();
    }

JS;

header('Content-Type: application/javascript');
echo $script;