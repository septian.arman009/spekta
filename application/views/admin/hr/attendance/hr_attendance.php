<?php
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function absensi() {	
        var legend = legendGrid();
        let currentDate = getCurrentDate(new Date());

        var attendaceTabs = mainTab.cells("hr_attendance_list").attachTabbar({
            tabs: [
                {id: "a", text: "Absen Karyawan", active: true},
                {id: "b", text: "Absen Summary"},
                {id: "c", text: "Absen Personil <span id='selected_emp_name'>(Silahkan Pilih Karyawan Dari Table Absen Summary)</span>"},
            ]
        });

        var hrAbsenListMenu = mainTab.cells("hr_attendance_list").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "search", text: "<div style='width:100%'>Search: <input type='text' id='hr_attendance_start' readonly value='"+currentDate+"' /> - <input type='text' id='hr_attendance_end' readonly value='"+currentDate+"' /> <button id='hr_attendance_filter'>Proses</button> | Status: <select id='hr_attendance_status'><option>ALL</option><option>MASUK</option><option>TIDAK MASUK</option></select> | Status Karyawan: <select id='hr_emp_status'><option>ALL</option><option>ACTIVE</option><option>INACTIVE</option></select></div>"},
            ]
        });

        var hrAbsenLayout = attendaceTabs.cells("a").attachLayout({
            pattern: "1C",
            cells: [
                {id: "a", header : false}
            ]
        });

        var hrAbsenMenu = hrAbsenLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
                {id: "export_advance", text: "Export To Excel (Advance)", img: "excel.png"},
                {id: "export_pdf", text: "Export To PDF", img: "export_pdf.png"},
            ]
        });

        var filterCalendar = new dhtmlXCalendarObject(["hr_attendance_start","hr_attendance_end"]);

        hrAbsenMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "export":
                    absenGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
                case "export_advance":
                    let start = $("#hr_attendance_start").val();
                    let end = $("#hr_attendance_end").val();
                    let status = $("#hr_attendance_status").val();
                    let empStatus = $("#hr_emp_status").val();

                    window.open(
                        Export("absenExport", {start, end, status, empStatus}),
                        "_blank"
                    );
                    break;
                case "refresh":
                    rAbsenGrid();
                    break;
                case "export_pdf":
                    exportPdf();
                    break;
            }
        });

        function exportPdf() {
            let expPdfWindow = createWindow("export_pdf_absen", "Export PDF", 500, 300);
            myWins.window("export_pdf_absen").skipMyCloseEvent = true;

            var expPdfForm = expPdfWindow.attachForm([
                {type: "fieldset", offsetLeft: 30, offsetTop: 30, label: "<span id='data_lembur'>Data Bagian</span>", list:[	
                    {type: "combo", name: "department_id", label: "Sub Unit", readonly: true, required: true, labelWidth: 130, inputWidth: 250},
                    {type: "combo", name: "sub_department_id", label: "Bagian", readonly: true, required: true, labelWidth: 130, inputWidth: 250},
                    {type: "combo", name: "division_id", label: "Sub Bagian", readonly: true, required: true, labelWidth: 130, inputWidth: 250},
                ]},
                {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                    {type: "button", name: "export", offsetLeft: 15, value: 'Export'},
                ]},
            ]);

            var addDeptCombo = expPdfForm.getCombo("department_id");
            var addSubDeptCombo = expPdfForm.getCombo("sub_department_id");
            var addDivCombo = expPdfForm.getCombo("division_id");

            addDeptCombo.load(Emp("getDepartment"));
            addDeptCombo.attachEvent("onChange", function(value, text){
                clearComboReload(expPdfForm, "sub_department_id", Emp("getSubDepartment", {deptId: value}));
            });
            addSubDeptCombo.attachEvent("onChange", function(value, text){
                clearComboReload(expPdfForm, "division_id", Emp("getDivision", {subDeptId: value}));
            });

            expPdfForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "export":
                        let deptId = addDeptCombo.getSelectedValue();
                        let subDeptId = addSubDeptCombo.getSelectedValue();
                        let divId = addDivCombo.getSelectedValue();
                        let start = $("#hr_attendance_start").val();
                        let end = $("#hr_attendance_end").val();

                        const data = {start, end};

                        if(deptId > 0) data.deptId = deptId;
                        if(subDeptId > 0) data.subDeptId = subDeptId;
                        if(divId > 0) data.divId = divId;

                        window.open(
                            MAbsen("printAbsenPersonal", data),
                            "_blank"
                        );
                        break;
                }
            });
        }

        $("#hr_attendance_filter").on("click", function() {
            if(checkFilterDate($("#hr_attendance_start").val(), $("#hr_attendance_end").val())) {
                rAbsenGrid();
            } 
        });

        $("#hr_attendance_status").on("change", function() {
            if(checkFilterDate($("#hr_attendance_start").val(), $("#hr_attendance_end").val())) {
                rAbsenGrid();
            }
        });

        $("#hr_emp_status").on("change", function() {
            if(checkFilterDate($("#hr_attendance_start").val(), $("#hr_attendance_end").val())) {
                rAbsenGrid();
            }
        });

        var hrAbsenStatus = hrAbsenLayout.cells("a").attachStatusBar();

        function absenGridCount() {
            hrAbsenStatus.setText("Total baris: " + absenGrid.getRowsNum() + " (" + legend.freeday_and_absen + ")");

            let shift1 = 0;
            let countShift1 = 0;
            let shift2 = 0;
            let countShift2 = 0;
            let shift3 = 0;
            let countShift3 = 0;

            let ovtShift1 = 0;
            let ovtCountShift1 = 0;
            let ovtShift2 = 0;
            let ovtCountShift2 = 0;
            let ovtShift3 = 0;
            let ovtCountShift3 = 0;

            if(absenGrid.getRowsNum() > 0) {
                for (let i = 0; i < absenGrid.getRowsNum(); i++) {
                    let id = absenGrid.getRowId(i);
                    let shift = absenGrid.cells(id, 8).getValue();
                    let shiftIn = absenGrid.cells(id, 12).getValue();

                    if(shift == 'Shift 1') {
                        shift1++;
                        if(shiftIn != '-') countShift1++;
                    } else if(shift == 'Shift 2') {
                        shift2++;
                        if(shiftIn != '-') countShift2++;
                    } else if(shift == 'Shift 3') {
                        shift3++;
                        if(shiftIn != '-') countShift3++;
                    } else if(shift == 'Lembur Shift 1') {
                        ovtShift1++;
                        if(shiftIn != '-') ovtCountShift1++;
                    } else if(shift == 'Lembur Shift 2') {
                        ovtShift2++;
                        if(shiftIn != '-') ovtCountShift2++;
                    } else if(shift == 'Lembur Shift 3') {
                        ovtShift3++;
                        if(shiftIn != '-') ovtCountShift3++;
                    }

                    $('#shift_1').html(countShift1 + ' / ' + shift1);
                    $('#shift_2').html(countShift2 + ' / ' + shift2);
                    $('#shift_3').html(countShift3 + ' / ' + shift3);
                    $('#ovt_shift_1').html(ovtCountShift1 + ' / ' + ovtShift1);
                    $('#ovt_shift_2').html(ovtCountShift2 + ' / ' + ovtShift2);
                    $('#ovt_shift_3').html(ovtCountShift3 + ' / ' + ovtShift3);
                }
            } else {
                $('#shift_1').html(countShift1 + ' / ' + shift1);
                $('#shift_2').html(countShift2 + ' / ' + shift2);
                $('#shift_3').html(countShift3 + ' / ' + shift3);
                $('#ovt_shift_1').html(ovtCountShift1 + ' / ' + ovtShift1);
                $('#ovt_shift_2').html(ovtCountShift2 + ' / ' + ovtShift2);
                $('#ovt_shift_3').html(ovtCountShift3 + ' / ' + ovtShift3);
            }
        }

        var absenGrid =  hrAbsenLayout.cells("a").attachGrid();
        absenGrid.setHeader("No,NPP,Sap ID,Nama Karyawan,Sub Unit,Bagian,Sub Bagian,Status,Shift Kerja,Tanggal,Jam Masuk,Jam Keluar,Absen Masuk,Absen Keluar,Keterlambatan,Pulang Cepat,Jam Produktif,Jam Tidak Produktif,Lokasi Absen,Jarak Dari Gate,QR Masuk,QR Keluar");
        absenGrid.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter")
        absenGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        absenGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        absenGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
        absenGrid.setInitWidthsP("5,10,10,20,15,20,20,10,10,10,15,15,15,15,10,10,10,10,10,15,20,20");
        absenGrid.enableSmartRendering(true);
        absenGrid.enableMultiselect(false);
        absenGrid.attachFooter(",Keterangan,Total Masuk / Total Shift");
        absenGrid.attachFooter(",Shift 1,<div id='shift_1'>0 / 0</div>");
        absenGrid.attachFooter(",Shift 2,<div id='shift_2'>0 / 0</div>");
        absenGrid.attachFooter(",Shift 3,<div id='shift_3'>0 / 0</div>");
        absenGrid.attachFooter(",Lembur Shift 1,<div id='ovt_shift_1'>0 / 0</div>");
        absenGrid.attachFooter(",Lembur Shift 2,<div id='ovt_shift_2'>0 / 0</div>");
        absenGrid.attachFooter(",Lembur Shift 3,<div id='ovt_shift_3'>0 / 0</div>");
        absenGrid.attachEvent("onXLE", function() {
            hrAbsenLayout.cells("a").progressOff();
        });
        absenGrid.init();

        function rAbsenGrid() {
            let start = $("#hr_attendance_start").val();
            let end = $("#hr_attendance_end").val();
            let status = $("#hr_attendance_status").val();
            let empStatus = $("#hr_emp_status").val();

            hrAbsenLayout.cells("a").progressOn();
            absenGrid.clearAndLoad(MAbsen("getAbsenList", {start, end, status, empStatus}), absenGridCount);
            rAbsenPersonilGrid(0);
            $("#selected_emp_name").html("(Silahkan Pilih Karyawan Dari Table Absen Summary)");
            rAbsenEmpGrid();
        }

        var hrAbsenEmpLayout = attendaceTabs.cells("b").attachLayout({
            pattern: "1C",
            cells: [
                {id: "a", header : false}
            ]
        });

        var hrAbsenEmpMenu = hrAbsenEmpLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
            ]
        });

        hrAbsenEmpMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "export":
                    absenEmpGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
                case "refresh":
                    rAbsenEmpGrid();
                    break;
            }
        });

        var hrAbsenEmpStatus = hrAbsenEmpLayout.cells("a").attachStatusBar();

        function absenEmpGridCount() {
            hrAbsenEmpStatus.setText("Total baris: " + absenEmpGrid.getRowsNum());

            let totalIn = 0;
            let totalNotIn = 0;
            let totalLeaveSick = 0;
            let totalLeave = 0;
            let totalVisit = 0;
            let totalProd = 0;
            let totalUnProd = 0;
            let totalLate = 0;
            let totalEarly = 0;

            if(absenEmpGrid.getRowsNum() > 0) {
                for (let i = 0; i < absenEmpGrid.getRowsNum(); i++) {
                    let id = absenEmpGrid.getRowId(i);
                    let absIn = absenEmpGrid.cells(id, 7).getValue();
                    let absNotIn = absenEmpGrid.cells(id, 8).getValue();
                    let prod = absenEmpGrid.cells(id, 9).getValue();
                    let unprod = absenEmpGrid.cells(id, 11).getValue();
                    let later = absenEmpGrid.cells(id, 13).getValue();
                    let early = absenEmpGrid.cells(id, 15).getValue();

                    
                    let leave = absenEmpGrid.cells(id, 17).getValue();
                    let visit = absenEmpGrid.cells(id, 18).getValue();
                    let sick = absenEmpGrid.cells(id, 23).getValue();


                    totalIn += parseFloat(absIn);
                    totalNotIn += parseFloat(absNotIn);
                    totalLeave += parseFloat(leave - sick);
                    totalLeaveSick += parseFloat(sick);
                    totalVisit += parseFloat(visit);
                    totalProd += parseFloat(prod.replace(",", "."));
                    totalUnProd += parseFloat(unprod.replace(",", "."));
                    totalLate += parseFloat(later.replace(",", "."));
                    totalEarly += parseFloat(early.replace(",", "."));

                    if(i == 0) {
                        let totalTextProd = absenEmpGrid.cells(id, 19).getValue();
                        let totalTextUnProd = absenEmpGrid.cells(id, 20).getValue();
                        let totalTextLate = absenEmpGrid.cells(id, 21).getValue();
                        let totalTextEarly = absenEmpGrid.cells(id, 22).getValue();



                        $('#abs_emp_prod_text').html(totalTextProd);
                        $('#abs_emp_unprod_text').html(totalTextUnProd);
                        $('#asb_emp_late_text').html(totalTextLate);
                        $('#abs_emp_early_text').html(totalTextEarly);
                    }
                }

                $('#abs_emp_in').html(totalIn);
                $('#abs_emp_notin').html(totalNotIn);
                $('#abs_emp_leave_sick').html(totalLeaveSick);
                $('#abs_emp_leave_other').html(totalLeave);
                $('#abs_emp_visit').html(totalVisit);
                $('#abs_emp_prod').html(totalProd.toFixed(2));
                $('#abs_emp_unprod').html(totalUnProd.toFixed(2));
                $('#asb_emp_late').html(totalLate.toFixed(2));
                $('#abs_emp_early').html(totalEarly.toFixed(2));
            } else {
                $('#abs_emp_in').html(totalIn);
                $('#abs_emp_notin').html(totalNotIn);
                $('#abs_emp_leave_sick').html(totalLeaveSick);
                $('#abs_emp_leave_other').html(totalLeave);
                $('#abs_emp_visit').html(totalVisit);
                $('#abs_emp_prod').html(totalProd);
                $('#abs_emp_unprod').html(totalUnProd);
                $('#asb_emp_late').html(totalLate);
                $('#abs_emp_early').html(totalEarly);

                $('#abs_emp_prod_text').html("-");
                $('#abs_emp_unprod_text').html("-");
                $('#asb_emp_late_text').html("-");
                $('#abs_emp_early_text').html("-");
            }
        }

        var absenEmpGrid = hrAbsenEmpLayout.cells("a").attachGrid();
        absenEmpGrid.setHeader("No,NPP,Sap ID,Nama Karyawan,Sub Unit,Bagian,Sub Bagian,Absen Masuk,Tidak Masuk,Prod,Jam Produktif,UnProd,Jam Tidak Produktif,Late,Terlambat,Early,Pulang Cepat,Cuti,Absen Visit,TotalProd,TotalUnProd,TotalLate,TotalEarly,LeaveSick");
        absenEmpGrid.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter")
        absenEmpGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        absenEmpGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        absenEmpGrid.setColAlign("center,left,left,left,left,left,left,center,center,center,left,center,left,center,left,center,left,left,left,left,left,left,left,left");
        absenEmpGrid.setInitWidthsP("5,10,10,20,15,20,20,5,5,0,10,0,10,0,10,0,10,5,5,0,0,0,0,0");
        absenEmpGrid.enableSmartRendering(true);
        absenEmpGrid.enableMultiselect(false);
        absenEmpGrid.attachFooter(",Keterangan,Total,Keterangan");
        absenEmpGrid.attachFooter(",Absen Masuk,<div id='abs_emp_in'>0</div>,<div id=''>-</div>");
        absenEmpGrid.attachFooter(",Tidak Masuk,<div id='abs_emp_notin'>0</div>,<div id=''>-</div>");
        absenEmpGrid.attachFooter(",Cuti Sakit,<div id='abs_emp_leave_sick'>0</div>,<div id=''>-</div>");
        absenEmpGrid.attachFooter(",Cuti Lainnya,<div id='abs_emp_leave_other'>0</div>,<div id=''>-</div>");
        absenEmpGrid.attachFooter(",Absen Visit,<div id='abs_emp_visit'>0</div>,<div id=''>-</div>");
        absenEmpGrid.attachFooter(",Jam Produktif,<div id='abs_emp_prod'>0</div>,<div id='abs_emp_prod_text'>0</div>");
        absenEmpGrid.attachFooter(",Jam Tidak Produktif,<div id='abs_emp_unprod'>0</div>,<div id='abs_emp_unprod_text'>0</div>");
        absenEmpGrid.attachFooter(",Terlambat,<div id='asb_emp_late'>0</div>,<div id='asb_emp_late_text'>0</div>");
        absenEmpGrid.attachFooter(",Pulang Cepat,<div id='abs_emp_early'>0</div>,<div id='abs_emp_early_text'>0</div>");
        absenEmpGrid.attachEvent("onXLE", function() {
            hrAbsenEmpLayout.cells("a").progressOff();
        });
        absenEmpGrid.attachEvent("onRowSelect", function(rId, cIdn){
            $("#selected_emp_name").html("("+absenEmpGrid.cells(rId, 1).getValue()+")");
            rAbsenPersonilGrid(rId);
        });
        absenEmpGrid.init();

        function rAbsenEmpGrid() {
            let start = $("#hr_attendance_start").val();
            let end = $("#hr_attendance_end").val();
            let status = $("#hr_attendance_status").val();

            hrAbsenEmpLayout.cells("a").progressOn();
            absenEmpGrid.clearAndLoad(MAbsen("getAbsenEmp", {start, end, status}), absenEmpGridCount);
        }

        var hrAbsenPersonilLayout = attendaceTabs.cells("c").attachLayout({
            pattern: "1C",
            cells: [
                {id: "a", header : false}
            ]
        });

        var hrAbsenPersonilMenu = hrAbsenPersonilLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
            ]
        });

        hrAbsenPersonilMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "export":
                    absenPersonilGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
                case "refresh":
                    rAbsenPersonilGrid();
                    break;
            }
        });

        var hrAbsenPersonilStatus = hrAbsenPersonilLayout.cells("a").attachStatusBar();
        function absenPersonilGridCount() {
            hrAbsenPersonilStatus.setText("Total baris: " + absenPersonilGrid.getRowsNum() + " (" + legend.freeday_and_absen + ")");

            let totalIn = 0;
            let totalNotIn = 0;
            let totalProd = 0;
            let totalUnProd = 0;
            let totalLate = 0;
            let totalEarly = 0;
            let totalLeaveSick = 0;
            let totalLeave = 0;
            let totalVisit = 0;

            if(absenPersonilGrid.getRowsNum() > 0) {
                for (let i = 0; i < absenPersonilGrid.getRowsNum(); i++) {
                    let id = absenPersonilGrid.getRowId(i);

                    let shift = absenPersonilGrid.cells(id, 7).getValue();
                    let dateIn = absenPersonilGrid.cells(id, 11).getValue();
                    let late = absenPersonilGrid.cells(id, 21).getValue();
                    let early = absenPersonilGrid.cells(id, 22).getValue();
                    let prod = absenPersonilGrid.cells(id, 23).getValue();
                    let unprod = absenPersonilGrid.cells(id, 24).getValue();
                    let leave = absenPersonilGrid.cells(id, 29).getValue();
                    let visit = absenPersonilGrid.cells(id, 30).getValue();
                    let leaveName = absenPersonilGrid.cells(id, 31).getValue();
                   
                    if(dateIn != '-') {
                        if(shift != 'Lembur Shift 1' && shift != 'Lembur Shift 2' && shift != 'Lembur Shift 3') {
                            totalIn++;
                        }
                    } else {
                        totalNotIn++;
                    }

                    totalProd += parseFloat(prod.replace(",", "."));
                    totalUnProd += parseFloat(unprod.replace(",", "."));
                    totalLate += parseFloat(late.replace(",", "."));
                    totalEarly += parseFloat(early.replace(",", "."));
                    totalLeave += parseFloat(leave - leaveName == 'Cuti Sakit' ? 1 : 0);
                    totalLeaveSick += parseFloat(leaveName == 'Cuti Sakit' ? 1 : 0);
                    totalVisit += parseFloat(visit);

                    if(i == 0) {
                        $('#abs_personil_prod_text').html(absenPersonilGrid.cells(id, 25).getValue());
                        $('#abs_personil_unprod_text').html(absenPersonilGrid.cells(id, 26).getValue());
                        $('#asb_personil_late_text').html(absenPersonilGrid.cells(id, 27).getValue());
                        $('#abs_personil_early_text').html(absenPersonilGrid.cells(id, 28).getValue());
                    }
                }

                $('#abs_personil_in').html(totalIn);
                $('#abs_personil_notin').html(totalNotIn);
                $('#abs_personil_prod').html(totalProd.toFixed(2));
                $('#abs_personil_unprod').html(totalUnProd.toFixed(2));
                $('#asb_personil_late').html(totalLate.toFixed(2));
                $('#abs_personil_early').html(totalEarly.toFixed(2));
                $('#abs_personil_leave_sick').html(totalLeaveSick);
                $('#abs_personil_leave_other').html(totalLeave);
                $('#abs_personil_visit').html(totalVisit);
            } else {
                $('#abs_personil_in').html(totalIn);
                $('#abs_personil_notin').html(totalNotIn);
                $('#abs_personil_prod').html(totalProd.toFixed(2));
                $('#abs_personil_unprod').html(totalUnProd.toFixed(2));
                $('#asb_personil_late').html(totalLate.toFixed(2));
                $('#abs_personil_early').html(totalEarly.toFixed(2));
                $('#abs_personil_leave_sick').html(totalLeaveSick);
                $('#abs_personil_leave_other').html(totalLeave);
                $('#abs_personil_visit').html(totalVisit);

                $('#abs_personil_prod_text').html("-");
                $('#abs_personil_unprod_text').html("-");
                $('#asb_personil_late_text').html("-");
                $('#abs_personil_early_text').html("-");
            }
        }

        var absenPersonilGrid =  hrAbsenPersonilLayout.cells("a").attachGrid();
        absenPersonilGrid.setHeader("No,NPP,Sap ID,Nama Karyawan,Sub Unit,Bagian,Sub Bagian,Shift Kerja,Tanggal,Jam Masuk,Jam Keluar,Absen Masuk,Absen Keluar,Keterlambatan,Pulang Cepat,Jam Produktif,Jam Tidak Produktif,Lokasi Absen,Jarak Dari Gate,QR Masuk, QR Keluar,countDifHourDateIn,countDifHourDateOut,countProd,countUnProd,totalProd,totalUnProd,totalLate,totalEarly,leave,visit,leaveName");
        absenPersonilGrid.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter")
        absenPersonilGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        absenPersonilGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        absenPersonilGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
        absenPersonilGrid.setInitWidthsP("5,10,10,20,15,20,20,10,10,15,15,15,15,10,10,10,10,10,15,20,20,0,0,0,0,0,0,0,0,0,0,0");
        absenPersonilGrid.attachFooter(",Keterangan,Total,Keterangan");
        absenPersonilGrid.attachFooter(",Absen Masuk,<div id='abs_personil_in'>0</div>,<div id=''>-</div>");
        absenPersonilGrid.attachFooter(",Tidak Masuk,<div id='abs_personil_notin'>0</div>,<div id=''>-</div>");
        absenPersonilGrid.attachFooter(",Cuti Sakit ,<div id='abs_personil_leave_sick'>0</div>,<div id=''>-</div>");
        absenPersonilGrid.attachFooter(",Cuti Lainnya,<div id='abs_personil_leave_other'>0</div>,<div id=''>-</div>");
        absenPersonilGrid.attachFooter(",Absen Visit,<div id='abs_personil_visit'>0</div>,<div id=''>-</div>");
        absenPersonilGrid.attachFooter(",Jam Produktif,<div id='abs_personil_prod'>0</div>,<div id='abs_personil_prod_text'>0</div>");
        absenPersonilGrid.attachFooter(",Jam Tidak Produktif,<div id='abs_personil_unprod'>0</div>,<div id='abs_personil_unprod_text'>0</div>");
        absenPersonilGrid.attachFooter(",Terlambat,<div id='asb_personil_late'>0</div>,<div id='asb_personil_late_text'>0</div>");
        absenPersonilGrid.attachFooter(",Pulang Cepat,<div id='abs_personil_early'>0</div>,<div id='abs_personil_early_text'>0</div>");
        absenPersonilGrid.enableSmartRendering(true);
        absenPersonilGrid.enableMultiselect(false);
        absenPersonilGrid.attachEvent("onXLE", function() {
            hrAbsenPersonilLayout.cells("a").progressOff();
        });
        absenPersonilGrid.init();

        function rAbsenPersonilGrid(empId) {
            let start = $("#hr_attendance_start").val();
            let end = $("#hr_attendance_end").val();
            let status = $("#hr_attendance_status").val();

            hrAbsenPersonilLayout.cells("a").progressOn();
            absenPersonilGrid.clearAndLoad(MAbsen("getAbsenPersonil", {start, end, status, equal_emp_id: empId}), absenPersonilGridCount);
        }

        rAbsenGrid();
    }

JS;

header('Content-Type: application/javascript');
echo $script;
