<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"
    
    function absensiInfoBoard() {	
        var addInfoForm;
        var editInfoForm;
        var fileError;
        var totalFile;
        var selectedInfo = [];

        var infoBoardLayout = mainTab.cells("hr_attendance_info_board").attachLayout({
            pattern: "3U",
            cells: [
                {
                    id: "a",
                    text: "Form Informasi",
                    header: true,
                    collapse: false,
                    height: 600
                },
                {
                    id: "b" ,
                    text: "Rich Text Plugin",
                    header: true,
                    collapse: false,
                    height: 600
                },
                {
                    id: "c",
                    text: "Daftar Informasi",
                    header: true,
                    collapse: false
                }
            ]
        });
        
        infoBoardLayout.cells("b").attachURL(BASE_URL + "share/editor");

        var infoToolbar = infoBoardLayout.cells("c").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "active", text: "Aktifkan", type: "button", img: "check.png"},
                {id: "nonactive", text: "Non Aktifkan", type: "button", img: "block.png"},
                {id: "edit", text: "Edit", type: "button", img: "edit.png"},
                {id: "delete", text: "Hapus", type: "button", img: "delete.png"},
                {id: "refresh", text: "Refresh", type: "button", img: "refresh.png"},
            ]
        });

        infoToolbar.attachEvent("onClick", function(id) {
            switch (id) {
                case "active":
                    if(selectedInfo.length == 0) return eAlert('Belum ada info dipilih');
                    infoBoardLayout.cells("b").progressOn();
                    reqJson(Attendance("activate"), "POST", {infoIds: selectedInfo, is_active: 1}, (err, res) => {
                        if(res.status === "success") {
                            init();
                            selectedInfo = [];
                            infoBoardLayout.cells("b").progressOff()
                            sAlert(res.message);
                        } else {
                            eAlert(res.message);
                        }
                    });
                    break;
                case "nonactive":
                    if(selectedInfo.length == 0) return eAlert('Belum ada info dipilih');
                    infoBoardLayout.cells("b").progressOn();
                    reqJson(Attendance("activate"), "POST", {infoIds: selectedInfo, is_active: 0}, (err, res) => {
                        if(res.status === "success") {
                            init();
                            selectedInfo = [];
                            infoBoardLayout.cells("b").progressOff()
                            sAlert(res.message);
                        } else {
                            eAlert(res.message);
                        }
                    });
                    break;
                case "edit":
                    editInfoHandler();
                    break;
                case "delete":
                    if (!infoGrid.getSelectedRowId()) {
                        return eAlert("Pilih baris yang akan dihapus!");
                    }

                    reqAction(infoGrid, Attendance("infoDelete"), 1, (err, res) => {
                        rInfoGrid();
                        addInfoHandler();
                        res.mSuccess && sAlert("Sukses Menghapus Record <br>" + res.mSuccess);
                        res.mError && eAlert("Gagal Menghapus Record <br>" + res.mError);
                    });
                    break;
                    break;
                case "refresh":
                    rInfoGrid();
                    break;
            }
        });


        function addInfoHandler() {
            addInfoForm = infoBoardLayout.cells("a").attachForm([
                {type: "fieldset", offsetTop: 30, offsetLeft: 30, label: "Menu Form", list: [
                    {type: "input", name: "name", label: "Nama Menu", labelWidth: 130, inputWidth: 350, required: true},
                    {type: "editor", name: "description", label: "Deskripsi", labelWidth: 130, inputWidth: 650, inputHeight: 200, required: true},
                    {type: "hidden", name: "filename", label: "Filename", readonly: true},
                    {type: "upload", name: "file_uploader", inputWidth: 420,
                        url: AppMaster("fileUpload", {save: false, folder: "info"}), 
                        swfPath: "./public/codebase/ext/uploader.swf", 
                        swfUrl: AppMaster("fileUpload")
                    },
                ]},
                {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                    {type: "button", name: "add", className: "button_update", offsetLeft: 15, value: "Simpan"},
                    {type: "newcolumn"},
                    {type: "button", name: "clear", className: "button_clear", offsetLeft: 30, value: "Clear"}
                ]},
            ]);

            addInfoForm.attachEvent("onBeforeFileAdd", async function (filename, size) {
                beforeFileAdd(addInfoForm, {filename, size});
            });

            addInfoForm.attachEvent("onBeforeFileUpload", function(mode, loader, formData){
                if(fileError) {
                    clearUploader(addInfoForm, "file_uploader");
                    eAlert("File error silahkan upload file sesuai ketentuan!");
                    fileError = false;
                } else {
                    return true;
                }
            });

            addInfoForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "add":
                        const uploader = addInfoForm.getUploader("file_uploader");
                        if(uploader.getStatus() === -1) {
                            if(!fileError) {
                                uploader.upload();
                            } else {
                                uploader.clear();
                                eAlert("File error silahkan upload file sesuai ketentuan!");
                                fileError = false;
                            }
                        } else {
                            addInfoFormSubmit();
                        }
                        break;
                    case "clear":
                        clearAllForm(addInfoForm);
                        clearUploader(addInfoForm, "file_uploader");
                        break;
                }
            });

            addInfoForm.attachEvent("onUploadFile", function(filename, servername){
                addInfoForm.setItemValue("filename", servername);
                addInfoFormSubmit();
            });

            function addInfoFormSubmit() {
                if (!addInfoForm.validate()) return eAlert("Input error!");

                setDisable(["add", "clear"], addInfoForm, infoBoardLayout.cells("a"));

                let addInfoFormDP = new dataProcessor(Attendance("infoForm"));
                addInfoFormDP.init(addInfoForm);
                addInfoForm.save();

                addInfoFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                    let message = tag.getAttribute("message");
                    switch (action) {
                        case "inserted":
                            sAlert("Berhasil Menambahkan Record <br>" + message);
                            clearAllForm(addInfoForm);
                            clearUploader(addInfoForm, "file_uploader");
                            setEnable(["add", "clear"], addInfoForm, infoBoardLayout.cells("a"));
                            rInfoGrid();
                            break;
                        case "error":
                            eAlert("Gagal Menambahkan Record <br>" + message);
                            setEnable(["add", "clear"], addInfoForm, infoBoardLayout.cells("a"));
                            break;
                    }
                });
            }
        }

        function editInfoHandler() {
            if (!infoGrid.getSelectedRowId()) {
                return eAlert("Pilih baris yang akan diupdate!");
            }

            editInfoForm = infoBoardLayout.cells("a").attachForm([
                {type: "fieldset", offsetTop: 30, offsetLeft: 30, label: "Info Form", list: [
                    {type: "hidden", name: "id", label: "ID", labelWidth: 130, inputWidth: 350},
                    {type: "hidden", name: "filename", label: "filename", readonly: true},
                    {type: "input", name: "name", label: "Nama Menu", labelWidth: 130, inputWidth: 350, required: true},
                    {type: "editor", name: "description", label: "Deskripsi", labelWidth: 130, inputWidth: 650, inputHeight: 200, required: true},
                    {type: "upload", name: "file_uploader", inputWidth: 420,
                        url: AppMaster("fileUpload", {save: false, folder: "info"}), 
                        swfPath: "./public/codebase/ext/uploader.swf", 
                        swfUrl: AppMaster("fileUpload"),
                        autoStart: true
                    },
                    {type: "container", name : "file_display", label: "<img src='./public/img/no-image.png' height='120' width='320'>"},

                    {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                        {type: "button", name: "update", className: "button_update", offsetLeft: 15, value: "Simpan"},
                        {type: "newcolumn"},
                        {type: "button", name: "cancel", className: "button_no", offsetLeft: 30, value: "Cancel"}
                    ]},
                ]}
            ]);

            const loadTemp = (filename) => {
                if (filename.length === 0) {
                    editMenuForm.setItemLabel("file_display", "<img src='./public/img/no-image.png' height='120' width='320'>");
                } else {
                    filename.map(file => {
                        if(file === '') {
                            editInfoForm.setItemLabel("file_display", "<img src='./public/img/no-image.png' height='120' width='320'>");
                        } else {
                            var fotoDisplay = "<img src='./assets/images/info/"+file+"' height='120' width='320'>"
                            editInfoForm.setItemLabel("file_display", fotoDisplay);
                        }
                    });
                }	
            }

            fetchFormData(Attendance("infoForm", {id: infoGrid.getSelectedRowId()}), editInfoForm, ["filename"], loadTemp);

            editInfoForm.attachEvent("onBeforeFileAdd", async function (filename, size) {
                beforeFileAdd(editInfoForm, {filename, size}, editInfoForm.getItemValue("id"));
            });

            editInfoForm.attachEvent("onBeforeFileUpload", function(mode, loader, formData){
                if(fileError) {
                    clearUploader(editInfoForm, "file_uploader");
                    eAlert("File error silahkan upload file sesuai ketentuan!");
                    fileError = false;
                } else {
                    return true;
                }
            });

            editInfoForm.attachEvent("onUploadFile", function(filename, servername){
                reqJson(AppMaster("updateAfterUpload"), "POST", {
                    id: editInfoForm.getItemValue("id"),
                    oldFile: editInfoForm.getItemValue("filename"),
                    filename: servername,
                    folder: "kf_hr.information_board",
                    customFolder: "info"
                }, (err, res) => {
                    if(res.status === "success") {
                        editInfoForm.setItemValue("filename", servername);
                        clearUploader(editInfoForm, "file_uploader");
                        editInfoForm.setItemLabel("file_display", "<img src='./assets/images/info/"+servername+"' height='120' width='320'>");
                        sAlert(res.message);
                    } else {
                        eAlert(res.message);
                    }
                });
            });  

            editInfoForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "update":
                        if (!editInfoForm.validate()) return eAlert("Input error!");

                        setDisable(["update", "cancel"], editInfoForm, infoBoardLayout.cells("a"));

                        let editInfoFormDP = new dataProcessor(Attendance("infoForm"));
                        editInfoFormDP.init(editInfoForm);
                        editInfoForm.save();

                        editInfoFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                            let message = tag.getAttribute("message");
                            switch (action) {
                                case "updated":
                                    sAlert("Berhasil Mengubah Record <br>" + message);
                                    infoBoardLayout.cells("a").progressOff();
                                    addInfoHandler();
                                    rInfoGrid();
                                    break;
                                case "error":
                                    eAlert("Gagal Mengubah Record <br>" + message);
                                    infoBoardLayout.cells("a").progressOff();
                                    addInfoHandler();
                                    rInfoGrid();
                                    break;
                            }
                        });
                        break;
                    case "cancel":
                        addInfoHandler();
                        break;
                }
            });
        }

        var infoStatusBar = infoBoardLayout.cells("c").attachStatusBar();
        function infoGridCount() {
            infoStatusBar.setText("Total baris: " + infoGrid.getRowsNum());
        }

        var infoGrid = infoBoardLayout.cells("c").attachGrid();
        infoGrid.setImagePath("./public/codebase/imgs/");
        infoGrid.setHeader("No,<input type='checkbox' id='info_board_checkbox' />,Nama,Deskripsi,Is Active,Di Buat, Di Update");
        infoGrid.attachHeader("#rspan,#rspan,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter");
        infoGrid.setColSorting("int,na,str,str,str,str,str");
        infoGrid.setColTypes("rotxt,ch,rotxt,rotxt,rotxt,rotxt,rotxt");
        infoGrid.setColAlign("center,left,left,left,left,left,left");
        infoGrid.setInitWidthsP("5,5,25,30,5,15,15");
        infoGrid.enableSmartRendering(true);
        infoGrid.enableMultiselect(false);
        infoGrid.attachEvent("onCheckbox", function(rId, cInd, state){
            state 
                ? selectedInfo.push(rId) 
                : selectedInfo.splice(selectedInfo.indexOf(rId), 1);
        });
        infoGrid.attachEvent("onXLE", function() {
            infoBoardLayout.cells("c").progressOff();
        });
        infoGrid.init();

        function rInfoGrid() {
            infoBoardLayout.cells("c").progressOn();
            infoGrid.clearAndLoad(Attendance("getInfoBoard"), infoGridCount);
        }

        $("#info_board_checkbox").on("change", function() {
            let checked = document.getElementById('info_board_checkbox').checked;
            doCheck(checked);
        });

        function doCheck(check = true) {
            if(check) {
                for (let i = 0; i < infoGrid.getRowsNum(); i++) {
                    let id = infoGrid.getRowId(i);
                    selectedInfo.push(id);
                    infoGrid.cells(id, 1).setValue(1);
                }
            } else {
                selectedInfo = [];
                for (let i = 0; i < infoGrid.getRowsNum(); i++) {
                    let id = infoGrid.getRowId(i);
                    infoGrid.cells(id, 1).setValue(0);
                }
            }
        }

        function init() {
            addInfoHandler();
            rInfoGrid();
        }

        init();

        async function beforeFileAdd(form, file, id = null) {
            if(form.validate()) {
                var ext = file.filename.split(".").pop();
                if (ext == "png" || ext == "jpg" || ext == "jpeg") {
                    if (file.size > 1000000) {
                        fileError = true;
                        eAlert("Tidak boleh melebihi 1 MB!");
                    } else {
                        if(totalFile > 0) {
                            eAlert("Maksimal 1 file");
                            fileError = true;
                        } else {
                            const data = id ? {id, name: form.getItemValue("name")} : {name: form.getItemValue("name")}
                            const check = await reqJsonResponse(Attendance("checkBeforeAddFile"), "POST", data);

                            if(check) {
                                if(check.status === "success") {
                                    totalFile++;
                                    return true;
                                } else if(check.status === "deleted") {
                                    fileError = false;
                                    totalFile= 0;
                                } else {
                                    eAlert(check.message);
                                    fileError = true;
                                }
                            }
                        }
                    }		    
                } else {
                    eAlert("Hanya png, jpg & jpeg saja yang bisa diupload!");
                    fileError = true;
                }
            } else {
                eAlert("Input error!");
            }	
        }
    }

JS;

header('Content-Type: application/javascript');
echo $script;