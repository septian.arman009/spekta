<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function hrVisit(){
        var hrVisitLayout = mainTab.cells("hr_visit").attachLayout({
            pattern: "1C",
            cells: [
                {id: "a", header : false}
            ]
        });

        var hrVisitMenu = hrVisitLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
                {id: "month", text: genSelectMonth("hr_visit_year", "hr_visit_month")},
            ]
        });

        hrVisitMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "export":
                    visitGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
                case "refresh":
                    rVisitGrid();
                    break;
            }
        });

        $("#hr_visit_year").on("change", function() {
            rVisitGrid();
        });

        $("#hr_visit_month").on("change", function() {
            rVisitGrid();
        });

        var hrVisitStatus = hrVisitLayout.cells("a").attachStatusBar();
        function visitGridCount() {
            hrVisitStatus.setText("Total baris: " + visitGrid.getRowsNum());
        }

        var visitGrid =  hrVisitLayout.cells("a").attachGrid();
        visitGrid.setHeader("No,Nama,Sub Unit,Bagian,Sub Bagian,Tanggal Visit,Tempat Kunjungan,Deskripsi,Alamat,Check IN,Check OUT,Latitude,Longitude,Jarak");
        visitGrid.attachHeader("#rspan,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter")
        visitGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str");
        visitGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        visitGrid.setColAlign("center,left,left,left,left,lef,lef,lef,lef,lef,lef,lef,lef,left");
        visitGrid.setInitWidthsP("5,20,20,20,20,12,20,25,40,5,5,10,10,5");
        visitGrid.enableSmartRendering(true);
        visitGrid.enableMultiselect(false);
        visitGrid.attachEvent("onXLE", function() {
            hrVisitLayout.cells("a").progressOff();
        });
        visitGrid.init();

        function rVisitGrid() {
            let year = $("#hr_visit_year").val();
            let month = $("#hr_visit_month").val();
            hrVisitLayout.cells("a").progressOn();
            visitGrid.clearAndLoad(Attendance("getVisit", {month, year}), visitGridCount);
        }

        rVisitGrid();
    }

JS;

header('Content-Type: application/javascript');
echo $script;