<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function showNonShiftManagement() {	
        var selectedLeftEmp = [];
        var selectedRightEmp = [];

        var comboUrl = {
            shift_id: {
                url: MAbsen("getShiftList"),
                reload: true
            }
        }
        
        var mNonShiftLayout = mainTab.cells("hr_manajemen_non_shift").attachLayout({
            pattern: "2U",
            cells: [{
                    id: "a",
                    text: "Karyawan Shift",
                    header: true,
                    collapse: false
                },
                {
                    id: "b",
                    text: "Karyawan Non Shift",
                    header: true,
                    collapse: false
                }
            ]
        });

        var shiftToolbar = mNonShiftLayout.cells("a").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "move", text: "Pindahkan Ke Non Shift", type: "button", img: "right_arrow.png"},
                {id: "refresh", text: "Refresh", type: "button", img: "refresh.png"},
            ]
        });

        var nonShiftToolbar = mNonShiftLayout.cells("b").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "move", text: "Pindahkan Ke Shift", type: "button", img: "left_arrow.png"},
                {id: "refresh", text: "Refresh", type: "button", img: "refresh.png"},
                {id: "edit", text: "Ubah Shift Kerja", type: "button", img: "clock.png"},
            ]
        });

        var leftEmpStatusBar = mNonShiftLayout.cells("a").attachStatusBar();
        function leftEmpGridCount() {
            let leftEmpGridRows = leftEmpGrid.getRowsNum();
            leftEmpStatusBar.setText("Total baris: " + leftEmpGridRows);
        }

        var leftEmpGrid = mNonShiftLayout.cells("a").attachGrid();
        leftEmpGrid.setImagePath("./public/codebase/imgs/");
        leftEmpGrid.setHeader("No,<input type='checkbox' id='abs_left_emp_checkbox' />,Nip,Nama,Shift,Waktu Masuk,Waktu Keluar,Bagian,Sub Bagian,Sub Unit,Jabatan");
        leftEmpGrid.attachHeader("#rspan,#rspan,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter");
        leftEmpGrid.setColSorting("int,na,str,str,str,str,str,str,str,str,str");
        leftEmpGrid.setColTypes("rotxt,ch,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        leftEmpGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left");
        leftEmpGrid.setInitWidthsP("5,5,15,25,0,0,0,25,25,25,25");
        leftEmpGrid.enableSmartRendering(true);
        leftEmpGrid.enableMultiselect(false);
        leftEmpGrid.attachEvent("onCheckbox", function(rId, cInd, state){
            state 
                ? selectedLeftEmp.push(rId) 
                : selectedLeftEmp.splice(selectedLeftEmp.indexOf(rId), 1);
        });
        leftEmpGrid.attachEvent("onXLE", function() {
            mNonShiftLayout.cells("a").progressOff();
        });
        leftEmpGrid.init();

        function rLeftEmpGrid() {
            mNonShiftLayout.cells("a").progressOn();

            var params = { 
                equal_shift_status: 1
            };

            if((userLogged.rankId >= 6 || userLogged.pltRankId >= 6) && userLogged.subId != 11) {
                params.in_sub_department_id = userLogged.subId+","+userLogged.pltSubId;
            }

            leftEmpGrid.clearAndLoad(MAbsen("getEmployees", params), leftEmpGridCount);
        }

        var rightEmpStatusBar = mNonShiftLayout.cells("b").attachStatusBar();
        function rightEmpGridCount() {
            let rightEmpGridRows = rightEmpGrid.getRowsNum();
            rightEmpStatusBar.setText("Total baris: " + rightEmpGridRows);
        }

        var rightEmpGrid = mNonShiftLayout.cells("b").attachGrid();
        rightEmpGrid.setImagePath("./public/codebase/imgs/");
        rightEmpGrid.setHeader("No,<input type='checkbox' id='abs_right_emp_checkbox' />,Nip,Nama,Shift,Waktu Masuk,Waktu Keluar,Sub Unit,Bagian,Sub Bagian,Jabatan");
        rightEmpGrid.attachHeader("#rspan,#rspan,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter");
        rightEmpGrid.setColSorting("int,na,str,str,str,str,str,str,str,str,str");
        rightEmpGrid.setColTypes("rotxt,ch,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        rightEmpGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left");
        rightEmpGrid.setInitWidthsP("5,5,10,25,10,10,10,25,25,25,25");
        rightEmpGrid.enableSmartRendering(true);
        rightEmpGrid.enableMultiselect(false);
        rightEmpGrid.attachEvent("onCheckbox", function(rId, cInd, state){
            state 
                ? selectedRightEmp.push(rId) 
                : selectedRightEmp.splice(selectedRightEmp.indexOf(rId), 1);
        });
        rightEmpGrid.attachEvent("onXLE", function() {
            mNonShiftLayout.cells("b").progressOff();
        });
        rightEmpGrid.init();

        function rRightEmpGrid() {
            mNonShiftLayout.cells("b").progressOn();
            var params = { 
                equal_shift_status: 0
            };

            if((userLogged.rankId >= 6 || userLogged.pltRankId >= 6) && userLogged.subId != 11) {
                params.in_sub_department_id = userLogged.subId+","+userLogged.pltSubId;
            }

            rightEmpGrid.clearAndLoad(MAbsen("getEmployees", params), rightEmpGridCount);
        }

        $("#abs_left_emp_checkbox").on("change", function() {
            let checked = document.getElementById('abs_left_emp_checkbox').checked;
            if(checked) {
                for (let i = 0; i < leftEmpGrid.getRowsNum(); i++) {
                    let id = leftEmpGrid.getRowId(i);
                    selectedLeftEmp.push(id);
                    leftEmpGrid.cells(id, 1).setValue(1);
                }
            } else {
                selectedLeftEmp = [];
                for (let i = 0; i < leftEmpGrid.getRowsNum(); i++) {
                    let id = leftEmpGrid.getRowId(i);
                    leftEmpGrid.cells(id, 1).setValue(0);
                }
            }
        });

        $("#abs_right_emp_checkbox").on("change", function() {
            let checked = document.getElementById('abs_right_emp_checkbox').checked;
            if(checked) {
                for (let i = 0; i < rightEmpGrid.getRowsNum(); i++) {
                    let id = rightEmpGrid.getRowId(i);
                    selectedRightEmp.push(id);
                    rightEmpGrid.cells(id, 1).setValue(1);
                }
            } else {
                selectedRightEmp = [];
                for (let i = 0; i < rightEmpGrid.getRowsNum(); i++) {
                    let id = rightEmpGrid.getRowId(i);
                    rightEmpGrid.cells(id, 1).setValue(0);
                }
            }
        });

        shiftToolbar.attachEvent("onClick", function(id) {
            switch (id) {
                case "move":
                    if(selectedLeftEmp.length == 0) return eAlert('Belum ada karyawan dipilih');
                    mNonShiftLayout.cells("a").progressOn();
                    reqJson(MAbsen("moveShift"), "POST", {empIds: selectedLeftEmp, shift_status: 0}, (err, res) => {
                        if(res.status === "success") {
                            init();
                            selectedLeftEmp = [];
                            mNonShiftLayout.cells("a").progressOff()
                            sAlert(res.message);
                        } else {
                            eAlert(res.message);
                        }
                    });
                    break;
                case "refresh":
                    rLeftEmpGrid();
                    break;
            }
        });

        nonShiftToolbar.attachEvent("onClick", function(id) {
            switch (id) {
                case "move":
                    if(selectedRightEmp.length == 0) return eAlert('Belum ada karyawan dipilih');
                    mNonShiftLayout.cells("b").progressOn();
                    reqJson(MAbsen("moveShift"), "POST", {empIds: selectedRightEmp, shift_status: 1}, (err, res) => {
                        if(res.status === "success") {
                            init();
                            selectedRightEmp = [];
                            mNonShiftLayout.cells("b").progressOff()
                            sAlert(res.message);
                        } else {
                            eAlert(res.message);
                        }
                    });
                    break;
                case "refresh":
                    rRightEmpGrid();
                    break;
                case "edit":
                    editShiftWindow();
                    break;
            }
        });

        function editShiftWindow() {
            if(!rightEmpGrid.getSelectedRowId()) return eAlert("Belum ada data yang di pilih!");

            var shiftWin = createWindow("edit_non_shift_win", "Ubah Shift Kerja", 500, 300);
            myWins.window("edit_non_shift_win").skipMyCloseEvent = true;

            var winShiftForm = shiftWin.attachForm([
                {type: "fieldset", offsetTop: 30, offsetLeft: 30, label: "Pilih Shift", list: [
                    {type: "combo", name: "shift_id", label: "Pilih Shift Kerja", readonly: true, labelWidth: 130, inputWidth: 250, required: true},
                    {type: "hidden", name: "empId", label: "Selected Emp", labelWidth: 130, inputWidth: 250, value: rightEmpGrid.getSelectedRowId()},
                ]},
                {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                    {type: "button", name: "update", className: "button_update", offsetLeft: 15, value: "Simpan"}
                ]},
            ]);

            var winShiftCombo = winShiftForm.getCombo("shift_id");
            winShiftCombo.load(comboUrl.shift_id.url);

            winShiftForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "update":
                        if (!winShiftForm.validate()) return eAlert("Input error!");

                        setDisable(["update"], winShiftForm, shiftWin);

                        let winShiftFormDP = new dataProcessor(MAbsen("updateNonShift"));
                        winShiftFormDP.init(winShiftForm);
                        winShiftForm.save();

                        winShiftFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                        let message = tag.getAttribute("message");
                            switch (action) {
                                case "updated":
                                    sAlert(message);
                                    rRightEmpGrid();
                                    setEnable(["update"], winShiftForm, shiftWin);
                                    closeWindow("edit_non_shift_win");
                                    break;
                                case "error":
                                    eAlert("Gagal Mengubah Record <br>" + message);
                                    setEnable(["update"], winShiftForm, shiftWin);
                                break;
                            }
                        });
                }
            });
        }

        function init() {
            rLeftEmpGrid();
            rRightEmpGrid();
        }

        init();
    }
        
JS;

header('Content-Type: application/javascript');
echo $script;
        