<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function hrUploadEmployee() {
        var addForm;
        var editForm;
        var fileError;
        var totalFile;

        var uploadLayout = mainTab.cells("hr_upload_employee").attachLayout({
            pattern: "3L",
            cells: [{
                    id: "a",
                    text: "Data Sementara",
                    header: true,
                    collapse: false
                },
                {
                    id: "b",
                    text: "Form Upload",
                    header: true,
                    collapse: false,
                    height: 530
                },
                {
                    id: "c",
                    text: "Daftar File",
                    header: true,
                    collapse: false
                }
            ]
        });

        var tempToolbar = uploadLayout.cells("a").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "save", text: "Simpan Ke Database", type: "button", img: "update.png"},
                {id: "export", text: "Export To Excel", type: "button", img: "excel.png"},
                {id: "clear", text: "Bersihkan", type: "button", img: "clear.png"},
                {id: "refresh", text: "Refresh", type: "button", img: "refresh.png"},
            ]
        });

        tempToolbar.attachEvent("onClick", function(id) {
            switch (id) {
                case "save":
                    reqConfirm3(HrUpload("saveToDatabase"), { }, (err, res) => {
                        if(err) {
                            eAlert(err.message);
                        } else {
                            if (res.status == 'success') {
                                rFileTempGrid();
                                sAlert(res.message);
                            } else {
                                eAlert(res.message);
                            }
                        }
                    });
                    break;
                case "export":
                    fileTempGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    break;
                case "clear":
                    reqConfirm3(HrUpload("deleteTemp"), { }, (err, res) => {
                        if(err) {
                            eAlert(err.message);
                        } else {
                            if (res.status == 'success') {
                                rFileTempGrid();
                                sAlert(res.message);
                            } else {
                                eAlert(res.message);
                            }
                        }
                    });
                    break;
                case "refresh":
                    rFileTempGrid();
                    break;
            }
        });

        var formToolbar = uploadLayout.cells("b").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "download", text: "Download Template Upload", type: "button", img: "download_16.png"},
            ]
        });

        formToolbar.attachEvent("onClick", function(id) {
            switch (id) {
                case "download":
                    toDownload("https://spekta.id/assets/hr_files_upload/FormUploadKaryawan.xlsx");
                    break;
            }
        });

        var fileToolbar = uploadLayout.cells("c").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "move", text: "Masukan Ke Data Sementara", type: "button", img: "left_arrow.png"},
                {id: "delete", text: "Hapus", type: "button", img: "delete.png"},
                {id: "refresh", text: "Refresh", type: "button", img: "refresh.png"},
            ]
        });

        fileToolbar.attachEvent("onClick", function(id) {
            switch (id) {
                case "move":
                    reqConfirm2(fileGrid, HrUpload("moveToTemp"), { id: fileGrid.getSelectedRowId() }, (err, res) => {
                        if(err) {
                            eAlert(err.message);
                        } else {
                            if (res.status == 'success') {
                                rFileGrid();
                                rFileTempGrid();
                                sAlert(res.message);
                            } else {
                                eAlert(res.message);
                            }
                        }
                    });
                    break;
                case "delete":
                    reqAction(fileGrid, HrUpload("fileDelete"), 1, (err, res) => {
                        rFileGrid();
                        addFileHandler();
                        res.mSuccess && sAlert("Sukses Menghapus Record <br>" + res.mSuccess);
                        res.mError && eAlert("Gagal Menghapus Record <br>" + res.mError);
                    });
                    break;
                case "refresh":
                    rFileGrid();
                    break;
            }
        });

        function addFileHandler() {
            addForm = uploadLayout.cells("b").attachForm([
                {type: "fieldset", offsetTop: 30, offsetLeft: 30, label: "Menu Form", list: [
                    {type: "input", name: "name", label: "Nama File", labelWidth: 130, inputWidth: 350, required: true},
                    {type: "input", name: "description", label: "Deskripsi", labelWidth: 130, inputWidth: 350, required: true},
                    {type: "hidden", name: "filename", label: "Filename", readonly: true},
                    {type: "upload", name: "file_uploader", inputWidth: 420, required: true,
                        url: AppMaster("fileUpload2", {save: false, folder: "hr_files_upload"}), 
                        swfPath: "./public/codebase/ext/uploader.swf", 
                        swfUrl: AppMaster("fileUpload2")
                    },
                ]},
                {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                    {type: "button", name: "add", className: "button_update", offsetLeft: 15, value: "Simpan"},
                    {type: "newcolumn"},
                    {type: "button", name: "clear", className: "button_clear", offsetLeft: 30, value: "Clear"}
                ]},
            ]);

            addForm.attachEvent("onBeforeFileAdd", async function (filename, size) {
                beforeFileAdd(addForm, {filename, size});
            });

            addForm.attachEvent("onBeforeFileUpload", function(mode, loader, formData){
                if(fileError) {
                    clearUploader(addForm, "file_uploader");
                    eAlert("File error silahkan upload file sesuai ketentuan!");
                    fileError = false;
                } else {
                    return true;
                }
            });

            addForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "add":
                        const uploader = addForm.getUploader("file_uploader");
                        if(uploader.getStatus() === -1) {
                            if(!fileError) {
                                uploader.upload();
                            } else {
                                uploader.clear();
                                eAlert("File error silahkan upload file sesuai ketentuan!");
                                fileError = false;
                            }
                        } else {
                            addFileSubmit();
                        }
                        break;
                    case "clear":
                        clearAllForm(addForm);
                        clearUploader(addForm, "file_uploader");
                        break;
                }
            });

            addForm.attachEvent("onUploadFile", function(filename, servername){
                addForm.setItemValue("filename", servername);
                addFileSubmit();
            });

            function addFileSubmit() {
                if (!addForm.validate()) return eAlert("Input error!");

                setDisable(["add", "clear"], addForm, uploadLayout.cells("b"));

                let addFormDP = new dataProcessor(HrUpload("fileForm"));
                addFormDP.init(addForm);
                addForm.save();

                addFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                    let message = tag.getAttribute("message");
                    switch (action) {
                        case "inserted":
                            sAlert("Berhasil Menambahkan Record <br>" + message);
                            clearAllForm(addForm);
                            clearUploader(addForm, "file_uploader");
                            setEnable(["add", "clear"], addForm, uploadLayout.cells("b"));
                            rFileGrid();
                            break;
                        case "error":
                            eAlert("Gagal Menambahkan Record <br>" + message);
                            setEnable(["add", "clear"], addForm, uploadLayout.cells("b"));
                            break;
                    }
                });
            }
        }

        var fileStatusBar = uploadLayout.cells("c").attachStatusBar();
        function fileGridCount() {
            fileStatusBar.setText("Total baris: " + fileGrid.getRowsNum());
        }

        var fileGrid = uploadLayout.cells("c").attachGrid();
        fileGrid.setImagePath("./public/codebase/imgs/");
        fileGrid.setHeader("No,Nama Data,Nama File,Di Buat, Di Upload");
        fileGrid.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#text_filter");
        fileGrid.setColSorting("int,str,str,str,str");
        fileGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt");
        fileGrid.setColAlign("center,left,left,left,left");
        fileGrid.setInitWidthsP("5,30,20,20,25");
        fileGrid.enableSmartRendering(true);
        fileGrid.enableMultiselect(false);
        fileGrid.attachEvent("onXLE", function() {
            uploadLayout.cells("c").progressOff();
        });
        fileGrid.init();

        function rFileGrid() {
            uploadLayout.cells("c").progressOn();
            fileGrid.clearAndLoad(HrUpload("getFiles"), fileGridCount);
        }

        var fileTempStatusBar = uploadLayout.cells("a").attachStatusBar();
        function fileTempGridCount() {
            fileTempStatusBar.setText("Total baris: " + fileTempGrid.getRowsNum());
            if(fileTempGrid.getRowsNum() == 0) {
                tempToolbar.disableItem('save');
                tempToolbar.disableItem('export');
                tempToolbar.disableItem('clear');
            } else {
               
                tempToolbar.enableItem('clear');
                tempToolbar.enableItem('export');

                var totalOk = 0;
                for (let i = 0; i < fileTempGrid.getRowsNum(); i++) {
                    let id = fileTempGrid.getRowId(i);
                    if(fileTempGrid.cells(id, 29).getValue() == 0) {
                        totalOk++;
                    }
                }

                if(totalOk > 0) {
                    tempToolbar.enableItem('save');
                } else {
                    tempToolbar.disableItem('save');
                }
            }
        }

        var fileTempGrid = uploadLayout.cells("a").attachGrid();
        fileTempGrid.setImagePath("./public/codebase/imgs/");
        fileTempGrid.setHeader("No,NPP,SAP ID,Nomor KK,NIK,NPWP,Telpon,Handphone,Nama Karyawan,Nama Atasan,Tempat Lahir,Tanggal Lahir,Usia,Jenis Kelamin,Agama,Alamat,Status,OS,Email,Sub Unit,Bagian,Sub Bagian,Jabatan,Status Lembur,Gaji Pokok,Total Gaji,Premi Overtime,Di Buat, Di Upload,Is Valid");
        fileTempGrid.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#select_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
        fileTempGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        fileTempGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        fileTempGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
        fileTempGrid.setInitWidthsP("5,25,25,25,25,25,25,25,25,25,25,25,5,25,25,30,25,30,30,30,30,30,25,25,25,25,25,25,25,0");
        fileTempGrid.enableSmartRendering(true);
        fileTempGrid.enableMultiselect(false);
        fileTempGrid.attachEvent("onXLE", function() {
            uploadLayout.cells("a").progressOff();
        });
        fileTempGrid.init();

        function rFileTempGrid() {
            uploadLayout.cells("a").progressOn();
            fileTempGrid.clearAndLoad(HrUpload("getFilesTemp"), fileTempGridCount);
        }

        function init() {
            addFileHandler();
            rFileGrid();
            rFileTempGrid();
        }

        init();

        async function beforeFileAdd(form, file, id = null) {
            if(form.validate()) {
                var ext = file.filename.split(".").pop();
                if (ext == "xlsx" || ext == "xlx") {
                    if (file.size > 1000000) {
                        fileError = true;
                        eAlert("Tidak boleh melebihi 1 MB!");
                    } else {
                        if(totalFile > 0) {
                            eAlert("Maksimal 1 file");
                            fileError = true;
                        } else {
                            const data = id ? {id, name: form.getItemValue("name")} : {name: form.getItemValue("name")}
                            const check = await reqJsonResponse(HrUpload("checkBeforeAddFile"), "POST", data);

                            if(check) {
                                if(check.status === "success") {
                                    totalFile++;
                                    return true;
                                } else if(check.status === "deleted") {
                                    fileError = false;
                                    totalFile= 0;
                                } else {
                                    eAlert(check.message);
                                    fileError = true;
                                }
                            }
                        }
                    }		    
                } else {
                    eAlert("Hanya xlx & xlsx saja yang bisa diupload!");
                    fileError = true;
                }
            } else {
                eAlert("Input error!");
            }	
        }
    }

JS;

header('Content-Type: application/javascript');
echo $script;
        