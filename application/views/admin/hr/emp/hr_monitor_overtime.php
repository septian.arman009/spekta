<?php
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"
	function showHrMonitorOvertime() {
        var legend = legendGrid();
        var times = createTime();

        var appvTabs = mainTab.cells("hr_monitor_overtime").attachLayout({
            pattern: "2E",
            cells: [
                {id: "a", text: "Daftar Lembur", active: true},
                {id: "b", text: "Detail Lembur", collapse: true},
            ]
        });

        var appvToolbar = appvTabs.cells("a").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", type: "button", img: "refresh.png"},
            ]
        });

        let currentDate = filterForMonth(new Date());
        var appvMenu =  appvTabs.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "search", text: "<div style='width:100%'>Search: <input type='text' id='other_start_ovt_appv' readonly value='"+currentDate.start+"' /> - <input type='text' id='other_end_ovt_appv' readonly value='"+currentDate.end+"' /> <button id='other_btn_ftr_ovt_appv'>Proses</button> | Status: <select id='other_status_ovt_appv'><option>PROCESS</option><option>REJECTED</option><option>ALL</option></select></div>"}
            ]
        });

        $("#other_btn_ftr_ovt_appv").on("click", function() {
            if(checkFilterDate($("#other_start_ovt_appv").val(), $("#other_end_ovt_appv").val())) {
                rOvtGrid();
            } 
        });

        $("#other_status_ovt_appv").on("change", function() {
            if(checkFilterDate($("#other_start_ovt_appv").val(), $("#other_end_ovt_appv").val())) {
                rOvtGrid();
                rOvtDetailGrid(null);
            }
        });

        var filterCalendar = new dhtmlXCalendarObject(["other_start_ovt_appv","other_end_ovt_appv"]);

        appvToolbar.attachEvent("onClick", function(id) {
            switch (id) {
                case "refresh":
                    rOvtGrid();
                    rOvtDetailGrid(null);
                    break;
            }
        });

        let ovtStatusBar = appvTabs.cells("a").attachStatusBar();
        function ovtGridCount() {
            var ovtGridRows = ovtGrid.getRowsNum();
            ovtStatusBar.setText("Total baris: " + ovtGridRows + " (" + legend.approval_overtime + ")");
        }

        appvTabs.cells("a").progressOn();
        var ovtGrid = appvTabs.cells("a").attachGrid();
        ovtGrid.setImagePath("./public/codebase/imgs/");
        ovtGrid.setHeader("No,Task ID,Sub Unit,Bagian,,Kebutuhan Orang,Status Hari,Tanggal Overtime,Waktu Mulai, Waktu Selesai,Catatan,Makan,Steam,AHU,Compressor,PW,Jemputan,Dust Collector,WFI,Mekanik,Listrik,H&N,QC,QA,Penandaan,GBK,GBB,Status Overtime,,Approval ASMAN,Approval PPIC,Approval MANAGER,Approval PLANT MANAGER,Revisi Jam Lembur,Rejection User Approval,Created By,Updated By,Created At,NIPSPV,NIPASMAN,NIPPPIC,NIPMGR");
        ovtGrid.attachHeader("#rspan,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter")
        ovtGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        ovtGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
        ovtGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        ovtGrid.setInitWidthsP("5,20,20,20,0,10,10,15,15,15,25,7,7,7,7,7,7,10,7,7,7,7,7,7,7,7,7,10,0,30,30,30,30,30,30,15,15,22,0,0,0,0");
        ovtGrid.enableSmartRendering(true);
        ovtGrid.attachEvent("onXLE", function() {
            appvTabs.cells("a").progressOff();
        });
        ovtGrid.attachEvent("onRowDblClicked", function(rId, cInd){
            appvTabs.cells("b").setText("Detail Lembur : " + ovtGrid.cells(rId, 1).getValue());
            appvTabs.cells("b").expand();
            rOvtDetailGrid(rId);
        });

        ovtGrid.init();

        function rOvtGrid() {
            appvTabs.cells("a").progressOn();
            let start = $("#other_start_ovt_appv").val();
            let end = $("#other_end_ovt_appv").val();
            let status = $("#other_status_ovt_appv").val();
            let params = {
                notin_status: "CANCELED,CREATED,ADD", 
                betweendate_overtime_date: start+","+end
            };

            if(status !== "ALL") {
                params.equal_status = status;
            }
            
            ovtGrid.clearAndLoad(Overtime("getAppvOvertimeGrid", params), ovtGridCount);
        }

        rOvtGrid();

        var ovtDetailGrid = appvTabs.cells("b").attachGrid();
        ovtDetailGrid.setImagePath("./public/codebase/imgs/");
        ovtDetailGrid.setHeader("No,Task ID,Nama Karyawan,Tugas,Sub Unit,Bagian,Sub Bagian,Nama Mesin,,Pelayanan Produksi,Tanggal Overtime,Waktu Mulai,Waktu Selesai,Status Hari,Jam Efektif,Jam Istirahat,Jam Ril,Jam Hit,Premi,Nominal Overtime,Makan,Status Overtime,Status Terakhir,Spv Approval,Created By,Updated By,Created At,");
        ovtDetailGrid.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter")
        ovtDetailGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        ovtDetailGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
        ovtDetailGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        ovtDetailGrid.setInitWidthsP("5,20,20,25,20,20,20,20,0,20,15,15,15,10,10,10,10,10,10,10,5,10,25,25,15,15,22,0");
        ovtDetailGrid.attachFooter(legend.approval_overtime_spv+",#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#stat_total,#stat_total,#stat_total,#stat_total,,<div id='other_total_ovt_appv'></div>,,,,,,,,");
        ovtDetailGrid.enableSmartRendering(true);
        ovtDetailGrid.attachEvent("onXLE", function() {
            appvTabs.cells("b").progressOff();
        });
        ovtDetailGrid.init();

        function rOvtDetailGrid(rId) {
            if(rId) {
                appvTabs.cells("b").progressOn();
                let taskId = ovtGrid.cells(rId, 1).getValue();
                ovtDetailGrid.clearAndLoad(Overtime("getOvertimeDetailGrid", {equal_task_id: taskId, notin_status: "CANCELED,ADD", order_by: 'id:asc', apv: true}), countTotalOvertime);
            } else {
                ovtDetailGrid.clearAll();
                ovtDetailGrid.callEvent("onGridReconstructed",[]);
                $("#other_total_ovt_appv").html("0");
            }
        }

        function countTotalOvertime() {
            sumGridToElement(ovtDetailGrid, 19, "other_total_ovt_appv");
        }
    }
JS;

header('Content-Type: application/javascript');
echo $script;
