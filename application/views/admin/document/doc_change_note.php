<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

	function changeNote() {	

        var chNoteTabs = mainTab.cells("doc_change_note").attachTabbar({
			tabs: [
				{id: "form", text: "Form Change Note", active: true}
			]
		});

        chNoteTabs.setArrowsMode("auto");
		chNoteTabs.enableAutoReSize(true);

        var chNoteForm = chNoteTabs.tabs("form").attachForm([
            {type: "block", offsetLeft: 30, list:[	
                {type: "block", list: [
                    {type: "input", name: "emp_id", label: "Emp ID", labelWidth: 250, inputWidth: 300, required: true, value: userLogged.empId},
                    {type: "input", name: "sub_department_id", label: "Sub ID", labelWidth: 250, inputWidth: 300, required: true, value: userLogged.subId},
                    {type: "input", name: "sub_department", label: "Usulan dari", labelWidth: 250, inputWidth: 300, required: true, readonly: true, value: userLogged.subDepartment},
                    {type: "calendar", name: "created_at", label: "Tanggal", labelWidth: 250, inputWidth: 300, required: true, readonly: true, value: getCurrentDate(new Date())},
                    {type: "combo", name: "no_protap", label: "Dokumen yang diusulkan untuk dirubah", labelWidth: 250, inputWidth: 300, 
                        validate: "NotEmpty", 
                        required: true
                    },
                ]},
                {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                    {type: "button", name: "add", className: "button_add", offsetLeft: 15, value: "Tambah"},
                    {type: "newcolumn"},
                    {type: "button", name: "clear", className: "button_clear", offsetLeft: 30, value: "Clear"},
                    {type: "newcolumn"},
                    {type: "button", name: "cancel", className: "button_no", offsetLeft: 30, value: "Cancel"}
                ]}
            ]}
        ]);

        var protapCombo = chNoteForm.getCombo("no_protap");
        protapCombo.enableFilteringMode(true, 'no_protap');
        protapCombo.attachEvent("onDynXLS", protapComboFilter);

        function protapComboFilter(text){
            protapCombo.clearAll();
            if(text.length > 3) {
                dhx.ajax.get(Document('searchProtap', {search: text}), function(xml){
                    if(xml.xmlDoc.responseText) {
                        protapCombo.load(xml.xmlDoc.responseText);
                        protapCombo.openSelect();
                    }
                });
            }
        };

    }

JS;

header('Content-Type: application/javascript');
echo $script;