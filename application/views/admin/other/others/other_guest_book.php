<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"
	
    function showGuestBook() {	

        var gbLayout = mainTab.cells("other_guest_book").attachLayout({
            pattern: "1C",
            cells: [
                {id: "a", header : false}
            ]
        });

        var gbMenu = gbLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
                {id: "month", text: genSelectMonth("gb_year", "gb_month")},
            ]
        });

        gbMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "export":
                    gbGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
                case "refresh":
                    rGbGrid();
                    break;
            }
        });
        
        $("#gb_year").on("change", function() {
            rGbGrid();
        });

        $("#gb_month").on("change", function() {
            rGbGrid();
        });

        var gbStatus = gbLayout.cells("a").attachStatusBar();
        function gbGridCount() {
            gbStatus.setText("Total baris: " + gbGrid.getRowsNum());
        }

        var gbGrid = gbLayout.cells("a").attachGrid();
        gbGrid.setHeader("No,Nama Tamu,Nomor KTP,No. HP,Organisasi,Perusahaan,Bertemu Dengan,Perusahaan Tujuan,Bagian,Keperluan,Waktu Kunjungan");
        gbGrid.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#select_filter")
        gbGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str");
        gbGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        gbGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left");
        gbGrid.setInitWidthsP("5,15,10,10,10,20,15,20,20,25,15");
        gbGrid.enableSmartRendering(true);
        gbGrid.enableMultiselect(false);
        gbGrid.attachEvent("onXLE", function() {
            gbLayout.cells("a").progressOff();
        });
        gbGrid.init();

        function rGbGrid() {
            let month = $("#gb_month").val();
            let year = $("#gb_year").val();
            gbLayout.cells("a").progressOn();
            gbGrid.clearAndLoad(GuestBook("getGustBooks", {month, year}), gbGridCount);
        }

        rGbGrid();
    }

JS;

header('Content-Type: application/javascript');
echo $script;