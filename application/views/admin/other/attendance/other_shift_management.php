<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function showShiftManagement() {	
        var times = createTime();
        var shiftForm;
        var selectedEmp = [];
        var selectedEmpId = 0;

        var comboUrl = {
            shift_id: {
                url: MAbsen("getShiftList"),
                reload: true
            }
        }

        var mShiftLayout = mainTab.cells("other_manajemen_shift").attachLayout({
            pattern: "3L",
            cells: [{
                    id: "a",
                    text: "Shift Kerja <span id='selected_emp_name'></span>",
                    header: true,
                    collapse: false
                },
                {
                    id: "b",
                    text: "Form Mapping Shift Kerja",
                    header: true,
                    collapse: false,
                    height: 380
                },
                {
                    id: "c",
                    text: "Daftar Karyawan",
                    header: true,
                    collapse: false
                }
            ]
        });

        var shiftToolbar = mainTab.cells("other_manajemen_shift").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "month", text: genSelectMonth("shift_mgt_year", "shift_mgt_month")},
            ]
        });

        var shiftGridToolBar = mShiftLayout.cells("a").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", type: "button", img: "refresh.png"},
                {id: "edit", text: "Ubah/Buat Jam Kerja", type: "button", img: "clock.png"},
            ]
        });

        var empGridToolBar = mShiftLayout.cells("c").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", type: "button", img: "refresh.png"},
            ]
        });

        shiftGridToolBar.attachEvent("onClick", function(id) {
            switch (id) {
                case "refresh":
                    rShiftGrid();
                    break;
                case "edit":
                    editShiftWindow();
                    break;
            }
        });

        empGridToolBar.attachEvent("onClick", function(id) {
            switch (id) {
                case "refresh":
                    rEmpGrid();
                    break;
            }
        });

        $("#shift_mgt_year").on("change", function() {
            rShiftGrid();
            totalDays();
        });

        $("#shift_mgt_month").on("change", function() {
            rShiftGrid();
            totalDays();
        });

        var shiftGridStatusBar = mShiftLayout.cells("a").attachStatusBar();
        function totalDays() {
            shiftGridStatusBar.setText("Total hari: " + shiftGrid.getRowsNum());
            setActiveDate();
        }

        var shiftGrid = mShiftLayout.cells("a").attachGrid();
        shiftGrid.setImagePath("./public/codebase/imgs/");
        shiftGrid.setHeader("No,Tanggal,Shift Kerja,Jam Masuk,Jam Keluar, Absen Masuk,Absen Keluar,Status,WeekEnd");
        shiftGrid.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#rspan,#rspan");
        shiftGrid.setColSorting("int,str,str,str,str,str,str,str,str");
        shiftGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        shiftGrid.setColAlign("center,left,left,left,left,left,left,left,left");
        shiftGrid.setInitWidthsP("5,35,20,20,20,20,20,0,0");
        shiftGrid.enableSmartRendering(true);
        shiftGrid.enableMultiselect(false);
        shiftGrid.attachEvent("onRowSelect", function(rId, cIdn){
           if(
                shiftGrid.cells(rId, 7).getValue() == 1 || 
                shiftGrid.cells(rId, 8).getValue() == 1 ||
                shiftGrid.cells(rId, 2).getValue() == 'Lembur' || 
                shiftGrid.cells(rId, 2).getValue() == 'Lembur Shift 1' || 
                shiftGrid.cells(rId, 2).getValue() == 'Lembur Shift 2' || 
                shiftGrid.cells(rId, 2).getValue() == 'Lembur Shift 3'
            ) {
                shiftGridToolBar.disableItem("edit");
           } else {
                shiftGridToolBar.enableItem("edit");
           }
        });
        shiftGrid.attachEvent("onXLE", function() {
            mShiftLayout.cells("a").progressOff();
        });
        shiftGrid.init();

        function rShiftGrid() {
            setTimeout(() => {
                let year = $("#shift_mgt_year").val();
                let month = $("#shift_mgt_month").val();

                mShiftLayout.cells("a").progressOn();
                shiftGrid.clearAndLoad(MAbsen("getAbsenOfMonth", { empId: selectedEmpId, year, month }), totalDays);
            }, 150);
        }

        var shiftStatusBar = mShiftLayout.cells("b").attachStatusBar();
        function userSelectedCount() {
            shiftStatusBar.setText("Total karyawan dipilih: " + selectedEmp.length);
        }

        shiftForm = mShiftLayout.cells("b").attachForm([
            {type: "fieldset", offsetTop: 30, offsetLeft: 30, label: "Pilih Shift", list: [
                {type: "calendar", name: "start_date", label: "Tanggal Absen Dari", labelWidth: 130, inputWidth: 350, readonly: true, required: true},
                {type: "calendar", name: "end_date", label: "Tanggal Absen Sampai", labelWidth: 130, inputWidth: 350, readonly: true, required: true},
                {type: "combo", name: "shift_id", label: "Pilih Shift Kerja", readonly: true, labelWidth: 130, inputWidth: 350, required: true},
                {type: "checkbox", name: "is_with_weekend", label: "Sertakan Hari Libur", labelWidth: 130, inputWidth: 250},
                {type: "hidden", name: "employee", label: "Selected Emp", labelWidth: 130, inputWidth: 350},
            ]},
            {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                {type: "button", name: "add", className: "button_update", offsetLeft: 15, value: "Simpan"},
                {type: "newcolumn"},
                {type: "button", name: "clear", className: "button_clear", offsetLeft: 30, value: "Clear"}
            ]},
        ]);

        function setActiveDate() {
            let year = $("#shift_mgt_year").val();
            let month = $("#shift_mgt_month").val();

            let start = new Date(year + '-' + month + '-01');
            let end = null;
           
            if(month == 12) {
                let day = new Date(parseInt(year + 1), 1, 0).getDate();
                end = new Date((parseInt(year) + 1) + '-01-' + day);
            } else {
                let day = new Date(year, (parseInt(month) + 1), 0).getDate();
                end = new Date(year + '-' + (parseInt(month) + 1) + '-' + day);
            }

            let startDate = shiftForm.getCalendar("start_date");
            startDate.setSensitiveRange(start, end);

            let endDate = shiftForm.getCalendar("end_date");
            endDate.setSensitiveRange(start, end);
        }

        shiftForm.attachEvent("onButtonClick", function (name) {
            switch (name) {
                case "add":
                    if (!shiftForm.validate()) return eAlert("Input error!");
                    if(selectedEmp.length == 0) return eAlert("Belum ada karyawan di pilih");

                    shiftForm.setItemValue("employee", JSON.stringify(selectedEmp));
                    setDisable(["add", "clear"], shiftForm, mShiftLayout.cells("b"));

                    let shiftFormDP = new dataProcessor(MAbsen("shiftForm"));
                    shiftFormDP.init(shiftForm);
                    shiftForm.save();

                    shiftFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                    let message = tag.getAttribute("message");
                    switch (action) {
                        case "inserted":
                            if(selectedEmpId > 0) rShiftGrid();
                            setTimeout(() => { selectedEmpId = 0; }, 150);
                            sAlert("Berhasil Menambahkan Record <br>" + message);
                            doCheck(false);
                            clearAllForm(shiftForm, comboUrl);
                            setEnable(["add", "clear"], shiftForm, mShiftLayout.cells("b"));
                            break;
                        case "error":
                            eAlert("Gagal Menambahkan Record <br>" + message);
                            setEnable(["add", "clear"], shiftForm, mShiftLayout.cells("b"));
                            break;
                    }
                });
            }
        });

        var shiftCombo = shiftForm.getCombo("shift_id");
        shiftCombo.load(comboUrl.shift_id.url);

        var empStatusBar = mShiftLayout.cells("c").attachStatusBar();
        function empGridCount() {
            let empGridRows = empGrid.getRowsNum();
            empStatusBar.setText("Total baris: " + empGridRows);
        }

        var empGrid = mShiftLayout.cells("c").attachGrid();
        empGrid.setImagePath("./public/codebase/imgs/");
        empGrid.setHeader("No,<input type='checkbox' id='abs_emp_checkbox' />,Nip,Nama,Bagian,Sub Bagian,Sub Unit,Jabatan");
        empGrid.attachHeader("#rspan,#rspan,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter");
        empGrid.setColSorting("int,na,str,str,str,str,str,str");
        empGrid.setColTypes("rotxt,ch,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        empGrid.setColAlign("center,left,left,left,left,left,left,left");
        empGrid.setInitWidthsP("5,5,15,25,25,25,25,25");
        empGrid.enableSmartRendering(true);
        empGrid.enableMultiselect(false);
        empGrid.attachEvent("onRowSelect", function(rId, cIdn){
            $("#selected_emp_name").html(empGrid.cells(rId, 3).getValue());
            selectedEmpId = rId;
            setTimeout(() => {
                rShiftGrid();
            }, 150);
        });
        empGrid.attachEvent("onCheckbox", function(rId, cInd, state){
            state 
                ? selectedEmp.push(rId) 
                : selectedEmp.splice(selectedEmp.indexOf(rId), 1);
                userSelectedCount();
        });
        empGrid.attachEvent("onXLE", function() {
            mShiftLayout.cells("c").progressOff();
        });
        empGrid.init();

        function rEmpGrid() {
            mShiftLayout.cells("c").progressOn();
            empGrid.clearAndLoad(MAbsen("getManageShiftEmp", { 
                equal_department_id: userLogged.deptId, 
                equal_sub_department_id: userLogged.subId,
                equal_shift_status: 1
            }), empGridCount);
        }

        function initial() {
            rShiftGrid();
            userSelectedCount();
            rEmpGrid();
        }

        $("#abs_emp_checkbox").on("change", function() {
            let checked = document.getElementById('abs_emp_checkbox').checked;
            doCheck(checked);
        });

        function doCheck(check = true) {
            if(check) {
                for (let i = 0; i < empGrid.getRowsNum(); i++) {
                    let id = empGrid.getRowId(i);
                    selectedEmp.push(id);
                    empGrid.cells(id, 1).setValue(1);
                }
            } else {
                selectedEmp = [];
                for (let i = 0; i < empGrid.getRowsNum(); i++) {
                    let id = empGrid.getRowId(i);
                    empGrid.cells(id, 1).setValue(0);
                }
            }
            userSelectedCount();
        }

        function editShiftWindow() {

            if(!shiftGrid.getSelectedRowId()) return eAlert("Belum ada data yang di pilih!");
            if(selectedEmpId == 0) return eAlert("Belum ada karyawan yang di pilih!");

            var shiftWin = createWindow("edit_shift_win", "Ubah Shift Kerja", 500, 300);
            myWins.window("edit_shift_win").skipMyCloseEvent = true;

            var winShiftForm = shiftWin.attachForm([
                {type: "fieldset", offsetTop: 30, offsetLeft: 30, label: "Pilih Shift", list: [
                    {type: "combo", name: "shift_id", label: "Pilih Shift Kerja", readonly: true, labelWidth: 130, inputWidth: 250, required: true},
                    {type: "hidden", name: "id", label: "Selected Emp", labelWidth: 130, inputWidth: 250, value: shiftGrid.getSelectedRowId()},
                    {type: "hidden", name: "empId", label: "Selected Emp", labelWidth: 130, inputWidth: 250, value: selectedEmpId},
                ]},
                {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                    {type: "button", name: "update", className: "button_update", offsetLeft: 15, value: "Simpan"}
                ]},
            ]);

            var winShiftCombo = winShiftForm.getCombo("shift_id");
            winShiftCombo.load(comboUrl.shift_id.url);

            winShiftForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "update":
                        if (!winShiftForm.validate()) return eAlert("Input error!");

                        setDisable(["update"], winShiftForm, shiftWin);

                        let winShiftFormDP = new dataProcessor(MAbsen("shiftForm"));
                        winShiftFormDP.init(winShiftForm);
                        winShiftForm.save();

                        winShiftFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                        let message = tag.getAttribute("message");
                            switch (action) {
                                case "updated":
                                    sAlert(message);
                                    rShiftGrid();
                                    setEnable(["update"], winShiftForm, shiftWin);
                                    closeWindow("edit_shift_win");
                                    break;
                                case "error":
                                    eAlert("Gagal Mengubah Record <br>" + message);
                                    setEnable(["update"], winShiftForm, shiftWin);
                                break;
                            }
                        });
                }
            });
        }

        initial();
    }
    
JS;

header('Content-Type: application/javascript');
echo $script;
    