<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

	function showDailyMenu() {	
        var menuForm;
        var editMenuForm;
        var selectedActiveMenu = [];
        var selectedMenu = [];
        var fileError;
        var totalFile;

        var dailyLayout = mainTab.cells("ga_daily_menu").attachLayout({
            pattern: "3L",
            cells: [{
                    id: "a",
                    text: "Menu Aktif",
                    header: true,
                    collapse: false
                },
                {
                    id: "b",
                    text: "Form Menu",
                    header: true,
                    collapse: false,
                    height: 530
                },
                {
                    id: "c",
                    text: "Daftar Menu",
                    header: true,
                    collapse: false
                }
            ]
        });

        var activeMenuToolbar = dailyLayout.cells("a").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "nonaktive", text: "Nonaktifkan", type: "button", img: "right_arrow.png"},
                {id: "update", text: "Update", type: "button", img: "update.png"},
                {id: "refresh", text: "Refresh", type: "button", img: "refresh.png"},
            ]
        });

        activeMenuToolbar.attachEvent("onClick", function(id) {
            switch (id) {
                case "nonaktive":
                    if(selectedActiveMenu.length == 0) return eAlert('Belum ada menu dipilih');
                    dailyLayout.cells("c").progressOn();
                    reqJson(GADailyMenu("activate"), "POST", {menuIds: selectedActiveMenu, is_active: 0}, (err, res) => {
                        if(res.status === "success") {
                            document.getElementById('daily_menu_act_checkbox').checked = false;
                            init();
                            selectedActiveMenu = [];
                            dailyLayout.cells("a").progressOff()
                            sAlert(res.message);
                        } else {
                            eAlert(res.message);
                        }
                    });
                    break;
                case "update":
                    if(!actMenuGrid.getChangedRows()) {
                        return eAlert("Belum ada row yang di edit!");
                    }

                    activeMenuToolbar.disableItem("update");
                    dailyLayout.cells("a").progressOn();
                    actMenuGridDP.sendData();
                    actMenuGridDP.attachEvent('onAfterUpdate', function(id, action, tid, tag) {
                        let message = tag.getAttribute('message');
                        switch (action) {
                            case 'updated':
                                sAlert(message);
                                rActMenuGrid();
                                activeMenuToolbar.enableItem("update");
                                dailyLayout.cells("a").progressOff();
                                setActiveMenuGridDP();
                                break;
                            case 'error':
                                eAlert(message);
                                activeMenuToolbar.enableItem("update");
                                dailyLayout.cells("a").progressOff();
                                break;
                        }
                    });
                    break;
                case "refresh":
                    rActMenuGrid();
                    break;
            }
        });

        var menuToolbar = dailyLayout.cells("c").attachToolbar({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "active", text: "Aktifkan", type: "button", img: "left_arrow.png"},
                {id: "edit", text: "Edit", type: "button", img: "edit.png"},
                {id: "delete", text: "Hapus", type: "button", img: "delete.png"},
                {id: "refresh", text: "Refresh", type: "button", img: "refresh.png"},
            ]
        });

        menuToolbar.attachEvent("onClick", function(id) {
            switch (id) {
                case "active":
                    if(selectedMenu.length == 0) return eAlert('Belum ada menu dipilih');
                    dailyLayout.cells("c").progressOn();
                    reqJson(GADailyMenu("activate"), "POST", {menuIds: selectedMenu, is_active: 1}, (err, res) => {
                        if(res.status === "success") {
                            document.getElementById('daily_menu_checkbox').checked = false;
                            init();
                            selectedMenu = [];
                            dailyLayout.cells("c").progressOff()
                            sAlert(res.message);
                        } else {
                            eAlert(res.message);
                        }
                    });
                    break;
                case "edit":
                    editMenuHandler();
                    break;
                case "delete":
                    if (!menuGrid.getSelectedRowId()) {
                        return eAlert("Pilih baris yang akan dihapus!");
                    }

                    reqAction(menuGrid, GADailyMenu("menuDelete"), 1, (err, res) => {
                        rMenuGrid();
                        addMenuHandler();
                        res.mSuccess && sAlert("Sukses Menghapus Record <br>" + res.mSuccess);
                        res.mError && eAlert("Gagal Menghapus Record <br>" + res.mError);
                    });
                    break;
                case "refresh":
                    rMenuGrid();
                    break;
            }
        });

        function addMenuHandler() {
            menuForm = dailyLayout.cells("b").attachForm([
                {type: "fieldset", offsetTop: 30, offsetLeft: 30, label: "Menu Form", list: [
                    {type: "input", name: "name", label: "Nama Menu", labelWidth: 130, inputWidth: 350, required: true},
                    {type: "input", name: "description", label: "Deskripsi", labelWidth: 130, inputWidth: 350, required: true},
                    {type: "hidden", name: "filename", label: "Filename", readonly: true},
                    {type: "upload", name: "file_uploader", inputWidth: 420,
                        url: AppMaster("fileUpload", {save: false, folder: "daily_menus"}), 
                        swfPath: "./public/codebase/ext/uploader.swf", 
                        swfUrl: AppMaster("fileUpload")
                    },
                ]},
                {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                    {type: "button", name: "add", className: "button_update", offsetLeft: 15, value: "Simpan"},
                    {type: "newcolumn"},
                    {type: "button", name: "clear", className: "button_clear", offsetLeft: 30, value: "Clear"}
                ]},
            ]);

            menuForm.attachEvent("onBeforeFileAdd", async function (filename, size) {
                beforeFileAdd(menuForm, {filename, size});
            });

            menuForm.attachEvent("onBeforeFileUpload", function(mode, loader, formData){
                if(fileError) {
                    clearUploader(menuForm, "file_uploader");
                    eAlert("File error silahkan upload file sesuai ketentuan!");
                    fileError = false;
                } else {
                    return true;
                }
            });

            menuForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "add":
                        const uploader = menuForm.getUploader("file_uploader");
                        if(uploader.getStatus() === -1) {
                            if(!fileError) {
                                uploader.upload();
                            } else {
                                uploader.clear();
                                eAlert("File error silahkan upload file sesuai ketentuan!");
                                fileError = false;
                            }
                        } else {
                            addMenuFormSubmit();
                        }
                        break;
                    case "clear":
                        clearAllForm(menuForm);
                        clearUploader(menuForm, "file_uploader");
                        break;
                }
            });

            menuForm.attachEvent("onUploadFile", function(filename, servername){
                menuForm.setItemValue("filename", servername);
                addMenuFormSubmit();
            });

            function addMenuFormSubmit() {
                if (!menuForm.validate()) return eAlert("Input error!");

                setDisable(["add", "clear"], menuForm, dailyLayout.cells("b"));

                let menuFormDP = new dataProcessor(GADailyMenu("menuForm"));
                menuFormDP.init(menuForm);
                menuForm.save();

                menuFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                    let message = tag.getAttribute("message");
                    switch (action) {
                        case "inserted":
                            sAlert("Berhasil Menambahkan Record <br>" + message);
                            clearAllForm(menuForm);
                            clearUploader(menuForm, "file_uploader");
                            setEnable(["add", "clear"], menuForm, dailyLayout.cells("b"));
                            rMenuGrid();
                            break;
                        case "error":
                            eAlert("Gagal Menambahkan Record <br>" + message);
                            setEnable(["add", "clear"], menuForm, dailyLayout.cells("b"));
                            break;
                    }
                });
            }
        }

        function editMenuHandler() {
            if (!menuGrid.getSelectedRowId()) {
                return eAlert("Pilih baris yang akan diupdate!");
            }

            editMenuForm = dailyLayout.cells("b").attachForm([
                {type: "fieldset", offsetTop: 30, offsetLeft: 30, label: "Menu Form", list: [
                    {type: "hidden", name: "id", label: "ID", labelWidth: 130, inputWidth: 350},
                    {type: "hidden", name: "filename", label: "filename", readonly: true},
                    {type: "input", name: "name", label: "Nama Menu", labelWidth: 130, inputWidth: 350, required: true},
                    {type: "input", name: "description", label: "Deskripsi", labelWidth: 130, inputWidth: 350, required: true},
                    {type: "upload", name: "file_uploader", inputWidth: 420,
                        url: AppMaster("fileUpload", {save: false, folder: "daily_menus"}), 
                        swfPath: "./public/codebase/ext/uploader.swf", 
                        swfUrl: AppMaster("fileUpload"),
                        autoStart: true
                    },

                    {type: "block", offsetLeft: 30, offsetTop: 10, list: [
                        {type: "button", name: "update", className: "button_update", offsetLeft: 15, value: "Simpan"},
                        {type: "newcolumn"},
                        {type: "button", name: "cancel", className: "button_no", offsetLeft: 30, value: "Cancel"}
                    ]},
                ]},
               
                {type: "fieldset", offsetTop: 30, offsetLeft: 30, label: "Foto Menu", list: [
                    {type: "container", name : "file_display", label: "<img src='./public/img/no-image.png' height='120' width='120'>"}
                ]},
            ]);

            const loadTemp = (filename) => {
                if (filename.length === 0) {
                    editMenuForm.setItemLabel("file_display", "<img src='./public/img/no-image.png' height='120' width='120'>");
                } else {
                    filename.map(file => {
                        if(file === '') {
                            editMenuForm.setItemLabel("file_display", "<img src='./public/img/no-image.png' height='120' width='120'>");
                        } else {
                            var fotoDisplay = "<img src='./assets/images/daily_menus/"+file+"' height='120' width='120'>"
                            editMenuForm.setItemLabel("file_display", fotoDisplay);
                        }
                    });
                }	
            }

            fetchFormData(GADailyMenu("menuForm", {id: menuGrid.getSelectedRowId()}), editMenuForm, ["filename"], loadTemp);

            editMenuForm.attachEvent("onBeforeFileAdd", async function (filename, size) {
                beforeFileAdd(editMenuForm, {filename, size}, editMenuForm.getItemValue("id"));
            });

            editMenuForm.attachEvent("onBeforeFileUpload", function(mode, loader, formData){
                if(fileError) {
                    clearUploader(editMenuForm, "file_uploader");
                    eAlert("File error silahkan upload file sesuai ketentuan!");
                    fileError = false;
                } else {
                    return true;
                }
            });

            editMenuForm.attachEvent("onUploadFile", function(filename, servername){
                reqJson(AppMaster("updateAfterUpload"), "POST", {
                    id: editMenuForm.getItemValue("id"),
                    oldFile: editMenuForm.getItemValue("filename"),
                    filename: servername,
                    folder: "kf_general.menus",
                    customFolder: "daily_menus"
                }, (err, res) => {
                    if(res.status === "success") {
                        editMenuForm.setItemValue("filename", servername);
                        clearUploader(editMenuForm, "file_uploader");
                        editMenuForm.setItemLabel("file_display", "<img src='./assets/images/daily_menus/"+servername+"' height='120' width='120'>");
                        sAlert(res.message);
                    } else {
                        eAlert(res.message);
                    }
                });
            });  

            editMenuForm.attachEvent("onButtonClick", function (name) {
                switch (name) {
                    case "update":
                        if (!editMenuForm.validate()) return eAlert("Input error!");

                        setDisable(["update", "cancel"], editMenuForm, dailyLayout.cells("b"));

                        let editMenuFormDP = new dataProcessor(GADailyMenu("menuForm"));
                        editMenuFormDP.init(editMenuForm);
                        editMenuForm.save();

                        editMenuFormDP.attachEvent("onAfterUpdate", function (id, action, tid, tag) {
                            let message = tag.getAttribute("message");
                            switch (action) {
                                case "updated":
                                    sAlert("Berhasil Mengubah Record <br>" + message);
                                    dailyLayout.cells("b").progressOff();
                                    addMenuHandler();
                                    rMenuGrid();
                                    break;
                                case "error":
                                    eAlert("Gagal Mengubah Record <br>" + message);
                                    dailyLayout.cells("b").progressOff();
                                    addMenuHandler();
                                    rMenuGrid();
                                    break;
                            }
                        });
                        break;
                    case "cancel":
                        addMenuHandler();
                        break;
                }
            });
        }

        var menuStatusBar = dailyLayout.cells("c").attachStatusBar();
        function menuGridCount() {
            menuStatusBar.setText("Total baris: " + menuGrid.getRowsNum());
        }

        var menuGrid = dailyLayout.cells("c").attachGrid();
        menuGrid.setImagePath("./public/codebase/imgs/");
        menuGrid.setHeader("No,<input type='checkbox' id='daily_menu_checkbox' />,Nama,Deskripsi,Available,Picked,Di Buat, Di Update");
        menuGrid.attachHeader("#rspan,#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
        menuGrid.setColSorting("int,na,str,str,str,str,str,str");
        menuGrid.setColTypes("rotxt,ch,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        menuGrid.setColAlign("center,left,left,left,left,left,center,center");
        menuGrid.setInitWidthsP("5,5,25,35,0,0,15,15");
        menuGrid.enableSmartRendering(true);
        menuGrid.enableMultiselect(false);
        menuGrid.attachEvent("onCheckbox", function(rId, cInd, state){
            state 
                ? selectedMenu.push(rId) 
                : selectedMenu.splice(selectedMenu.indexOf(rId), 1);
        });
        menuGrid.attachEvent("onXLE", function() {
            dailyLayout.cells("c").progressOff();
        });
        menuGrid.init();

        function rMenuGrid() {
            dailyLayout.cells("c").progressOn();
            menuGrid.clearAndLoad(GADailyMenu("getMenus", { 
                equal_is_active: 0
            }), menuGridCount);
        }

        $("#daily_menu_checkbox").on("change", function() {
            let checked = document.getElementById('daily_menu_checkbox').checked;
            doCheck(checked);
        });

        function doCheck(check = true) {
            if(check) {
                for (let i = 0; i < menuGrid.getRowsNum(); i++) {
                    let id = menuGrid.getRowId(i);
                    selectedMenu.push(id);
                    menuGrid.cells(id, 1).setValue(1);
                }
            } else {
                selectedMenu = [];
                for (let i = 0; i < menuGrid.getRowsNum(); i++) {
                    let id = menuGrid.getRowId(i);
                    menuGrid.cells(id, 1).setValue(0);
                }
            }
        }

        var actMenuStatusBar = dailyLayout.cells("a").attachStatusBar();
        function actMenuGridCount() {
            actMenuStatusBar.setText("Total baris: " + actMenuGrid.getRowsNum());
        }

        var actMenuGrid = dailyLayout.cells("a").attachGrid();
        actMenuGrid.setImagePath("./public/codebase/imgs/");
        actMenuGrid.setHeader("No,<input type='checkbox' id='daily_menu_act_checkbox' />,Nama,Deskripsi,Available,Picked,Di Buat, Di Update");
        actMenuGrid.attachHeader("#rspan,#rspan,#text_search,#text_search,#text_search,#text_search,#text_search,#text_search");
        actMenuGrid.setColSorting("int,na,str,str,str,str,str,str");
        actMenuGrid.setColTypes("rotxt,ch,edn,edn,edn,edn,rotxt,rotxt");
        actMenuGrid.setColAlign("center,left,left,left,center,center,left,left");
        actMenuGrid.setInitWidthsP("5,5,30,40,10,10,0,0");
        actMenuGrid.enableSmartRendering(true);
        actMenuGrid.enableMultiselect(false);
        actMenuGrid.setEditable(true);
        actMenuGrid.setNumberFormat("0,000",4,".",",");
        actMenuGrid.setNumberFormat("0,000",5,".",",");
        actMenuGrid.attachEvent("onCheckbox", function(rId, cInd, state){
            state 
                ? selectedActiveMenu.push(rId) 
                : selectedActiveMenu.splice(actMenuGrid.indexOf(rId), 1);
        });
        actMenuGrid.attachEvent("onXLE", function() {
            dailyLayout.cells("a").progressOff();
        });
        actMenuGrid.init();

        $("#daily_menu_act_checkbox").on("change", function() {
            let checked = document.getElementById('daily_menu_act_checkbox').checked;
            doCheckActive(checked);
        });

        function doCheckActive(check = true) {
            if(check) {
                for (let i = 0; i < actMenuGrid.getRowsNum(); i++) {
                    let id = actMenuGrid.getRowId(i);
                    selectedActiveMenu.push(id);
                    actMenuGrid.cells(id, 1).setValue(1);
                }
            } else {
                selectedActiveMenu = [];
                for (let i = 0; i < actMenuGrid.getRowsNum(); i++) {
                    let id = actMenuGrid.getRowId(i);
                    actMenuGrid.cells(id, 1).setValue(0);
                }
            }
        }

        function setActiveMenuGridDP() {
            actMenuGridDP = new dataProcessor(GADailyMenu('updateMenuQuantity'));
            actMenuGridDP.setTransactionMode("POST", true);
            actMenuGridDP.setUpdateMode("Off");
            actMenuGridDP.init(actMenuGrid);
        }

        function rActMenuGrid() {
            dailyLayout.cells("a").progressOn();
            actMenuGrid.clearAndLoad(GADailyMenu("getMenus", { 
                equal_is_active: 1
            }), actMenuGridCount);
        }

        function init() {
            addMenuHandler();
            rActMenuGrid();
            setActiveMenuGridDP();
            rMenuGrid();
        }

        init();

        async function beforeFileAdd(form, file, id = null) {
            if(form.validate()) {
                var ext = file.filename.split(".").pop();
                if (ext == "png" || ext == "jpg" || ext == "jpeg") {
                    if (file.size > 1000000) {
                        fileError = true;
                        eAlert("Tidak boleh melebihi 1 MB!");
                    } else {
                        if(totalFile > 0) {
                            eAlert("Maksimal 1 file");
                            fileError = true;
                        } else {
                            const data = id ? {id, name: form.getItemValue("name")} : {name: form.getItemValue("name")}
                            const check = await reqJsonResponse(GADailyMenu("checkBeforeAddFile"), "POST", data);

                            if(check) {
                                if(check.status === "success") {
                                    totalFile++;
                                    return true;
                                } else if(check.status === "deleted") {
                                    fileError = false;
                                    totalFile= 0;
                                } else {
                                    eAlert(check.message);
                                    fileError = true;
                                }
                            }
                        }
                    }		    
                } else {
                    eAlert("Hanya png, jpg & jpeg saja yang bisa diupload!");
                    fileError = true;
                }
            } else {
                eAlert("Input error!");
            }	
        }

    }

JS;

header('Content-Type: application/javascript');
echo $script;