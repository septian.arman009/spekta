<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function showCatchMonitoring() {
        let currentDate = getCurrentDate(new Date());

        var shift1 = 0;
        var countShift1 = 0;
        var mealShift1 = 0;
        var mealNeedShift1 = 0;

        var shift2 = 0;
        var countShift2 = 0;
        var mealShift2 = 0;
        var mealNeedShift2 = 0;

        var shift3 = 0;
        var countShift3 = 0;
        var mealShift3 = 0;
        var mealNeedShift3 = 0;

        var ovtShift1 = 0;
        var ovtCountShift1 = 0;
        var mealOvtCountShift1 = 0;
        var mealNeedOvtCountShift1 = 0;

        var ovtShift2 = 0;
        var ovtCountShift2 = 0;
        var mealOvtCountShift2 = 0;
        var mealNeedOvtCountShift2 = 0;

        var ovtShift3 = 0;
        var ovtCountShift3 = 0;
        var mealOvtCountShift3 = 0;
        var mealNeedOvtCountShift3 = 0;

        var monitoringTabs = mainTab.cells("ga_monitoring").attachTabbar({
            tabs: [
                {id: "a", text: "Monitoring Katering", active: true},
                {id: "b", text: "Cost Center Summary"},
                {id: "c", text: "Cost Center Detail <span id='selected_cost_center'>(Silahkan Pilih Cost Center Dari Table Cost Center Summary)</span>"},
            ]
        });

        var monitorLayout = monitoringTabs.cells("a").attachLayout({
            pattern: "3L",
            cells: [{
                    id: "a",
                    text: "Daftar Absen Karyawan",
                    header: true,
                    collapse: false
                },
                {
                    id: "b",
                    text: "Kapasitas Kantin",
                    header: true,
                    collapse: false,
                    height: 360
                },
                {
                    id: "c",
                    text: "Daftar Menu Aktif",
                    header: true,
                    collapse: false
                }
            ]
        });

        var absenMenu = monitorLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "filter", text: "<div style='width:100%'>Filter: <input type='text' id='canteen_monitoring_date' readonly value='"+currentDate+"' /> <button id='monitoring_filter'>Proses</button></div>"},
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
            ]
        });

        var filterCalendar = new dhtmlXCalendarObject(["canteen_monitoring_date"]);

        $("#monitoring_filter").on("click", function() {
            rAbsenGrid();
        });

        absenMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "refresh":
                    rAbsenGrid();
                    break;
                case "export":
                    absenGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
            }
        });

        function countCathering() {
            shift1 = 0;
            countShift1 = 0;
            mealShift1 = 0;
            mealNeedShift1 = 0;

            shift2 = 0;
            countShift2 = 0;
            mealShift2 = 0;
            mealNeedShift2 = 0;

            shift3 = 0;
            countShift3 = 0;
            mealShift3 = 0;
            mealNeedShift3 = 0;

            ovtShift1 = 0;
            ovtCountShift1 = 0;
            mealOvtCountShift1 = 0;
            mealNeedOvtCountShift1 = 0;

            ovtShift2 = 0;
            ovtCountShift2 = 0;
            mealOvtCountShift2 = 0;
            mealNeedOvtCountShift2 = 0;

            ovtShift3 = 0;
            ovtCountShift3 = 0;
            mealOvtCountShift3 = 0;
            mealNeedOvtCountShift3 = 0;

            if(absenGrid.getRowsNum() > 0) {
                for (let i = 0; i < absenGrid.getRowsNum(); i++) {
                    let id = absenGrid.getRowId(i);
                    let shift = absenGrid.cells(id, 3).getValue();
                    let shiftIn = absenGrid.cells(id, 6).getValue();
                    let meal = absenGrid.cells(id, 9).getValue();
                    let mealNeed = absenGrid.cells(id, 10).getValue();

                    if(shift == 'Shift 1') {
                        shift1++;
                        if(shiftIn != '-') countShift1++;

                        mealShift1 += parseInt(meal);
                        mealNeedShift1 += parseInt(mealNeed);
                    } else if(shift == 'Shift 2') {
                        shift2++;
                        if(shiftIn != '-') countShift2++;

                        mealShift2 += parseInt(meal);
                        mealNeedShift2 += parseInt(mealNeed);
                    } else if(shift == 'Shift 3') {
                        shift3++;
                        if(shiftIn != '-') countShift3++;

                        mealShift3 += parseInt(meal);
                        mealNeedShift3 += parseInt(mealNeed);
                    } else if(shift == 'Lembur Shift 1') {
                        ovtShift1++;
                        if(shiftIn != '-') ovtCountShift1++;

                        mealOvtCountShift1 += parseInt(meal);
                        mealNeedOvtCountShift1 += parseInt(mealNeed);
                    } else if(shift == 'Lembur Shift 2') {
                        ovtShift2++;
                        if(shiftIn != '-') ovtCountShift2++;

                        mealOvtCountShift2 += parseInt(meal);
                        mealNeedOvtCountShift2 += parseInt(mealNeed);
                    } else if(shift == 'Lembur Shift 3') {
                        ovtShift3++;
                        if(shiftIn != '-') ovtCountShift3++;

                        mealOvtCountShift3 += parseInt(meal);
                        mealNeedOvtCountShift3 += parseInt(mealNeed);
                    }

                    $('#shift_1').html(countShift1 + ' / ' + shift1);
                    $('#meal_shift_1').html(mealShift1);
                    $('#meal_need_shift_1').html(mealNeedShift1);

                    $('#shift_2').html(countShift2 + ' / ' + shift2);
                    $('#meal_shift_2').html(mealShift2);
                    $('#meal_need_shift_2').html(mealNeedShift2);

                    $('#shift_3').html(countShift3 + ' / ' + shift3);
                    $('#meal_shift_3').html(mealShift3);
                    $('#meal_need_shift_3').html(mealNeedShift3);

                    $('#ovt_shift_1').html(ovtCountShift1 + ' / ' + ovtShift1);
                    $('#meal_ovt_shift_1').html(mealOvtCountShift1);
                    $('#meal_need_ovt_shift_1').html(mealNeedOvtCountShift1);

                    $('#ovt_shift_2').html(ovtCountShift2 + ' / ' + ovtShift2);
                    $('#meal_ovt_shift_2').html(mealOvtCountShift2);
                    $('#meal_need_ovt_shift_2').html(mealNeedOvtCountShift2);

                    $('#ovt_shift_3').html(ovtCountShift3 + ' / ' + ovtShift3);
                    $('#meal_ovt_shift_3').html(mealOvtCountShift3);
                    $('#meal_need_ovt_shift_3').html(mealNeedOvtCountShift3);
                }
            } else {
                $('#shift_1').html(countShift1 + ' / ' + shift1);
                $('#shift_2').html(countShift2 + ' / ' + shift2);
                $('#shift_3').html(countShift3 + ' / ' + shift3);
                $('#ovt_shift_1').html(ovtCountShift1 + ' / ' + ovtShift1);
                $('#ovt_shift_2').html(ovtCountShift2 + ' / ' + ovtShift2);
                $('#ovt_shift_3').html(ovtCountShift3 + ' / ' + ovtShift3);
            }
        }

        var absenGrid = monitorLayout.cells("a").attachGrid();
        absenGrid.setImagePath("./public/codebase/imgs/");
        absenGrid.setHeader("No,Nama Karyawan,Tanggal,Shift Kerja,Jadwal Masuk,Jadwal Keluar,Absen Masuk,Cost Center,Nama Cost Center,Sudah Ambil Makan,Kebutuhan Makan");
        absenGrid.attachHeader("#rspan,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter");
        absenGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str");
        absenGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        absenGrid.setColAlign("center,left,left,left,left,left,left,left,left,center,center");
        absenGrid.setInitWidthsP("7,30,20,20,30,30,30,15,35,15,15");
        absenGrid.enableSmartRendering(true);
        absenGrid.enableMultiselect(false);
        absenGrid.attachFooter(",Keterangan,Total Masuk / Total Shift,Sudah Ambil Makan,Kebutuhan Makan");
        absenGrid.attachFooter(",Shift 1,<div id='shift_1'>0 / 0</div>,<div id='meal_shift_1'>0</div>,<div id='meal_need_shift_1'>0</div>");
        absenGrid.attachFooter(",Shift 2,<div id='shift_2'>0 / 0</div>,<div id='meal_shift_2'>0</div>,<div id='meal_need_shift_2'>0</div>");
        absenGrid.attachFooter(",Shift 3,<div id='shift_3'>0 / 0</div>,<div id='meal_shift_3'>0</div>,<div id='meal_need_shift_3'>0</div>");
        absenGrid.attachFooter(",Lembur Shift 1,<div id='ovt_shift_1'>0 / 0</div>,<div id='meal_ovt_shift_1'>0</div>,<div id='meal_need_ovt_shift_1'>0</div>");
        absenGrid.attachFooter(",Lembur Shift 2,<div id='ovt_shift_2'>0 / 0</div>,<div id='meal_ovt_shift_2'>0</div>,<div id='meal_need_ovt_shift_2'>0</div>");
        absenGrid.attachFooter(",Lembur Shift 3,<div id='ovt_shift_3'>0 / 0</div>,<div id='meal_ovt_shift_3'>0</div>,<div id='meal_need_ovt_shift_3'>0</div>");
        absenGrid.attachEvent("onXLE", function() {
            monitorLayout.cells("a").progressOff();
        });

        absenGrid.init();
        function rAbsenGrid() {
            monitorLayout.cells("a").progressOn();
            var date = $("#canteen_monitoring_date").val();
            absenGrid.clearAndLoad(GAOther("getAbsenGrid", {date: date}), countCathering);
            rCostSummaryGrid();
        }

        monitorLayout.cells("b").attachURL(BASE_URL + "index.php?d=erp/g_affair&c=OtherController&m=canteenCapacity");
        monitorLayout.cells("c").attachURL(BASE_URL + "index.php?d=erp/g_affair&c=OtherController&m=canteenMenuList");

        var costSummaryMenu = monitoringTabs.cells("b").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
            ]
        });

        costSummaryMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "refresh":
                    rCostSummaryGrid();
                    break;
                case "export":
                    costSummaryGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
            }
        });

        function countCostSummaryCathering() {
            $('#cs_shift_1').html(countShift1 + ' / ' + shift1);
            $('#meal_cs_shift_1').html(mealShift1);
            $('#meal_need_cs_shift_1').html(mealNeedShift1);

            $('#cs_shift_2').html(countShift2 + ' / ' + shift2);
            $('#meal_cs_shift_2').html(mealShift2);
            $('#meal_need_cs_shift_2').html(mealNeedShift2);

            $('#cs_shift_3').html(countShift3 + ' / ' + shift3);
            $('#meal_cs_shift_3').html(mealShift3);
            $('#meal_need_cs_shift_3').html(mealNeedShift3);

            $('#cs_ovt_shift_1').html(ovtCountShift1 + ' / ' + ovtShift1);
            $('#meal_cs_ovt_shift_1').html(mealOvtCountShift1);
            $('#meal_need_cs_ovt_shift_1').html(mealNeedOvtCountShift1);

            $('#cs_ovt_shift_2').html(ovtCountShift2 + ' / ' + ovtShift2);
            $('#meal_cs_ovt_shift_2').html(mealOvtCountShift2);
            $('#meal_need_cs_ovt_shift_2').html(mealNeedOvtCountShift2);
            
            $('#cs_ovt_shift_3').html(ovtCountShift3 + ' / ' + ovtShift3);
            $('#meal_cs_ovt_shift_3').html(mealOvtCountShift3);
            $('#meal_need_cs_ovt_shift_3').html(mealNeedOvtCountShift3);
        }

        var costSummaryGrid = monitoringTabs.cells("b").attachGrid();
        costSummaryGrid.setImagePath("./public/codebase/imgs/");
        costSummaryGrid.setHeader("No,Nama Cost Center,Cost Center,Shift 1,Lembur Shift 1,Shift 2,Lembur Shift 2,Shift 3,Lembur Shift 3");
        costSummaryGrid.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
        costSummaryGrid.setColSorting("int,str,str,str,str,str,str,str,str,str");
        costSummaryGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        costSummaryGrid.setColAlign("center,left,left,left,left,left,left,left,left,left");
        costSummaryGrid.setInitWidthsP("5,25,10,10,10,10,10,10,10,10");
        costSummaryGrid.enableSmartRendering(true);
        costSummaryGrid.enableMultiselect(false);
        costSummaryGrid.attachFooter(",Keterangan,Total Masuk / Total Shift,Sudah Ambil Makan,Kebutuhan Makan");
        costSummaryGrid.attachFooter(",Shift 1,<div id='cs_shift_1'>0 / 0</div>,<div id='meal_cs_shift_1'>0</div>,<div id='meal_need_cs_shift_1'>0</div>");
        costSummaryGrid.attachFooter(",Shift 2,<div id='cs_shift_2'>0 / 0</div>,<div id='meal_cs_shift_2'>0</div>,<div id='meal_need_cs_shift_2'>0</div>");
        costSummaryGrid.attachFooter(",Shift 3,<div id='cs_shift_3'>0 / 0</div>,<div id='meal_cs_shift_3'>0</div>,<div id='meal_need_cs_shift_3'>0</div>");
        costSummaryGrid.attachFooter(",Lembur Shift 1,<div id='cs_ovt_shift_1'>0 / 0</div>,<div id='meal_cs_ovt_shift_1'>0</div>,<div id='meal_need_cs_ovt_shift_1'>0</div>");
        costSummaryGrid.attachFooter(",Lembur Shift 2,<div id='cs_ovt_shift_2'>0 / 0</div>,<div id='meal_cs_ovt_shift_2'>0</div>,<div id='meal_need_cs_ovt_shift_2'>0</div>");
        costSummaryGrid.attachFooter(",Lembur Shift 3,<div id='cs_ovt_shift_3'>0 / 0</div>,<div id='meal_cs_ovt_shift_3'>0</div>,<div id='meal_need_cs_ovt_shift_3'>0</div>");
        costSummaryGrid.attachEvent("onRowSelect", function(rId, cIdn){
            const costCenter = costSummaryGrid.cells(rId, 2).getValue();
            const costCenterName = costSummaryGrid.cells(rId, 1).getValue();
            rCostDetailGrid(costCenter, costCenterName);
        });
        costSummaryGrid.attachEvent("onXLE", function() {
            monitoringTabs.cells("b").progressOff();
        });

        costSummaryGrid.init();
        function rCostSummaryGrid() {
            monitoringTabs.cells("b").progressOn();
            var date = $("#canteen_monitoring_date").val();
            costSummaryGrid.clearAndLoad(GAOther("getCostCenterCummaryGrid", {date: date}), countCostSummaryCathering);
        }

        rAbsenGrid();

        var costDetailMenu = monitoringTabs.cells("c").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"},
            ]
        });

        costDetailMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "refresh":
                    rCostDetailGrid();
                    break;
                case "export":
                    costDetailGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
            }
        });

        function countDetailCathering() {
            let dShift1 = 0;
            let dCountShift1 = 0;
            let dShift2 = 0;
            let dCountShift2 = 0;
            let dShift3 = 0;
            let dCountShift3 = 0;

            let dOvtShift1 = 0;
            let dOvtCountShift1 = 0;
            let dOvtShift2 = 0;
            let dOvtCountShift2 = 0;
            let dOvtShift3 = 0;
            let dOvtCountShift3 = 0;

            if(costDetailGrid.getRowsNum() > 0) {
                for (let i = 0; i < costDetailGrid.getRowsNum(); i++) {
                    let id = costDetailGrid.getRowId(i);
                    let shift = costDetailGrid.cells(id, 3).getValue();
                    let shiftIn = costDetailGrid.cells(id, 5).getValue();

                    if(shift == 'Shift 1') {
                        dShift1++;
                        if(shiftIn != '-') dCountShift1++;
                    } else if(shift == 'Shift 2') {
                        dShift2++;
                        if(shiftIn != '-') dCountShift2++;
                    } else if(shift == 'Shift 3') {
                        dShift3++;
                        if(shiftIn != '-') dCountShift3++;
                    } else if(shift == 'Lembur Shift 1') {
                        dOvtShift1++;
                        if(shiftIn != '-') dOvtCountShift1++;
                    } else if(shift == 'Lembur Shift 2') {
                        dOvtShift2++;
                        if(shiftIn != '-') dOvtCountShift2++;
                    } else if(shift == 'Lembur Shift 3') {
                        dOvtShift3++;
                        if(shiftIn != '-') dOvtCountShift3++;
                    }

                    $('#detail_shift_1').html(dCountShift1 + ' / ' + dShift1);
                    $('#detail_shift_2').html(dCountShift2 + ' / ' + dShift2);
                    $('#detail_shift_3').html(dCountShift3 + ' / ' + dShift3);
                    $('#detail_ovt_shift_1').html(dOvtCountShift1 + ' / ' + dOvtShift1);
                    $('#detail_ovt_shift_2').html(dOvtCountShift2 + ' / ' + dOvtShift2);
                    $('#detail_ovt_shift_3').html(dOvtCountShift3 + ' / ' + dOvtShift3);
                }
            } else {
                $('#detail_shift_1').html(dCountShift1 + ' / ' + dShift1);
                $('#detail_shift_2').html(dCountShift2 + ' / ' + dShift2);
                $('#detail_shift_3').html(dCountShift3 + ' / ' + dShift3);
                $('#detail_ovt_shift_1').html(dOvtCountShift1 + ' / ' + dOvtShift1);
                $('#detail_ovt_shift_2').html(dOvtCountShift2 + ' / ' + dOvtShift2);
                $('#detail_ovt_shift_3').html(dOvtCountShift3 + ' / ' + dOvtShift3);
            }
        }

        var costDetailGrid = monitoringTabs.cells("c").attachGrid();
        costDetailGrid.setImagePath("./public/codebase/imgs/");
        costDetailGrid.setHeader("No,Nama Karyawan,Tanggal,Shift Kerja,Jadwal Masuk,Jadwal Keluar,Absen Masuk,Cost Center,Nama Cost Center");
        costDetailGrid.attachHeader("#rspan,#text_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter");
        costDetailGrid.setColSorting("int,str,str,str,str,str,str,str,str");
        costDetailGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        costDetailGrid.setColAlign("center,left,left,left,left,left,left,left,left");
        costDetailGrid.setInitWidthsP("7,25,10,10,15,15,15,10,25");
        costDetailGrid.enableSmartRendering(true);
        costDetailGrid.enableMultiselect(false);
        costDetailGrid.attachFooter(",Keterangan,Total Masuk / Total Shift");
        costDetailGrid.attachFooter(",Shift 1,<div id='detail_shift_1'>0 / 0</div>");
        costDetailGrid.attachFooter(",Shift 2,<div id='detail_shift_2'>0 / 0</div>");
        costDetailGrid.attachFooter(",Shift 3,<div id='detail_shift_3'>0 / 0</div>");
        costDetailGrid.attachFooter(",Lembur Shift 1,<div id='detail_ovt_shift_1'>0 / 0</div>");
        costDetailGrid.attachFooter(",Lembur Shift 2,<div id='detail_ovt_shift_2'>0 / 0</div>");
        costDetailGrid.attachFooter(",Lembur Shift 3,<div id='detail_ovt_shift_3'>0 / 0</div>");
        costDetailGrid.attachEvent("onXLE", function() {
            monitoringTabs.cells("c").progressOff();
        });

        costDetailGrid.init();
        function rCostDetailGrid(costCenter, costCenterName) {
            monitoringTabs.cells("c").progressOn();
            var date = $("#canteen_monitoring_date").val();
            $("#selected_cost_center").html(" (" + costCenterName +")");
            costDetailGrid.clearAndLoad(GAOther("getAbsenGrid", { costCenter: costCenter, date: date }), countDetailCathering);
        }
    }

JS;

header('Content-Type: application/javascript');
echo $script;