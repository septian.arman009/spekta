<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function showDashAbsenSum(){
        var type = "line";
        var yearName = "dash_absen_year_summary";
        var monthName = "dash_absen_month_summary";

        var date = new Date();
        var currentYear = date.getFullYear();
        var currentMonth = date.getMonth() + 1 < 10 ? "0"+(date.getMonth() + 1) : date.getMonth() + 1;
        var currentDay = date.getDate() < 10 ? "0"+date.getDate() : date.getDate();


        var sumAbsenLayout = mainTab.cells("dashboard_absen_summary").attachLayout({
            pattern: "3T",
            cells: [
                {id: "a", header: false},
                {id: "b", header: true, text: 'Shift Hari Ini ( '+currentDay+'/'+currentMonth+'/'+currentYear+' )'},
                {id: "c", header: false},
            ]
        });

        var sumAbsenMenu =  mainTab.cells("dashboard_absen_summary").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "month", text: genSelectMonth(yearName, monthName)},
                {id: "line", text: "Line Chart", img: "double_chart.png"},
                {id: "column", text: "Bar Chart", img: "bar_chart.png"},
                {id: "refresh", text: "Refresh", img: "resize.png"},
                {id: "data_absen", text: "Data Absensi", img: "app18.png"}
            ]
        });

        sumAbsenMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "data_absen":
                    let year = $("#"+yearName).val();
                    let month = $("#"+monthName).val();
            
                    if (!mainTab.tabs("hr_attendance_summary_list")){
                        mainTab.addTab("hr_attendance_summary_list", tabsStyle("clock.png", "Summary Absensi " + "(" +currentMonth+ "/" +currentYear+")", "background-size: 16px 16px"), null, null, true, true);
                        absensiSummary(year, month);
                    } else {
                        mainTab.tabs("hr_attendance_summary_list").setActive();
                    }
                    break;
                case "refresh":
                    loadMainChart();
                    loadDonutChart();
                    break;
                case "line":
                    type = "line";
                    loadMainChart();
                    loadDonutChart();
                    break;
                case "column":
                    type = "column";
                    loadMainChart();
                    loadDonutChart();
                    break;
            }
        });

        sumAbsenLayout.cells("a").attachHTMLString("<div class='hc_graph' id='absen_monthly_summary' style='height:100%;width:100%;'></div>");
        sumAbsenLayout.cells("c").attachHTMLString("<div class='hc_graph' id='absen_monthly_donut_summary' style='height:100%;width:100%;'></div>");

        $("#"+yearName).on("change", function() {
            loadMainChart();
            loadDonutChart();
        });

        $("#"+monthName).on("change", function() {
            loadMainChart();
            loadDonutChart();
        });

        function loadMainChart() {
            let year = $("#"+yearName).val();
            let month = $("#"+monthName).val();
            sumAbsenLayout.cells("a").progressOn();

            reqJson(DashAttendance("getSummaryAbsen"), "POST", {year, month}, (err, res) => {
                    sumAbsenLayout.cells("a").progressOff();

                Highcharts.chart('absen_monthly_summary', {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: "Absen Bulan " + nameOfMonth(month)
                    },
                    xAxis: {
                        categories: res.categories
                    },
                    yAxis: {
                        title: {
                            text: 'Persentase Absensi'
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: true
                        }
                    },
                    series: res.series
                });
            });
        }

        loadMainChart();

        function loadDonutChart() {
            var year = $("#"+yearName).val();
            var month = $("#"+monthName).val();
            sumAbsenLayout.cells("c").progressOn();

            reqJson(DashAttendance("getSummaryDonutAbsen"), "POST", { year, month }, (err, res) => {
                sumAbsenLayout.cells("c").progressOff();
                Highcharts.chart('absen_monthly_donut_summary', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Persentase Absensi Bulan ' + nameOfMonth(month)
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                            }
                        }
                    },
                    series: [{
                        name: 'Absensi',
                        colorByPoint: true,
                        data: res.series
                    }]
                });
            });
        }

        loadDonutChart();

        var sumAbsenGrid = sumAbsenLayout.cells("b").attachGrid();
        sumAbsenGrid.setHeader("No,Shift,Total Masuk / Total Shift");
        sumAbsenGrid.setColSorting("int,str,str");
        sumAbsenGrid.setColTypes("rotxt,rotxt,rotxt");
        sumAbsenGrid.setColAlign("center,left,left");
        sumAbsenGrid.setInitWidthsP("5,45,50");
        sumAbsenGrid.enableSmartRendering(true);
        sumAbsenGrid.attachEvent("onXLE", function() {
            sumAbsenLayout.cells("b").progressOff();
        });
        sumAbsenGrid.init();

        function rSumAbsenGrid() {
            sumAbsenLayout.cells("b").progressOn();
            sumAbsenGrid.clearAndLoad(DashAttendance("getAbsenSummaryGrid"));
        };

        rSumAbsenGrid();
    }

JS;

header('Content-Type: application/javascript');
echo $script;