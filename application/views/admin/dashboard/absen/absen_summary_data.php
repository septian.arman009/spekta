<?php 
if ((strpos(strtolower($_SERVER['SCRIPT_NAME']), strtolower(basename(__FILE__)))) !== false) { // NOT FALSE if the script"s file name is found in the URL
    header('HTTP/1.0 403 Forbidden');
    die('<h2>Direct access to this page is not allowed.</h2>');
}

$script = <<< "JS"

    function absensiSummary(year, month) {	
        var legend = legendGrid();
        var hrAbsenSummaryLayout = mainTab.cells("hr_attendance_summary_list").attachLayout({
            pattern: "1C",
            cells: [
                {id: "a", header : false}
            ]
        });

        var hrAbsenSummaryMenu = hrAbsenSummaryLayout.cells("a").attachMenu({
            icon_path: "./public/codebase/icons/",
            items: [
                {id: "refresh", text: "Refresh", img: "refresh.png"},
                {id: "export", text: "Export To Excel", img: "excel.png"}
            ]
        });

        hrAbsenSummaryMenu.attachEvent("onClick", function(id) {
            switch (id) {
                case "export":
                    absenSummaryGrid.toExcel("./public/codebase/grid-to-excel-php/generate.php");
                    sAlert("Export Data Dimulai");
                    break;
                case "refresh":
                    rAbsenGrid(year, month);
                    break;
            }
        });

        var hrAbsenSummaryStatus = hrAbsenSummaryLayout.cells("a").attachStatusBar();
        function absenSummaryGridCount() {
            hrAbsenSummaryStatus.setText("Total baris: " + absenSummaryGrid.getRowsNum() + " (" + legend.freeday_and_absen + ")");
        }

        var absenSummaryGrid =  hrAbsenSummaryLayout.cells("a").attachGrid();
        absenSummaryGrid.setHeader("No,NPP,Sap ID,Nama Karyawan,Sub Unit,Bagian,Sub Bagian,Shift Kerja,Tanggal,Jam Masuk,Jam Keluar,Absen Masuk,Absen Keluar,Keterlambatan,Pulang Cepat,Jam Produktif,Jam Tidak Produktif,Lokasi Absen,Jarak Dari Gate,QR Masuk, QR Keluar");
        absenSummaryGrid.attachHeader("#rspan,#text_filter,#text_filter,#text_filter,#select_filter,#select_filter,#select_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter")
        absenSummaryGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
        absenSummaryGrid.setColTypes("rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt,rotxt");
        absenSummaryGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
        absenSummaryGrid.setInitWidthsP("5,10,10,25,20,25,25,5,15,15,15,15,15,10,10,10,10,10,15,20,20");
        absenSummaryGrid.enableSmartRendering(true);
        absenSummaryGrid.enableMultiselect(false);
        absenSummaryGrid.attachEvent("onXLE", function() {
            hrAbsenSummaryLayout.cells("a").progressOff();
        });
        absenSummaryGrid.init();

        function rAbsenGrid(year, month) {
            hrAbsenSummaryLayout.cells("a").progressOn();
            absenSummaryGrid.clearAndLoad(MAbsen("getAbsenList", {month, year}), absenSummaryGridCount);
        }
        rAbsenGrid(year, month);
    }

JS;

header('Content-Type: application/javascript');
echo $script;