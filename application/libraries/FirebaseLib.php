<?php
defined('BASEPATH') OR exit("No direct script access allowed");

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class FirebaseLib {
    protected $ci;
    protected $firebase;

    public function __construct() {
        $this->ci = &get_instance();
        $this->firebase = (new Factory)->withServiceAccount("/mnt/d/Freelance/clean-skill-394615-352ba555c04c.json");
    }

    public function sendNotification($deviceToken, $data) {
        $title = $data['title'];
        $body = $data['body'];
        $payload = $data['payload'];

        $notification = Notification::create($title, $body);
        $message = CloudMessage::withTarget('token', $deviceToken)
            ->withNotification($notification)
            ->withData($payload);

        $messaging = $this->firebase->createMessaging();

        $messaging->send($message);
    }

    public function sendMultiNotification($deviceTokens, $data) {
        $title = $data['title'];
        $body = $data['body'];
        $payload = $data['payload'];

        $notification = Notification::create($title, $body);
        $message = CloudMessage::new()
            ->withNotification($notification)
            ->withData($payload);

        $messaging = $this->firebase->createMessaging();

        $messaging->sendMulticast($message, $deviceTokens);
    }
}
