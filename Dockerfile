FROM php:7.4-apache

RUN apt-get update \
    && apt-get install -y libpq-dev curl

RUN curl -sSLf \
    -o /usr/local/bin/install-php-extensions \
    https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions && \
    chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions pdo_mysql mysqli gd

RUN a2enmod rewrite

WORKDIR /var/www/html   