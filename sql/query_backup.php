<?php

$sql = "SELECT a.*,b.name AS emp_sub_name,c.name AS emp_division,d.name AS ovt_sub_name,e.name AS ovt_division,f.employee_name,g.overtime_review,
            (SELECT employee_name FROM employees WHERE id = a.created_by) AS emp1,
            (SELECT employee_name FROM employees WHERE id = a.updated_by) AS emp2,
            (SELECT employee_name FROM employees WHERE nip = a.status_by) AS status_updater,
            (SELECT name FROM $this->kf_mtn.production_machines WHERE id = a.machine_1) AS machine_1,
            (SELECT name FROM $this->kf_mtn.production_machines WHERE id = a.machine_2) AS machine_2
            FROM $this->kf_hr.employee_overtimes_detail a, $this->kf_hr.sub_departments b, $this->kf_hr.divisions c, 
                $this->kf_hr.sub_departments d, $this->kf_hr.divisions e, $this->kf_hr.employees f, $this->kf_hr.employee_overtimes g
            WHERE a.sub_department_id = b.id
            AND a.division_id = c.id
            AND a.ovt_sub_department = d.id
            AND a.ovt_division = e.id
            AND a.emp_id = f.id
            AND a.task_id = g.task_id
            AND a.location = '$this->empLoc'
            $where";